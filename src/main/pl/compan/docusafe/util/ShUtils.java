package pl.compan.docusafe.util;

import java.io.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ShUtils.java,v 1.5 2009/02/28 07:41:55 wkuty Exp $
 */
public class ShUtils
{
    public static final int COPY_BUFFER_SIZE = 2048;

    public static void copyFile(File input, File output) throws IOException
    {
        if (input == null)
            throw new NullPointerException("input");
        if (output == null)
            throw new NullPointerException("output");

        InputStream is = new FileInputStream(input);
        OutputStream os = new FileOutputStream(output);
        org.apache.commons.io.IOUtils.copy(is,os);       
        org.apache.commons.io.IOUtils.closeQuietly(is);
        org.apache.commons.io.IOUtils.closeQuietly(os);        
    }

    public static void createFile(InputStream is, File output) throws IOException
    {
        if (is == null)
            throw new NullPointerException("inputStream");
        if (output == null)
            throw new NullPointerException("output");

        OutputStream os = new FileOutputStream(output);

        org.apache.commons.io.IOUtils.copy(is,os);

        org.apache.commons.io.IOUtils.closeQuietly(is);
        org.apache.commons.io.IOUtils.closeQuietly(os);
    }

    public static void delete(File file)
    {
        if (file.isFile())
        {
            file.delete();
        }
        else if (file.isDirectory())
        {
            File[] files = file.listFiles();
            for (int i=0; i < files.length; i++)
            {
                if (files[i].isFile())
                    files[i].delete();
                else
                    delete(files[i]);
            }
        }
    }

}
