/**
 * Created by IntelliJ IDEA on Apr 4, 2003, 6:29:07 PM
 */
package pl.compan.docusafe.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @FIXME Klasa przestarza�a u�ywa IPv4 tylko rzuca b��dami w przypadku IPv6
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: NetUtil.java,v 1.4 2006/02/20 15:42:31 lk Exp $
 */
@Deprecated
public class NetUtil
{
    private static Log log = LogFactory.getLog(NetUtil.class);

    public static class HostAddress
    {
        private String ip;
        private byte[] quads;

        public String toString()
        {
            return getClass().getName()+"[ip="+ip+"]";
        }

        public String getIp()
        {
            return ip;
        }

        public void setIp(String ip)
        {
            this.ip = ip;
        }

        public byte[] getQuads()
        {
            return quads;
        }

        public void setQuads(byte[] quads)
        {
            this.quads = quads;
        }

        private String remoteHost;

        public String getResolvedHostName()
        {
            if (remoteHost != null)
                return remoteHost;

            try
            {
                long time = System.currentTimeMillis();

                InetAddress ia = InetAddress.getByAddress(quads);
                remoteHost = ia.getHostName();

                if (log.isDebugEnabled())
                    log.debug("InetAddress.getHostName("+ip+") -> "+remoteHost+
                            ": "+(System.currentTimeMillis()-time)+"ms");
            }
            catch (UnknownHostException e)
            {
                remoteHost = ip;
            }

            return remoteHost;
        }

        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof HostAddress)) return false;

            final HostAddress hostAddress = (HostAddress) o;

            if (ip != null ? !ip.equals(hostAddress.ip) : hostAddress.ip != null) return false;

            return true;
        }

        public int hashCode()
        {
            return (ip != null ? ip.hashCode() : 0);
        }
    }

    public static HostAddress getClientAddr(HttpServletRequest request)
    {
        if (request == null)
            throw new NullPointerException("request");

        HostAddress address = new HostAddress();

        String remoteAddr = request.getRemoteAddr();
        //String remoteHost = request.getRemoteHost();
        String forwardedFor = request.getHeader("x-forwarded-for");

        if (log.isDebugEnabled())
            log.debug("request.remoteAddr="+remoteAddr+
                    //", request.remoteHost="+remoteHost+
                    ", request.[x-forwarded-for]="+forwardedFor);

        // BUTIK.PL:
        // warto�ci remoteAddr i remoteHost maj� zawsze warto�� 127.0.0.1
        // poniewa� wszystkie wywo�ania przechodz� przez mod_proxy
        // lokalnego apache'a

        // w ka�dym wywo�aniu powinien by� nag��wek X-Forwarded-For
        // zawieraj�cy jeden lub wi�cej adres�w IP (oddzielonych przecinkami)

        // w�a�ciwy adres jest zawsze na pocz�tku, ale niekiedy proxy u�ytkownika
        // maskuje go wpisuj�c "unknown"; w takiej sytuacji szukam kolejnego
        // adresu zawartego w X-Forwarded-For i tak a� do skutku, tzn. a� znajd�
        // rzeczywisty adres IP
        // dodatkowo odrzucam adresy z klas prywatnych: 10/24, 192.168/16,
        // 172.16/12 (chyba, �e jedyne adresy jakie znajd� pochodz� z klasy
        // prywatnej - wtedy bior� pierwszy z nich).

        if (forwardedFor != null)
        {
            String clientAddr = null;
            String recentAddr = null;

            String[] addrs = StringUtils.split(forwardedFor, ", ");
            for (int i=0; addrs != null && i < addrs.length; i++)
            {
                String addr = addrs[i];

                if (addr == null || !addr.matches("^[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+$"))
                    continue;

                // zapami�tuj� ten adres IP w zmiennej pomocniczej, na wypadek
                // gdyby nie by�o ich wi�cej, ale wci�� mo�e si� on okaza� adresem
                // z klasy prywatnej
                try
                {
                    if (!isPrivateClass(addr))
                    {
                        clientAddr = addr;
                        break;
                    }

                    // zapami�tuj� tylko pierwszy z adres�w, kt�re w pierwszym
                    // przebiegu odrzucam
                    if (recentAddr == null)
                        recentAddr = addr;
                }
                catch (RuntimeException e)
                {
                    continue;
                }
            }

            // je�eli jedyny adres, jaki znalaz�em pochodzi z klasy prywatnej,
            // u�ywam go (ale z przykro�ci� :-) )
            if (clientAddr == null)
                clientAddr = recentAddr;

            // je�eli znalaz�em w X-Forwarded-For jaki� adres IP, u�ywam go,
            // w przeciwnym razie pozostaj� przy request.remoteAddr
            if (clientAddr != null)
            {
                remoteAddr = clientAddr;
                //remoteHost = null;
            }

            if (log.isDebugEnabled())
                log.debug("x-forwarded-for: "+remoteAddr);
        }

        // zamieniam adres w postaci tekstowej na czterobajtow� tablic�
        byte[] remoteQuad = null;
        try
        {
            remoteQuad = ip2quad(remoteAddr);
        }
        catch (RuntimeException e)
        {
            // b��d mo�e wyst�pi� je�eli zdalny adres jest r�wny null
            // (ma�o prawdopodobne, to musia�by by� bug resina)
            log.error("ip2quad", e);
            address.setIp(remoteAddr);
            address.setQuads(new byte[] { 0, 0, 0, 0 });
            //address.setHostName(remoteHost != null ? remoteHost : remoteAddr);
            return address;
        }

        address.setIp(remoteAddr);
        address.setQuads(remoteQuad);

        if (log.isDebugEnabled())
            log.debug("getClientAddr: "+address);

        return address;
    }

    /**
     * Zwraca true, je�eli przekazany adres IP pochodzi z klasy prywatnej,
     * jednej z 10/24, 172.16/12, 192.168/16.
     * @param textualIP
     * @throws NullPointerException Je�eli przekazany adres jest r�wny null.
     * @throws IllegalArgumentException Je�eli przekazany adres nie by�
     *  prawid�owym IP.
     */
    public static boolean isPrivateClass(String textualIP)
    {
        byte[] quad = ip2quad(textualIP);

        // 10/24
        if (quad[0] == 10)
            return true;
        // 172.16/12
        if (quad[0] == (byte)172 && (quad[1] & 0x10) == 0x10)
            return true;
        // 192.168/16
        if (quad[0] == (byte)192 && quad[1] == (byte)168)
            return true;

        return false;
    }

    /**
     * Zamienia tekstow� reprezentacj� adresu IP na czterobajtow�
     * tablic�.
     * @param textualIP
     * @throws NullPointerException Je�eli przekazany adres jest r�wny null.
     * @throws IllegalArgumentException Je�eli przekazany adres nie by�
     *  prawid�owym IP.
     */
    private static byte[] ip2quad(String textualIP)
    {
        if (textualIP == null)
            throw new NullPointerException("textualIP");

        String[] quads = StringUtils.split(textualIP, ".");
        if (quads != null && quads.length == 4)
        {
            byte[] ip = new byte[4];
            for (int i=0; i < ip.length; i++)
            {
                int q = Integer.parseInt(quads[i]);
                if (q > 255)
                    throw new IllegalArgumentException("Niepoprawna liczba w adresie: "+q);
                ip[i] = (byte) q;
            }
            return ip;
        }
        else
        {
            throw new IllegalArgumentException(textualIP);
        }
    }
}
