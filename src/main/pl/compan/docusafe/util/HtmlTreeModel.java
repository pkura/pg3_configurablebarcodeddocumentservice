package pl.compan.docusafe.util;

import java.util.List;

import pl.compan.docusafe.core.users.DSDivision;

/**
 * Model drzewa generowanego przez HtmlTree.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTreeModel.java,v 1.6 2008/07/30 13:17:41 pecet4 Exp $
 */
public interface HtmlTreeModel
{
    /**
     * Zwraca g��wny element drzewa. Null, je�eli g��wny element
     * nie ma by� wy�wietlany.
     * @return G��wny element drzewa lub null.
     */
    Object getRootElement();
    /**
     * Zwraca odno�nik, jaki powinien by� zwi�zany z przekazanym elementem
     * drzewa.
     */
    String getUrl(Object element);
    /** 
     * Zwraca nazwe kotwicy
     */
    String getAnchorName(Object element);
    /**
     * Zwraca opis danego elementu drzewa.
     */
    String getTitle(Object element);
    boolean hasChildElements(Object element);
    /**
     * Zwraca list� element�w podrz�dnych w stosunku do przekazanego.
     * Zwracana lista mo�e mie� warto�� null, je�eli przekazany element
     * nie ma element�w podrz�dnych.
     * @param element Element, kt�rego dzieci powinny by� zwr�cone przez
     *  t� metod�. Je�eli ma warto�� null, oznacza to, �e metoda getRootElement()
     *  zwr�ci�a warto�� null i metoda powinna zwr�ci� list� dzieci elementu
     *  g��wnego.
     * @return Lista element�w podrz�dnych lub null.
     */
    List getChildElements(Object element);
    /**
     * Zwraca true, je�eli przekazany element powinien by� pokazany
     * jako rozwini�ty. Aby drzewo wygl�da�o dobrze, metoda powinna
     * zwraca� true dla ka�dego elementu znajduj�cego si� na �cie�ce
     * do elementu docelowego.
     */
    boolean isExpanded(Object element);
    
   /**
    * Zwraca id katalogu 
    */
    Long getIdRequest();

    /**
     * Zwraca true, je�eli na elemencie ma byc kotwica
     * 
     */
    boolean isSelected(Object element);
    
    /**
     * Zwraca guid galezi
     */
    public String getDivision(Object element);
    
    /**
     * Zwraca true, je�eli opis przekazanego elementu ma by� pogrubiony
     * (tag <code>strong</code>).
     */
    //boolean isHighlighted(Object element);
    /**
     * Zwraca true, je�eli tytu� elementu powinien odr�nia� si� od
     * pozosta�ych (tag <code>em</code>).
     */
    //boolean isEmphasised(Object element);
    
    HtmlTreeTheme getTheme();
}
