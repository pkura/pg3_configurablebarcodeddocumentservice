package pl.compan.docusafe.util;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTableTree.java,v 1.4 2008/10/23 14:35:38 pecet1 Exp $
 */
public class HtmlTableTree
{
    private HtmlTableTreeModel model;
    private String contextPath;
    private HtmlTreeTheme theme;

    private int[] columnInfo = new int[1];

    private static final int LAST_SIBLING = 1;
    private static final int MIDDLE_SIBLING = 2;

    private HtmlTreeStyleVisitor styleVisitor;
    private HtmlTreeImageVisitor imageVisitor;

    public HtmlTableTree(HtmlTableTreeModel model, String contextPath)
    {
        this.model = model;
        this.contextPath = contextPath != null ? contextPath : "";
        this.theme = model.getTheme();
    }

    public String generateTree()
    {
        StringBuilder out = new StringBuilder();

        Object rootElement = model.getRootElement();

        // numer kolejnej kolumny drzewa; root dostaje 0, je�eli
        // istnieje
        int nextColumn = 0;

        String[] columnHeaders = model.getColumnHeaders();
        String[] columnWidths = model.getColumnWidths();

        if (columnHeaders == null)
            throw new NullPointerException("HtmlTableTreeModel.columnHeaders");
        if (columnWidths == null)
            throw new NullPointerException("HtmlTableTreeModel.columnWidths");
        if (columnHeaders.length != columnWidths.length)
            throw new IllegalArgumentException("columnHeaders.length != columnWidths.length");
        if (model.getLevels() >= columnHeaders.length)
            throw new IllegalArgumentException("HtmlTableTreeModel.levels >= columnHeaders.length");

        out.append("<table cellspacing=0 cellpadding=0><tr>");

        // nag��wek tabeli
        for (int i=0; i < columnHeaders.length; i++)
        {
            out.append("<th width='");
            out.append(columnWidths[i]);
            out.append("' valign='top'>");
            out.append(columnHeaders[i]);
            out.append("</th>");
        }

        out.append("</tr>");

        // ga��� g��wna
        if (rootElement != null)
        {
            out.append("<tr>");

            HtmlImage image = getIconImage(rootElement, true);
            out.append("<td><img src='"+contextPath + image.getSrc()+"' " +
                "width='"+image.getWidth()+"' " +
                "height='"+image.getHeight()+"' " +
                "border='0' " +
                "class='tree'></td>");
            //out.append("&nbsp;");

            String styleClass =
                styleVisitor != null ? styleVisitor.getStyleClass(rootElement) : null;

            for (int i=0; i < model.getLevels(); i++)
                out.append("<td></td>");

            String[] titles = model.getTitles(rootElement);
            HtmlTableTreeModel.Link[] urls = model.getUrls(rootElement);

/*
            if (titles.length != (columnHeaders.length - model.getLevels()))
                throw new IllegalArgumentException("Spodziewano si� "+
                    (columnHeaders.length - model.getLevels())+" tytu��w dla "+
                    rootElement);
*/

            for (int i=0; i < titles.length; i++)
            {
                out.append("<td>");
                if (urls[i] != null)
                    out.append("<a class='"+(styleClass != null ? styleClass : theme.getLinkClass())+"' " +
                        "href='"+urls[i].getHref()+"'"+
                        (urls[i].getOnclick() != null ? " onclick='"+urls[i].getOnclick()+"'" : "")+
                        ">");
                out.append("&nbsp;");
                out.append(titles[i]);
                if (urls[i] != null)
                    out.append("</a>");
                out.append("</td>");
            }

            out.append("</tr>");

            setColumnInfo(0, LAST_SIBLING);
            nextColumn++;
        }

        walkTree(out, rootElement, model.getChildElements(rootElement), nextColumn);

        out.append("</table>");

        return out.toString();
    }

    /**
     * Rekurencyjnie generuje reszt� drzewa rozwijaj�c tylko te
     * ga��zie, dla kt�rych metoda {@link HtmlTreeModel#isExpanded}
     * zwraca true.
     * @param out
     * @param parent Element nadrz�dny dla listy element�w childElements.
     *  U�ywany do generowania odno�nika do elementu nadrz�dnego (ikonka
     *  z minusem s�u��ca do "zwijania" fragmentu drzewa prowadzi do folderu
     *  nadrz�dnego).
     * @param childElements Lista element�w, kt�re maj� by� pokazane na
     *  aktualnym poziomie. Przekazuj� list�, zamiast pozwoli� metodzie
     *  na samodzielne pobranie tej listy z elementu parent, aby unikn��
     *  dwukrotnego pobierania element�w podrz�dnych z tego samego elementu
     *  nadrz�dnego.
     */
    private void walkTree(StringBuilder out, Object parent, List childElements,
                          int column)
    {
        if (childElements == null || childElements.size() == 0)
            return;

        for (int i=0, n=childElements.size(); i < n; i++)
        {
            Object child = childElements.get(i);
            List children = model.getChildElements(child);

            setColumnInfo(column, i < n-1 ? MIDDLE_SIBLING : LAST_SIBLING);

            boolean isLastSibling = i == n-1;
            boolean isExpanded = model.isExpanded(child);
            boolean hasChildren = children != null && children.size() > 0;
            String link = model.getControlUrl(child);
            HtmlTableTreeModel.Link[] urls = model.getUrls(child);

            out.append("<tr>");

            // wype�niacz (pionowe linie, gdzie s� potrzebne, w pozosta�ych
            // miejscach pusty obrazek)
            for (int col=1; col < column; col++)
            {
                if (getColumnInfo(col) == MIDDLE_SIBLING)
                {
                    out.append("<td><img " +
                        "src='"+contextPath+theme.getVerticalLineImage().getSrc()+"' " +
                        "width='"+theme.getVerticalLineImage().getWidth()+"' " +
                        "height='"+theme.getVerticalLineImage().getHeight()+"' " +
                        "border='0' class='tree'></td>");
                }
                else //if (getColumnInfo(col) == LAST_SIBLING)
                {
                    out.append("<td><img " +
                        "src='"+contextPath+theme.getBlankImage().getSrc()+"' " +
                        "width='"+theme.getBlankImage().getWidth()+"' " +
                        "height='"+theme.getBlankImage().getHeight()+"' " +
                        "border='0' class='tree'></td>");
                }
            }

            // odpowiedni znaczek (plus/minus) przy ikonce folderu
            if (isExpanded)
            {
                String parentLink = parent != null ? model.getControlUrl(parent) : null;
                if (isLastSibling)
                {
                    // mimo oznaczenia elementu jako rozwini�tego (expanded),
                    // nie mo�na wykluczy�, �e model zwr�ci takie oznaczenie
                    // dla folderu nie maj�cego folder�w podrz�dnych
                    if (hasChildren)
                    {
                        out.append("<td>"+
                            (parentLink != null ? "<a class='"+theme.getControlLinkClass()+"' href='"+parentLink+"'>" : "") +
                            "<img " +
                                "src='"+contextPath+theme.getLastExpandedControlImage().getSrc()+"' " +
                                "width='"+theme.getLastExpandedControlImage().getWidth()+"' " +
                                "height='"+theme.getLastExpandedControlImage().getHeight()+"' " +
                                "border='0' class='tree'>" +
                            (parentLink != null ? "</a>" : "")+"</td>");
                    }
                    else
                    {
                        out.append("<td><img " +
                            "src='"+contextPath+theme.getLastChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getLastChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getLastChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'></td>");
                    }
                }
                else
                {
                    if (hasChildren)
                    {
                        out.append("<td>"+
                            (parentLink != null ? "<a class='"+theme.getControlLinkClass()+"' href='"+parentLink+"'>" : "") +
                            "<img " +
                            "src='"+contextPath+theme.getMiddleExpandedControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleExpandedControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleExpandedControlImage().getHeight()+"' " +
                            "border='0' class='tree'>" +
                            (parentLink != null ? "</a>" : "")+"</td>");
                    }
                    else
                    {
                        out.append("<td><img " +
                            "src='"+contextPath+theme.getMiddleChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'></td>");
                    }
                }
            }
            else
            {
                if (isLastSibling)
                {
                    if (hasChildren)
                    {
                        out.append("<td><a class='"+theme.getControlLinkClass()+"' href='"+link+"'>" +
                            "<img " +
                            "src='"+contextPath+theme.getLastCollapsedControlImage().getSrc()+"' " +
                            "width='"+theme.getLastCollapsedControlImage().getWidth()+"' " +
                            "height='"+theme.getLastCollapsedControlImage().getHeight()+"' " +
                            "border='0' class='tree'></a></td>");
                    }
                    else
                    {
                        out.append("<td><img " +
                            "src='"+contextPath+theme.getLastChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getLastChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getLastChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'></td>");
                    }
                }
                else
                {
                    if (hasChildren)
                    {
                        out.append("<td><a class='"+theme.getControlLinkClass()+"' href='"+link+"'>" +
                            "<img " +
                            "src='"+contextPath+theme.getMiddleCollapsedControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleCollapsedControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleCollapsedControlImage().getHeight()+"' " +
                            "border='0' class='tree'></a></td>");
                    }
                    else
                    {
                        out.append("<td><img " +
                            "src='"+contextPath+theme.getMiddleChildlessControlImage().getSrc()+"' " +
                            "width='"+theme.getMiddleChildlessControlImage().getWidth()+"' " +
                            "height='"+theme.getMiddleChildlessControlImage().getHeight()+"' " +
                            "border='0' class='tree'></td>");
                    }
                }
            }

            String iconText = model.getIconReplacementText(child);

            // ikona folderu lub napis
            if (iconText == null)
            {
                HtmlImage image = getIconImage(child, isExpanded);

                out.append("<td><img src='"+contextPath+image.getSrc()+"' " +
                    "width='"+image.getWidth()+"' " +
                    "height='"+image.getHeight()+"' " +
                    "border='0' " +
                    "class='tree'></td>");
            }
            else
            {
                out.append("<td align='right'>");
                out.append(iconText);
                out.append("</td>");
            }

            String styleClass =
                styleVisitor != null ? styleVisitor.getStyleClass(child) : null;

            for (int c=column; c < model.getLevels(); c++)
                out.append("<td></td>");

            String[] titles = model.getTitles(child);
            //int levels = model.getLevels();

            for (int c=0; c < titles.length; c++)
            {
                out.append("<td>");
                if (urls[c] != null)
                    out.append("<a class='"+(styleClass != null ? styleClass : theme.getLinkClass())+
                        "' href='"+urls[c].getHref()+"'"+
                        (urls[c].getOnclick() != null ? " onclick='"+urls[c].getOnclick()+"'" : "") +
                        ">");
                out.append("&nbsp;");
                out.append(titles[c] != null ? titles[c] : "");
                if (urls[c] != null)
                    out.append("</a>");
                out.append("</td>");
            }

            out.append("</tr>");

            if (isExpanded && children != null && children.size() > 0)
            {
                walkTree(out, child, children, column+1);
            }
        }
    }



    private HtmlImage getIconImage(Object element, boolean expanded)
    {
        HtmlImage image = null;
        if (imageVisitor != null)
        {
            image = imageVisitor.getIconImage(element, expanded);
        }
        if (image == null)
        {
            if (expanded)
                image = theme.getExpandedFolderImage();
            else
                image = theme.getCollapsedFolderImage();
        }
        return image;
    }

    private void setColumnInfo(int column, int info)
    {
        if (columnInfo.length < column+1)
        {
            int[] tmp = new int[column+1];
            System.arraycopy(columnInfo, 0, tmp, 0, columnInfo.length);
            columnInfo = tmp;
        }

        columnInfo[column] = info;
    }

    private int getColumnInfo(int column)
    {
        if (columnInfo.length < column+1)
            throw new IllegalArgumentException("columnInfo.length="+columnInfo.length+
                ", column="+column);
        return columnInfo[column];
    }

    public void setStyleVisitor(HtmlTreeStyleVisitor styleVisitor)
    {
        this.styleVisitor = styleVisitor;
    }

    public void setImageVisitor(HtmlTreeImageVisitor imageVisitor)
    {
        this.imageVisitor = imageVisitor;
    }
}
