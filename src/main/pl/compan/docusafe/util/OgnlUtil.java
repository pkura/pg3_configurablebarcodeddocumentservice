package pl.compan.docusafe.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;
import pl.compan.docusafe.core.EdmException;

/**
 * Funkcje zwi�zane z wyliczaniem warto�ci wyra�e� OGNL.
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class OgnlUtil
{
    /**
     * Oblicza warto�� przekazanego wyra�enia.
     * 
     * @param expression wyra�enie OGNL w postaci napisu
     * @param trueVariables tablica zmiennych z wyra�enia 'expression', kt�re maj� warto�� TRUE
     * @return warto�� przekazanego wyra�enia OGNL 
     * @throws EdmException
     */
    public static boolean checkOgnlValue(String expression, String[] trueVariables) throws EdmException
    {
        try
        {
            Map<String,Boolean> varMap = new HashMap<String, Boolean>();
            for (String var : trueVariables)
            {
                varMap.put(var, Boolean.TRUE);
            }
            
            Boolean result = (Boolean) Ognl.getValue(
                    Ognl.parseExpression(expression), new OgnlContext(), varMap, Boolean.class);
            return result != null && result.booleanValue();
        }
        catch (OgnlException e)
        {
            throw new EdmException("B��d podczas wyliczania wyra�enia OGNL");
        }
    }
}
