package pl.compan.docusafe.util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HttpUtils.java,v 1.17 2006/03/17 15:51:06 lk Exp $
 */
public final class HttpUtils
{
    private HttpUtils()
    {
    }

    public static String firstValue(Object value)
    {
        if (value instanceof String)
        {
            return (String) value;
        }
        else if (value instanceof String[])
        {
            if (((String[]) value).length > 0)
            {
                return (((String[]) value)[0]);
            }
        }
        return null;
    }

    /**
     * Metoda dzia�a tak samo jak {@link URLEncoder#encode(java.lang.String, java.lang.String)},
     * ale maskuje wyj�tek UnsupportedEncodingException rzucaj�c zamiast
     * niego RuntimeException.
     * @param s
     * @param enc
     */
    public static String urlEncode(String s, String enc)
    {
        try
        {
            return URLEncoder.encode(s, enc);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static String urlEncode(String s)
    {
        return urlEncode(s, "iso-8859-2");
    }

    public static String contentDispositionFilename(String s)
    {
        StringBuilder result = new StringBuilder(s.length() * 2);
        CharacterIterator iter=new StringCharacterIterator(s);
        for (char c=iter.first(); iter.current() != CharacterIterator.DONE; c=iter.next())
        {
            if (c > 127)
            {
                if (c == '�') c = 'a';
                else if (c == '�') c = 'A';
                else if (c == '�') c = 'c';
                else if (c == '�') c = 'C';
                else if (c == '�') c = 'e';
                else if (c == '�') c = 'E';
                else if (c == '�') c = 'l';
                else if (c == '�') c = 'L';
                else if (c == '�') c = 'n';
                else if (c == '�') c = 'N';
                else if (c == '�') c = 'o';
                else if (c == '�') c = 'O';
                else if (c == '�') c = 's';
                else if (c == '�') c = 'S';
                else if (c == '�' || c == '�') c = 'z';
                else if (c == '�' || c == '�') c = 'Z';
            }

            if (c > 127)
            {
                result.append("_"+Integer.toHexString(c));
            }
//            else if (Character.isWhitespace(c))
//            {
//                result.append("%20");
//            }
            else
            {
                result.append(c);
            }
        }

        return result.toString();
    }

    /**
     * Tworzy url zawieraj�cy przekazane parametry zakodowane w utf-8.
     * Lista parametr�w musi mie� parzyst� d�ugo��; wszystkie elementy
     * s� konwertowane do typu String, za� parametry o numerach parzystych,
     * je�eli s� tablicami, s� "klonowane".
     */
    public static String makeUrl(String base, Object[] params)
    {
        if (params != null && params.length % 2 != 0)
            throw new IllegalArgumentException("Tablica parametr�w musi mie� " +
                "parzyst� d�ugo��.");

        StringBuilder url = new StringBuilder(base);

        int paramCount = 0;
        for (int i=0; params != null && i < params.length; i+=2)
        {
            Object val = params[i+1];
            if (val == null)
                continue;
            if (val.getClass().isArray())
            {
                for (int j=0; j < Array.getLength(val); j++)
                {
                    url.append(paramCount++ > 0 ? '&' : '?');
                    url.append(
                        urlEncode(String.valueOf(params[i]), "iso-8859-2") + '=' +
                        urlEncode(String.valueOf(Array.get(val, j)), "iso-8859-2"));
                }
            }
            else
            {
                url.append(paramCount++ > 0 ? '&' : '?');
                url.append(
                    urlEncode(String.valueOf(params[i]), "iso-8859-2") + '=' +
                    urlEncode(String.valueOf(val), "iso-8859-2"));
            }
        }

        return url.toString();
    }

    public static String valueOrNull(Object values)
    {
        if (values instanceof String)
            return TextUtils.trimmedStringOrNull((String) values);

        if (values instanceof String[])
        {
            return TextUtils.trimmedStringOrNull(firstValue(values));
/*
            String[] array = (String[]) values;
            if (array.length > 0)
                return TextUtils.trimmedStringOrNull(array[0]);
*/
        }

        if (values != null)
        {
            return TextUtils.trimmedStringOrNull(values.toString());
        }

        return null;
    }
}
