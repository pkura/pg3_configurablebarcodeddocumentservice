package pl.compan.docusafe.util;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONWriter;

import java.io.*;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ServletUtils.java,v 1.7 2009/09/07 07:25:34 mariuszk Exp $
 */
public abstract class ServletUtils
{
    private ServletUtils()
    {
    }

    public static void streamResponse(HttpServletResponse response, InputStream stream,
                                      String contentType, int contentLength,
                                      String... headers)
        throws IOException
    {
        response.setContentType(contentType);
        response.setContentLength(contentLength);

        if (headers != null)
        {
            for (String header : headers)
            {
                int col = header.indexOf(':');
                if (col < 0)
                    throw new IllegalArgumentException("Niepoprawna posta� nag��wka: "+header);
                response.addHeader(header.substring(0, col), header.substring(col+1));
            }
        }
        byte[] buf = new byte[8192];
        int len;
        OutputStream os = response.getOutputStream();
        while ((len = stream.read(buf)) > 0)
        {
            os.write(buf, 0, len);
            os.flush();
        }
        os.close();
        stream.close();
    }
    
    public static void stringResponse(HttpServletResponse response,String msg) throws IOException {
    	response.setContentType("text/html");
    	PrintWriter out = response.getWriter();
    	out.write(msg);
    	out.flush();
    	out.close();
	}

    /**
     * @TODO metoda moze powodowa� og�lny b��d wyrzucania pliku an przegladark�. Stosowa� IOUtils.copy metod�.
     * @param response
     * @param file
     * @param contentType
     * @param headers
     * @throws IOException
     */
    public static void streamFile(HttpServletResponse response, File file, String contentType,
                                  String... headers)
        throws IOException
    {
        response.setContentType(contentType);
        response.setContentLength((int) file.length());
        FileInputStream is = new FileInputStream(file);
        streamResponse(response, is, contentType, (int) file.length(), headers);
        org.apache.commons.io.IOUtils.closeQuietly(is);
    }
}
