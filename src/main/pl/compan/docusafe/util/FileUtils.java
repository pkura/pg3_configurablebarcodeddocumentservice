package pl.compan.docusafe.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import pl.compan.docusafe.core.EdmException;

public class FileUtils {

    private static final Logger log = LoggerFactory.getLogger(FileUtils.class.getPackage().getName());
    
	public static File fileFromBlobInputStream(InputStream biss) throws IOException	
	{
		File retFile = File.createTempFile("hotIcr", "tif");
		retFile.deleteOnExit();
		
		byte bufor[ ] = new byte[16536];
        BufferedOutputStream outs =  new BufferedOutputStream(new FileOutputStream(retFile),bufor.length);
        int leng;
        while ((leng = biss.read(bufor)) > 0)
        {
            outs.write(bufor, 0, leng);
        }
        outs.flush();
        outs.close();
        biss.close();
		
		return retFile;
	}
	
	
	public static File fileFromBlobInputStream(InputStream biss, String filename) throws IOException	
	{
		File retFile = File.createTempFile(filename, "");
		retFile.deleteOnExit();
		
		byte bufor[ ] = new byte[16536];
        BufferedOutputStream outs =  new BufferedOutputStream(new FileOutputStream(retFile),bufor.length);
        int leng;
        while ((leng = biss.read(bufor)) > 0)
        {
            outs.write(bufor, 0, leng);
        }
        outs.flush();
        outs.close();
        biss.close();
		
		return retFile;
	}
	public static void writeIsToFile(InputStream is, File file) throws IOException
	{
		byte bufor[ ] = new byte[16536];
        BufferedOutputStream outs =  new BufferedOutputStream(new FileOutputStream(file),bufor.length);
        int leng;
        while ((leng = is.read(bufor)) > 0)
        {
            outs.write(bufor, 0, leng);
        }
        outs.flush();
        outs.close();        
        is.close();
	}
	
	public static void copyFileToFile(File from, File to) throws IOException
	{
		to.createNewFile();
			
		FileOutputStream fo = new FileOutputStream(to);
		FileInputStream fi = new FileInputStream(from);
		
		byte buf[ ] = new byte[16536];
		
		BufferedOutputStream bfo = new BufferedOutputStream(fo,buf.length);
		BufferedInputStream bfi = new BufferedInputStream(fi,buf.length);
		
		boolean readit = true;
        int readsize = 0;
        
        while(readit)
        {
        	readsize = bfi.available();
            if (readsize > buf.length)
            {
                bfi.read(buf);
                bfo.write(buf);
            }
            else
            {
                bfi.read(buf,0, readsize);
                bfo.write(buf,0, readsize);
            }
            if (bfi.available()<=0)
            {
                readit = false;
            }
        }
        
        bfo.flush();
        bfi.close();
        bfo.close();
        fi.close();
        fo.close();
	}
	
	public static List<File> findAllFilesInDocusafeByName(String name) throws Exception
	{
		List<File> files = new ArrayList<File>();
		ClassLoader cld = Thread.currentThread().getContextClassLoader();
		Enumeration<URL> resources = cld.getResources("/");
		while (resources.hasMoreElements()) {
			files.add(new File(URLDecoder.decode(resources.nextElement().getPath(), "UTF-8")));
        }
		
		return FileUtils.findAllFilesInDirAndSubdirs(files.get(0), name);
	}
	
	public static List<File> findAllFilesInDirAndSubdirs(File dir, String name)
	{
		ArrayList<File> files = new ArrayList<File>();
		
		for(File fl : dir.listFiles())
		{
			if(fl.isDirectory())
			{
				files.addAll(findAllFilesInDirAndSubdirs(fl, name));
			}
			else if(fl.getName().equalsIgnoreCase(name))
			{
				files.add(fl);
			}
		}
		
		return files;
	}
	
	public static List<File> findAllFilesInDirByName(File directory, String fileName)
	{
		final String fileNameF = fileName;
		
		List<File> files = new ArrayList<File>();
		for(File fl : directory.listFiles(new FileFilter()
		{
			public boolean accept(File file)
            {
                return file.isDirectory();
            }
		}
		))
			files.addAll(findFilesInDirByName(fl, fileName));
			
				
		for(File file : directory.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
            {
		        return name.toLowerCase().equalsIgnoreCase(fileNameF);
            }
		}))
			files.add(file);
		return files;
	}
	
	public static List<File> findFilesInDirByName(File directory, String fileName)
	{
		final String fileNameF = fileName;
		List<File> files = new ArrayList<File>();
				
		for(File file : directory.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
            {
		        return name.toLowerCase().equalsIgnoreCase(fileNameF);
            }
		}))
			files.add(file);
		return files;
	}
	
	public static List<Class> getClassesForPackage(String pckgname) throws ClassNotFoundException {
        // This will hold a list of directories matching the pckgname. There may be more than one if a package is split over multiple jars/paths
        ArrayList<File> directories = new ArrayList<File>();
        try {
            ClassLoader cld = Thread.currentThread().getContextClassLoader();
            if (cld == null) {
                throw new ClassNotFoundException("Can't get class loader.");
            }
            String path = pckgname.replace('.', '/');
            // Ask for all resources for the path
            Enumeration<URL> resources = cld.getResources(path);
            while (resources.hasMoreElements()) {
                directories.add(new File(URLDecoder.decode(resources.nextElement().getPath(), "UTF-8")));
            }
        } catch (NullPointerException x) {
            throw new ClassNotFoundException(pckgname + " does not appear to be a valid package (Null pointer exception)");
        } catch (UnsupportedEncodingException encex) {
            throw new ClassNotFoundException(pckgname + " does not appear to be a valid package (Unsupported encoding)");
        } catch (IOException ioex) {
            throw new ClassNotFoundException("IOException was thrown when trying to get all resources for " + pckgname);
        }
 
        ArrayList<Class> classes = new ArrayList<Class>();
        // For every directory identified capture all the .class files
        for (File directory : directories) {
            if (directory.exists()) {
                // Get the list of the files contained in the package
                String[] files = directory.list();
                for (String file : files) {
                    // we are only interested in .class files
                    if (file.endsWith(".class")) {
                        // removes the .class extension
                        classes.add(Class.forName(pckgname + '.' + file.substring(0, file.length() - 6)));
                    }
                }
            } else {
                throw new ClassNotFoundException(pckgname + " (" + directory.getPath() + ") does not appear to be a valid package");
            }
        }
        return classes;
    }

	public static byte[] getBytesFromFile(File file) throws IOException 
    {
        InputStream is = new FileInputStream(file);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        
        IOUtils.copy(is, os);
        IOUtils.closeQuietly(is);
        IOUtils.closeQuietly(os);
        
        return os.toByteArray();
    }
	

	public static Integer lenghtFormInputStream(InputStream inputStream) throws IOException 
	{
        ByteArrayOutputStream out = new ByteArrayOutputStream();; 
        byte[] buf = new byte[1024];
        int len;
        while ((len = inputStream.read(buf)) > 0)
        {
            out.write(buf, 0, len);
        }
        out.close();
		return out.toByteArray().length;
	}
        
        /**
         * Encoduje do stringa plik wykonuje polecenie:
         * org.apache.commons.codec.binary.Base64.encodeBase64String(getBytesFromFile(file));
         * @param file
         */
        public static String encodeByBase64(File file) throws IOException
        {
           return org.apache.commons.codec.binary.Base64.encodeBase64String(getBytesFromFile(file));
        }
        
        /**
         * Ujednolicenie obu funcji wywoluje Base64.decodeBase64(string)
         * @param string
         * @return
         * @throws IOException 
         */
        public static byte[] decodeByBase64(String string) throws IOException
        {
            return org.apache.commons.codec.binary.Base64.decodeBase64(string);
        }
	    
        
        /**
         * decoduje to pliku lancuch znak�w
         * @param string
         * @return
         * @throws IOException 
     */
    public static File decodeByBase64ToFile(String string) throws IOException, EdmException
    {
        FileOutputStream outputStream = null;
        ByteArrayInputStream inputStream = null;

        try
        {
            byte[] bytes = decodeByBase64(string);
            byte[] buf = new byte[1024];
            File file = File.createTempFile("docusafe_", "_tmp");
            outputStream = new FileOutputStream(file);
            inputStream = new ByteArrayInputStream(bytes);

            while ((inputStream.read(buf)) > 0)
            {
                outputStream.write(buf);
            }

            outputStream.close();
            inputStream.close();

            return file;
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        } finally
        {
            try
            {
                if (outputStream != null)
                {
                    outputStream.close();
                }
                if (inputStream != null)
                {
                    inputStream.close();
                }
            } catch (IOException ex)
            {
            }
        }
    }
    
    public static File getFileFromBytes(byte[] bytes) throws IOException, EdmException
    {
        FileOutputStream outputStream = null;
        ByteArrayInputStream inputStream = null;

        try
        {
            byte[] buf = new byte[1024];
            File file = File.createTempFile("docusafe_", "_tmp");
            outputStream = new FileOutputStream(file);
            inputStream = new ByteArrayInputStream(bytes);

            while ((inputStream.read(buf)) > 0)
            {
                outputStream.write(buf);
            }

            outputStream.close();
            inputStream.close();

            return file;
        } catch (Exception ex)
        {
            log.error(ex.getMessage(), ex);
            throw new EdmException(ex.getMessage());
        } finally
        {
            try
            {
                if (outputStream != null)
                {
                    outputStream.close();
                }
                if (inputStream != null)
                {
                    inputStream.close();
                }
            } catch (IOException ex)
            {
            }
        }
    }

    /**
     * <p>
     *     Tworzy plik tymczasowy zachowuj�c rozszerzenie. Np. <code>myFile.rtf --> myFile89175438923754.rtf</code>
     * </p>
     *
     * @param filename
     * @return Utworzony plik tymczasowy
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     */
    public static File createTempFile(String filename) throws IOException {
        final String baseName = FilenameUtils.getBaseName(filename);
        final String extension = FilenameUtils.getExtension(filename);

        File file = File.createTempFile(baseName, "."+extension);

        return file;
    }

    /**
     * <p>
     *     Tworzy plik tymczasowy zachowuj�c rozszerzenie. Np. <code>myFile.rtf --> myFile89175438923754.rtf</code>.
     *     Plik wype�niany jest danymi ze strumienia <code>content</code>.
     * </p>
     * @param filename
     * @param content
     * @return Utworzony plik tymczasowy
     * @throws IOException
     */
    public static File createTempFile(String filename, InputStream content) throws IOException {
        final File tempFile = createTempFile(filename);

        byte[] bytes = IOUtils.toByteArray(content);
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, bytes);

        return tempFile;
    }


}
