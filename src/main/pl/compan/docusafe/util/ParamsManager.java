package pl.compan.docusafe.util;

import org.smartparam.engine.config.ParamEngineConfig;
import org.smartparam.engine.config.ParamEngineConfigBuilder;
import org.smartparam.engine.config.ParamEngineFactory;
import org.smartparam.engine.core.ParamEngine;
import org.smartparam.repository.fs.FSParamRepository;
import pl.compan.docusafe.boot.Docusafe;

/**
 * User: Tomasz
 * Date: 16.01.14
 * Time: 15:26
 */
public class ParamsManager {
    private static final Logger LOG = LoggerFactory.getLogger(ParamsManager.class);

    private static FSParamRepository repository = null;
    private static ParamEngineConfig config = null;
    private static ParamEngine engine = null;

    public static ParamEngine getEngine() {
        if(engine == null) {
            repository = new FSParamRepository(Docusafe.getHome().getAbsolutePath()+"\\params\\",".*\\.param$");
            config = ParamEngineConfigBuilder.paramEngineConfig().withParameterRepositories(repository).build();
            engine = ParamEngineFactory.paramEngine(config);
        }
        return engine;
    }

    public static void reloadEngine() {
        repository = null;
        config = null;
        engine = null;
    }
}
