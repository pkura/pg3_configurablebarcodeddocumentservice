package pl.compan.docusafe.util;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EdmHibernateException;

public class DSCountry {

	private Integer id;
	private String cn;
	private String title;
	
	DSCountry() {
	}

	public static DSCountry findBy(Integer id) throws EdmException {
		return (DSCountry) Finder.find(DSCountry.class, id);
	}
	
	public static List<DSCountry> find(Integer id, String cn, String title) throws EdmException
	{
		try
		{
			Criteria criteria = DSApi.context().session().createCriteria(DSCountry.class);
			if (id != null)
				criteria.add(Restrictions.eq("id", id));
			else if (cn != null)
				criteria.add(Restrictions.eq("cn", cn));
			else if (title != null)
				criteria.add(Restrictions.eq("title", title));
			List<DSCountry> list = criteria.list();

			if (list.size() > 0)
			{
				return list;
			}

			return null;
		}
		catch (HibernateException e)
		{
			throw new EdmHibernateException(e);
		}
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
