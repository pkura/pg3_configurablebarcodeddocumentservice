package pl.compan.docusafe.util;

import pl.compan.docusafe.core.EdmException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RequiredFormatMissingException.java,v 1.2 2006/02/20 15:42:31 lk Exp $
 */
public class RequiredFormatMissingException extends EdmException
{
    public RequiredFormatMissingException(String message)
    {
        super(message);
    }
}
