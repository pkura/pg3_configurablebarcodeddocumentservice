package pl.compan.docusafe.util.sql;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.DbUtils;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.EdmSQLException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Klasa narz�dziowa do prostych zapyta� z u�yciem JDBC
 *
 * Metody korzystaj� z DSApi do pobierania po��cze�
 */
public final class JdbcUtils {

    /**
     * Wykonuje zapytanie, kt�re powinno zwr�ci� pojedyncz� kolumn� ze stringiem
     * @param query zapytanie
     * @param params parametry zapytania
     * @return kolejne warto�ci z pierwszej kolumny wynik�w
     */
    public static List<String> selectStrings(String query, Object... params) throws  EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(query);
            int paramIndex = 1;
            for(Object param: params){
                ps.setObject(paramIndex, param);
                paramIndex++;
            }
            rs = ps.executeQuery();
            List<String> ret = Lists.newArrayList();
            while(rs.next()){
                ret.add(rs.getString(1));
            }
            return ret;
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }
    }

    /**
     * Wykonuje zapytanie, kt�re powinno zwr�ci� pojedyncz� kolumn� z longiem
     * @param query zapytanie
     * @param params parametry zapytania
     * @return kolejne warto�ci z pierwszej kolumny wynik�w
     */
    public static List<Long> selectLongs(String query, Object... params) throws  EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(query);
            int paramIndex = 1;
            for(Object param: params){
                ps.setObject(paramIndex, param);
                paramIndex++;
            }
            rs = ps.executeQuery();
            List<Long> ret = Lists.newArrayList();
            while(rs.next()){
                ret.add(rs.getLong(1));
            }
            return ret;
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }
    }

    /**
     * Wykonuje zapytanie, kt�re powinno zwr�ci� pojedynczy wiersz z pojedyncz� kolumn� z longiem
     * @param query zapytanie
     * @param params parametry zapytania
     * @return kolejne warto�ci z pierwszej kolumny wynik�w
     */
    public static long selectLong(String query, Object... params) throws  EdmException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DSApi.context().prepareStatement(query);
            int paramIndex = 1;
            for(Object param: params){
                ps.setObject(paramIndex, param);
                paramIndex++;
            }
            rs = ps.executeQuery();
            boolean next = rs.next();
            if(!next){
                throw new RuntimeException("Brak rezultatu w zapytaniu '" + query + "'");
            }
            return rs.getLong(1);
        } catch (SQLException e) {
            throw new EdmSQLException(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DSApi.context().closeStatement(ps);
        }
    }

    private JdbcUtils(){}
}
