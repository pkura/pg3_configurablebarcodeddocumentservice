package pl.compan.docusafe.util.sql;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
/* User: Administrator, Date: 2005-11-23 12:01:04 */

/**
 * Pozwala na stopniowe budowanie zapytania SQL i jednoczesne
 * dodawanie parametr�w bez potrzeby okre�lania ich pozycji.
 * <p>
 * Tre�� zapytania tworzona jest przy pomocy metody {@link #append(String)},
 * parametry dodawane s� przy pomocy metod add*, za� gotowe zapytanie
 * w postaci obiektu PreparedStatement tworzone jest metod�
 * {@link #build(java.sql.Connection)}.
 * <p>
 * Klasa nie kontroluje, czy liczba miejsc na parametry w tre�ci
 * zapytania (znaki '?') zgadza si� z liczb� podanych parametr�w.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PreparedStatementBuilder.java,v 1.4 2009/02/28 09:19:13 wkuty Exp $
 */
public class PreparedStatementBuilder
{
    private static class parameter
    {
        Object value;
        int type;

        public parameter(Object value, int type)
        {
            this.value = value;
            this.type = type;
        }

        public String toString()
        {
            return String.valueOf(value)+":"+
                (value != null ? value.getClass().getName() : String.valueOf(type));
        }
    }

    private List parameters = new LinkedList();
    private StringBuilder sql = new StringBuilder(256);

    public PreparedStatement build(Connection conn) throws SQLException
    {
        PreparedStatement ps = conn.prepareStatement(sql.toString());

        int i=0;
        for (Iterator iter=parameters.iterator(); iter.hasNext(); )
        {
            i++;
            parameter p = (parameter) iter.next();

            if (p.value == null)
            {
            	ps.setObject(i, null);
            }
            else
            {
                switch (p.type)
                {
                    case Types.BIGINT:
                        ps.setLong(i, ((Long) p.value).longValue());
                        break;
                    case Types.BOOLEAN:
                        ps.setBoolean(i, ((Boolean) p.value).booleanValue());
                        break;
                    case Types.DATE:
                        ps.setDate(i, (java.sql.Date) p.value);
                        break;
                    case Types.DOUBLE:
                        ps.setDouble(i, ((Double) p.value).doubleValue());
                        break;
                    case Types.FLOAT:
                        ps.setFloat(i, ((Float) p.value).floatValue());
                        break;
                    case Types.INTEGER:
                        ps.setInt(i, ((Integer) p.value).intValue());
                        break;
                    case Types.NUMERIC:
                        ps.setBigDecimal(i, ((BigDecimal) p.value));
                        break;
                    case Types.SMALLINT:
                        ps.setShort(i, ((Short) p.value).shortValue());
                        break;
                    case Types.VARCHAR:
                        ps.setString(i, ((String) p.value));
                        break;
                    case Types.TIME:
                        ps.setTime(i, ((Time) p.value));
                        break;
                    case Types.TIMESTAMP:
                        ps.setTimestamp(i, ((Timestamp) p.value));
                        break;
                    case Types.TINYINT:
                        ps.setByte(i, ((Byte) p.value).byteValue());
                        break;
                    case Types.VARBINARY:
                    case Types.STRUCT:
                    case Types.REAL:
                    case Types.OTHER:
                    case Types.NULL:
                    case Types.LONGVARCHAR:
                    case Types.LONGVARBINARY:
                    case Types.JAVA_OBJECT:
                    case Types.DISTINCT:
                    case Types.DECIMAL:
                    case Types.CHAR:
                    case Types.BLOB:
                    case Types.BIT:
                    case Types.BINARY:
                    case Types.ARRAY:
                    default:
                        throw new IllegalArgumentException("Brak wsparcia dla " +
                            "argumentu typu "+p.type);
                }
            }
        }

        return ps;
    }

    public PreparedStatementBuilder append(String s)
    {
        if (s == null)
            throw new NullPointerException("s");
        sql.append(s);
        return this;
    }

    public void addString(String value)
    {
        parameters.add(new parameter(value, Types.VARCHAR));
    }

    public void addByte(byte value)
    {
        parameters.add(new parameter(new Byte(value), Types.TINYINT));
    }

    public void addShort(short value)
    {
        parameters.add(new parameter(new Short(value), Types.SMALLINT));
    }

    public void addInt(int value)
    {
        parameters.add(new parameter(new Integer(value), Types.INTEGER));
    }

    public void addLong(long value)
    {
        parameters.add(new parameter(new Long(value), Types.BIGINT));
    }

    public void addBoolean(boolean value)
    {
        parameters.add(new parameter(Boolean.valueOf(value), Types.BOOLEAN));
    }

    public void addDate(Date value)
    {
        if (value == null)
            addNull(Types.DATE);
        else
            parameters.add(new parameter(new java.sql.Date(value.getTime()), Types.DATE));
    }

    public void addTime(Date value)
    {
        if (value == null)
            addNull(Types.TIME);
        else
            parameters.add(new parameter(new java.sql.Time(value.getTime()), Types.TIME));
    }

    public void addTimestamp(Date value)
    {
        if (value == null)
            addNull(Types.TIMESTAMP);
        else
            parameters.add(new parameter(new java.sql.Timestamp(value.getTime()), Types.TIMESTAMP));
    }

    public void addDouble(double value)
    {
        parameters.add(new parameter(new Double(value), Types.DOUBLE));
    }

    public void addFloat(float value)
    {
        parameters.add(new parameter(new Float(value), Types.FLOAT));
    }

    public void addBigDecimal(BigDecimal value)
    {
        if (value == null)
            addNull(Types.NUMERIC);
        else
            parameters.add(new parameter(value, Types.NUMERIC));
    }

    public void addNull(int type)
    {
        parameters.add(new parameter(null, type));
    }

    public String toString()
    {
        return sql.toString()+" @ "+parameters;
    }
}
