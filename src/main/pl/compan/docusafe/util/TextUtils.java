package pl.compan.docusafe.util;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.CharacterIterator;
import java.text.Normalizer;
import java.text.StringCharacterIterator;
import java.util.*;
import java.util.regex.Pattern;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: TextUtils.java,v 1.24 2010/07/29 11:00:14 mariuszk Exp $
 */
public class TextUtils {
    private final static Logger LOG = LoggerFactory.getLogger(TextUtils.class);

	final static String CHARACTER_ENCODING = "windows-1250";
	
	
	static final String[][] potegi = {{"", 			"", 		""},
		  {"tysi�c", 	"tysi�ce", 	"tysi�cy"},
		  {"milion",	"miliony",	"milion�w"},
		  {"miliard",	"miliardy",	"miliard�w"}};

	static final String[] cyfry = {"zero", "jeden", "dwa", "trzy", "cztery", "pi��",
		   "sze��", "siedem", "osiem", "dziewi��"};
	
	static final String[] nascie = {"dziesi��", "jedena�cie", "dwana�cie", "trzyna�cie",
		   "czterna�cie", "pi�tna�cie", "szesna�cie", "siedemna�cie",
		   "osiemna�cie", "dziewi�tna�cie"};
	
	static final String[] dziesiatki = {"dwadzie�cia", "trzydzie�ci", "czterdzie�ci", "pi��dziesi�t", 
					"sze��dziesi�t", "siedemdziesi�t", "osiemdziesi�t", "dziewi��dziesi�t"};
	
	static final String[] setki = {"sto", "dwie�cie", "trzysta", "czterysta", "pi��set",
		  "sze��set", "siedemset", "osiemset", "dziewi��set"};
	
	static final String minus = "minus"; 
	
	static final String przecinek = "przecinek";
	
	
    /**
     * Zwraca napis zawieraj�cy reprezentacje napisowe element�w
     * tablicy rozdzielone pojedynczymi spacjami.
     */
    public static String concat(Object[] array)
    {
        StringBuilder result = new StringBuilder();
        for (int i=0; i < array.length; i++)
        {
            String str = array[i] != null ? array[i].toString() : null;
            if (str != null && str.length() > 0)
            {
                if (i > 0)
                    result.append(" ");
                result.append(str);
            }
        }
        return result.toString();
    }

    /**
     * Je�eli przekazany napis jest pusty (StringUtils.isEmpty()
     * zwraca true), zwracana jest warto�� null, w przeciwnym razie
     * zwracany jest przekazany napis.
     */
    public static String trimmedStringOrNull(String s)
    {
        return StringUtils.isEmpty(s) ? null : s.trim();
    }

    public static String trimmedStringOrNull(String s, int maxLength)
    {
        s = StringUtils.isEmpty(s) ? null : s.trim();
        if (s != null && s.length() > maxLength)
            s = s.substring(0, maxLength);
        return s;
    }


    /**
     * Je�eli przekazany napis ma warto�� null, zwracany jest pusty
     * napis, w przeciwnym wypadku napis oryginalny.
     */
    public static String nullSafeString(String s)
    {
        return s == null ? "" : s;
    }
    

    /**
     * Je�eli przekazany napis ma warto�� null, zwracany jest pusty
     * napis, w przeciwnym wypadku napis oryginalny.
     */
    public static String nullSafeObject(Object o)
    {
        return nullSafeObject(o, "");
    }

    public static String nullSafeObject(Object o, String nullValue)
    {
        return o == null ? nullValue : o.toString();
    }

    /**
     * Zwraca liczb� typu BigDecimal o podanej reprezentacji napisowej.
     * Dozwolone jest stosowanie przecinka dziesi�tnego zamiast kropki.
     * Liczba zaokr�glana jest do podanej liczby cyfr po przecinku poprzez
     * odrzucenie pozosta�ych (bez zaokr�glania).
     */
    public static BigDecimal nullSafeParseBigDecimal(String big, int scale)
    {
        if (big != null)
        {
            try
            {
                return new BigDecimal(big.trim().replace(',', '.').replaceAll("\\s+", "")).setScale(2, BigDecimal.ROUND_DOWN);
            }
            catch (Exception e)
            {
            }
        }

        return null;
    }

    public static String join(Collection<String> coll, String str)
    {
        StringBuilder result = new StringBuilder();
        Iterator<String> iter = coll.iterator();
        if (iter.hasNext())
        {
            result.append(iter.next());
        }
        while (iter.hasNext())
        {
            result.append(str);
            result.append(iter.next());
        }
        return result.toString();
    }

    public static String joinNotBlankElements (String separator, Collection<String>  elements){
        return joinNotBlankElements(separator, elements.toArray(new String[elements.size()]));
    }
    public static String joinNotBlankElements (String separator, String ... elements){
        List<String> notBlankElements = new ArrayList<String>();
        for (String el :elements)
            if (StringUtils.isNotBlank(el))
                notBlankElements.add(el);
        return StringUtils.join(notBlankElements.toArray(new String[notBlankElements.size()]), separator);
    }

    /**
     *
     * @param joinedNames " ,,s, ss, sss ,ssss , ,    ,"
     * @param separator ","
     * @return "s","ss","sss","ssss"
     */
    public static ArrayList<String> splitNamesToList(String joinedNames, String separator) {
        //        Splitter.on(separator).trimResults().omitEmptyStrings().splitToList(joinedNames);
        //
        Iterable<String> splitedNames = Splitter.on(separator).trimResults().omitEmptyStrings().split(joinedNames);
        return Lists.newArrayList(splitedNames);
    }
    /**
     * @param joinedNames" ,,s, ss, sss ,ssss , ,    ,"
     * @param separator ","
     * @return "s","ss","sss","ssss"
     */
    public static String[] splitNames(String joinedNames, String separator) {
        ArrayList<String> splitedNamesList = splitNamesToList(joinedNames, separator);
        return splitedNamesList.toArray(new String [splitedNamesList.size()]);
        //
        //        String regexSeparator = escapeToRegex(separator);
        //        return joinedNames
        //                .trim()
        //                .replaceAll("(^(\\s*"+regexSeparator+"\\s*)+)|((\\s*"+regexSeparator+"\\s*)+$)", "")
        //                .split("(\\s*"+regexSeparator+"\\s*)+");
    }

    public static boolean contains(String separator, String joinedNames, String searchName) {
        String [] names = splitNames(joinedNames, separator);
        for (String n : names)
            if (n.equals(searchName))
                return true;
        return false;
    }

    /**
     * @param text <pre>"111 # 222 #   333 #444      "</pre>
     * @param sign #
     * @param occurCount 2
     * @return <pre>"   333 #444      "</pre>
     */
    public static String getTextFromSign(String text, String sign, int occurCount){
        String regexSign = escapeToRegex(sign);
        if (!Pattern.compile("(.*" + regexSign + ".*){" + occurCount + "}").matcher(text).find()) return "";
        return text.replaceAll("^([^" + regexSign + "]*" + regexSign + "){" + occurCount + "}", "");
    }
    /**
     * @param text <pre>"111 # 222 #   333 #444      "</pre>
     * @param sign #
     * @param occurCount 2
     * @return <pre>"   333 "</pre>
     */
    public static String getTextFromSignToNextSign(String text, String sign, int occurCount){
        if (occurCount>0)
            text = getTextFromSign(text, sign, occurCount);
        return getTextToSign(text, sign);
    }
    /**
     * @param text <pre>"  111 # 222 #   333 #444      "</pre>
     * @param sign #
     * @return <pre>"  111 "</pre>
     */
    public static String getTextToSign(String text, String sign){
        String regexSign = escapeToRegex(sign);
        return text.replaceAll(regexSign+".*$","");
//        int idx = text.indexOf(sign);
//        if (idx < 0) return text;
//        return text.substring(0,idx);
    }
    /**
     *
     * @param sequence "*sequence?"
     * @return "\*sequence\?"
     */
    public static String escapeToRegex(String sequence){
        final String META_REGEX_SIGNS = ".\\+*?[^]$(){}=!<>|:-";
        final String escapeSigns = META_REGEX_SIGNS.replaceAll(".", "\\\\$0");
        final String escapeSequence = sequence.replaceAll("["+escapeSigns+"]", "\\\\$0");
        return escapeSequence;
    }

    public static String latinize(String s)
    {
        StringBuilder result = new StringBuilder(s.length());
        CharacterIterator iter = new StringCharacterIterator(s);
        for (char c=iter.first(); c != CharacterIterator.DONE; c = iter.next())
        {
            if (c < 127)
            {
                result.append(c);
            }
            else
            {
                switch (c)
                {
                    case '�': result.append('a'); break;
                    case '�': result.append('c'); break;
                    case '�': result.append('e'); break;
                    case '�': result.append('l'); break;
                    case '�': result.append('n'); break;
                    case '�': result.append('o'); break;
                    case '�': result.append('s'); break;
                    case '�': result.append('z'); break;
                    case '�': result.append('z'); break;
                    case '�': result.append('A'); break;
                    case '�': result.append('C'); break;
                    case '�': result.append('E'); break;
                    case '�': result.append('L'); break;
                    case '�': result.append('N'); break;
                    case '�': result.append('O'); break;
                    case '�': result.append('S'); break;
                    case '�': result.append('Z'); break;
                    case '�': result.append('Z'); break;
                    default:
                        result.append('_');
                }
            }
        }
        return result.toString();
    }
    
    /**
     * Parsuje przekazany obiekt na tablic� liczb (zak�adam, �e przekazany obiekt jest napisem
     * lub tablic� napis�w).
     */
    public static Integer[] parseIntoIntegerArray(Object value)
    {
        if (value == null)
            return new Integer[]{};
        String[] enumStr;
        if (value instanceof String)
        {
            enumStr = new String[] {(String) value};
        }
        else
        {
            enumStr = (String[]) value;
        }                           
        Integer[] enumIds = new Integer[enumStr.length];
        for (int e=0; e < enumStr.length; e++)  {
            try {enumIds[e] = new Integer(enumStr[e]);} catch(NumberFormatException ex) {}
        }
        return enumIds;
    }
    
    /**
     * Parsuje przekazany obiekt na tablic� liczb (zak�adam, �e przekazany obiekt jest napisem
     * lub tablic� napis�w).
     */
    public static Integer[] integerToIntegerArray(Integer value)
    {                        
        Integer[] enumIds = new Integer[1];
        enumIds[0] = value;
        return enumIds;
    }
    
    
    public static String[] parseStringArray(Object value)
    {
        if (value == null)
            return new String[]{}
        ;
        String[] enumStr;
        if (value instanceof String)
        {
            enumStr = new String[] {(String) value};
        }
        else
        {
            enumStr = (String[]) value;
        }                           
       
        return enumStr;
    }
    
    /**
     * 
     * @param param - <b>String</b> dla ktorego metoda wyczysci znaki <u>konca linii</u>
     * @return - <b>String</b> bez znakow <u>konica linii</u> lub <b>null</b> jesli nie udalo sie wyczyscic znakow
     */
    public static String clearEntersOrNull(String param)
    {
    	StringBuffer sb = new StringBuffer();
    	try
    	{
    		appendText(sb, param);
    	}
    	catch(Exception e)
    	{
    		return null;
    	}
    	return sb.toString();
    }
    
    public static void appendText(StringBuffer line,String param) throws Exception
    {                  
        if (param!=null)
        {
             byte[] bytes = param.getBytes(CHARACTER_ENCODING);
             int i = 0;
             while (i<bytes.length)
             {
            	 byte co = bytes[i];
                 if (co!=10 && co !=13 && co!=59)
                 {
                	 byte[] oneByte = new byte[1];
                     oneByte[0] = co;
                     line.append(new String(oneByte,CHARACTER_ENCODING));                                        
                 }
             i++;
             }                     
        }
    }

	/** Zamienia object na string je�li nie jest == null, je�li jest zwraca null */
	public static String toString(Object object){
		return object != null ? object.toString() : null;
	}

    /**
     * Zamienia �le encodowany string - b��d zwi�zany z wywo�aniami ajax
     * @param string
     * @return
     * @throws Exception
     */
	public static String isoToUtf(String string)
	{
		if(string == null)
			return "";
//        try {
//            return new String(string.getBytes("ISO-8859-2"),"utf-8");
            return string;
//        } catch (UnsupportedEncodingException e) {
//            LOG.error(e.getMessage(), e);
//            return string;
//        }
    }
	 /*
     * Okresla wersje i nazw� przegladarki po otrzymaniu nag��wka User-Agent
     */
    public static String parseUserAgentHeader(String userAgent)
    {
    	try
    	{
		int tempIndexOf = userAgent.indexOf("Firefox");
		if(tempIndexOf!=-1) 
		{ 
			userAgent = userAgent.substring(tempIndexOf); 					
		}
		else if(userAgent.indexOf("MSIE")!=-1)
		{
			tempIndexOf = userAgent.indexOf("MSIE");
			userAgent = userAgent.substring(tempIndexOf, userAgent.indexOf(";", tempIndexOf)); 					
		}
		else if(userAgent.indexOf("Chrome")!=-1)
		{
			tempIndexOf = userAgent.indexOf("Chrome");
			userAgent = userAgent.substring(tempIndexOf, userAgent.indexOf(" ", tempIndexOf)); 					
		}
		else if(userAgent.indexOf("Opera")!=-1)
		{
			tempIndexOf = userAgent.indexOf("Opera");
			userAgent = userAgent.substring(tempIndexOf); 					
		}
		else if(userAgent.indexOf("Netscape")!=-1)
		{
			tempIndexOf = userAgent.indexOf("Netscape");
			userAgent = userAgent.substring(tempIndexOf); 					
		}
		else  
		{
			return "Nieznana 1.0";
		}
		return userAgent;
		}catch(Exception e) { return "Blad przy parsowaniu stringa"; }
    }
    /*
     * Zamienia Stringa z liczb� na zapis s�owny liczby
     */
	public static final String numberToText(BigDecimal num) 
	{
		return numberToText(num.toPlainString());
	}
	
	/**
	 * Wyci�ga liczb� z �a�cucha tekstowego ignoruj�c wszystkie znaki nie b�d�ce cyfr�
	 * @param num
	 * @return
	 */
	public static final Long parseLongSafe(String num) {
		String str = num.replaceAll("[^\\d]", "");
		return Long.parseLong(str);
	}
	
	public static final String numberToText(String num) {
		if (num == null) {
			return "";
		}
		// pre parsing ...
		String val = num.replaceAll("[\\s]", "").replaceAll(",", ".");
		String full = null;
		String fraction = null;
		if (val.contains(".")) {
			int dotPos = val.indexOf(".");
			full=val.substring(0, dotPos);
			fraction = val.substring(dotPos+1);
		} else {
			full = val;
		}
		
		if (full.length()==0) {
			full="0";
		}
		
		// now we have both parts
		// parsing full part
		int len = full.length();
		List<String> triades = new ArrayList<String>();
		List<String> triadesNum = new ArrayList<String>();
		while (len!=0) {
			String triade = full.substring(len-3<0?0:len-3, len);
			triades.add(triadeToText(triade));
			triadesNum.add(triade);
			full=full.substring(0, len-triade.length());
			len = full.length();
		}
		
		StringBuilder wholeNumber = new StringBuilder();
		
		if (full.startsWith("-")) {
			full=full.substring(1);
			wholeNumber.append(minus).append(" ");
		}
		
		int size = triades.size();
		for (int count = size-1; count>=0; count--) {
			wholeNumber.append(triades.get(count));
			wholeNumber.append(" ");
			int triadeVal = Integer.parseInt(triadesNum.get(count));
			if (count == 0 && triadeVal==0) {
				if (size==1) {
					wholeNumber.append(cyfry[0]);
					wholeNumber.append(" ");
				}
			} else if (count ==0 || triadeVal==0) {
				// nothing
			} else if (triadeVal == 1) {
				wholeNumber.append(potegi[count][0]);
				wholeNumber.append(" ");
			} else if (triadeVal%100>=10 && triadeVal%100<20) {
				wholeNumber.append(potegi[count][2]);
				wholeNumber.append(" ");
			} else {
				switch (triadeVal%10) {
					case 1:
					case 0:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
						wholeNumber.append(potegi[count][2]);
						wholeNumber.append(" ");
						break;
					case 2:
					case 3:
					case 4:
						wholeNumber.append(potegi[count][1]);
						wholeNumber.append(" ");
						break;
						
				}
			}
		}
		
		// parsing fraction part
		if (fraction!= null && fraction.length()!=0) {
			wholeNumber.append(przecinek).append(" ");
			for (int a=0, frLen=fraction.length(); a<frLen; a++) {
				wholeNumber.append(cyfry[fraction.charAt(a)-48]).append(" ");
			}
		}
		
		return wholeNumber.toString().replaceAll(" +", " ").trim();
	}
	
	/**
	 * Zamienia wszystkie ogonki, akcenty na znaki ascii
	 * @param src Znormalizowany string
	 * @return
	 */
	public static String normalizeString(String src) {
		return Normalizer.normalize(src, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
				.replace("\u0142", "l").replace("\u0141", "L");
	}
	
	public static String normalizeForSqlTableName(String input) {
		input = normalizeString(input).toLowerCase(Locale.ENGLISH);
		input = CharMatcher.WHITESPACE.trimFrom(input);
		input = CharMatcher.WHITESPACE.collapseFrom(input, '_');
		input = CharMatcher.anyOf(",.-/").removeFrom(input);
		return input;
	}
	
	/**
	 * Zamienia backslash "\" na slash "/"
	 * @param src
	 * @return
	 */
	public static String replaceBackslash(String src) {
		if (src == null) return null;
		return src.replace('\\', '/');
	}
	
	private static final String triadeToText(String triade) {
		int len = triade.length();
		StringBuilder triadeText = new StringBuilder();
		if (len ==0 || len >3) {
			throw new IllegalArgumentException("zerowej dlugosci triada, tak byc nie moze!");
		} else {
			int val = Integer.parseInt(triade);
			if (val>=100) {
				triadeText.append(setki[val/100-1]).append(" ");
			}
			if (val%100==0) {
				//triadeText.append(potegi[val/100]).append(" ");
			} else if (val%100<20) {
				if (val%100>=10) {
					triadeText.append(nascie[val%100-10]).append(" ");
				} else {// <10
					triadeText.append(cyfry[val%100]);
				}
			} else {
				triadeText.append(dziesiatki[val%100/10-2]).append(" ");
				if (val%10!=0) {
					triadeText.append(cyfry[val%10]);
				}
			}
		}
		
		return triadeText.toString().trim();
	}
}
