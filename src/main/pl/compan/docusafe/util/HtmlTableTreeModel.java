package pl.compan.docusafe.util;

import java.util.List;

/**
 * Model drzewa generowanego przez HtmlTree.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTableTreeModel.java,v 1.2 2006/02/20 15:42:30 lk Exp $
 */
public interface HtmlTableTreeModel
{

    String[] getColumnHeaders();
    String[] getColumnWidths();
    int getLevels();
    /**
     * Je�eli funkcja zwraca warto�� r�n� od null, zwr�cony ci�g
     * b�dzie wy�wietlony zamiast obrazka folderu.
     */
    String getIconReplacementText(Object element);
    /**
     * Zwraca g��wny element drzewa. Null, je�eli g��wny element
     * nie ma by� wy�wietlany.
     * @return G��wny element drzewa lub null.
     */
    Object getRootElement();
    /**
     * Zwraca odno�nik, jaki powinien by� zwi�zany z przekazanym elementem
     * drzewa.
     */
    Link[] getUrls(Object element);
    /**
     * Url u�ywany przez ikony kontroluj�ce rozwijanie i zwijanie ga��zi
     * drzewa.
     */
    String getControlUrl(Object element);
    /**
     * Zwraca opis danego elementu drzewa dla kolumn zdefiniowanych
     * jako zawieraj�ce opis elementu.
     */
    String[] getTitles(Object element);
    /**
     * Zwraca list� element�w podrz�dnych w stosunku do przekazanego.
     * Zwracana lista mo�e mie� warto�� null, je�eli przekazany element
     * nie ma element�w podrz�dnych.
     * @param element Element, kt�rego dzieci powinny by� zwr�cone przez
     *  t� metod�. Je�eli ma warto�� null, oznacza to, �e metoda getRootElement()
     *  zwr�ci�a warto�� null i metoda powinna zwr�ci� list� dzieci elementu
     *  g��wnego.
     * @return Lista element�w podrz�dnych lub null.
     */
    List getChildElements(Object element);
    /**
     * Zwraca true, je�eli przekazany element powinien by� pokazany
     * jako rozwini�ty. Aby drzewo wygl�da�o dobrze, metoda powinna
     * zwraca� true dla ka�dego elementu znajduj�cego si� na �cie�ce
     * do elementu docelowego.
     */
    boolean isExpanded(Object element);
    /**
     * Zwraca true, je�eli opis przekazanego elementu ma by� pogrubiony
     * (tag <code>strong</code>).
     */
    //boolean isHighlighted(Object element);
    /**
     * Zwraca true, je�eli tytu� elementu powinien odr�nia� si� od
     * pozosta�ych (tag <code>em</code>).
     */
    //boolean isEmphasised(Object element);

    HtmlTreeTheme getTheme();

    class Link
    {
        private String href;
        private String onclick;

        public Link(String href)
        {
            this.href = href;
        }

        public Link(String href, String onclick)
        {
            this.href = href;
            this.onclick = onclick;
        }

        public String getHref()
        {
            return href;
        }

        public void setHref(String href)
        {
            this.href = href;
        }

        public String getOnclick()
        {
            return onclick;
        }

        public void setOnclick(String onclick)
        {
            this.onclick = onclick;
        }
    }
}
