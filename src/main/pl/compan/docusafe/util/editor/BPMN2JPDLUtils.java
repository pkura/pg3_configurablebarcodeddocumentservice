package pl.compan.docusafe.util.editor;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.QName;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultElement;

import com.bea.xml.stream.NamespaceBase;

import pl.compan.docusafe.core.jbpm4.Jbpm4Utils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class BPMN2JPDLUtils 
{
	private static final String TASK_ELEMENT 			= "task";
	private static final String TASK_TRANSITION_ELEMENT = "transition";
	private static final String STATE_ELEMENT 			= "state";
	private static final String STATE_BUTTON_ELEMENT 	= "button";
	private static final String ACTIVITIES_ROOT_ELEMENT = "activities";
	private static final String CUSTOM_ELEMENT 			= "custom";
	private static final String OUTGOING_ELEMENT 		= "outgoing";
	private static final String DECISION_ELEMENT 		= "decision";
	private static final String PROCESS_ELEMENT 		= "process";
	private static final String START_ELEMENT 			= "start";
	private static final String END_ELEMENT 			= "end";
	private static final String NAME_ATTRIBUTE 			= "name";
	private static final String LABEL_ATTRIBUTE 		= "label";
	private static final String TO_ATTRIBUTE 			= "to";
	private static final String ID_ATTRIBUTE 			= "id";
	private static final String CLASS_ATTRIBUTE 		= "class";
	
	private static final Namespace ns = new Namespace("", "http://jbpm.org/4.3/jpdl");
	
	//ACTIVITIES
	
	public static Document convertJPDLToActivities(Document jpdlDocument)
	{
		Element rootElement = null;
		Iterator<Element> iterator = null;
		Element childElement = null;
		rootElement = jpdlDocument.getRootElement();
		iterator = rootElement.elementIterator();
		Document activitiesDocument = DocumentHelper.createDocument();
		activitiesDocument.addElement(ACTIVITIES_ROOT_ELEMENT);
		while(iterator.hasNext())
		{
			childElement = iterator.next();
			if(childElement.getName().equals(TASK_ELEMENT))
			{
				generateStateFromTaskElement(childElement,activitiesDocument);
			}
		}
		return activitiesDocument;
	}
	
	private static void generateStateFromTaskElement(Element taskNode, Document activitiesDocument)
	{
		String taskNameValue = taskNode.attributeValue(NAME_ATTRIBUTE);
		String taskTransitionNameValue;
		
		List<Element> taskTransitionElements = (List<Element>)taskNode.elements(TASK_TRANSITION_ELEMENT);
		Element stateElement = activitiesDocument.getRootElement().addElement(STATE_ELEMENT);
		stateElement.addAttribute(NAME_ATTRIBUTE, taskNameValue);
		for(Element transitionElement : taskTransitionElements)
		{
			taskTransitionNameValue = transitionElement.attributeValue(NAME_ATTRIBUTE);
			stateElement.addElement(STATE_BUTTON_ELEMENT).addAttribute(NAME_ATTRIBUTE, taskTransitionNameValue).addAttribute(LABEL_ATTRIBUTE, taskTransitionNameValue);
		}	
	}

	//JPDL
	private final static Logger LOG = LoggerFactory.getLogger(BPMN2JPDLUtils.class);
	
	private static enum BpmnElement
	{
		STARTEVENT, ENDEVENT, USERTASK, EXCLUSIVEGATEWAY, SEQUENCEFLOW, SERVICETASK
	}
	
//  TODO poprawi� aby w przypadku braku r�cznego ustawienia atrybutu name w elemencie userTask zosta� on dodany w odpowiadaj�cym mu tagu task w pliku .jpdl
	public static Map<String,Document> bpmn2jpdl(Document source)
	{
		Element process = source.getRootElement().element(PROCESS_ELEMENT);
		Document jpdl = DocumentHelper.createDocument();
		Element jpdlProcess = DocumentHelper.createElement(new QName(PROCESS_ELEMENT,ns));
		jpdlProcess.addAttribute(NAME_ATTRIBUTE, process.attributeValue(ID_ATTRIBUTE));
		jpdl.setRootElement(jpdlProcess);
		
		Iterator itr = process.elementIterator();
		while(itr.hasNext())
		{
			Element el = (Element) itr.next();
			try
			{
				switch(BpmnElement.valueOf(el.getName().toUpperCase()))
				{
					case STARTEVENT:
						jpdlProcess.add(toStart(el));
						break;
					case ENDEVENT:
						jpdlProcess.add(toEnd(el));
						break;					
					case SEQUENCEFLOW:	
						jpdlProcess.add(toCustomTransition(el));
						break;
					case SERVICETASK:
						jpdlProcess.add(toCustom(el));
						break;
					case USERTASK:
						jpdlProcess.add(enrichTaskWithAssigmentHandler(el, toTask(el)));
						break;
					case EXCLUSIVEGATEWAY:	
						jpdlProcess.add(enrichTaskWithHandler(el,toDecision(el)));						
						break;
					default:
						LOG.debug("element default {}",el.getName());
						break;
				}
			}
			catch(IllegalArgumentException e){ 
				LOG.debug(e.getMessage(),e); 
			}
		}
		
		
		Map<String,Document> retMap = new HashMap<String, Document>();
		retMap.put(Jbpm4Utils.JPDL, jpdl);
		retMap.put(Jbpm4Utils.ACTIVITIES, convertJPDLToActivities(jpdl));
		return retMap;
	}
	
	private static void removeNamespaces(DefaultElement n)
	{
		n.setNamespace(Namespace.NO_NAMESPACE);
		n.setNamespace(ns);
			
		for(Object o : n.elements())
			removeNamespaces((DefaultElement) o);
	}
	
	public static Document xml2dom(final File f) throws DocumentException
	{
		SAXReader reader = new SAXReader();
		Document xml = reader.read(f);
		return xml;
	}
	
	private static Element toDecision(Element el)
	{
		Element decision = DocumentHelper.createElement(new QName(DECISION_ELEMENT,ns));
		decision.addAttribute(NAME_ATTRIBUTE, el.attributeValue(ID_ATTRIBUTE));
		
		String xPath;
		Element transition;
		
		for(Object o1 : el.elements(OUTGOING_ELEMENT))
		{
			xPath = "/*[name()='definitions']/*[name()='process']/*[name()='sequenceFlow'][@id='"+((Element) o1).getText()+"']";
			Object o2 = el.getDocument().selectSingleNode(xPath);
			transition = decision.addElement(TASK_TRANSITION_ELEMENT);
            String tmp = ((Element) o2).attribute("value").getText();
            if(tmp.equals("True") || tmp.equals("False"))
                tmp = tmp.toLowerCase();
            transition.addAttribute(NAME_ATTRIBUTE, tmp);
			transition.addAttribute(TO_ATTRIBUTE, ((Element) o1).getText());
		}
		
		return decision;
	}
	
	private static Element enrichTaskWithHandler(Element el, Element decision)
	{
		Element ah = el.element("handler");
		el.remove(ah);
		removeNamespaces((DefaultElement) ah);
		decision.add(ah);
		return decision;
	}
	
	private static Element toStart(Element el)
	{
		Element start = DocumentHelper.createElement(new QName(START_ELEMENT,ns));
		start.addAttribute(NAME_ATTRIBUTE, "start");
		Element transition = start.addElement(TASK_TRANSITION_ELEMENT);
		transition.addAttribute(NAME_ATTRIBUTE, "to "+el.element(OUTGOING_ELEMENT).getText());
		transition.addAttribute(TO_ATTRIBUTE, el.element(OUTGOING_ELEMENT).getText());
		return start;
	}
	
	private static Element toEnd(Element el)
	{
		Element end = DocumentHelper.createElement(new QName(END_ELEMENT,ns));
		end.addAttribute(NAME_ATTRIBUTE, el.attributeValue(ID_ATTRIBUTE));
		return end;
	}
	
	private static Element toCustomTransition(Element el)
	{
		Element custom = DocumentHelper.createElement(new QName(CUSTOM_ELEMENT,ns));
		custom.addAttribute(CLASS_ATTRIBUTE, "pl.compan.docusafe.core.jbpm4.CustomTransition");
		custom.addAttribute(NAME_ATTRIBUTE, el.attributeValue(ID_ATTRIBUTE));
		Element transition = custom.addElement(TASK_TRANSITION_ELEMENT);
		transition.addAttribute(NAME_ATTRIBUTE, "to "+el.attributeValue("targetRef"));
		transition.addAttribute(TO_ATTRIBUTE, el.attributeValue("targetRef"));
		return custom;
	}
	
	private static Element toCustom(Element el)
	{
		Element custom = DocumentHelper.createElement(new QName(CUSTOM_ELEMENT,ns));
		custom.addAttribute(CLASS_ATTRIBUTE, el.element(CUSTOM_ELEMENT).attributeValue(CLASS_ATTRIBUTE));
		custom.addAttribute(NAME_ATTRIBUTE, el.attributeValue(ID_ATTRIBUTE));
		Element transition = custom.addElement(TASK_TRANSITION_ELEMENT);
		transition.addAttribute(NAME_ATTRIBUTE, "to "+el.element(OUTGOING_ELEMENT).getText());
		transition.addAttribute(TO_ATTRIBUTE, el.element(OUTGOING_ELEMENT).getText());
		return custom;
	}
	
	private static Element toTask(Element el)
	{
		Element task = DocumentHelper.createElement(new QName(TASK_ELEMENT,ns));
		task.addAttribute(NAME_ATTRIBUTE, el.attributeValue(ID_ATTRIBUTE));
		Element transition = null;
		String xPath;
		String name;
		for(Object out : el.elements(OUTGOING_ELEMENT))
		{
			transition = task.addElement(TASK_TRANSITION_ELEMENT);
			xPath = "/*[name()='definitions']/*[name()='process']/*[name()='sequenceFlow'][@id='"+((Element) out).getText()+"']";
			Object o1 = el.getDocument().selectSingleNode(xPath);
			name = ((Element) o1).attribute(NAME_ATTRIBUTE) != null ? ((Element) o1).attribute(NAME_ATTRIBUTE).getText() : "to "+((Element) out).getText();
			transition.addAttribute(NAME_ATTRIBUTE, name);
			transition.addAttribute(TO_ATTRIBUTE, ((Element) out).getText());
		}
		return task;
	}
	
	private static Element enrichTaskWithAssigmentHandler(Element el, Element task)
	{
		Element ah = el.element("assignment-handler");
		el.remove(ah);
		removeNamespaces((DefaultElement) ah);
        task.add(ah);
		return task;
	}
}
