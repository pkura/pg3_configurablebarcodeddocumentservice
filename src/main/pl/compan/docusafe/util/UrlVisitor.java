package pl.compan.docusafe.util;

/**
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UrlVisitor.java,v 1.1 2004/05/16 20:10:38 administrator Exp $
 */
public interface UrlVisitor
{
    String getUrl(Object element);
}
