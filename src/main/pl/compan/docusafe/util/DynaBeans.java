package pl.compan.docusafe.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

import pl.compan.docusafe.core.users.DSUser;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DynaBeans.java,v 1.23 2007/10/22 14:41:12 mariuszk Exp $
 */
public class DynaBeans
{
    /**
     * Klasa zawierająca własności: <code>label</code>.
     */
    public static final DynaClass label = new BasicDynaClass("label", null,
        new DynaProperty[] {
            new DynaProperty("label", String.class)
        });

    /**
     * Klasa zawierająca własności: <code>label</code>, <code>value</code>.
     */
    public static final DynaClass htmlOption = new BasicDynaClass("htmlOption", null,
        new DynaProperty[] {
            new DynaProperty("label", String.class),
            new DynaProperty("value", String.class)
        });

    /**
     * Klasa zawierająca własności: <ul>
     * <li>link - String
     * <li>title - String
     * <li>tooltip - String
     * <li>name - String
     * <li>value - String
     * <li>selected - Boolean
     * </ul>
     */
    public static final DynaClass htmlLink = new BasicDynaClass("htmlLink", null,
        new DynaProperty[] {
            new DynaProperty("link", String.class),
            new DynaProperty("title", String.class),
            new DynaProperty("tooltip", String.class),
            new DynaProperty("name", String.class),
            new DynaProperty("value", String.class),
            new DynaProperty("selected", Boolean.class)
        });

    /**
     * Klasa zawierająca własności <code>author</code>, <code>content</code>
     * i <code>ctime</code>.
     */
    public static final DynaClass remark = new BasicDynaClass("remark", null,
        new DynaProperty[] {
            new DynaProperty("author", String.class),
            new DynaProperty("content", String.class),
            new DynaProperty("ctime", Date.class)
        });

    public static final DynaClass assignment = new BasicDynaClass("assignment", null,
        new DynaProperty[] {
            new DynaProperty("description", String.class),
            new DynaProperty("assigneeId", String.class)
        });

    public static final DynaClass assignmentHistory = new BasicDynaClass("assignmentHistory", null,
        new DynaProperty[] {
            new DynaProperty("assigner", String.class),
            new DynaProperty("assignee", String.class),
            new DynaProperty("startDate", Date.class),
            new DynaProperty("endDate", Date.class),
            new DynaProperty("description", String.class)
        });

    public static final DynaClass officeDocument = new BasicDynaClass("task", null,
        new DynaProperty[] {
            new DynaProperty("incoming", Boolean.class),
            new DynaProperty("outgoing", Boolean.class),
            new DynaProperty("internal", Boolean.class),
            new DynaProperty("sender", String.class),
            new DynaProperty("recipient", String.class),
            new DynaProperty("recipients", List.class),
            new DynaProperty("externalId", String.class),
            new DynaProperty("pressmark", String.class),
            new DynaProperty("incomingDate", Date.class),
            new DynaProperty("outgoingDate", Date.class),
            new DynaProperty("description", String.class),
            new DynaProperty("link", String.class),
            new DynaProperty("documentId", Long.class),
            new DynaProperty("caseDocumentIdentifier", String.class),
            new DynaProperty("assignmentCtime", Date.class),
            new DynaProperty("journalSeqId", Integer.class),
            new DynaProperty("sourceUser", String.class),
            new DynaProperty("targetUser", String.class),
            new DynaProperty("officeNumber", String.class)
        });
    
    public static final DynaClass caseToCase = new BasicDynaClass("casetocase", null,
    		 new DynaProperty[] {
    		//Dane z case To Case
    		new DynaProperty("caseToCaseId", Long.class),
    		new DynaProperty("creatingUserId", Long.class),
    		new DynaProperty("creatingUserName", String.class),
            new DynaProperty("creationTime", Date.class), 
    		//Dane z konkretnej sprawy 
            new DynaProperty("caseContainerId", Long.class),
            new DynaProperty("caseOfficeId", String.class),
            new DynaProperty("linkToCase", String.class),
            new DynaProperty("caseTitle", String.class),
            new DynaProperty("caseAuthor", String.class),
            new DynaProperty("caseOpenDate", Date.class),
            new DynaProperty("caseFinishDate", Date.class),
            
        });
        
	
    public static final DynaClass task = new BasicDynaClass("task", null,
        new DynaProperty[] {
            new DynaProperty("mainJournalEntrySeq", Integer.class),
            new DynaProperty("link", String.class),
            new DynaProperty("status", String.class),
            new DynaProperty("caseDocumentId", String.class),
            new DynaProperty("description", String.class),
            new DynaProperty("sender", String.class),
            new DynaProperty("ctime", Date.class),
            new DynaProperty("accepted", Boolean.class)
        });

    public static final DynaClass wfTask = new BasicDynaClass("wfTask", null,
        new DynaProperty[] {
            new DynaProperty("key", String.class),
            new DynaProperty("name", String.class),
            new DynaProperty("description", String.class),
            new DynaProperty("accepted", Boolean.class),
            new DynaProperty("link", String.class)
        });

    public static final DynaClass user = new BasicDynaClass("user", null,
        new DynaProperty[] {
            new DynaProperty("name", String.class),
            new DynaProperty("description", String.class),
            new DynaProperty("firstname", String.class),
            new DynaProperty("lastname", String.class),
            new DynaProperty("editLink", String.class),
            new DynaProperty("deleteLink", String.class),
            new DynaProperty("workingTasks", Integer.class),
            new DynaProperty("awaitingTasks", Integer.class),
            new DynaProperty("assignedCases", Integer.class)
        });

    public static final DynaClass addressee = new BasicDynaClass("addressee", null,
        new DynaProperty[] {
            new DynaProperty("title", String.class),
            new DynaProperty("firstname", String.class),
            new DynaProperty("lastname", String.class),
            new DynaProperty("organization", String.class),
            new DynaProperty("organizationDivision", String.class),
            new DynaProperty("street", String.class),
            new DynaProperty("zip", String.class),
            new DynaProperty("location", String.class),
            new DynaProperty("country", String.class),
            new DynaProperty("email", String.class),
            new DynaProperty("fax", String.class),
            new DynaProperty("remarks", String.class),
            new DynaProperty("editLink", String.class),
            new DynaProperty("pickLink", String.class)
        });

    public static final DynaClass role = new BasicDynaClass("role", null,
        new DynaProperty[] {
            new DynaProperty("id", Long.class),
            new DynaProperty("name", String.class),
            new DynaProperty("editLink", String.class)
        });

    public static final DynaClass division = new BasicDynaClass("role", null,
        new DynaProperty[] {
            new DynaProperty("guid", String.class),
            new DynaProperty("name", String.class),
            new DynaProperty("prettyPath", String.class),
            new DynaProperty("position", Boolean.class),
        });

    /**
     * Klasa posiada własności <code>value (Object)</code>,
     * <code>label (String)</code>, <code>checked (Boolean)</code>.
     */
    public static final DynaClass checkbox = new BasicDynaClass("checkbox", null,
        new DynaProperty[] {
            new DynaProperty("value", Object.class),
            new DynaProperty("label", String.class),
            new DynaProperty("checked", Boolean.class)
        });

/*
    public static final DynaClass case_ = new BasicDynaClass("case", null,
        new DynaProperty[] {
            new DynaProperty("id", Long.class),
            new DynaProperty("assignedUser", String.class),
            new DynaProperty("caseId", String.class),
            new DynaProperty("editLink", String.class),
            new DynaProperty("caseDocumentIdentifier", String.class),
            new DynaProperty("openDate", Date.class),
            new DynaProperty("finishDate", Date.class),
            new DynaProperty("author", String.class),
            new DynaProperty("daysToFinish", Integer.class),
        });
*/

/*
    public static final DynaClass workHistoryEntry = new BasicDynaClass("workHistoryEntry", null,
        new DynaProperty[] {
            new DynaProperty("ctime", Date.class),
            new DynaProperty("username", String.class),
            new DynaProperty("description", String.class),
        });
*/

/*
    public static final DynaClass wfActivity = new BasicDynaClass("wfActivity", null,
        new DynaProperty[] {
            new DynaProperty("name", String.class),
            new DynaProperty("description", String.class),
        });
*/

    public static final DynaClass htmlInput = new BasicDynaClass("htmlInput", null,
        new DynaProperty[] {
            new DynaProperty("type", String.class),
            new DynaProperty("name", String.class),
            new DynaProperty("value", String.class),
            new DynaProperty("label", String.class),
            new DynaProperty("readOnly", Boolean.class),
            new DynaProperty("options", Collection.class)
        });

    public static final DynaClass audit = new BasicDynaClass("audit", null,
        new DynaProperty[] {
            new DynaProperty("description", String.class),
            new DynaProperty("property", String.class),
            new DynaProperty("newValue", String.class),
            new DynaProperty("oldValue", String.class),
            new DynaProperty("user", String.class),
            new DynaProperty("ctime", Date.class),
        });

    public static final DynaClass documentWatch = new BasicDynaClass("documentWatch", null,
        new DynaProperty[] {
            new DynaProperty("id", Long.class),
            new DynaProperty("documentId", Long.class),
            new DynaProperty("documentTitle", String.class),
            new DynaProperty("folderTitle", String.class),
            new DynaProperty("user", String.class),
            new DynaProperty("firstname", String.class),
            new DynaProperty("lastname", String.class),
            new DynaProperty("documentCtime", Date.class),
            new DynaProperty("documentMtime", Date.class),
            new DynaProperty("link", String.class),
            new DynaProperty("documentInaccessible", Boolean.class),
        });
    
    public static final DynaClass test = new BasicDynaClass("test", null,
    	new DynaProperty[] {
    		new DynaProperty("id", Long.class),
    		new DynaProperty("nrdokumentu", String.class),
    });
    
    public static final DynaClass substituteHistory = new BasicDynaClass("substituteHistory", null,
            new DynaProperty[] {
                new DynaProperty("login", String.class),
                new DynaProperty("substId", Long.class),
                new DynaProperty("substitutedFrom", Date.class),
                new DynaProperty("substitutedThrough", Date.class),
                new DynaProperty("substLogin", String.class),
                new DynaProperty("username", String.class),
                new DynaProperty("substUsername", String.class),
                new DynaProperty("substituteCreator", String.class),
                new DynaProperty("status", String.class),
                new DynaProperty("cancelDate", String.class),
            });

    public static DynaBean bean(DynaClass clazz)
    {
        return new BasicDynaBean(clazz);
    }


    /**
     * Zwraca nowy DynaBean klasy {@link #htmlOption}.
     */
    public static DynaBean newHtmlOption()
    {
        return new BasicDynaBean(htmlOption);
    }

    /**
     * Zwraca nowy DynaBean klasy {@link #htmlLink}.
     */
    public static DynaBean newHtmlLink()
    {
        return new BasicDynaBean(htmlLink);
    }

    public static DynaBean[] getHtmlOptions(DSUser[] users)
    {
        List beans = new ArrayList(users.length);
        for (int i=0; i < users.length; i++)
        {
            DynaBean bean = bean(DynaBeans.htmlOption);
            bean.set("label", users[i].asFirstnameLastnameName());
            bean.set("value", users[i].getName());
            beans.add(bean);
        }
        return (DynaBean[]) beans.toArray(new DynaBean[beans.size()]);
    }

    private DynaBeans()
    {
    }
}
