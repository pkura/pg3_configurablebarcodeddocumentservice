package pl.compan.docusafe.util;

import java.text.Collator;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.Folder;


/**
 * Klasa pomocnicza do wstawiania pism do odpowiednich katalogow
 * @author Micha� Sankowski
 */
public class FolderInserter {

	private final static Logger LOG = LoggerFactory.getLogger(FolderInserter.class);

	private final Folder folder;

	private FolderInserter(Folder folder){
		this.folder = folder;
	}

	public Folder getFolder(){
		return folder;
	}

	public static FolderInserter root() throws EdmException{
		return new FolderInserter(Folder.getRootFolder());
	}

	public FolderInserter subfolder(final String name) throws EdmException
	{
		return new FolderInserter(folder.createSubfolderIfNotPresent(name));
	}

	public FolderInserter subfolders(final String... names) throws EdmException{
		FolderInserter ret = this;
		for(String name:names){
			ret = ret.subfolder(name);
		}
		return ret;
	}

	public void insert(final Document d) throws EdmException{
		d.setFolder(folder);
	}

	public static String toQuarter(final String string)
	{
		StringComparator cc = new StringComparator();
		final String c = String.valueOf(string.toLowerCase().charAt(0));
		
		if (cc.compare(c, "g") < 0)
			return "A-F";
		else if (cc.compare(c, "m") < 0)
			return "G-�";
		else if (cc.compare(c, "t") < 0)
			return "M-S";
		else
			return "T-�";
	}
	
	
	private static class StringComparator implements Comparator<String>
    {
    	private static final Collator COLLATOR = Collator.getInstance(new Locale("PL","pl"));
    	
        public int compare(String e1, String e2)
        {
            COLLATOR.setStrength(Collator.SECONDARY);
        	return COLLATOR.compare(e1, e2);        	
        }
    }
	
	public static String to3Letters(String s)
	{
		LOG.trace("String: {}",s);
		
		final StringComparator cc = new StringComparator();
		final String c = String.valueOf(s.toLowerCase().charAt(0));
		
		LOG.trace("Pierwsza litera: {}",c);
		if (cc.compare(c, "d") < 0)
		   return("A-C");
		else if (cc.compare(c, "g") < 0)
			return("D-F");
		else if (cc.compare(c, "j") < 0)
			return("G-I");
		else if (cc.compare(c, "m") < 0)
			return("J-L");
		else if (cc.compare(c, "p") < 0)
			return("M-O");
		else if (cc.compare(c, "t") < 0)
			return("P-S");
		else if (cc.compare(c, "x") < 0)
			return("T-W");
		else
			return("Y-�");
				
	}
	
	public static String to1Letters(String s)
	{
		LOG.trace("String: {}",s);
		
		final StringComparator cc = new StringComparator();
		final String c = String.valueOf(s.toLowerCase().charAt(0));
		
		LOG.trace("Pierwsza litera: {}",c);
		
		if (cc.compare(c, "a") <= 0)
		   return("A");
		else if (cc.compare(c, "c") < 0)
			return("B");
		else if (cc.compare(c, "d") < 0)
			return("C");
		else if (cc.compare(c, "e") < 0)
			return("D");
		else if (cc.compare(c, "f") < 0)
			return("E");
		else if (cc.compare(c, "g") < 0)
			return("F");
		else if (cc.compare(c, "h") < 0)
			return("G");
		else if (cc.compare(c, "i") < 0)
			return("H");
		else if (cc.compare(c, "j") < 0)
			return("I");
		else if (cc.compare(c, "k") < 0)
			return("J");
		else if (cc.compare(c, "l") < 0)
			return("K");
		else if (cc.compare(c, "m") < 0)
			return("L");
		else if (cc.compare(c, "n") < 0)
			return("M");
		else if (cc.compare(c, "o") < 0)
			return("N");
		else if (cc.compare(c, "p") < 0)
			return("O");
		else if (cc.compare(c, "r") < 0)
			return("P");
		else if (cc.compare(c, "s") < 0)
			return("R");
		else if (cc.compare(c, "t") < 0)
			return("S");
		else if (cc.compare(c, "u") < 0)
			return("T");
		else if (cc.compare(c, "w") < 0)
			return("U");
		else if (cc.compare(c, "x") < 0)
			return("W");
		else if (cc.compare(c, "y") < 0)
			return("X");
		else if (cc.compare(c, "z") < 0)
			return("Y");
		else
			return("Z");
				
	}
	
	public static String to2Letters(String s)
	{
		LOG.trace("String: {}",s);
		
		final StringComparator cc = new StringComparator();
		final String c = String.valueOf(s.toLowerCase().charAt(0));
		
		LOG.trace("Pierwsza litera: {}",c);
		
		if (cc.compare(c, "c") < 0)
		   return("A-B");
		else if (cc.compare(c, "e") < 0)
			return("C-D");
		else if (cc.compare(c, "g") < 0)
			return("E-F");
		else if (cc.compare(c, "i") < 0)
			return("G-H");
		else if (cc.compare(c, "k") < 0)
			return("I-J");
		else if (cc.compare(c, "m") < 0)
			return("K-�");
		else if (cc.compare(c, "o") < 0)
			return("M-N");
		else if (cc.compare(c, "r") < 0)
			return("O-P");
		else if (cc.compare(c, "t") < 0)
			return("R-S");
		else if (cc.compare(c, "w") < 0)
			return("T-V");
		else if (cc.compare(c, "x") < 0)
			return("W-X");
		else
			return("Y-�");
				
	}

	public static String toYearMonthDay(Date o){
		return DateUtils.formatSqlDate(o);
	}

	public static String toYearMonthDay(Object o) throws ClassCastException{
		return toYearMonthDay((Date)o);
	}

	public static String toYear(Date o){
		return DateUtils.formatYear(o);
	}

	public static String toYear(Object o) throws ClassCastException{
		return toYear((Date)o);
	}

	public static String toMonth(Date o)
	{
		return DateUtils.formatPolishMonth(o);
	}

	public static String toMonth(Object o) throws ClassCastException{
		return toMonth((Date)o);		
	}
	
	public static String toNumberMonth(Object o) throws ClassCastException
	{
		StringBuffer sb = new StringBuffer();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime((Date)o);
		int month = calendar.get(Calendar.MONTH);
		month++; //bo month jest liczony od 0
		if(month < 10)
		{
			sb.append('0');
		}
		sb.append(month);
		sb.append(' ');
		sb.append(toMonth(calendar.getTime()));
		return sb.toString();
	}
}
