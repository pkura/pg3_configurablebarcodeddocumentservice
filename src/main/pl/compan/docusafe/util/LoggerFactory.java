package pl.compan.docusafe.util;

import org.slf4j.Marker;

/**
 *
 * @author Micha� Sankowski
 */
public class LoggerFactory 
{

	public static Logger getLogger(Class name){
		return new Slf4jLoggerAdapter(org.slf4j.LoggerFactory.getLogger(name));
	}

	public static Logger getLogger(String name){
		return new Slf4jLoggerAdapter(org.slf4j.LoggerFactory.getLogger(name));
	}
}

class Slf4jLoggerAdapter implements Logger{
	org.slf4j.Logger base;

	Slf4jLoggerAdapter(org.slf4j.Logger base){
		this.base = base;
	}

	public String getName() {
		return base.getName();
	}

	public boolean isTraceEnabled() {
		return base.isTraceEnabled();
	}

	public void trace(String msg) {
		base.trace(msg);
	}

	public void trace(String format, Object... arg) {
		base.trace(format, arg);
	}

	public void trace(String msg, Throwable t) {
		base.trace(msg, t);
	}

	public boolean isTraceEnabled(Marker marker) {
		return base.isTraceEnabled();
	}

	public void trace(Marker marker, String msg) {
		base.trace(marker, msg);
	}

	public void trace(Marker marker, String format, Object... arg) {
		base.trace(marker, format, arg);
	}

	public void trace(Marker marker, String msg, Throwable t) {
		base.trace(marker, msg, t);
	}

	public boolean isDebugEnabled() {
		return base.isDebugEnabled();
	}

	public void debug(String msg) {
		base.debug(msg);
	}

	public void debug(String format, Object... arg) {
		base.debug(format, arg);
	}

	public void debug(String msg, Throwable t) {
		base.debug(msg, t);
	}

	public boolean isDebugEnabled(Marker marker) {
		return base.isDebugEnabled(marker);
	}

	public void debug(Marker marker, String msg) {
		base.debug(marker, msg);
	}

	public void debug(Marker marker, String format, Object... arg) {
		base.debug(marker, format, arg);
	}

	public void debug(Marker marker, String msg, Throwable t) {
		base.debug(marker, msg, t);
	}

	public boolean isInfoEnabled() {
		return base.isInfoEnabled();
	}

	public void info(String msg) {
		base.info(msg);
	}

	public void info(String format, Object... arg) {
		base.info(format, arg);
	}

	public void info(String msg, Throwable t) {
		base.info(msg, t);
	}

	public boolean isInfoEnabled(Marker marker) {
		return base.isInfoEnabled(marker);
	}

	public void info(Marker marker, String msg) {
		base.info(marker, msg);
	}

	public void info(Marker marker, String format, Object... arg) {
		base.info(marker, format, arg);
	}

	public void info(Marker marker, String msg, Throwable t) {
		base.info(marker, msg, t);
	}

	public boolean isWarnEnabled() {
		return base.isWarnEnabled();
	}

	public void warn(String msg) {
		base.warn(msg);
	}

	public void warn(String format, Object... arg) {
		base.warn(format, arg);
	}

	public void warn(String msg, Throwable t) {
		base.warn(msg, t);
	}

	public boolean isWarnEnabled(Marker marker) {
		return base.isWarnEnabled(marker);
	}

	public void warn(Marker marker, String msg) {
		base.warn(marker, msg);
	}

	public void warn(Marker marker, String format, Object... arg) {
		base.warn(marker, format, arg);
	}

	public void warn(Marker marker, String msg, Throwable t) {
		base.warn(marker, msg, t);
	}

	public boolean isErrorEnabled() {
		return base.isErrorEnabled();
	}

	public void error(String msg) {
		base.error(msg);
	}

	public void error(String format, Object... arg) {
		base.error(format, arg);
	}

	public void error(String msg, Throwable t) {
		base.error(msg, t);
	}
	
	public void error(Throwable t) {
		base.error(t.getMessage(), t);
	}

	public boolean isErrorEnabled(Marker marker) {
		return base.isErrorEnabled(marker);
	}

	public void error(Marker marker, String msg) {
		base.error(marker, msg);
	}

	public void error(Marker marker, String format, Object... arg) {
		base.error(marker, format, arg);
	}

	public void error(Marker marker, String msg, Throwable t) {
		base.error(marker, msg, t);
	}

	public void debug(Object msg) {
		base.debug(""+msg);
		
	}

}

