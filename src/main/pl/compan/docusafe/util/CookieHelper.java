package pl.compan.docusafe.util;

import com.google.common.base.Optional;
import com.opensymphony.webwork.ServletActionContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
 */
public class CookieHelper {

    public static Optional<String> getCookieValue(HttpServletRequest request, String name) throws Exception {
        Cookie[] cookies = request.getCookies();

        String cookieString = "";

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    cookieString = cookie.getValue();
                }
            }

            if(cookies.length > 0) {
                return Optional.of(cookieString);
            }
        }

        return Optional.absent();

    }

    public static String getCookieValueOrError(HttpServletRequest request, String name) throws Exception {
        Optional<String> cookie = getCookieValue(request, name);
        if(cookie.isPresent()) {
            return cookie.get();
        } else {
            throw new Exception("Cookie "+name+" not found");
        }
    }

    public static Optional<String> getCookieValue(String name) throws Exception {
        return getCookieValue(ServletActionContext.getRequest(), name);
    }

    public static String getCookieValueOrError(String name) throws Exception {
        return getCookieValueOrError(ServletActionContext.getRequest(), name);
    }

}
