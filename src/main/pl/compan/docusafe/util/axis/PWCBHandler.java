package pl.compan.docusafe.util.axis;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;

/**
 * Handler przygotowujacy autoryzacje oparta o haslo
 * @author wkutyla
 */
public class PWCBHandler implements CallbackHandler
{
    static final Logger log = LoggerFactory.getLogger(PWCBHandler.class);
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++)
        {
            try
            {
                WSPasswordCallback pwcb = (WSPasswordCallback)callbacks[i];
                String id = Docusafe.getAdditionProperty(AxisClientConfigurator.AXIS_USERNAME);
                String pass = Docusafe.getAdditionProperty(AxisClientConfigurator.AXIS_PASSWORD);
                log.trace("id={}",id);
                pwcb.setIdentifier(id);
                pwcb.setPassword(pass);
            }
            catch (Exception e)
            {
                log.error("",e);
                throw new IOException(e.toString());
            }
        }
    }
}