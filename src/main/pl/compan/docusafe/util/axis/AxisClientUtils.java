package pl.compan.docusafe.util.axis;

import com.google.common.primitives.Ints;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import pl.compan.docusafe.boot.Docusafe;

public class AxisClientUtils {

    private static final MultiThreadedHttpConnectionManager httpConnectionManager = makeConnectionManager();

    private static MultiThreadedHttpConnectionManager makeConnectionManager() {
        MultiThreadedHttpConnectionManager manager = new MultiThreadedHttpConnectionManager();
        HttpConnectionManagerParams params = manager.getParams();
        if (params == null) {
            params = new HttpConnectionManagerParams();
        }
        params.setMaxTotalConnections(Ints.tryParse(Docusafe.getAdditionPropertyOrDefault("axis-client-utils.http-client.max-total-connection", "30")));
        params.setDefaultMaxConnectionsPerHost(Ints.tryParse(Docusafe.getAdditionPropertyOrDefault("axis-client-utils.http-client.max-connections-per-host", "30")));
        manager.setParams(params);
        return manager;
    }

    @Deprecated
    public static synchronized HttpClient createHttpClient(int maxTotalConnections, int defaultMaxConnectionsPerHost) {
        MultiThreadedHttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpConnectionManagerParams params = httpConnectionManager.getParams();
        if (params == null) {
            params = new HttpConnectionManagerParams();
        }
        params.setMaxTotalConnections(maxTotalConnections);
        params.setDefaultMaxConnectionsPerHost(defaultMaxConnectionsPerHost);
        httpConnectionManager.setParams(params);
        HttpClient httpClient = new HttpClient(httpConnectionManager);
        return httpClient;
    }

    public static HttpClient createHttpClient() {
        HttpClient httpClient = new HttpClient(httpConnectionManager);
        return httpClient;
    }
}
