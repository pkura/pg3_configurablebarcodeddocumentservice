package pl.compan.docusafe.util.axis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Properties;

import com.google.common.base.Strings;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axis.utils.StringUtils;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;
import org.apache.commons.io.IOUtils;
import org.apache.neethi.Policy;
import org.apache.neethi.PolicyEngine;
import org.apache.rampart.RampartMessageData;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.http.EasySSLProtocolSocketFactory;


/**
 * Klasa odpowiedzialna za konfigurowanie klienta
 * AXIS - ustawienia zwiazane 
 * @author wkutyla
 */
public class AxisClientConfigurator
{
    final static Logger log = LoggerFactory.getLogger(AxisClientConfigurator.class);
    public final static String AXIS_HOST = "axis.host";
    public final static String REPLICATION_ON = "axis.on";
	public static final String HTTP_TIMEOUT = "axis.connection.timeout";
	public static final String AXIS_PASSWORD = "axis.password";
	public static final String AXIS_USERNAME = "axis.username";
	public static final String AXIS_POLICY = "axis.policy";
	public static final String CALL_TRANSPORT_CLEANUP = "axis.call_transport_cleanup";
	public static final String HTTP_PROXY_ON = "http.proxy.on";
	public static final String HTTP_PROXY_USERNAME = "http.proxy.username";
	public static final String HTTP_PROXY_PASSWORD = "http.proxy.password";
	public static final String HTTP_PROXY_NTLM_DOMAIN = "http.proxy.ntlmdomain";
	public static final String HTTP_PROXY_HOST_NAME = "http.proxy.hostname";
	public static final String HTTP_PROXY_AUTENTICATION = "http.proxy.autentication";
	public static final String HTTP_PROXY_PORT = "http.proxy.port";
	public static final String CLIENT_PROPERTIES = "client.properties";
	public static final String CERTIFICATE_ON = "certificate.on";
	public static final String CERTIFICATE_MULTI = "certificates.multi";
	public static final String CERTIFICATE_FILE = "certificate.pfx";
	public static final String CERT_X509_PASSWORD = "cert.X509.password";
	public static boolean doHttpsInit = true;
	
	/** @var Jesibysmy chcieli definiowa� certyfikat inaczej ani�eli w addsach*/
	private String certificateName = null;
	/** @var has�o do certyfikatu **/
	private String password = null;

    private String additionPropertiesPrefix;
	
    public AxisClientConfigurator()
	{
	}
	
    public AxisClientConfigurator(String certificateName, String password)
	{
		super();
		this.certificateName = certificateName;
		this.password = password;
	}

    public AxisClientConfigurator(String additionPropertiesPrefix) {
        this.additionPropertiesPrefix = additionPropertiesPrefix;
    }

    public AxisClientConfigurator(String additionPropertiesPrefix, String certificateName, String password) {
        this.additionPropertiesPrefix = additionPropertiesPrefix;
        this.certificateName = certificateName;
        this.password = password;
    }

    private String makeAdditionPropertyPrefix() {
        return !Strings.isNullOrEmpty(additionPropertiesPrefix) ? additionPropertiesPrefix + "." : "";
    }

    /**
     * Metoda wykonuje ustawienia adresu, portu, proxy, etc dla uslugi
     * @param stub
     * @param serviceSuffix - suffix us�ugi - zaczyna sie od /
     * @throws Exception
     */
    public void setUpHttpParameters(Stub stub, String serviceSuffix) throws Exception
    {
        if (doHttpsInit)
            doHttpsInit();
        log.debug("setUpParameters suffix={}",serviceSuffix);
        String hostName = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+AXIS_HOST);
        String serviceUrl = this.getServiceAddress(hostName,serviceSuffix);
        log.trace("hostName={}",hostName);
        log.trace("serviceUrl={}",serviceUrl);
        ServiceClient serviceClient = stub._getServiceClient();
        Options options = serviceClient.getOptions();
        options.setTo(new EndpointReference(serviceUrl));
        
        setCallTransportCleanup(options);
        setUpHttpTimeout(options);
        setUpRampart(serviceClient);
        

        String proxyOn = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+HTTP_PROXY_ON);
        log.trace("proxyOn={}",proxyOn);
        if ("true".equalsIgnoreCase(proxyOn))
        {
            String proxyHost = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+(HTTP_PROXY_HOST_NAME));
            String proxyAutentication = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+HTTP_PROXY_AUTENTICATION);
            log.debug("wykorzystujemy proxy host={}, autentication={}",proxyHost,proxyAutentication);

            HttpTransportProperties.ProxyProperties proxyProperties =  new HttpTransportProperties.ProxyProperties();


            String proxyPort = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+HTTP_PROXY_PORT);
            Integer port = 8080;
            try
            {
                if(proxyPort!=null && proxyPort.length()>0)
                    port = Integer.valueOf(proxyPort);
            }
            catch (Exception e)
            {
                log.error(e.getMessage(),e);
                port = 8080;
            }
            log.trace("proxyPort={}",port);
            
            proxyProperties.setProxyName(proxyHost);
            proxyProperties.setProxyPort(port);

            String proxyUser = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+HTTP_PROXY_USERNAME);
            String proxyPassword = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+HTTP_PROXY_PASSWORD);
            log.trace("proxyUser={}",proxyUser);
            if (proxyPassword!=null && proxyPassword.trim().length()>0)
                    log.trace("haslo nie jest puste");
            if (proxyUser!=null && proxyUser.trim().length()>0 && proxyPassword!=null && proxyPassword.trim().length()>0)
            {
                log.trace("proxy z autoryzacja");
                proxyProperties.setUserName(proxyUser);
                proxyProperties.setPassWord(proxyPassword);
                if ("NTLM".equalsIgnoreCase(proxyAutentication))
                {                    
                    String domain = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+HTTP_PROXY_NTLM_DOMAIN);
                    proxyProperties.setDomain(domain);
                    log.trace("NTLM proxy domain={}",domain);
                }
            }
            else
            {
                log.trace("uzytkownik pusty - nie ustawiamy autoryzacji");
            }
            options.setProperty(org.apache.axis2.transport.http.HTTPConstants.PROXY,proxyProperties);
        }
    }

	private void setCallTransportCleanup(Options options) {
		options.setCallTransportCleanup(Boolean.valueOf(Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+CALL_TRANSPORT_CLEANUP)));
	}

    /**
     * sets http client
     * @param client
     * @throws Exception
     */
    public void setHttpClient(HttpClient client) throws Exception {
        getAxisConfigurationContext().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
    }

    private static Long timeoutInMillis = null;

    /**
     * Ustawianie timeoutu dla polaczenia HTTP
     * @param options
     * @param env
     */
    protected void setUpHttpTimeout(Options options)
    {
        if (timeoutInMillis == null)
        {
            try
            {
                String timeOut = Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+HTTP_TIMEOUT);
                Long timeOutInSecs = Long.valueOf(timeOut);
                timeoutInMillis = 1000l * timeOutInSecs;
                log.debug("timeout w ms = {}",timeoutInMillis);

            }
            catch (Exception e)
            {
                timeoutInMillis = 30000l;
                log.error("",e);
                log.error("domyslny timeout = {}",timeoutInMillis);

            }
        }
        options.setTimeOutInMilliSeconds(timeoutInMillis);
    }
    /**
     * Przygotowuje adres uslugi
     * @param hostName
     * @param serviceSuffix
     * @return
     */
    protected String getServiceAddress(String hostName, String serviceSuffix)
    {
        if (hostName==null || hostName.trim().length()==0)
        {
            throw new java.lang.IllegalArgumentException("hostName null");
        }
        String newHost = hostName.toLowerCase().trim();
        if (!newHost.startsWith("http"))
        {
            log.debug("brak prefixu http lub https - dodajemy https");
            newHost = "https://"+newHost;
        }
        return newHost+serviceSuffix;
    }

    /**
     * Dynamiczna konfiguracja OutFlowSecurity
     * @param serviceClient
     * @throws java.lang.Exception
     */
    protected void setUpRampart(ServiceClient serviceClient) throws Exception
    {
        log.trace("setUpRampart");
        Options options = serviceClient.getOptions();
        OutflowConfiguration of = null;
        if(AvailabilityManager.isAvailable(CERTIFICATE_ON))
        {
	        String certName = dumpCertificate();
	        String pass =  getCertificatePassword();
	        log.trace("certificatePassword={}",pass);
	        String alias = findKeyAliasName(certName,pass);		// <- bardzo dlugi czas wywolania! nawet 3 sekundy
	        log.trace("keyAlias={}",alias);
	        
	        if (isEmpty(certName) || isEmpty(alias) || isEmpty(pass))
	        {
	            log.trace("nie ma certyfikatu");
	            of = new OutflowConfiguration( 1 ); // 1 token
	        }
	        else
	        {
	        	
	        	if(Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+CERTIFICATE_MULTI).equalsIgnoreCase("true"))
	        	{
	        		of = new OutflowConfiguration( 1 ); // 2 tokeny, potrzebne np. dla ePUAP
		        	String clientPropFileName = prepareClientProperties(pass,alias, true);
		            of.setActionItems("Signature");
		            of.setUser(alias);       
		            of.setSignaturePropFile(clientPropFileName);
		            of.setSignatureKeyIdentifier(WSSHandlerConstants.BST_DIRECT_REFERENCE);
		            of.setPasswordCallbackClass("pl.compan.docusafe.util.axis.PWCBHandler");
		            serviceClient.getOptions().setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, of.getProperty());
	        	}else{
	        		of = new OutflowConfiguration( 2 ); // 2 tokeny, potrzebne np. dla ePUAP
		        	prepareClientProperties(pass,alias);
		            of.setActionItems("Signature");
		            of.setUser(alias);       
		            of.setSignaturePropFile(CLIENT_PROPERTIES);
		            of.setSignatureKeyIdentifier(WSSHandlerConstants.BST_DIRECT_REFERENCE);
		            of.setPasswordCallbackClass("pl.compan.docusafe.util.axis.PWCBHandler");
		            of.nextAction();
	        	}
	        }
        }
        else
        {
        	of = new OutflowConfiguration( 1 ); // 1 token
        	of.setActionItems("UsernameToken");
        	of.setUser(Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+AxisClientConfigurator.AXIS_USERNAME));
            of.setPasswordCallbackClass("pl.compan.docusafe.util.axis.PWCBHandler");
        	of.setPasswordType("PasswordText");
            serviceClient.getOptions().setProperty(WSSHandlerConstants.OUTFLOW_SECURITY, of.getProperty());
            Policy policy = loadPolicy(Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+AXIS_POLICY));
            options.setProperty(RampartMessageData.KEY_RAMPART_POLICY,policy);
            options.setUserName(Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+AxisClientConfigurator.AXIS_USERNAME));
            options.setPassword(Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+AxisClientConfigurator.AXIS_PASSWORD));
        }
        
        serviceClient.engageModule("rampart");
    }
    
    /**
     * Zwraca domy�lne has�o z adds�w je�li przekazane jest nullem
     * @return
     */
    private String getCertificatePassword()
    {
    	if(StringUtils.isEmpty(password))
    		return Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+CERT_X509_PASSWORD);
    	else
    		return password;	
    }
    
    /**
     * Ladowanie polisy Ramparta
     * @param xmlPath
     * @return
     * @throws Exception
     */
    private Policy loadPolicy(String xmlPath) throws Exception {
        StAXOMBuilder builder = new StAXOMBuilder(xmlPath);
        return PolicyEngine.getPolicy(builder.getDocumentElement());
    }

    protected boolean isEmpty(String str)
    {
        if (str==null)
            return true;
        if (str.trim().length()==0)
            return true;
        return false;
    }
    /**
     * Zapisuje certyfikat w /WEB-INF/classes - certyfikat jest pobierany z parametrow konfiguracyjnych
     * @param env
     * @return - nazwa certyfiktatu lub null jesli go nie ma
     * @throws java.lang.Exception
     */ 
    protected String dumpCertificate() throws Exception
    {
    	/*String certFile = getCertificateName();
    	java.io.File tmp = new java.io.File(Docusafe.getHome() + "/"+certFile);
	    java.io.InputStream is = new FileInputStream(tmp);
        if (is==null)
            return null;
        String path = Docusafe.getServletContext().getRealPath("WEB-INF/classes");
        System.out.println(path);
        log.trace("certyfikay {}",path);
        FileOutputStream fo = new FileOutputStream(path,false);
        org.apache.commons.io.IOUtils.copy(is, fo);
        return certFile;*/
    	return getCertificateName();
    }

    /**
     * Metoda wyszukuje nazwe (alias) klucza w certyfikacie
     * @param env
     * @param certFileName
     * @param keyPass
     * @return
     * @throws Exception
     */
    protected String findKeyAliasName(String certFileName,String keyPass) throws Exception
    {
    	if(CERTIFICATE_FILE == null)
    		return "alias";
        String path =  Docusafe.getHome()+"//"+getCertificateName(); // robi cos takiego: D:\_projekty\nfos\HOME//umzyrardow.pfx
        FileInputStream fis = null;
        try
        {
        	fis = new FileInputStream(path);
        }
        catch (Exception e) 
        {
			return null;
		}
        KeyStore keystore = null;
        if(getCertificateName().contains(".pfx"))
        	keystore = KeyStore.getInstance("PKCS12","BC");
        else if(getCertificateName().contains(".jks"))
        	keystore = KeyStore.getInstance("JKS");
        keystore.load(fis,keyPass != null ? keyPass.toCharArray(): null);
        java.util.Enumeration<String> aliases = keystore.aliases();
        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();
            log.trace("znaleziono alias={}",alias);
            return alias;
        }
        log.error("nie znaleziono klucza prywatnego");
        return "alias";
    }
    /**
     * Metoda przygotowuje wlasnosc dla autoryzacji certyfikatem
     * @throws java.lang.Exception
     */
    protected synchronized void prepareClientProperties(String pass,String alias) throws Exception
    {
       Properties clientProperties = new Properties();
        clientProperties.setProperty("org.apache.ws.security.crypto.provider", "org.apache.ws.security.components.crypto.Merlin");
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.provider", "BC");
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.type", "pkcs12");
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", pass);
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.alias", alias);
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.alias.password", pass);
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.file", 
        		Docusafe.getHome()+"//"+getCertificateName());
        String path =  Docusafe.getServletContext().getRealPath("WEB-INF/classes/"+CLIENT_PROPERTIES);
        FileOutputStream fo = new FileOutputStream(path,false);
        clientProperties.store(fo, "plik wygenerowany automatycznie");
        fo.close();
        String path2 =  Docusafe.getServletContext().getRealPath("WEB-INF/classes/"+Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+CERTIFICATE_FILE));
        File from = new File(Docusafe.getHome()+"//"+Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+CERTIFICATE_FILE));
        File to = new File(path2);
        FileUtils.copyFileToFile(from, to);
        /*File from = new File(Docusafe.getHome()+"//"+getCertificateName());
        File to = new File(Docusafe.getServletContext().getRealPath("WEB-INF/classes/"+CERTIFICATE_FILE));
        FileUtils.copyFileToFile(from, to);
        */
       
    }
    
    protected synchronized String prepareClientProperties(String pass,String alias, boolean createUniqFileName) throws Exception
    {
    	String certName = getCertificateName();
    	String fileName = null;
    	
    	if(createUniqFileName){
    		String hashedCertName = MD5(certName);
    		fileName = "client_"+hashedCertName+".properties";
    	}else{
    		fileName = CLIENT_PROPERTIES;
    	}
    	
    	Properties clientProperties = new Properties();
        clientProperties.setProperty("org.apache.ws.security.crypto.provider", "org.apache.ws.security.components.crypto.Merlin");
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.provider", "BC");
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.type", "pkcs12");
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.password", pass);
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.keystore.alias", alias);
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.alias.password", pass);
        clientProperties.setProperty("org.apache.ws.security.crypto.merlin.file", 
        		Docusafe.getHome()+"//"+certName);
        
        String path =  Docusafe.getServletContext().getRealPath("WEB-INF/classes/"+fileName);
        FileOutputStream fo = new FileOutputStream(path,false);
        clientProperties.store(fo, "plik wygenerowany automatycznie");
        fo.close();
        
        return fileName;
    }

    protected void doHttpsInit() throws Exception
    {
        org.apache.commons.httpclient.protocol.Protocol myHTTPS = new  org.apache.commons.httpclient.protocol.Protocol( "https", (ProtocolSocketFactory) getSSLProtocolFactory(), 9443 );
        org.apache.commons.httpclient.protocol.Protocol.registerProtocol( "https", myHTTPS );
        doHttpsInit = false;
    }

    protected SecureProtocolSocketFactory getSSLProtocolFactory() throws Exception
    {
        return new EasySSLProtocolSocketFactory();
    }
    
    private static ConfigurationContext axisContext = null;
    
    /**
     * Pobiera kontekst wykorzystujacy ramparta
     * @return
     * @throws java.lang.Exception
     */
    public synchronized ConfigurationContext getAxisConfigurationContext() throws Exception
    {
        if (axisContext==null)
        {
            InputStream is = null;
            FileOutputStream fo = null;
            try
            {
                axisContext = ConfigurationContextFactory.createConfigurationContextFromFileSystem(Docusafe.getHome().getAbsolutePath()+"/axis2Context");
                log.debug("zainicjowano kontekst axisa");
            }
            finally
            {
                IOUtils.closeQuietly(is);
                IOUtils.closeQuietly(fo);
            }
        }
        return axisContext;
    }
    
    /**
     * Jesli certificateName jest null zwraca domyslny z adds�w. 
     * Mozna ustawi� przy tworzenia klasy nazwe certyfiaktu kt�ra bedzie szukana w HOME. 
     * Mo�liwo�� uzywania r�nych certyfikat�w
     * @return
     */
    public String getCertificateName()
    {
    	if(certificateName == null || certificateName.isEmpty())
    		return Docusafe.getAdditionProperty(makeAdditionPropertyPrefix()+CERTIFICATE_FILE);
    	else
    		return certificateName;
    }
    
    public String MD5(String md5) {
    	   try {
    	        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
    	        byte[] array = md.digest(md5.getBytes());
    	        StringBuffer sb = new StringBuffer();
    	        for (int i = 0; i < array.length; ++i) {
    	          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
    	       }
    	        return sb.toString();
    	    } catch (java.security.NoSuchAlgorithmException e) {
    	    }
    	    return null;
    	}
}
