package pl.compan.docusafe.util.axis.simple;


import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryFacade;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.core.dockinds.field.DataBaseEnumField;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.Person;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.QueryForm;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.simple.InvoiceServiceSQLExceptionException;
import pl.compan.docusafe.ws.simple.InvoiceServiceStub;
import pl.compan.docusafe.ws.simple.InvoiceServiceStub.PutInvoice;
import pl.compan.docusafe.ws.simple.InvoiceServiceStub.PutInvoiceResponse;
import pl.compan.docusafe.ws.simple.ProductServiceStub;
import pl.compan.docusafe.ws.simple.SimpleDictionaryServiceStub;
import pl.compan.docusafe.ws.simple.SimpleDictionaryServiceStub.DictionaryItem;
import pl.compan.docusafe.ws.simple.SimpleDictionaryServiceStub.GetDictionaryByName;
import pl.compan.docusafe.ws.simple.SimpleDictionaryServiceStub.GetDictionaryByNameResponse;
import pl.compan.docusafe.ws.simple.SupplierServiceStub;
import pl.compan.docusafe.ws.simple.SupplierServiceStub.GetSuppliers;
import pl.compan.docusafe.ws.simple.SupplierServiceStub.GetSuppliersResponse;
import pl.compan.docusafe.ws.simple.SupplierServiceStub.PutSupplier;
import pl.compan.docusafe.ws.simple.SupplierServiceStub.PutSupplierResponse;
import pl.compan.docusafe.ws.simple.SupplierServiceStub.Supplier;

/**
 * Klasa AssecoImportManager.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class SimpleManager 
{
    private final static Logger LOG = LoggerFactory.getLogger(SimpleManager.class);
	private SimpleDictionaryServiceStub serviceDic;
	private SupplierServiceStub serviceSupp;
	private InvoiceServiceStub serviceInv;
	private ProductServiceStub serviceProd;
	
	public SimpleManager() throws Exception 
	{
		super();
		AxisClientConfigurator conf = new AxisClientConfigurator();
		this.serviceDic = new SimpleDictionaryServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(serviceDic, "/services/SimpleDictionaryService");  
		
		this.serviceSupp = new  SupplierServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(serviceSupp, "/services/SupplierService");  
		
		this.serviceInv = new InvoiceServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(serviceInv, "/services/InvoiceService");  
		
		this.serviceProd = new ProductServiceStub(conf.getAxisConfigurationContext());
		conf.setUpHttpParameters(serviceProd, "/services/ProductService");  

	}
	
	public InvoiceServiceStub.MethodOutput exportinvoice(String xml) throws RemoteException, InvoiceServiceSQLExceptionException
	{
		PutInvoice putInvoice = new PutInvoice();
		InvoiceServiceStub.Invoice inv = new InvoiceServiceStub.Invoice ();
		inv.setXML(xml);
		putInvoice.setInvoice(inv);
		PutInvoiceResponse resp = serviceInv.putInvoice(putInvoice);
		return resp.get_return();
	}
	
	public SupplierServiceStub.MethodOutput setSuppliers(Person person) throws  EdmException
	{
		try
		{
			PutSupplier putSupplier = new PutSupplier();
			Supplier sup = new Supplier();
			sup.setDostawcaIdn(""+person.getId());
			sup.setKodpoczt(person.getZip());
			sup.setMiasto(person.getLocation());
			sup.setNazwa(person.getOrganization());
			sup.setNip(person.getNip());
			sup.setPoczta(person.getLocation());
			sup.setUlica(person.getStreet());
			putSupplier.setSupplier(sup);
			PutSupplierResponse resp = serviceSupp.putSupplier(putSupplier);
			person.setWparam(Long.parseLong(""+resp.get_return().getId()));
			person.setLparam(""+person.getId());
			return resp.get_return();
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new EdmException(e);
		}
	}
	
	public void getSuppliers() throws Exception
	{
		try
		{
			GetSuppliers getSupp = new GetSuppliers();
			GetSuppliersResponse suppResp = serviceSupp.getSuppliers(getSupp);
			Supplier[] supps = suppResp.get_return();
			for (Supplier supplier : supps) 
			{
				QueryForm form = new QueryForm(0, 10);
				form.addProperty("lparam", supplier.getDostawcaIdn());
				 SearchResults<? extends Person> results = Person.search(form);
				 if(results.count() > 0)
				 {
					 Person person = results.next();
					 setPersonAttr(person,supplier);
					 person.update();
				 }
				 else
				 {
					 Person person = new Person();
					 setPersonAttr(person,supplier);
					 person.create();
				 }				
			}
		
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	private void setPersonAttr(Person person, Supplier sup) throws EdmException
	{
		person.setWparam(sup.getDostawcaId());
		person.setLparam(sup.getDostawcaIdn());
		person.setLocation(sup.getMiasto());
		person.setNip(sup.getNip());
		person.setStreet(sup.getUlica()+" "+sup.getNrdomu());
		person.setZip(sup.getKodpoczt());
		person.setOrganization(sup.getNazwa());
	}
	
	public String getInsertSQLDic(String name,DictionaryItem [] dicsR)
	{
		StringBuilder sb = new StringBuilder("insert into "+ name +" (ID,");
		StringBuilder values = new StringBuilder("values(?,");
		try
		{			

				SimpleDictionaryServiceStub.Attribute[] asd =dicsR[0].getAttributes();
				for (SimpleDictionaryServiceStub.Attribute attribute : asd) 
				{
					if(attribute != null)
					{
						sb.append(attribute.getName()+",");
						values.append("?,");
					}
				}
			
			sb.delete(sb.length()-1, sb.length());
			sb.append(")");
			values.delete(values.length()-1, values.length());
			values.append(")");
		
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return sb.toString()+values.toString();
	}
	
	public GetDictionaryByNameResponse getDictionaryByNameResponse(String name) throws EdmException
	{
		try
		{
			GetDictionaryByName getDic = new GetDictionaryByName();
			getDic.setCount(1000);
			getDic.setDictionaryName(name);
			return serviceDic.getDictionaryByName(getDic);
		}
		catch (Exception e) {
			throw new EdmException(e);
		}
	}
	
	public String getCreateSQLDic(String name,DictionaryItem [] dicsR) throws EdmException
	{
		StringBuilder sb = new StringBuilder("create table "+ name +" (\n");
		try
		{			
				sb.append("ID numeric(18,0),\n");
			
				SimpleDictionaryServiceStub.Attribute[] asd =dicsR[0].getAttributes();
				for (SimpleDictionaryServiceStub.Attribute attribute : asd) 
				{
					if(attribute != null)
						sb.append(attribute.getName()+ " varchar(200),\n");
				}
			sb.delete(sb.length()-2, sb.length());
			sb.append(");");
		
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new EdmException(e);
		}
		return sb.toString();
	}
	
	public static void exportFromDoc(Long docId) throws Exception
	{
		//DSApi.context().begin();
		SimpleManager mgr = new SimpleManager();
		//SimpleManager.exportFromDoc(docId);
		OfficeDocument doc = OfficeDocument.find(docId);
		FieldsManager fm = doc.getFieldsManager();
		Person contractor = doc.getSender();
		if (contractor.getLparam() == null || contractor.getLparam().equalsIgnoreCase("null"))
		{
			SupplierServiceStub.MethodOutput respCon = mgr.setSuppliers(contractor);
			if (respCon.getError() > 0)
				throw new EdmException(respCon.getErrorDescription());
			contractor.update();
		}

		DocumentFactory factory = DocumentFactory.getInstance();
		org.dom4j.Document document = factory.createDocument();
		org.dom4j.Element eksport = document.addElement("DZK");
		eksport.addAttribute("typdok_idn", fm.getEnumItem("RODZAJ_FAKTURY").getCn());
		eksport.addAttribute("dok_idm", fm.getValue("NUMER_FAKTURY") + "");
		eksport.addAttribute("datdok", DateUtils.formatSimpleExportNullableDateTime((Date) fm.getKey("DATA_WYSTAWIENIA")));
		eksport.addAttribute("datodb", DateUtils.formatSimpleExportNullableDateTime((Date) fm.getKey("DATA_ODBIORU")));
		eksport.addAttribute("datprzyj", DateUtils.formatSimpleExportNullableDateTime(doc.getCtime()));
		eksport.addAttribute("dokobcy", doc.getId() + "");
		eksport.addAttribute("dostawca_idn", contractor.getLparam());
		eksport.addAttribute("warplat_idn", fm.getEnumItem("WARPLAT").getCn());
		eksport.addAttribute("komorka_idn", fm.getEnumItemCn("KOMORKA"));
		eksport.addAttribute("pracownik_idn", fm.getEnumItemCn("PRACOWNIK"));
		eksport.addAttribute("zlecprod_idm", fm.getEnumItemCn("ZLECPROD"));
		
		eksport.addAttribute("uwagi", fm.getValue("UWAGI") + "");
		
		
		Object advances = fm.getKey("POZYCJE");
		Map<String, Object> advancesValues = new HashMap<String, Object>();
		String wytwor_idm, jm_idn, vatstaw_ids, zlecprod_idm, pracownik_idn, komorka_idn;
		for (Long advnaceId : (List<Long>) advances)
		{
			org.dom4j.Element poz = eksport.addElement("DZK_POZ");
			wytwor_idm = null; jm_idn = null; vatstaw_ids = null; zlecprod_idm = null; pracownik_idn = null; komorka_idn = null;
			advancesValues = DwrDictionaryFacade.dictionarieObjects.get("POZYCJE").getValues(advnaceId.toString());
			wytwor_idm = DataBaseEnumField.getEnumItemForTable("ERP_v_br_wytwor_view", Integer.parseInt(((EnumValues) advancesValues.get("POZYCJE_WYTWOR")).getSelectedOptions().get(0))).getCn();
			poz.addAttribute("wytwor_idm", wytwor_idm);
			poz.addAttribute("ilosc", advancesValues.get("POZYCJE_ILOSC") + "");
			poz.addAttribute("cena", advancesValues.get("POZYCJE_CENA") + "");
			jm_idn = DataBaseEnumField.getEnumItemForTable("ERP_jm_view", Integer.parseInt(((EnumValues) advancesValues.get("POZYCJE_JM")).getSelectedOptions().get(0))).getCn();
			poz.addAttribute("jm_idn", jm_idn);
			vatstaw_ids = DataBaseEnumField.getEnumItemForTable("ERP_vatstaw_view", Integer.parseInt(((EnumValues) advancesValues.get("POZYCJE_VATSTAW")).getSelectedOptions().get(0))).getCn();
			poz.addAttribute("vatstaw_ids", vatstaw_ids);
			poz.addAttribute("kwota_brutto", advancesValues.get("POZYCJE_KWOTABRUTTO") + "");
			try {
				zlecprod_idm = DataBaseEnumField.getEnumItemForTable("ERP_pp_zlecprod_view", Integer.parseInt(((EnumValues) advancesValues.get("POZYCJE_ZLECPROD")).getSelectedOptions().get(0))).getCn();
				LOG.warn("zlecprod_idm: {}", zlecprod_idm);
				poz.addAttribute("zlecprod_idm", zlecprod_idm);
				pracownik_idn = DataBaseEnumField.getEnumItemForTable("ERP_pracownik_view", Integer.parseInt(((EnumValues) advancesValues.get("POZYCJE_PRACOWNIK")).getSelectedOptions().get(0))).getCn();
				LOG.warn("pracownik_idn: {}", pracownik_idn);
				poz.addAttribute("pracownik_idn", pracownik_idn);
				komorka_idn = DataBaseEnumField.getEnumItemForTable("ERP_komorka_view", Integer.parseInt(((EnumValues) advancesValues.get("POZYCJE_KOMORKA")).getSelectedOptions().get(0))).getCn();
				LOG.warn("komorka_idn: {}", komorka_idn);
				poz.addAttribute("komorka_idn", komorka_idn);
			}
			catch(Exception e)
			{
				LOG.warn("Exception: zlecprod_idm: {}", zlecprod_idm);
				LOG.warn("Exception: pracownik_idn: {}", pracownik_idn);
				LOG.warn("Exception: komorka_idn: {}", komorka_idn);
				poz.addAttribute("zlecprod_idm", zlecprod_idm);
				poz.addAttribute("pracownik_idn", pracownik_idn);
				poz.addAttribute("komorka_idn", komorka_idn);
			}
			poz.addAttribute("uwagi", advancesValues.get("POZYCJE_UWAGI") + "");
		}
		String xml = document.asXML();
		String dsXml = xml.substring(39);
		System.out.println(dsXml);
		LOG.error(dsXml);
		InvoiceServiceStub.MethodOutput resp = mgr.exportinvoice(dsXml);
//		DSApi.context().commit();
		if (resp.getError() > 0)
			throw new EdmException(resp.getErrorDescription());
	}
}