package pl.compan.docusafe.util.querybuilder;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.querybuilder.select.ColumnAttribute;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: TableAlias.java,v 1.4 2006/02/20 15:42:31 lk Exp $
 */
public class TableAlias
{
    private String table;
    private String alias;
    private boolean nolock;

    public TableAlias(String table, String alias, boolean nolock)
    {
        this.table = table;
        this.alias = alias;
        this.nolock = nolock;
    }

    /**
     * Zwraca obiekt, kt�ry mo�e by� u�yty w wyra�eniach dodawanych
     * do {@link pl.compan.docusafe.util.querybuilder.select.WhereClause}.
     * @param name Nazwa kolumny w tabeli.
     */
    public Attribute attribute(String name)
    {
        return new ColumnAttribute(this, name);
    }

    /**
     * Zwraca nazw� kolumny poprzedzon� nazw� tabeli, kt�rej mo�na
     * u�y� w SELECT w samodzielnie tworzonych wyra�eniach SQL.
     * @see pl.compan.docusafe.util.querybuilder.select.SelectClause#addSql(java.lang.String)
     * @param name Nazwa kolumny.
     */
    public String column(String name)
    {
        return alias+"."+name;
    }

    public String getTable()
    {
        return table;
    }

    public String getAlias()
    {
        return alias;
    }

    public boolean isNolock()
    {
        return nolock;
    }

    public String toSqlString(Query query)
    {
        String nolock = DSApi.isSqlServer() ? " WITH (NOLOCK)" : "";
        return table+" "+alias + nolock;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof TableAlias)) return false;

        final TableAlias tableAlias = (TableAlias) o;

        if (!alias.equals(tableAlias.alias)) return false;
        if (!table.equals(tableAlias.table)) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = table.hashCode();
        result = 29 * result + alias.hashCode();
        return result;
    }

    public String toString()
    {
        return "TableAlias["+table+", "+alias+"]";
    }
}
