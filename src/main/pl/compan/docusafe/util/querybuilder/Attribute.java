package pl.compan.docusafe.util.querybuilder;

/**
 * Reprezentuje kolumn� w tabeli wymienionej w FROM lub
 * alias kolumny w SELECT (niekoniecznie odpowiadaj�cy
 * kolumnie tabeli, mo�e to by� wyra�enie).
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Attribute.java,v 1.4 2009/06/01 14:28:29 mariuszk Exp $
 */
public abstract class Attribute
{
    /**
     * Zwraca nazw� atrybutu, kt�rej mo�na u�y� w WHERE.
     */
    public abstract String toWhereSqlString(Query query);
    /**
     * Zwraca nazw� atrybutu, kt�rej mo�na u�y� w ORDER BY
     */
    public abstract String toOrderSqlString(Query query);
    /**
     * Zwraca nazw� atrybutu, kt�rej mo�na u�y� w GROUP BY
     * */
    public abstract String toGroupSqlString(Query query);

    public abstract boolean equals(Object obj);

    public abstract int hashCode();
}
