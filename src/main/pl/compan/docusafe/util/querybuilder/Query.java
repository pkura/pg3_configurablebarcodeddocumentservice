package pl.compan.docusafe.util.querybuilder;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.axis.components.logger.LogFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Query.java,v 1.9 2009/07/14 14:21:28 mariuszk Exp $
 */
public abstract class Query
{
	static final Logger log = LoggerFactory.getLogger(Query.class);
	
    public abstract String getAlias(TableAlias tableAlias, String columnName);
    
    protected void prepareStatement(PreparedStatement ps, List parameters, List types)
        throws SQLException
    {
    	log.trace("prepareStatement");
        if (ps == null)
            throw new NullPointerException("ps");
        if (parameters == null)
            throw new NullPointerException("parameters");
        if (types == null)
            throw new NullPointerException("types");

        if (parameters.size() != types.size())
            throw new IllegalArgumentException("Rozmiary tablic parameters i types musz� by� r�wne");       
        
        for (int i=0; i < parameters.size(); i++)
        {
        	
            int index = i+1;
            Object value = parameters.get(i);
           // System.out.println("param {}={}"+value);
            log.debug("param {}={}",index,value);
            Class type = (Class) types.get(i);
            
            
            // dlaczego nie ps.setObject????
            if (type == BigDecimal.class)
            {
                ps.setBigDecimal(index, (BigDecimal) value);
            }
            else if (type == Boolean.class)
            {
                ps.setBoolean(index, ((Boolean) value).booleanValue());
            }
            else if (type == Byte.class)
            {
                ps.setByte(index, ((Byte) value).byteValue());
            }
            else if (type == byte[].class)
            {
                ps.setBytes(index, (byte[]) value);
            }
            else if (type == java.sql.Date.class)
            {
                ps.setDate(index, (java.sql.Date) value);
            }
            else if (type == java.util.Date.class)
            {
                ps.setDate(index, new java.sql.Date(((java.util.Date) value).getTime()));
            }
            else if (type == Double.class)
            {
                ps.setDouble(index, ((Double) value).doubleValue());
            }
            else if (type == Float.class)
            {
                ps.setFloat(index, ((Float) value).floatValue());
            }
            else if (type == Integer.class)
            {
                ps.setInt(index, ((Integer) value).intValue());
            }
            else if (type == Long.class)
            {
                ps.setLong(index, ((Long) value).longValue());
            }
            else if (type == Short.class)
            {
            	
                ps.setShort(index, ((Short) value).shortValue());
            }
            else if (type == String.class)
            {
            	String val = value != null ? (String) value : "";
                ps.setString(index, val);
            }
            else if (type == java.sql.Time.class)
            {
                ps.setTime(index, (java.sql.Time) value);
            }
            else if (type == java.sql.Timestamp.class)
            {
                ps.setTimestamp(index, (java.sql.Timestamp) value);
            }
            else if (type == URL.class)
            {
                ps.setURL(index, (URL) value);
            }
            else
            {
                throw new IllegalArgumentException("Nieznany typ: "+type);
            }
        }
    }
}
