package pl.compan.docusafe.util.querybuilder.expression;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LtExpression.java,v 1.3 2006/02/20 15:42:32 lk Exp $
 */
class LtExpression extends Expression
{
    private Attribute attribute;
    private Object value;
    private boolean orEqual;

    public LtExpression(Attribute attribute, Object value, boolean orEqual)
    {
        if (value == null)
            throw new NullPointerException("value");

        this.attribute = attribute;
        this.value = value;
        this.orEqual = orEqual;
    }

    public String toSqlString(Query query, List parameters, List types)
    {
        parameters.add(value);
        types.add(value.getClass());
        return attribute.toOrderSqlString(query) + (orEqual ? " <= ?" : " < ?");
    }
}
