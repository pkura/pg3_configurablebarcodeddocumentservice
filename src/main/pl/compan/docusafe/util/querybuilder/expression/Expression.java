package pl.compan.docusafe.util.querybuilder.expression;

import java.io.File;
import java.util.List;

import net.sf.jni4net.Bridge;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;
import dsnet.DSParser;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Expression.java,v 1.8 2010/01/06 12:19:14 tomekl Exp $
 */
public abstract class Expression
{
	private static final Logger log = LoggerFactory.getLogger(Expression.class);
	
	static 
	{
    	try
    	{
//	    	Bridge.init();
//			Bridge.LoadAndRegisterAssemblyFrom(new java.io.File("DSNet.j4n.dll"));
    	}
    	catch (Exception e) {
			log.error("B��d �adownia DSNet.j4n.dll"+e.getMessage(), e);
		}
	}
    public static Expression eq(Attribute attribute, Object value)
    {
        return new EqExpression(attribute, value, false);
    }

    public static Expression ne(Attribute attribute, Object value)
    {
        return new EqExpression(attribute, value, true);
    }

    public static Expression gt(Attribute attribute, Object value)
    {
        return new GtExpression(attribute, value, false);
    }

    public static Expression ge(Attribute attribute, Object value)
    {
        return new GtExpression(attribute, value, true);
    }

    public static Expression lt(Attribute attribute, Object value)
    {
        return new LtExpression(attribute, value, false);
    }

    public static Expression le(Attribute attribute, Object value)
    {
        return new LtExpression(attribute, value, true);
    }

    public static Expression eqAttribute(Attribute attributeA, Attribute attributeB)
    {
        return new EqAttributeExpression(attributeA, attributeB, false);
    }

    public static Expression neAttribute(Attribute attributeA, Attribute attributeB)
    {
        return new EqAttributeExpression(attributeA, attributeB, true);
    }

    public static Expression like(Attribute attribute, String value)
    {
        return new LikeExpression(attribute, value);
    }
    
    public static Expression like(Attribute attribute, String value, boolean ignoreCase)
    {
        return new LikeExpression(attribute, value , ignoreCase);
    }

    public static Expression likeLeftRight(Attribute attribute, String value, boolean ignoreCase)
    {
    	StringBuffer bf = new StringBuffer();
    	bf.append("%");
    	bf.append(value);
    	bf.append("%");
    	return new LikeExpression(attribute,  bf.toString(), ignoreCase);
    }
    
    public static Expression in(Attribute attribute, Object[] values)
    {
        return new InExpression(attribute, values);
    }
    
    public static Expression notIn(Attribute attribute, Object[] values)
    {
        return new NotInExpression(attribute, values);
    }

    public static Junction conjunction()
    {
        return new Conjunction();
    }

    public static Junction disjunction()
    {
        return new Disjunction();
    }

    public static Expression eq_string(String columnParam, String value) {
    	return new EqStringExpression(columnParam, value);
    }

    public abstract String toSqlString(Query query, List parameters, List types);

    public static Expression oracleContains(final String column, final String textQuery) {
        return new Expression() {
            @Override
            public String toSqlString(Query query, List parameters, List types) {
                parameters.add(textQuery);
                types.add(String.class);
                return " CONTAINS (" + column + ",?) > 0 ";
            }
        };
    }
    
    public static Expression mssqlContains(final String column, final String textQuery) {
        return new Expression() {
            @Override
            public String toSqlString(Query query, List parameters, List types) 
            {
            	String textQueryReplace = textQuery.replaceAll(",", "");
            	System.out.println("textQueryReplace"+textQueryReplace);
            	textQueryReplace = textQueryReplace.replaceAll("\\.", "");
            	System.out.println("textQueryReplace stop"+textQueryReplace);
                parameters.add(convertQueryToFTS(textQueryReplace));
                types.add(String.class);
                return " CONTAINS (" + column + ",?)";
            }
        };
    }
    
    public static String convertQueryToFTS(String query)
    {
    	try
    	{
            final java.security.CodeSource source = Bridge.class.getProtectionDomain().getCodeSource();
            String file = source.getLocation().getPath();
            String filen = file.replace("jni4net.j-0.8.6.0.jar", "");
	    	Bridge.init();
	    	System.out.println("mariuszaaaaaaaaaaaaa");
			Bridge.LoadAndRegisterAssemblyFrom(new java.io.File(filen+"DSNet.j4n.dll"));
			DSParser parser = new DSParser();
			String ftsString =  parser.toFT(query);
			log.error("Przerobi� "+ query +" na -> "+ftsString);
			System.out.println("Przerobi� "+ query +" na -> "+ftsString);
			return ftsString;
    	}
    	catch (Exception e) {
			log.error(e.getMessage(), e);
			return query;
		}
    }
}
