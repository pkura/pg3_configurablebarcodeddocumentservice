package pl.compan.docusafe.util.querybuilder.expression;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.util.querybuilder.Query;

public class EqStringExpression extends Expression {
	String columnParam;
	String value;
	
    public EqStringExpression(String columnParam, String value) {
        this.columnParam = columnParam;
        this.value = value;
    }

	@Override
	public String toSqlString(Query query, List parameters, List types) {
        if (StringUtils.isEmpty(columnParam) || StringUtils.isEmpty(value)) {
            return "";
        } else {
            parameters.add(value);
            types.add(value.getClass());
            return  columnParam + " = ?";
        }
	}	
}
