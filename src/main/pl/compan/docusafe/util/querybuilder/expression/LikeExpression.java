package pl.compan.docusafe.util.querybuilder.expression;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LikeExpression.java,v 1.7 2009/08/14 12:24:16 pecet1 Exp $
 */
class LikeExpression extends Expression
{
    private static final Logger log = LoggerFactory.getLogger(LikeExpression.class);
    private Attribute attribute;
    private String value;
    private boolean ignoreCase;

    public LikeExpression(Attribute attribute, String value)
    {
		this(attribute, value, false);
    }
    
    public LikeExpression(Attribute attribute, String value,boolean ignoreCase)
    {
        if (value == null)
            throw new NullPointerException("value");
        this.attribute = attribute;
        this.value = value;
        this.ignoreCase = ignoreCase;
//		if(ignoreCase){
//			log.error("UPPER! - " + attribute + " , " + value, new Throwable());
//		}
    }

    public String toSqlString(Query query, List parameters, List types)
    {
        if(ignoreCase)
        {
            parameters.add(((String)value).toUpperCase());
            types.add(value.getClass());
            return "upper("+attribute.toOrderSqlString(query) + ") like ?";
        }
        else
        {
        	//System.out.println(attribute.toOrderSqlString(query) + " like ?");
            parameters.add(value);
            types.add(value.getClass());
            return attribute.toOrderSqlString(query) + " like ?";
        }
    }
}
