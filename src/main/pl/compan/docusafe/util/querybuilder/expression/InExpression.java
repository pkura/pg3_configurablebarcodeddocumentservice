package pl.compan.docusafe.util.querybuilder.expression;

import java.util.List;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

/* User: Administrator, Date: 2005-05-13 13:04:55 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: InExpression.java,v 1.7 2008/11/10 12:13:06 mariuszk Exp $
 */
public class InExpression extends Expression
{
    private Attribute attribute;
    private Object[] values;

    public InExpression(Attribute attribute, Object[] values)
    {
        this.attribute = attribute;
        this.values = values;
    }

    public String toSqlString(Query query, List parameters, List types)
    {
        if (values == null || values.length == 0)
        {
            return "";
        }
        else
        {
           StringBuilder sql = new StringBuilder(attribute.toOrderSqlString(query)+" IN (");

            for (int i=0; i < values.length; i++)
            {
                if (values[i] != null)
                {
                    parameters.add(values[i]);
                    types.add(values[i].getClass());
                    sql.append("?");
                    if(i < values.length-1)
                    {
                    	sql.append(", ");
                    }
                }
            }
            sql.append(")");
            
            return sql.toString();
        }
    }
}
