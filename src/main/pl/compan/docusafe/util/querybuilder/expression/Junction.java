package pl.compan.docusafe.util.querybuilder.expression;

import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.Query;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Junction.java,v 1.7 2008/04/18 15:55:56 mariuszk Exp $
 */
public abstract class Junction extends Expression
{
    private List expressions = new LinkedList();

    public Junction add(Expression expression)
    {
        expressions.add(expression);
        return this;
    }

    public boolean isEmpty()
    {
        return expressions.isEmpty();
    }

    protected String getJunction(String operator, Query query, List parameters, List types)
    {
        StringBuilder sql = new StringBuilder("(");

		for (int i=0; i < expressions.size(); i++)
        {
            sql.append("("+((Expression) expressions.get(i)).toSqlString(query, parameters, types)+")");
            if (i < expressions.size()-1)
                sql.append(" "+operator+" ");
        }

        sql.append(")");

        return sql.toString();
    }
}
