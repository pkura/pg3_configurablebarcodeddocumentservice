package pl.compan.docusafe.util.querybuilder.expression;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EqAttributeExpression.java,v 1.3 2006/02/20 15:42:32 lk Exp $
 */
class EqAttributeExpression extends Expression
{
    private Attribute attributeA;
    private Attribute attributeB;
    private boolean notEqual;

    public EqAttributeExpression(Attribute attributeA, Attribute attributeB, boolean notEqual)
    {
        if (attributeA == null)
            throw new NullPointerException("attributeA");
        if (attributeB == null)
            throw new NullPointerException("attributeB");

        this.attributeA = attributeA;
        this.attributeB = attributeB;
        this.notEqual = notEqual;
    }

    public String toSqlString(Query query, List parameters, List types)
    {
        return attributeA.toOrderSqlString(query) + (notEqual ? " != " : " = ") +
            attributeB.toOrderSqlString(query);
    }
}
