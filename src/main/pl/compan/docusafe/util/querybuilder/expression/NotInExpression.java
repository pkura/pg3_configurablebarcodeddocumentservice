package pl.compan.docusafe.util.querybuilder.expression;

import java.util.List;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

public class NotInExpression extends Expression
{
	private Attribute attribute;
    private Object[] values;

    public NotInExpression(Attribute attribute, Object[] values)
    {
        this.attribute = attribute;
        this.values = values;
    }

    public String toSqlString(Query query, List parameters, List types)
    {
        if (values == null || values.length == 0)
        {
            return "";
        }
        else
        {
           StringBuilder sql = new StringBuilder(attribute.toOrderSqlString(query)+" NOT IN (");

            for (int i=0; i < values.length; i++)
            {
                if (values[i] != null)
                {
                    parameters.add(values[i]);
                    types.add(values[i].getClass());
                    sql.append("?");
                    if(i < values.length-1)
                    {
                    	sql.append(", ");
                    }
                }
            }
            sql.append(")");
            
            return sql.toString();
        }
    }
}
