package pl.compan.docusafe.util.querybuilder.expression;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: EqExpression.java,v 1.5 2005/06/20 15:39:06 lk Exp $
 */
class EqExpression extends Expression
{
    private Attribute attribute;
    private Object value;
    private boolean notEqual;

    public EqExpression(Attribute attribute, Object value, boolean notEqual)
    {
        this.attribute = attribute;
        this.value = value;
        this.notEqual = notEqual;
    }

    public String toSqlString(Query query, List parameters, List types)
    {
        if (value == null)
        {
            return attribute.toOrderSqlString(query) + " IS "+(notEqual ? "NOT " : "") + "NULL";
        }
        else
        {
            parameters.add(value);
            types.add(value.getClass());
            return attribute.toOrderSqlString(query) + (notEqual ? " != ?" : " = ?");
        }
    }

    public String toString()
    {
        return getClass().getName()+"[attribute="+attribute+" value="+value+"]";
    }
}
