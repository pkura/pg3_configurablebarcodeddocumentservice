package pl.compan.docusafe.util.querybuilder.expression;

import pl.compan.docusafe.util.querybuilder.Query;

import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Conjunction.java,v 1.1 2004/07/15 14:00:42 lk Exp $
 */
class Conjunction extends Junction
{
    public String toSqlString(Query query, List parameters, List types)
    {
        return getJunction("AND", query, parameters, types);
    }
}
