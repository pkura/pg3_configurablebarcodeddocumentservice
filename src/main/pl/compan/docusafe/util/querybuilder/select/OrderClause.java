package pl.compan.docusafe.util.querybuilder.select;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OrderClause.java,v 1.4 2006/02/20 15:42:32 lk Exp $
 */
public class OrderClause
{
    public static final Boolean ASC = Boolean.TRUE;
    public static final Boolean DESC = Boolean.FALSE;

    private List columns = new ArrayList(5);

    public OrderClause add(Attribute attribute, Boolean direction)
    {
        // zabezpieczenie przed podaniem dwa razy tej samej kolumny
        for (Iterator iter=columns.iterator(); iter.hasNext(); )
        {
            OrderColumn oc = (OrderColumn) iter.next();
            if (oc.attribute.equals(attribute))
                return this;
        }

        columns.add(new OrderColumn(attribute, direction));
        return this;
    }

    public boolean isEmpty()
    {
        return columns.isEmpty();
    }

    public String toSqlString(Query query)
    {
        StringBuilder sql = new StringBuilder("ORDER BY ");

        for (int i=0; i < columns.size(); i++)
        {
            OrderColumn col = (OrderColumn) columns.get(i);
            // nie mo�na u�y� aliasu z SELECT, tylko nazw� kolumny z tabeli
            sql.append(col.attribute.toOrderSqlString(query));
            sql.append(col.direction == ASC ? " ASC" : " DESC");
            if (i < columns.size()-1)
                sql.append(", ");
        }

        return sql.toString();
    }

    private static class OrderColumn
    {
        private Attribute attribute;
        private Boolean direction;

        public OrderColumn(Attribute attribute, Boolean direction)
        {
            this.attribute = attribute;
            this.direction = direction;
        }
    }
}
