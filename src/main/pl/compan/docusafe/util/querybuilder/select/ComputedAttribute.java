package pl.compan.docusafe.util.querybuilder.select;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

/**
 * Gdzie ta klasa jest u�ywana? Czy toWhereSqlString mo�e zwraca� alias?
 *
 *
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ComputedAttribute.java,v 1.3 2009/06/01 14:54:41 mariuszk Exp $
 */
public class ComputedAttribute extends Attribute
{
    private SelectColumn selectColumn;
    private String expression;

    public ComputedAttribute(SelectColumn selectColumn, String expression)
    {
        this.selectColumn = selectColumn;
        this.expression = expression;
    }

    public String toWhereSqlString(Query query)
    {
        return selectColumn.getAlias();
    }

    public String toOrderSqlString(Query query)
    {
        return selectColumn.getAlias();
    }
    
	public String toGroupSqlString(Query query)
	{
		 return selectColumn.getAlias();
	}

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ComputedAttribute)) return false;

        final ComputedAttribute computedAttribute = (ComputedAttribute) o;

        if (expression != null ? !expression.equals(computedAttribute.expression) : computedAttribute.expression != null) return false;
        if (selectColumn != null ? !selectColumn.equals(computedAttribute.selectColumn) : computedAttribute.selectColumn != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (selectColumn != null ? selectColumn.hashCode() : 0);
        result = 29 * result + (expression != null ? expression.hashCode() : 0);
        return result;
    }
}
