package pl.compan.docusafe.util.querybuilder.select;

import pl.compan.docusafe.util.querybuilder.Query;
import pl.compan.docusafe.util.querybuilder.TableAlias;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Reprezentuje cz�� SELECT zapytania SQL.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SelectClause.java,v 1.6 2006/02/20 15:42:32 lk Exp $
 */
public class SelectClause
{
    private List selectColumns = new ArrayList(10);
    private int counter;
    private boolean distinct;

    public SelectClause()
    {
    }

    public SelectClause(boolean distinct)
    {
        this.distinct = distinct;
    }

    public SelectColumn add(TableAlias tableAlias, String columnName)
    {
        SelectColumn selectColumn = SelectColumn.tableColumn(tableAlias, columnName, createAlias(columnName), selectColumns.size()+1);
        selectColumns.add(selectColumn);
        return selectColumn;
    }

    public SelectColumn addSql(String sql)
    {
        SelectColumn selectColumn = SelectColumn.sql(sql, createAlias("expr"), selectColumns.size()+1);
        selectColumns.add(selectColumn);
        return selectColumn;
    }

    /**
     * Zwraca obiekt SelectColumn odpowiadaj�cy przekazanemu aliasowi
     * tabeli i jej kolumnie. Je�eli w SELECT dana kolumna tabeli wyst�puje
     * wi�cej ni� raz, zwracana jest jedna z nich, nie zawsze pierwsza.
     * @param tableAlias
     * @param columnName
     * @return SelectColumn lub null.
     */
    public SelectColumn getSelectColumn(TableAlias tableAlias, String columnName)
    {
        for (Iterator iter=selectColumns.iterator(); iter.hasNext(); )
        {
            SelectColumn selectColumn = (SelectColumn) iter.next();
            if (selectColumn.getTableAlias() == tableAlias &&
                selectColumn.getColumnName() != null &&
                selectColumn.getColumnName().equals(columnName))
                return selectColumn;
        }

        return null;
    }

    public String toSqlString(Query query)
    {
        // ma�ymi literami, poniewa� Hibernate por�wnuje pocz�tek zapytania
        // z ci�giem "select distinct" podczas wyszukiwania miejsca na dodanie
        // parametr�w LIMIT i OFFSET
        StringBuilder sql = new StringBuilder("select ");

        if (distinct)
            sql.append("distinct ");

        for (int i=0; i < selectColumns.size(); i++)
        {
            SelectColumn selectColumn = (SelectColumn) selectColumns.get(i);
            sql.append(selectColumn.toSqlString(query));
            if (i < selectColumns.size()-1)
                sql.append(", ");
        }

        return sql.toString();
    }

    private String createAlias(String name)
    {
        if (name.length() > 8) name = name.substring(0, 9);
        return name + "_" + (++counter);
    }
}
