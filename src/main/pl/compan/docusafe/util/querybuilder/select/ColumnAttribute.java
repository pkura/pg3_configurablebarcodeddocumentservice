package pl.compan.docusafe.util.querybuilder.select;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;
import pl.compan.docusafe.util.querybuilder.TableAlias;


/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: ColumnAttribute.java,v 1.4 2009/06/01 14:28:27 mariuszk Exp $
 */
public class ColumnAttribute extends Attribute
{
    private TableAlias tableAlias;
    private String columnName;

    public ColumnAttribute(TableAlias tableAlias, String columnName)
    {
        this.tableAlias = tableAlias;
        this.columnName = columnName;
    }

    /**
     * Zwraca nazw�, przy pomocy kt�rej mo�na odwo�ywa� si� do atrybutu
     * w cz�ci ORDER BY zapytania SQL. Zwracany jest alias tabeli i nazwa
     * kolumny, nigdy alias z SELECT.
     */
    public String toOrderSqlString(Query query)
    {
        // w ORDER nie mo�na u�y� aliasu z SELECT
        return tableAlias.getAlias() + "." + columnName;
    }

    /**
     * Zwraca nazw�, przy pomocy kt�rej mo�na odwo�ywa� si� do atrybutu
     * w cz�ci ORDER BY zapytania SQL. Zwracany jest alias tabeli i nazwa
     * kolumny, nigdy alias z SELECT.
     */
    public String toGroupSqlString(Query query)
    {
        // w GROUP nie mo�na u�y� aliasu z SELECT
        return tableAlias.getAlias() + "." + columnName;
    }
    
    /**
     * Zwraca nazw�, przy pomocy kt�rej mo�na odwo�ywa� si� do atrybutu
     * w cz�ci WHERE zapytania SQL. Je�eli atrybut zosta� u�yty
     * w SELECT, zwracany jest jego alias z tej cz�ci zapytania,
     * w przeciwnym razie zwracany jest alias tabeli i nazwa kolumny.
     */
    public String toWhereSqlString(Query query)
    {
        // je�eli dana kolumna tabeli ma w zapytaniu jaki� alias, jest on zwracany
        String alias = query.getAlias(tableAlias, columnName);

        if (alias != null)
        {
            return alias;
        }
        else
        {
            return tableAlias.getAlias() + "." + columnName;
        }
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ColumnAttribute)) return false;

        final ColumnAttribute columnAttribute = (ColumnAttribute) o;

        if (columnName != null ? !columnName.equals(columnAttribute.columnName) : columnAttribute.columnName != null) return false;
        if (tableAlias != null ? !tableAlias.equals(columnAttribute.tableAlias) : columnAttribute.tableAlias != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (tableAlias != null ? tableAlias.hashCode() : 0);
        result = 29 * result + (columnName != null ? columnName.hashCode() : 0);
        return result;
    }
}
