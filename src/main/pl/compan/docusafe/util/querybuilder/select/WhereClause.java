package pl.compan.docusafe.util.querybuilder.select;

import pl.compan.docusafe.util.querybuilder.Query;
import pl.compan.docusafe.util.querybuilder.expression.Expression;
import pl.compan.docusafe.util.querybuilder.expression.Junction;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.LogFactory;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: WhereClause.java,v 1.8 2008/12/09 13:59:18 mariuszk Exp $
 */
public class WhereClause
{
    private List constraints = new ArrayList(10);

    /**
     * Dodaje wyra�enie do klauzuli WHERE.  Je�eli przekazane wyra�enie
     * jest typu Junction i jest puste, nie jest dodawane.
     * @param expression
     */
    public WhereClause add(Expression expression)
    {
        if (expression instanceof Junction && ((Junction) expression).isEmpty())
            return this;
        constraints.add(expression);
        return this;
    }

    public String toSqlString(Query query, List parameters, List types)
    {
        StringBuilder sql = new StringBuilder("WHERE ");

        for (int i=0; i < constraints.size(); i++)
        {
            Expression expression = (Expression) constraints.get(i);
            sql.append("("+expression.toSqlString(query, parameters, types)+")");
            if (i < constraints.size()-1)
                sql.append(" AND ");
        }
        //LogFactory.getLog("mariusz").debug(sql.toString()); 
        return sql.toString();
    }

    public boolean isEmpty()
    {
        return constraints.isEmpty();
    }

    public String toString()
    {
        return getClass().getName()+"[constraints="+constraints+"]";
    }
}
