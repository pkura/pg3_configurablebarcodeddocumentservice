package pl.compan.docusafe.util.querybuilder.select;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.dialect.FirebirdDialect;
import org.hibernate.dialect.Oracle10gDialect;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.Query;
import pl.compan.docusafe.util.querybuilder.TableAlias;

/**
 * Dodanie kolumny do ORDER powinno j� umieszcza� w SELECT (o ile
 * jej tam nie ma), poniewa� Oracle ma taki wym�g.
 *
 * Mo�liwo�� robienia JOIN-�w, zapyta� hierarchicznych (Oracle).
 * <p>
 * Mo�liwo�� zmiany klauzuli SELECT bez zmiany reszty zapytania
 * (np. COUNT(*) zamiast czego� innego)
 * * mo�liwo�� zmiany dowolnej klauzuli? wyliczy� powi�zania mi�dzy nimi
 * - order wymaga obecno�ci swoich p�l w SELECT
 * <p>
 * Mo�liwo�� utworzenia podzapytania i u�ycia go w wyra�eniu.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SelectQuery.java,v 1.48 2010/08/12 07:07:15 mariuszk Exp $
 */
public class SelectQuery extends Query
{
    private static final Log log = LogFactory.getLog(SelectQuery.class);

    private SelectClause selectClause;
    private FromClause fromClause;
    private WhereClause whereClause;
    private OrderClause orderClause;
    private GroupClause groupClause;

    private Integer firstResult;
    private Integer maxResults;
    
    /**
     * Zapisujemy tu liste parametrow do zrzutu
     */
    private List<Object> traceParameters = null;
    /**
     * Ostatnio wykonane zapytanie
     */
    private String sql;

    public SelectQuery(SelectClause selectClause, FromClause fromClause, WhereClause whereClause, OrderClause orderClause)
    {
        this.selectClause = selectClause;
        this.fromClause = fromClause;
        this.whereClause = whereClause;
        this.orderClause = orderClause;
    }
    
    public SelectQuery(SelectClause selectClause, FromClause fromClause, WhereClause whereClause, OrderClause orderClause,GroupClause groupClause)
    {
    	this.selectClause = selectClause;
        this.fromClause = fromClause;
        this.whereClause = whereClause;
        this.orderClause = orderClause;
        this.groupClause = groupClause;
    }

    public void setFirstResult(int firstResult)
    {
        if (firstResult < 0) firstResult = 0;
        this.firstResult = new Integer(firstResult);
    }

    public void setMaxResults(int maxResults)
    {
       // if (maxResults < 1) throw new IllegalArgumentException(""+maxResults);
        this.maxResults = new Integer(maxResults);
    }

    /**
     * Metoda tworzy statement w czasie wyszukiwania
     * @param conn
     * @param limitOffset
     * @return
     * @throws SQLException
     */
    private PreparedStatement createStatement(boolean limitOffset) throws SQLException, EdmException
    {
    	return createStatement(limitOffset, false); 
    }
    
    private PreparedStatement createStatement(boolean limitOffset, boolean forwardOnly) throws SQLException, EdmException
    {
    	log.trace("createStatement");
        List parameters = new ArrayList();
        List types = new ArrayList();

        sql = toSqlString(parameters, types, limitOffset);
        //System.out.println(sql);
        log.debug(sql);
        PreparedStatement ps = DSApi.context().prepareStatement(sql, forwardOnly ? ResultSet.TYPE_FORWARD_ONLY : ResultSet.TYPE_SCROLL_INSENSITIVE,
            ResultSet.CONCUR_READ_ONLY);
        prepareStatement(ps, parameters, types);
 
        return ps;
    }

    /**
     * Nie uzywac - polaczenie bedzie i tak pobierane z DSApi.context()
     * @param conn
     * @return
     * @throws SQLException
     */
    @Deprecated
    public ResultSet resultSet(Connection conn) throws SQLException
    {
    	try
    	{
    		return resultSet();
    	}
    	catch (EdmException e)
    	{
    		throw new java.lang.IllegalStateException("Przykrycie wyjatku EDM",e);
    	}
    }
    /**
     * Zwraca ResultSet z wynikami zapytania.  Zwr�cony obiekt nale�y
     * koniecznie zamkn�� metod� close(), poniewa� jest to jedyna
     * metoda zamkni�cia obiektu Statement.
     * @return
     * @throws SQLException
     */
    public ResultSet resultSet() throws SQLException, EdmException
    {
        PreparedStatement ps = createStatement(true);
        ResultSet rs = ps.executeQuery();

        // przewijanie ResultSet do ��danego wyniku, je�eli dialekt nie wspiera
        // offsetu 
        boolean supportsOffset = DSApi.getDialect().supportsLimitOffset();
        if (!supportsOffset && firstResult != null && firstResult.intValue() > 0)
        {
//            int off = firstResult.intValue();
//            while (off-- > 0 && rs.next())
//                ;
             rs.absolute(firstResult.intValue());
        }
        return rs;
    }

    public ResultSet resultSet(boolean forwardOnly) throws SQLException, EdmException
    {
    	PreparedStatement ps = createStatement(true,true);
        ResultSet rs = ps.executeQuery();
        // przewijanie ResultSet do ��danego wyniku, je�eli dialekt nie wspiera
        // offsetu 
        boolean supportsOffset = DSApi.getDialect().supportsLimitOffset();
        if (!supportsOffset && firstResult != null && firstResult.intValue() > 0)
        {
//            int off = firstResult.intValue();
//            while (off-- > 0 && rs.next())
//                ;
             rs.absolute(firstResult.intValue());
        }
        //return rs;
        return rs;
    }
    

    /**
     * Zwraca alias, jaki kolumna danej tabeli ma w SELECT. Je�eli kolumna
     * nie wyst�puje w SELECT, zwracana jest warto�� null. Je�eli kolumna
     * wyst�puje kilka razy, zwracany jest jeden (losowo wybrany) alias.
     * <p>
     * Metoda u�ywana podczas budowania wyra�e� w WHERE, aby wybra� pomi�dzy
     * u�yciem aliasu kolumny, a jej pe�n� nazw� wraz z nazw� tabeli.
     */
    public String getAlias(TableAlias tableAlias, String columnName)
    {
        SelectColumn selectColumn = selectClause.getSelectColumn(tableAlias, columnName);
        return selectColumn != null ? selectColumn.getAlias() : null;
    }

    public String toSqlString(List parameters, List types, boolean limitOffset)
    {
    	
        String sql =
            selectClause.toSqlString(this) +
            " " + fromClause.toSqlString(this) +
            (whereClause != null && !whereClause.isEmpty() ? " " + whereClause.toSqlString(this, parameters, types) : "") +
            (groupClause != null && !groupClause.isEmpty() ? " " + groupClause.toSqlString(this) : "")+
            (orderClause != null && !orderClause.isEmpty() ? " " + orderClause.toSqlString(this) : "");

        if (limitOffset && (maxResults != null || firstResult != null))
        {
            // zak�adam, �e dialekt wspiera LIMIT, wi�c sprawdzanie dotyczy
            // tylko OFFSET (supportsLimit=true)
            boolean supportsOffset = DSApi.getDialect().supportsLimitOffset();
            
            int offset = firstResult != null ? firstResult.intValue() : 0;

            if (maxResults != null)
            {
                if (supportsOffset)
                {
                	//w oraclu limit oznacza do ktorego (a nie ile) wyniku dlatego trzeba dodac offset
                	if(DSApi.isOracleServer())
                	{
                		if(firstResult != null)
                		{
                			maxResults += firstResult;
                		}
                	}
                    sql = DSApi.getDialect().getLimitString(sql, offset, maxResults);
                }
                else
                {
                    sql = DSApi.getDialect().getLimitString(sql, 0,
                        offset + maxResults.intValue());
                }
            }
            
            
            // tylko je�eli dialekt wspiera OFFSET, parametry s� dodawane
            // do zapytania przy pomocy '?'
            if (supportsOffset)
            {
                if (DSApi.getDialect().bindLimitParametersInReverseOrder())
                {   
                	if(DSApi.getDialect() instanceof FirebirdDialect)
                	{
                		if(firstResult == null || (firstResult != null && firstResult == 0))
                		{
                			parameters.add(0, maxResults);
                            types.add(0, Integer.class);
                		}
                		else
                		{
                			parameters.add(0, maxResults);
                            types.add(0, Integer.class);
                            parameters.add(1, firstResult);
                            types.add(1, Integer.class);
                		}
                	}
                	else if (DSApi.getDialect().bindLimitParametersFirst())
                    {
                        parameters.add(0, maxResults);
                        types.add(0, Integer.class);
                        if (firstResult != null && (!(DSApi.getDialect() instanceof FirebirdDialect)) )
                        {
                            parameters.add(1, firstResult);
                            types.add(1, Integer.class);
                        }
                    }
                    else
                    {  
                        parameters.add(maxResults);
                        types.add(Integer.class);
                        //jesli oracle i offset = 0 nie dodajemy do parametrow 
                        //Oracl nie dodaje do zapytanie offsetu jesli jest rowny null badz 0
                        boolean supportOffset = !(DSApi.isOracleServer() || DSApi.isPostgresServer());
                        if (firstResult != null && (supportOffset || ( firstResult != null && firstResult > 0)))
                        {
                            parameters.add(firstResult);
                            types.add(Integer.class);
                            
                        }

                    }
                }
                else
                {
                    if (DSApi.getDialect().bindLimitParametersFirst())
                    {
                        if (firstResult != null)
                        {
                            parameters.add(0, firstResult);
                            types.add(0, Integer.class);
                        }
                        parameters.add(firstResult != null ? 1 : 0, maxResults);
                        types.add(firstResult != null ? 1 : 0, Integer.class);
                    }
                    else
                    {
                        if (firstResult != null)
                        {
                            parameters.add(firstResult);
                            types.add(Integer.class);
                        }
                        parameters.add(maxResults);
                        types.add(Integer.class);
                    }
                }
            }
        }
        log.trace(sql);
        traceParameters = new LinkedList(parameters);
        return sql;
    }

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public List<Object> getTraceParameters() {
		return traceParameters;
	}

	public void setTraceParameters(List<Object> traceParameters) {
		this.traceParameters = traceParameters;
	}
}
