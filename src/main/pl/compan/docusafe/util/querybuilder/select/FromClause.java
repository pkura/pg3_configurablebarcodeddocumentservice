package pl.compan.docusafe.util.querybuilder.select;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.util.querybuilder.Query;
import pl.compan.docusafe.util.querybuilder.TableAlias;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: FromClause.java,v 1.11 2009/02/28 09:05:30 wkuty Exp $
 */
public class FromClause
{
    public static final int LEFT_OUTER = 1;
    public static final int JOIN = 0;
    public static final int INNER_JOIN = 2;
    
    private static class Join
    {
        private int joinType;
        private TableAlias left;
        private TableAlias right;
        private String condition;
    }

/*
    public static class JoinType
    {
        private String sql;

        private JoinType(String sql)
        {
            this.sql = sql;
        }

        public String getSql()
        {
            return sql;
        }
    }
*/

/*
    private static class Join
    {
        private JoinType joinType;
        private Join join;
        private TableAlias tableA;
        private TableAlias tableB;

        public Join(JoinType joinType, Join join, TableAlias tableB)
        {
            this.joinType = joinType;
            this.join = join;
            this.tableB = tableB;
        }

        private Join(JoinType joinType, TableAlias tableA, TableAlias tableB)
        {
            this.joinType = joinType;
            this.tableA = tableA;
            this.tableB = tableB;
        }

        public TableAlias getTableA()
        {
            return tableA;
        }

        public TableAlias getTableB()
        {
            return tableB;
        }

    }
*/

//    public static final JoinType LEFT_OUTER = new JoinType("LEFT OUTER JOIN");
//    public static final JoinType LEFT_INNER = new JoinType("LEFT INNER JOIN");

    private List aliases = new ArrayList(10);
//    private List joins = new ArrayList(10);
    private int counter;

    private List joins = new ArrayList(10);

    /**
     * createTable(table, true)
     */
    public TableAlias createTable(String table)
    {
        return createTable(table, true);
    }

    public TableAlias createTable(String table, boolean nolock)
    {
        TableAlias alias = new TableAlias(table, createAlias(table), nolock);
        aliases.add(alias);
        return alias;
    }

    public TableAlias createJoinedTable(String table, boolean nolock,
                                        TableAlias joinedTable,
                                        int joinType, String condition)
    {
        TableAlias newTable = createTable(table, nolock);

        Join join = new Join();
        join.joinType = joinType;
        join.left = joinedTable;
        join.right = newTable;

        if (condition != null)
        {
            join.condition = condition.replaceAll("\\$l", joinedTable.getAlias()).
                replaceAll("\\$r", newTable.getAlias());
        }

        joins.add(join);

        return join.right;
    }

/*
    public void join(JoinType joinType, TableAlias tableA, TableAlias tableB)
    {
        aliases.remove(tableA);
        aliases.remove(tableB);
        joins.add(new Join(joinType, tableA, tableB));
    }

    public void join(JoinType joinType, Join join, TableAlias table)
    {
        aliases.remove(table);
        join.add(joinType, table);
    }
*/

    public String toSqlString(Query query)
    {
        StringBuilder sql = new StringBuilder("FROM ");

        List joinsCopy = new ArrayList(joins);

        Set usedTables = new HashSet();

        for (int i=0; i < aliases.size(); i++)
        {
            TableAlias alias = (TableAlias) aliases.get(i);

            if (usedTables.contains(alias))
                continue;
            sql.append(alias.toSqlString(query));
            addJoin(alias,null, joinsCopy, sql, query,usedTables);
            sql.append(", ");
        }
        sql.setLength(sql.length()-2);
        return sql.toString();
    }
        
        
    private void addJoin(TableAlias alias,Join join,List joinsCopy,StringBuilder sql,Query query,Set usedTables )
    {
    	
    	if(join == null)
    		join = findJoin(alias, joinsCopy);

        while (join != null)
        {
            switch (join.joinType)
            {
                case LEFT_OUTER:
                    sql.append(" LEFT OUTER JOIN ");
                    break;
                case JOIN:
                    sql.append(" JOIN ");
                    break;
                case INNER_JOIN:
                    sql.append(" INNER JOIN ");
                    break;
                default:
                    throw new IllegalArgumentException("Nieznany rodzaj joina: "+join.joinType);
            }
            sql.append(join.right.toSqlString(query));
            if (join.condition != null)
            {
                sql.append(" ON ");
                sql.append(join.condition);
            }
            Join rightJoin = findJoin(join.right, joinsCopy);
            if(rightJoin != null)
            {
            	addJoin(null,rightJoin, joinsCopy, sql, query,usedTables);
            }
            usedTables.add(join.right);
            join = findJoin(alias, joinsCopy);
        }
    }

    private Join findJoin(TableAlias left, List joins)
    {
        for (Iterator iter=joins.iterator(); iter.hasNext(); )
        {
            Join join = (Join) iter.next();
            if (join.left.equals(left))
            {
                iter.remove();
                return join;
            }
        }
        return null;
    }

    private String createAlias(String name)
    {
        if (name.length() > 8) name = name.substring(0, 8);
        return name + "_" + (++counter);
    }
}
