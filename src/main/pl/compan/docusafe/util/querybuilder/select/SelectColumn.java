package pl.compan.docusafe.util.querybuilder.select;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;
import pl.compan.docusafe.util.querybuilder.TableAlias;

/**
 * Opisuje jedn� kolumn� wyra�on� po s�owie SELECT. Mo�e odpowiada�
 * jednej kolumnie tabeli, obliczonemu wyra�eniu lub wielu kolumnom
 * (sk�adnia "*").
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SelectColumn.java,v 1.2 2006/02/20 15:42:32 lk Exp $
 */
public class SelectColumn
{
    private TableAlias tableAlias;
    private String columnName;
    private int position;
    private boolean multiple;
    private String sql;
    private String alias;

    private SelectColumn(TableAlias tableAlias, String columnName, String alias, boolean multiple, int position)
    {
        this.tableAlias = tableAlias;
        this.columnName = columnName;
        this.alias = alias;
        this.multiple = multiple;
        this.position = position;
    }

    private SelectColumn(String sql, String alias, int position)
    {
        this.sql = sql;
        this.alias = alias;
        this.position = position;
    }

    public static SelectColumn tableColumn(TableAlias tableAlias, String column,
                                           String alias, int position)
    {
        return new SelectColumn(tableAlias, column, alias, false, position);
    }

//    public static SelectColumn all(TableAlias alias)
//    {
//        return new SelectColumn(alias, null, true, -1);
//    }
//
//    public static SelectColumn all()
//    {
//        return new SelectColumn(null, null, true, -1);
//    }
//

    public static SelectColumn sql(String sql, String alias, int position)
    {
        return new SelectColumn(sql, alias, position);
    }


    public int getPosition()
    {
        if (multiple)
            throw new IllegalStateException("Wyra�enie typu * nie ma numeru kolumny");
        return position;
    }

    public Attribute attribute()
    {
        if (multiple)
            throw new IllegalStateException("Wyra�enie typu * nie ma atrybutu");

        if (sql != null)
            return new ComputedAttribute(this, sql);

        return new ColumnAttribute(tableAlias, columnName);
    }

    public String getAlias()
    {
        if (multiple)
            throw new IllegalStateException("Wyra�enie typu * nie ma aliasu");

        return alias;
    }

    public TableAlias getTableAlias()
    {
        return tableAlias;
    }

    public String getColumnName()
    {
        return columnName;
    }

    public String toSqlString(Query query)
    {
        if (sql != null)
        {
            return sql + " AS " + alias;
        }
        else
        {
            return (tableAlias != null ? tableAlias.getAlias() + "." : "") +
                (multiple ? "*" : columnName) +
                " AS "+alias;
        }
    }
}
