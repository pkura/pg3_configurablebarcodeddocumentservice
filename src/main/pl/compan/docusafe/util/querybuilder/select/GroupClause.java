package pl.compan.docusafe.util.querybuilder.select;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

/**
 * @author Mariusz Kilja�czyk
 * @version $Id: GroupClause.java,v 1.0 2006/02/20 15:42:32 lk Exp $
 */
public class GroupClause
{
    private List columns = new ArrayList(5);

    public GroupClause add(Attribute attribute)
    {
        // zabezpieczenie przed podaniem dwa razy tej samej kolumny
        for (Iterator iter=columns.iterator(); iter.hasNext(); )
        {
            GroupColumn oc = (GroupColumn) iter.next();
            if (oc.attribute.equals(attribute))
                return this;
        }

        columns.add(new GroupColumn(attribute));
        return this;
    }

    public boolean isEmpty()
    {
        return columns.isEmpty();
    }

    public String toSqlString(Query query)
    {
        StringBuilder sql = new StringBuilder("GROUP BY ");

        for (int i=0; i < columns.size(); i++)
        {
        	GroupColumn col = (GroupColumn) columns.get(i);
            // nie mo�na u�y� aliasu z SELECT, tylko nazw� kolumny z tabeli
            sql.append(col.attribute.toGroupSqlString(query));
            if (i < columns.size()-1)
                sql.append(", ");
        }
        return sql.toString();
    }

    private static class GroupColumn
    {
        private Attribute attribute;

        public GroupColumn(Attribute attribute)
        {
            this.attribute = attribute;
        }
    }
}
