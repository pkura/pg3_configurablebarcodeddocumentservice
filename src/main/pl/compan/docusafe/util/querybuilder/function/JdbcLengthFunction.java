package pl.compan.docusafe.util.querybuilder.function;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

/**
 * Funkcja zwracaj�ca d�ugo�� w znakach przekazanej kolumny.  Korzysta
 * ze sk�adni JDBC: {fn length($)}.
 * XXX: Firebird nie wspiera tej sk�adni, funkcja dzia�a dla MSSQL
 * 
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: JdbcLengthFunction.java,v 1.4 2009/06/01 14:54:39 mariuszk Exp $
 */
public class JdbcLengthFunction extends Function
{
    private Attribute attribute;

    public JdbcLengthFunction(Attribute attribute)
    {
        this.attribute = attribute;
    }

    public String toWhereSqlString(Query query)
    {
        return toOrderSqlString(query);
    }

    public String toOrderSqlString(Query query)
    {
        return "{fn length("+attribute.toOrderSqlString(query)+")}";
    }
    
	public String toGroupSqlString(Query query)
	{
		return attribute.toOrderSqlString(query);
	}

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final JdbcLengthFunction that = (JdbcLengthFunction) o;

        if (attribute != null ? !attribute.equals(that.attribute) : that.attribute != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (attribute != null ? attribute.hashCode() : 0);
    }

}
