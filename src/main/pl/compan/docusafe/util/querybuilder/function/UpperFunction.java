package pl.compan.docusafe.util.querybuilder.function;

import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: UpperFunction.java,v 1.5 2009/06/01 14:54:39 mariuszk Exp $
 */
class UpperFunction extends Function
{
    private Attribute attribute;

    public UpperFunction(Attribute attribute)
    {
        this.attribute = attribute;
    }

    public String toWhereSqlString(Query query)
    {
        return toOrderSqlString(query);
    }

    public String toOrderSqlString(Query query)
    {
        return "UPPER("+attribute.toOrderSqlString(query)+")";
    }
    
	public String toGroupSqlString(Query query)
	{
		return attribute.toGroupSqlString(query);
	}

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof UpperFunction)) return false;

        final UpperFunction upperFunction = (UpperFunction) o;

        if (attribute != null ? !attribute.equals(upperFunction.attribute) : upperFunction.attribute != null) return false;

        return true;
    }

    public int hashCode()
    {
        return (attribute != null ? attribute.hashCode() : 0);
    }
}
