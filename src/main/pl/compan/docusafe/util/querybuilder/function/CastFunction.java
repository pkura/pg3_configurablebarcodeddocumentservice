package pl.compan.docusafe.util.querybuilder.function;

import org.hibernate.HibernateException;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: CastFunction.java,v 1.7 2009/06/01 14:54:39 mariuszk Exp $
 */
public class CastFunction extends Function
{
    private Attribute attribute;
    private int asType;

    public CastFunction(Attribute attribute, int asType)
    {
        this.attribute = attribute;
        this.asType = asType;
    }

    public String toWhereSqlString(Query query)
    {
        return toOrderSqlString(query);
    }

    public String toOrderSqlString(Query query)
    {
        try
        {
            return "CAST("+attribute.toOrderSqlString(query)+" AS "+DSApi.getDialect().getTypeName(asType)+")";
        }
        catch (HibernateException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

	public String toGroupSqlString(Query query)
	{
		return attribute.toOrderSqlString(query);
	}

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof CastFunction)) return false;

        final CastFunction castFunction = (CastFunction) o;

        if (asType != castFunction.asType) return false;
        if (attribute != null ? !attribute.equals(castFunction.attribute) : castFunction.attribute != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (attribute != null ? attribute.hashCode() : 0);
        result = 29 * result + asType;
        return result;
    }

}
