package pl.compan.docusafe.util.querybuilder.function;

import org.dom4j.DocumentHelper;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.querybuilder.Attribute;
import pl.compan.docusafe.util.querybuilder.Query;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Function.java,v 1.8 2009/08/14 12:24:15 pecet1 Exp $
 */
public abstract class Function extends Attribute
{
    private static final Logger log = LoggerFactory.getLogger(Function.class);
    public static Attribute upper(Attribute attribute)
    {
		//log.error("UPPER!", new Throwable());
        return new UpperFunction(attribute);
    }

    public static Attribute cast(Attribute attribute, int asType)
    {
        return new CastFunction(attribute, asType);
    }

    public static Attribute fnLength(Attribute attribute)
    {
        return new JdbcLengthFunction(attribute);
    }

    public abstract String toWhereSqlString(Query query);

    /**
     * @throws UnsupportedOperationException
     */
    public abstract String toOrderSqlString(Query query);

    public abstract boolean equals(Object obj);

    public abstract int hashCode();
}
