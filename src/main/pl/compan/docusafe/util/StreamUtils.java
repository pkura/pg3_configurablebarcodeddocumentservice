package pl.compan.docusafe.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: StreamUtils.java,v 1.5 2009/02/13 10:28:35 mariuszk Exp $
 */
public class StreamUtils
{
    public static byte[] toByteArray(InputStream stream) throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[2048];
        int count;

        while ((count = stream.read(buf)) > 0)
        {
            baos.write(buf, 0, count);
        }

        return baos.toByteArray();
    }

    public static File toTempFile(InputStream stream) throws IOException
    {
        File tmp = File.createTempFile("docusafe", ".tmp");
        //tmp.deleteOnExit();
        FileOutputStream os = new FileOutputStream(tmp);
        byte[] buf = new byte[2048];
        int count;
        while ((count = stream.read(buf)) > 0)
        {
            os.write(buf, 0, count);
        }
        os.close();
        return tmp;
    }

    public static String toString(InputStream stream, String encoding) throws IOException
    {
        return new String(toByteArray(stream), encoding);
    }

    public static String toString(InputStream stream) throws IOException
    {
        return new String(toByteArray(stream));
    }

    public static String toString(Reader reader) throws IOException
    {
        StringBuilder result = new StringBuilder();
        char[] buf = new char[1024];
        int count;
        while ((count = reader.read(buf)) > 0)
        {
            result.append(buf, 0, count);
        }
        return result.toString();
    }
}
