package pl.compan.docusafe.util;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTreeImageVisitor.java,v 1.2 2008/10/23 14:35:38 pecet1 Exp $
 */
public interface HtmlTreeImageVisitor
{
    /**
     * Zwraca obrazek, kt�ry powinien znale�� si� przy danym
     * elemencie drzewa zamiast standardowej ikonki okre�lonej
     * przez {@link HtmlTreeTheme#getCollapsedFolderImage()}
     * oraz {@link HtmlTreeTheme#getExpandedFolderImage()}/
     */
    HtmlImage getIconImage(Object element, boolean expanded);
}
