package pl.compan.docusafe.util;

public class IdentificationNumbersVerify {

	/*
	 * Mo�na poda� z kreskami lub bez
	 */
	public static boolean isNipValid(String nip) {
		boolean valid = false;
		byte NIP[] = new byte[10];
		nip = nip.replaceAll("-", "");
		if (nip.length() != 10){
			valid = false;
		}
		else {
			for (int i = 0; i < 10; i++){
				NIP[i] = Byte.parseByte(nip.substring(i, i + 1));
			}
			int sum;
			sum = 6 * NIP[0] +
			      5 * NIP[1] +
				  7 * NIP[2] +
				  2 * NIP[3] +
				  3 * NIP[4] +
				  4 * NIP[5] +
				  5 * NIP[6] +
				  6 * NIP[7] +
				  7 * NIP[8];
			sum %= 11;
			if (NIP[9] == sum)
				valid = true;
		}
		return valid;
	}
	
	public static boolean isPeselValid(String pesel) {
		byte PESEL[] = new byte[11];
		if (pesel.length() != 11){
			return false;
		}
		else {
			for (int i = 0; i < 11; i++){
				PESEL[i] = Byte.parseByte(pesel.substring(i, i+1));
			}
			
			int sum =
			  1 * PESEL[0] + 
	          3 * PESEL[1] +
			  7 * PESEL[2] +
			  9 * PESEL[3] +
			  1 * PESEL[4] +
			  3 * PESEL[5] +
			  7 * PESEL[6] +
			  9 * PESEL[7] +
			  1 * PESEL[8] +
			  3 * PESEL[9];
			sum %= 10;
			sum = 10 - sum;
			sum %= 10;		
			if (sum != PESEL[10]) {
				return false;
			}
			
			int year = 10 * PESEL[0] + PESEL[1];
			int month = 10 * PESEL[2] + PESEL[3];
			int day = 10 * PESEL[4] + PESEL[5];

			if (month > 80 && month < 93) {
				year += 1800;
			}
			else if (month > 0 && month < 13) {
				year += 1900;
			}
			else if (month > 20 && month < 33) {
				year += 2000;
			}
			else if (month > 40 && month < 53) {
				year += 2100;
			}
			else if (month > 60 && month < 73) {
				year += 2200;
			}
			if (year <1800 || year > 2299)
				return false;			

			
			if (month<1 || month>11)
				return false;
			
			
			if ((day >0 && day < 32) &&
					(month == 1 || month == 3 || month == 5 ||
					 month == 7 || month == 8 || month == 10 ||
					 month == 12)) {
					return true;
				} 
				else if ((day >0 && day < 31) &&
					(month == 4 || month == 6 || month == 9 ||
					 month == 11)) {
					return true;
				}
				else if ((day >0 && day < 30 && leapYear(year)) ||
						 (day >0 && day < 29 && !leapYear(year))) {
					return true;
				}
				else {
					return false;
				}			
		}
	}
	
	private static boolean leapYear(int year) {
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
			return true;
		else
		    return false;
	}
	
	
	public static boolean isRegonValid(String REGONNumber) {
		byte REGON[] = new byte[14];
		int REGONLength;
		boolean valid = false;
		if ((REGONNumber.length() != 9) && (REGONNumber.length() != 14)) {
			valid = false;
		}
		else {
			REGONLength = REGONNumber.length();
			for (int i = 0; i < REGONLength; i++){
				REGON[i] = Byte.parseByte(REGONNumber.substring(i, i+1));
			}
			if (checkSum(REGONLength, REGON)) {
				valid = true;
			}
			else {
				valid = false;	
			}
		}
		return valid;
	}

	private static boolean checkSum9(byte REGON[]) {
		int sum = 8 * REGON[0] + 
		          9 * REGON[1] +
				  2 * REGON[2] +
				  3 * REGON[3] +
				  4 * REGON[4] +
				  5 * REGON[5] +
				  6 * REGON[6] +
				  7 * REGON[7];
		sum %= 11;
		if (sum == 10) {
			sum = 0;
		}
		if (sum == REGON[8]) {
			return true;
		}
		else {
			return false;
		}
	}

	private static boolean checkSum14(byte REGON[]) {
		int sum = 2 * REGON[0] + 
		          4 * REGON[1] +
				  8 * REGON[2] +
				  5 * REGON[3] +
				  0 * REGON[4] +
				  9 * REGON[5] +
				  7 * REGON[6] +
				  3 * REGON[7] +
				  6 * REGON[8] +
				  1 * REGON[9] +
				  2 * REGON[10] +
				  4 * REGON[11] +
				  8 * REGON[12];
		sum %= 11;
		if (sum == 10) {
			sum = 0;
		}
		if (sum == REGON[13]) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private static boolean checkSum(int regonLen, byte regon[]) {
		if (regonLen == 9) {
			return checkSum9(regon);
		}
		else {
			return (checkSum9(regon) && checkSum14(regon));
		}
	}

}
