package pl.compan.docusafe.util;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.parametrization.swd.MultipleDictionaryFactory;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * przyk�adowe u�ycie {@link pl.compan.docusafe.parametrization.ams.DsgDysponentSrodkow}
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public abstract class MultipleDictionaryReplicator extends MultipleDictionaryFactory.SqlManager<Map<String, FieldData>> {

    public static List<Long> replicateDictionary(Document newDoc, String dictCn, MultipleDictionaryReplicator sqlManager) throws EdmException, SQLException {
        List<Long> newValues = new ArrayList<Long>();
        List<Map<String, FieldData>> values = MultipleDictionaryFactory.getEntries(sqlManager);
        for (Map<String, FieldData> line : values){
            long newLine = MultipleDictionaryFactory.saveEntry(newDoc, dictCn, line);
            newValues.add(newLine);
        }
        return newValues;
    }

    protected MultipleDictionaryReplicator(Document document, Object multipleDictTable, String dictCn) {
        super(document, multipleDictTable, dictCn);
    }

    @Override
    public Map<String, FieldData> createObject(ResultSet rs) throws SQLException {
        Map<String, FieldData> values = new HashMap<String, FieldData>();

        Map<String, Field.Type> columnsWithType = getColumnsWithType();
        for (String key : columnsWithType.keySet()) {
            FieldData val;
            Field.Type type = columnsWithType.get(key);
            switch (type) {
                case DATE:
                    val = new FieldData(type, rs.getDate(key));
                    break;
                case STRING:
                    val = new FieldData(type, rs.getString(key));
                    break;
                case INTEGER:
                    val = new FieldData(type, rs.getInt(key));
                    break;
                case LONG:
                    val = new FieldData(type, rs.getLong(key));
                    break;
                case BOOLEAN:
                    val = new FieldData(type, rs.getBoolean(key));
                    break;
                case MONEY:
                    val = new FieldData(type, new BigDecimal(rs.getDouble(key)));
                    break;
                case TEXTAREA:
                case LINK:
                case ENUM:
                case TIMESTAMP:
                case LIST_VALUE:
                case ENUM_AUTOCOMPLETE:
                case ENUM_MULTIPLE:
                case DICTIONARY:
                case ATTACHMENT:
                case HTML:
                case HTML_EDITOR:
                case BUTTON:
                default:
                    throw new IllegalArgumentException(this.getClass().getName() + " : createObject : " + type.name());
            }

            values.put(key, val);
        }

        return values;
    }

    @Override
    public Collection<String> getColumns() {
        return getColumnsWithType().keySet();
    }

    public abstract Map<String, Field.Type> getColumnsWithType();
}
