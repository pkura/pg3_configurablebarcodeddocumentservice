package pl.compan.docusafe.util;

import org.apache.commons.lang.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.util.Random;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: IdUtils.java,v 1.7 2006/02/20 15:42:30 lk Exp $
 */
public class IdUtils
{
    private static final Random random = new Random(System.currentTimeMillis());

    public static String getNetworkSafeGuid() //throws UnknownHostException
    {
        byte[] addr;
        try
        {
            addr = InetAddress.getLocalHost().getAddress();
        }
        catch (UnknownHostException e)
        {
            addr = new byte[] { 127, 0, 0, 1 };
        }
        int ip = (addr[0] << 24) + (addr[1] << 16) + (addr[2] << 8) + addr[3];
        String strIp = StringUtils.leftPad(Integer.toHexString(ip).toUpperCase(), 8, "0");

        return strIp + getGuid();
    }

    public static String getGuid()
    {
        StringBuilder guid = new StringBuilder(32);

        // TODO: czy to wlasciwie jest potrzebne?
        String strHash = StringUtils.leftPad(Integer.toHexString(System.identityHashCode(IdUtils.class)).toUpperCase(), 8, "0");

        guid.append(strHash);

        // 11 cyfr hex wystarczy do roku 2527
        String strTime = StringUtils.leftPad(Long.toHexString(System.currentTimeMillis()).toUpperCase(), 11, "0");

        guid.append(strTime);

        String strRand = StringUtils.leftPad(Integer.toHexString(random.nextInt()).toUpperCase(), 8, "0");

        guid.append(strRand);

        return guid.toString();
    }

    private static NumberFormat trackingRandomFormat = NumberFormat.getIntegerInstance();

    static
    {
        trackingRandomFormat.setMinimumIntegerDigits(7);
        trackingRandomFormat.setMaximumIntegerDigits(7);
        trackingRandomFormat.setGroupingUsed(false);
    }

    /**
     * Tworzy unikalny numer sprawy dla dokumentu o podanym identyfikatorze.
     * Numer zawiera identyfikator dokumentu oraz liczb� losow�, a wi�c
     * kolejne wywo�ania funkcji z tym samym argumentem zwr�c� r�ne
     * warto�ci.
     * @return Numer �ledzenia sprawy/pisma. Maksymalnie 20 znak�w.
     */
    public static String getTrackingNumber()
    {
        String t = StringUtils.left(Long.toHexString(System.currentTimeMillis()), 11);
        String r = StringUtils.rightPad(Integer.toHexString(random.nextInt()), 8, "0");
        return r + t;
    }

    public static String getTrackingPassword()
    {
        return Integer.toHexString(random.nextInt());
    }
}
