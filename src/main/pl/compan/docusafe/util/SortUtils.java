package pl.compan.docusafe.util;

import java.text.CollationKey;
import java.text.Collator;
import java.util.Locale;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SortUtils.java,v 1.5 2006/02/20 15:42:31 lk Exp $
 */
public class SortUtils
{
    public interface CollationKeyGenerator
    {
        String getKey(Object object);
    }

    /**
     * Por�wnuje przekazane obiekty. Je�eli obydwa obiekty s� r�ne od null,
     * zwracany jest wynik por�wnania ich metod� Comparable.compareTo(), obiekt
     * r�wny null jest uwa�any za mniejszy od ka�dego obiektu r�nego od null
     * i r�wny ka�demu innemu obiektowi r�wnemu null.
     */
    public static int compare(Comparable a, Comparable b)
    {
        if (a != null && b != null) return a.compareTo(b);
        else if (a == null && b != null) return -1;
        else if (a != null && b == null) return 1;
        else return 0;
    }


    /**
     * Sortuje tablic� obiekt�w zgodnie z systemowym Locale.
     * Funkcja przed sortowaniem tablicy obiekt�w tworzy dla ka�dego
     * jej elementu instancj� klasy {@link CollationKey}, a nast�pnie
     * sortuje te obiekty, co znacznie przyspiesza proces. Ponadto
     * instancje klasy {@link CollationKeyGenerator} s� prostsze
     * w tworzeniu ni� instancje {@link java.util.Comparator}.
     * @param objects Tablica obiekt�w do posortowania, nie mo�e by�
     *  r�wna null. Przekazana instanacja {@link CollationKeyGenerator}
     *  musi dzia�a� poprawnie dla ka�dego obiektu w tej tablicy.
     * @param generator
     */
    public static void sortObjects(Object[] objects, CollationKeyGenerator generator,
                                   Locale locale, boolean ascending)
    {
        if (objects == null)
            throw new NullPointerException("objects");
        if (objects.length < 2)
            return;

        final Collator collator = Collator.getInstance(locale);

        CollationKey[] keys = new CollationKey[objects.length];
        for (int i=0, n=objects.length; i < n; i++)
        {
            keys[i] = collator.getCollationKey(generator.getKey(objects[i]));
        }

        qsort_parallel(keys, objects, 0, objects.length-1, ascending);
    }

    /**
     * Metoda sortuje tablic� lead, a wraz z ni� uaktualnia
     * kolejno�� element�w w tablicy other.
     * <p>
     * Obydwie tablice musz� mie� jednakowe d�ugo�ci. Elementy
     * tablicy lead odpowiadaj� elementom tablicy other i s�
     * zamiast nich sortowane.
     */
    private static void qsort_parallel(Comparable[] lead, Object[] other, int l, int u,
                                       boolean ascending)
    {
        if (l >= u)
            return;

        int mod = ascending ? 1 : -1;

        Comparable t = lead[l];
        int i = l;
        int j = u+1;
        do
        {
            do { i++; } while (i <= u && mod*lead[i].compareTo(t) < 0);
            do { j--; } while (mod*lead[j].compareTo(t) > 0);
            if (i > j)
                break;

            // zamiana
            Comparable temp = lead[i];
            lead[i] = lead[j];
            lead[j] = temp;

            Object temp2 = other[i];
            other[i] = other[j];
            other[j] = temp2;
        }
        while (true);

        // zamiana
        Comparable temp = lead[l];
        lead[l] = lead[j];
        lead[j] = temp;

        Object temp2 = other[l];
        other[l] = other[j];
        other[j] = temp2;

        qsort_parallel(lead, other, l, j-1, ascending);
        qsort_parallel(lead, other, j+1, u, ascending);
    }

}

