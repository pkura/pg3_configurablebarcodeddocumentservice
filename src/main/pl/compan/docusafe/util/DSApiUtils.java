package pl.compan.docusafe.util;

import com.google.common.base.Throwables;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;

import java.util.concurrent.Callable;

import static com.google.common.base.Preconditions.checkState;

/**
 * Template do wykonywania fragment�w kodu w kontek�cie lub w transakcji
 *
 */
public final class DSApiUtils {

    private final static Logger log = LoggerFactory.getLogger(DSApiUtils.class);

    public static void checkInTransaction(){
        checkState(DSApi.context().inTransaction(), "not in transaction");
    }

    /**
     * Wykonuje przekazane runnable w kontek�cie DSApi
     * @param run
     */
    public static void inAdminContext(Runnable run){
        try {
            DSApi.openAdmin();
            run.run();
        } catch (Exception ex){
            Throwables.propagate(ex);
        } finally {
            DSApi._close();
        }
    }

    /**
     * Metoda u�ywana do opakowania mehcanizmu logowania w WebSerwisach - rampart
     * @param run
     * @param <T>
     * @return
     */
    public static <T> T inWSContext(Callable<T> run){
        try {
            AbstractAuthenticator.openContext();
            return run.call();
        } catch (Exception ex){
            log.error("", ex);
            throw Throwables.propagate(ex);
        } finally {
            DSApi._close();
            AbstractAuthenticator._closeContext();
        }
    }

    public static <T> T inWSContextTransaction(Callable<T> run){
        boolean commit = false;
        T ret;
        try {
            AbstractAuthenticator.openContext();
            DSApi.beginTransacionSafely();
            ret = run.call();
            commit = true;
            DSApi.context().commit();
            return ret;
        } catch (Exception ex){
            log.error("", ex);
            throw Throwables.propagate(ex);
        } finally {
            try {
                if (!commit) {
                    DSApi.context().rollback();
                }
            } catch (EdmException ex) {
                throw Throwables.propagate(ex);
            } finally {
                DSApi._close();
                AbstractAuthenticator._closeContext();
            }
        }
    }

    public static <T> T inAdminContext(Callable<T> run){
        try {
            DSApi.openAdmin();
            return run.call();
        } catch (Exception ex){
            log.error("", ex);
            throw Throwables.propagate(ex);
        } finally {
            DSApi._close();
        }
    }

    public static <T> T inAdminContextTransaction(Callable<T> run){
        boolean commit = false;
        T ret;
        try {
            DSApi.openAdmin();
            DSApi.beginTransacionSafely();
            ret = run.call();
            commit = true;
            DSApi.context().commit();
            return ret;
        } catch (Exception ex){
            throw Throwables.propagate(ex);
        } finally {
            try {
                if (!commit) {
                    DSApi.context().rollback();
                }
            } catch (EdmException ex) {
                throw Throwables.propagate(ex);
            } finally {
                DSApi._close();
            }
        }
    }

    /**
     * Wykonuje przekazane runnable w kontek�cie DSApi i w transakcji
     * @param run
     */
    public static void inAdminContextTransaction(Runnable run){
        boolean commit = false;
        try {
            DSApi.openAdmin();
            DSApi.beginTransacionSafely();
            run.run();
            commit = true;
            DSApi.context().commit();
        } catch (Exception ex){
            throw Throwables.propagate(ex);
        } finally {
            try {
                if (!commit) {
                    DSApi.context().rollback();
                }
            } catch (EdmException ex) {
                throw Throwables.propagate(ex);
            } finally {
                DSApi._close();
            }
        }
    }

    private DSApiUtils(){}
}
