/*
 * $Header: /export/spare/cvs/docusafe/src/main/pl/compan/docusafe/util/StringManager.java,v 1.47 2009/07/07 09:24:24 mariuszk Exp $
 * $Revision: 1.47 $
 * $Date: 2009/07/07 09:24:24 $
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 1999 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 * [Additional notices, if required by prior licensing conditions]
 *
 */

package pl.compan.docusafe.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.GlobalPreferences;

/**
 * An internationalization / localization helper class which reduces the bother
 * of handling ResourceBundles and takes care of the common cases of message
 * formating which otherwise require the creation of Object arrays and such.
 * 
 * <p>
 * The StringManager operates on a package basis. One StringManager per package
 * can be created and accessed via the getManager method call.
 * 
 * <p>
 * The StringManager will look for a ResourceBundle named by the package name
 * given plus the suffix of "LocalStrings". In practice, this means that the
 * localized information will be contained in a LocalStrings.properties file
 * located in the package directory of the classpath.
 * 
 * <p>
 * Please see the documentation for java.util.ResourceBundle for more
 * information.
 * 
 * @author James Duncan Davidson [duncan@eng.sun.com]
 * @author James Todd [gonzo@eng.sun.com]
 * @author Lukasz Kowalczyk [lukasz@wa.home.pl]
 */

public class StringManager
{

	private static final Logger log = LoggerFactory.getLogger(StringManager.class);

	/**
	 * The default ResourceBundle for this StringManager (associated with the
	 * default locale).
	 */

	protected ResourceBundle defaultBundle;

	/**
	 * Name of the ResourceBundle associated with this StringManager.
	 */
	private String bundleName;
	private String packageN;
	protected static Properties homeLocalStringsProperties = new Properties();

	/***
	 * Inicjuje mape z kluczami znajduj�cymi sie w HOME
	 */
	public static void initHomeLocalStringsProperties()
	{
		homeLocalStringsProperties = new Properties();
		File f = new File(Docusafe.getHome().getAbsolutePath() + "/" + "LocalStrings.properties");
		if (f.exists())
		{
			InputStream is;
			try
			{
				is = new FileInputStream(f);
				homeLocalStringsProperties.load(is);
			}
			catch (Exception e)
			{
				log.error("", e);
			}
		}
	}
	
	protected StringManager() {}

	/**
	 * Creates a new StringManager for a given package. This is a private method
	 * and all access to it is arbitrated by the static getManager method call
	 * so that only one StringManager per package will be created.
	 * 
	 * @param packageName
	 *            Name of package to create StringManager for.
	 */

	private StringManager(String packageName, String resourceName)
	{
		if (resourceName == null)
			resourceName = "LocalStrings";

		packageN = packageName;
		bundleName = (packageName.length() > 0 ? packageName + "." : "") + resourceName;

		Locale locale = null;

		locale = Docusafe.getCurrentLanguageLocale();

		try {
			defaultBundle = loadBundle(bundleName, locale);
		} catch (Exception e) {
			try
			{
							
				log.warn("Brak definicji dla " + bundleName + " w lokalizacji " + Docusafe.getCurrentLanguageLocaleString());
				defaultBundle = loadBundle(bundleName, Docusafe.getDefaultLocale());
			}
			catch (Exception e1)
			{
				log.error("Brak domyslnej definicji dla " + bundleName);
			}
		}
	}

	private StringManager(String packageName, String resourceName, Locale locale)
	{
		log.trace("Inicjowanie obiektu StringManager");
		
		if (resourceName == null)
			resourceName = "LocalStrings";
		packageN = packageName;
		bundleName = (packageName.length() > 0 ? packageName + "." : "") + resourceName;
		if (log.isTraceEnabled())
		{
			log.trace("PackageName={}", packageN);
			log.trace("BundleName={}", bundleName);
		}
		try
		{
			defaultBundle = loadBundle(bundleName, locale);
		}
		catch (MissingResourceException e)
		{
		
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param name
	 * @param locale
	 * @return
	 * @throws MissingResourceException
	 */
	private ResourceBundle loadBundle(String name, Locale locale) throws MissingResourceException
	{
		log.trace("load bundle");
		ResourceBundle bundle = null;
		try
		{
			bundle = ResourceBundle.getBundle(name, locale);
		}
		catch (MissingResourceException e)
		{
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			if (cl != null)
			{
				try
				{
					bundle = ResourceBundle.getBundle(name, locale, cl);
				}
				catch (MissingResourceException e2)
				{
					log.debug("", e);
					log.debug("", e2);
					throw e2;
				}
			}

			if (bundle == null)
				throw e;
		}
		return bundle;
	}

	/**
	 * Zwraca dany string metoda r�wnowa�na do getString, wykorzystywana g��wnie
	 * w szablona Velocity Nale�y j� usun�� teraz nie jest potrzebna
	 * 
	 * @param key
	 * @return
	 */
	@Deprecated
	public String get(String key)
	{
		return getStringFromPath(key);
	}

	/**
	 * Szuka klucza na podstawie paczki w jakiej znajduj� si� klasa. Je�li nie
	 * znjadzie przechodzi jeden poziom wyzej
	 * 
	 * @param key
	 */
	private String getStringFromPath(String key)
	{
		try
		{
			String value = null;
			if (homeLocalStringsProperties.containsKey(key))
			{
				if (log.isTraceEnabled())
				{
					log.trace("Wartosc znaleziona w properties={}", homeLocalStringsProperties.get(key));
				}
				value = (String) homeLocalStringsProperties.get(key);
			}
			else
			{
				String sciezka = this.packageN;
				String[] path = sciezka.split("\\.");
				int d = sciezka.length();
				String tmp = sciezka;
				try
				{
					value = this.getStringInternal(key);
					return value;
				}
				catch (StringManagerExeption e)
				{

				}
				for (int i = path.length - 1; i >= 0; i--)
				{
					try
					{
						value = getStringWithoutAtt(tmp, key);
						return value;
					}
					catch (StringManagerExeption e)
					{

					}
					d = tmp.length();
					if (d > 0)
						tmp = tmp.substring(0, d - path[i].length() - 1);
					else
					{
						try
						{
							value = getStringWithoutAtt("", key);
							return value;
						}
						catch (StringManagerExeption e)
						{
							value = key;
						}
					}
					if (tmp.equals("pl.compan.docusafe"))
					{
						try
						{
							value = getStringWithoutAtt("", key);
							return value;
						}
						catch (StringManagerExeption e)
						{
							value = key;
						}
					}
				}
			}
			return value;
		}
		catch (Exception e)
		{
			return key;
		}
	}

	/**
	 * Zwraca wartosc klucza jesli znajude sie w okreslonej sciezce, jesli nie rzuca wyjatek 
	 * Nie wstawia atrybutow
	 * @param path
	 * @param key
	 * @return
	 * @throws StringManagerExeption
	 */
	private static String getStringWithoutAtt(String path, String key) throws StringManagerExeption
	{
		StringManager sm = GlobalPreferences.loadPropertiesFile(path, null);
		return MessageFormat.format(sm.getStringInternal(key), (Object[]) null);
	}

	protected String getStringInternal(String key) throws StringManagerExeption
	{
		if (key == null)
		{
			String msg = "key is null";

			throw new NullPointerException(msg);
		}

		String str;

		if (defaultBundle == null)
			return key;

		ResourceBundle localBundle = null;

		// if context locale is defined and differs from the
		// default locale, attempt to load resource bundle
		// associated with the context locale
		Locale contextLocale = null;

		contextLocale = new Locale(Docusafe.getCurrentLanguage());// Locale();

		if (contextLocale != null)
		{
			try
			{
				localBundle = loadBundle(bundleName, contextLocale);
			}
			catch (MissingResourceException e)
			{}
		}

		try
		{
			if (localBundle != null)
			{
				str = localBundle.getString(key);
			}
			else
			{
				str = defaultBundle.getString(key);
			}
		}
		catch (MissingResourceException mre)
		{
			throw new StringManagerExeption("Cannot find key " + key);
		}
		return str;
	}

	/**
	 * Get a string from the underlying resource defaultBundle and format it
	 * with the given set of arguments. Tutaj dodaje atrybuty do tekstu
	 * 
	 * @param key
	 * @param args
	 */

	public String getString(String key, Object[] args)
	{
		String iString;
		String value = getStringFromPath(key);// getStringInternal(key);

		// this check for the runtime exception is some pre 1.1.6
		// VM's don't do an automatic toString() on the passed in
		// objects and barf out

		try
		{
			// ensure the arguments are not null so pre 1.2 VM's don't barf
			Object nonNullArgs[] = args;
			for (int i = 0; i < args.length; i++)
			{
				if (args[i] == null)
				{
					if (nonNullArgs == args)
						nonNullArgs = (Object[]) args.clone();
					nonNullArgs[i] = "null";
				}
			}

			iString = MessageFormat.format(value, nonNullArgs);
		}
		catch (IllegalArgumentException iae)
		{
			StringBuilder buf = new StringBuilder();
			buf.append(value);
			for (int i = 0; i < args.length; i++)
			{
				buf.append(" arg[" + i + "]=" + args[i]);
			}
			iString = buf.toString();
		}
		return iString;
	}

	public String getString(String key)
	{
		return getStringFromPath(key);
	}

	/**
	 * Get a string from the underlying resource defaultBundle and format it
	 * with the given object argument. This argument can of course be a String
	 * object.
	 * 
	 * @param key
	 * @param arg
	 */

	public String getString(String key, Object arg)
	{
		Object[] args = new Object[] { arg };
		return getString(key, args);
	}

	/**
	 * Get a string from the underlying resource defaultBundle and format it
	 * with the given object arguments. These arguments can of course be String
	 * objects.
	 * 
	 * @param key
	 * @param arg1
	 * @param arg2
	 */

	public String getString(String key, Object arg1, Object arg2)
	{
		Object[] args = new Object[] { arg1, arg2 };
		return getString(key, args);
	}

	/**
	 * Get a string from the underlying resource defaultBundle and format it
	 * with the given object arguments. These arguments can of course be String
	 * objects.
	 * 
	 * @param key
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */

	public String getString(String key, Object arg1, Object arg2, Object arg3)
	{
		Object[] args = new Object[] { arg1, arg2, arg3 };
		return getString(key, args);
	}

	/**
	 * Get a string from the underlying resource defaultBundle and format it
	 * with the given object arguments. These arguments can of course be String
	 * objects.
	 * 
	 * @param key
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */

	public String getString(String key, Object arg1, Object arg2, Object arg3, Object arg4)
	{
		Object[] args = new Object[] { arg1, arg2, arg3, arg4 };
		return getString(key, args);
	}

	public String getString(String key, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5)
	{
		Object[] args = new Object[] { arg1, arg2, arg3, arg4, arg5 };
		return getString(key, args);
	}

	// --------------------------------------------------------------
	// STATIC SUPPORT METHODS
	// --------------------------------------------------------------

	private static Hashtable<String, StringManager> managers = new Hashtable<String, StringManager>();

	/**
	 * Get the StringManager for a particular package. If a manager for a
	 * package already exists, it will be reused, else a new StringManager will
	 * be created and returned.
	 * 
	 * @param packageName
	 */

	public synchronized static StringManager getManager(String packageName, String bundleName)
	{
		String key = packageName + "." + bundleName;
		log.trace("Pobieram managera {}", key);
		StringManager mgr = managers.get(key);

		if (mgr == null)
		{
			log.trace("Manager nieznaleziony w cache");
			mgr = new StringManager(packageName, bundleName);
			managers.put(packageName, mgr);
		}
		return mgr;
	}

	/**
	 * Zwraca obiekt String manager
	 * 
	 * @param packageName
	 * @param bundleName
	 * @param locale
	 * @return
	 */
	public synchronized static StringManager getManager(String packageName, String bundleName, Locale locale)
	{
		String key = packageName + "." + bundleName + "." + locale.getLanguage();
		log.trace("Pobieram managera {}", key);
		StringManager mgr = managers.get(key);
		if (mgr == null)
		{
			log.trace("Manager nie znaleziony w tablicy");
			mgr = new StringManager(packageName, bundleName, locale);
			managers.put(packageName + "." + bundleName + "." + locale.getLanguage(), mgr);
		}
		return mgr;
	}

	/**
	 * 
	 * @param packageName
	 * @return
	 */
	public synchronized static StringManager getManager(String packageName)
	{
		return GlobalPreferences.loadPropertiesFile(packageName, null);
	}
}
