package pl.compan.docusafe.util;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: TableTreeUrlVisitor.java,v 1.2 2006/02/20 15:42:31 lk Exp $
 */
public interface TableTreeUrlVisitor
{
    HtmlTableTreeModel.Link[] getUrls(Object element);
    String getControlUrl(Object element);
}
