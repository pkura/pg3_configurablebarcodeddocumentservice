package pl.compan.docusafe.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.util.MimeMessageParser;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.dwr.Field;
import pl.compan.docusafe.core.mail.DSEmailChannel;
import pl.compan.docusafe.core.mail.MailMessage;
import pl.compan.docusafe.web.common.HtmlUtils;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.text.html.HTML.Tag;
import java.io.IOException;
import java.util.Properties;

public class EmailUtils {

    /**
     * Metoda konwertuje zawartosc maila ze strumienia do czytelnego Stringa
     * @param mailMessage
     * @return
     * @throws Exception
     */
    public static String getEmailContent(MailMessage mailMessage) throws Exception {
        Session session = Session.getDefaultInstance(new Properties(), null);
        MimeMessage msg = new MimeMessage(session, mailMessage.getBinaryStream());
        MimeMessageParser parser = new MimeMessageParser(msg);
        parser.parse();
        if(isAppleClientSpecific(parser)){
            return getContentForAppleClient(msg);
        } else if(parser.hasHtmlContent()) {
            return parser.getHtmlContent();
        } else {
            return HtmlUtils.wrap(parser.getPlainContent(), Tag.PRE);
        }
    }


    /**
     * Metoda weryfikuje czy wiadomosc e-mail zostala wyslana z urzadzenia firmy Apple i zawiera jakiekolwiek za��czniki (jest to kluczowe w kontekscie prawidlowego wyswietlenia zawartosci wiadomosci)
     * @param parser
     * @return
     * @throws MessagingException
     */
    private static boolean isAppleClientSpecific(MimeMessageParser parser) throws MessagingException {
        return isAppleContentType(parser.getMimeMessage().getContentType()) && parser.getAttachmentList().size() > 0;
    }


    /**
     * Metoda weryfikuje czy wiadomosc e-mail zostala wyslana z urzadzenia firmy Apple
     * @param contentType
     * @return
     */
    private static boolean isAppleContentType(String contentType){
        return contentType.toLowerCase().contains("apple-mail");
    }

    /**
     * Metoda umozliwia prawidlowe wyswietlenia wiadomosci e-mail wyslanej z urzadzenia firmy Apple
     * @param message
     * @return
     * @throws EdmException
     * @throws MessagingException
     * @throws IOException
     */
    private static String getContentForAppleClient(MimeMessage message) throws EdmException, MessagingException, IOException {

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        String content = null;
        if (message.getContent() instanceof Multipart)
        {
            Multipart mp = (Multipart) message.getContent();
            for (int j=0, m = mp.getCount(); j < m; j++) {
                Part part = mp.getBodyPart(j);
                String disposition = part.getDisposition();
                if (disposition == null && !(part.getContent() instanceof MimeMultipart) && part.getContent() instanceof String && !StringUtils.isWhitespace(part.getContent().toString()))
                {
                    content = prepareContentByType(part, (String) part.getContent());
                }
                else if(disposition != null && disposition.equals(Part.INLINE) && !(part.getContent() instanceof MimeMultipart) && part.getContent() instanceof String && !StringUtils.isWhitespace(part.getContent().toString()))
                {
                    //	an inline content-disposition, which means that it should be automatically displayed when the message is displayed,
                    //	or an attachment content-disposition, in which case it is not displayed automatically and requires some form of action from the user to open it.
                    content = prepareContentByType(part, (String) part.getContent());
                }
            }
        }
        return content;
    }

    private static String prepareContentByType(Part message, String messageContent) throws MessagingException{
        // formatowanie tresci wiadomosci w zaleznosci od jej typu
        if(message.getContentType().contains(EmailConstants.TEXT_PLAIN)) {
            return HtmlUtils.wrap(messageContent, Tag.PRE);
        }else if(message.getContentType().contains(EmailConstants.TEXT_HTML)) {
            return messageContent;
        }
        return messageContent;
    }

    /**
     * Metoda weryfikuje poprawno�� ustawie� kana�u e-mail na podstawie powi�zanej z nim wiadomo�ci
     * i odpowiednio modyfikuje komunikat wy�wietlany u�ytkownikowi w przypadku wykrycia b��du.
     * @param msg wiadomo�� wy�wietlana u�ytkownikowi
     * @param messageId id wiadomo�ci email s�u��ce do identyfikacji kana�u email
     * @throws EdmException
     */
    public static boolean verifyEmailChannel(Field msg, Long messageId) throws EdmException{
        MailMessage mail = MailMessage.find(messageId);
        DSEmailChannel channel = mail.getEmailChannel();

        boolean isValid = false;
        if(channel == null){
            msg.setLabel("Nie znaleziono w systemie kana�u email " + " ( " + mail.getFrom() + " ) " + ", z kt�rego pobrano wiadomo��");
            //	jesli kanal email, z ktorego zostal pobrany mail, jest aktualnie aktywny
        }else if(channel.isEnabled()){
            //	jesli kanal email, z ktorego zostal pobrany mail, ma aktywn� poczt� wychodzac�
            if(channel.isSmtpEnabled()){
                return true;
            }else{
                msg.setLabel("Kana� email " + channel.getKolejka() + " ( " + channel.getUser() +" ) " + "nie ma aktywnej konfiguracji SMTP");
            }
        }else{
            msg.setLabel("Kana� email " + channel.getKolejka() + " ( " + channel.getUser() +" ) " + "nie jest aktywny");
        }
        return isValid;
    }
}
