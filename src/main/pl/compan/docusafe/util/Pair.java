package pl.compan.docusafe.util;

/**
 * Para właściwości
 * 
 * @author User
 *
 * @param <T>
 */
public class Pair<T> 
{
	/**
	 * Pierwsza właściwość
	 */
	private T first;
	
	/**
	 * Druga właściwość
	 */
	private T second;
	
	/**
	 * Domyślny konstruktor
	 */
	public Pair() 
	{
	}

	/**
	 * Konstruktor ustawiający właściwości
	 * 
	 * @param first
	 * @param second
	 */
	public Pair(T first, T second) 
	{
		super();
		this.first = first;
		this.second = second;
	}

	public T getFirst() 
	{
		return first;
	}

	public void setFirst(T first) 
	{
		this.first = first;
	}

	public T getSecond() 
	{
		return second;
	}

	public void setSecond(T second) 
	{
		this.second = second;
	}
}
