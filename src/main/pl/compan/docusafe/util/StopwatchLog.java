package pl.compan.docusafe.util;

/**
 * Stoper, kt�ry loguje wyniki ze sformatowanymi informacjami dotycz�cymi poszczeg�lnych wywo�a�
 *
 * Nie dolicza czasu logowania do analizowanego.
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class StopwatchLog {
    private final static Logger LOG = LoggerFactory.getLogger(StopwatchLog.class);

    private String currentRegionName;
    private long currentRegionStartTime;

    /**
     * Rozpoczyna pomiar danego regionu kodu (nazwa b�dzie u�ywana w logach)
     * i ko�czy pomiar poprzedniego (je�eli istnia� - w takim przypadku loguje czas wykonania)
     * @param name
     */
    public void startRegion(String name) {
        checkRegionEnd();
        currentRegionName = name;
        currentRegionStartTime = System.currentTimeMillis();
    }

    /**
     * Ko�czy pomiar danego regionu kodu (je�eli istnia� - w takim przypadku loguje czas wykonania)
     * Je�eli wcze�niej nie by�a wywo�ana metoda startRegion to metoda nic nie robi.
     */
    public void end(){
        checkRegionEnd();
    }

    private void checkRegionEnd() {
        long time = System.currentTimeMillis();
        if(currentRegionName != null){
            LOG.info("STOPWATCH '{}' - {} ms", currentRegionName, time - currentRegionStartTime);
        }
    }
}