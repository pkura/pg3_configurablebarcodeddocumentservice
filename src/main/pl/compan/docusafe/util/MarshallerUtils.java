package pl.compan.docusafe.util;

import pl.compan.docusafe.api.user.office.FullTextSearchDto;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import java.io.StringReader;
import java.io.StringWriter;

/**
 * Klasa pomocnicza dla Jaxb marshallera
 * �ukasz Wo�niak - lukasz.wozniak@docusafe.pl
 */
public class MarshallerUtils {

    /**
     * Konwertuje Obiekt do xmla
     * @param c
     * @return
     * @throws JAXBException
     */
    public static String marshall(Object c) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(c.getClass());
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);

        StringWriter sw = new StringWriter();

        marshaller.marshal(c, sw);

        return sw.toString();
    }
    
    /**
     * Konwertuje xmla do Obiekt
     * 
     * @param c
     * @return
     * @throws JAXBException
     */
    public static Object unmarshall(Object c, String stringXml) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(c.getClass());
        Unmarshaller u = jc.createUnmarshaller();

        StringReader reader = new StringReader(stringXml);
        Object object = (Object) u.unmarshal(reader);

        return object;
    }
}
