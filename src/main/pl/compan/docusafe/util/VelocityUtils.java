package pl.compan.docusafe.util;

import java.io.Serializable;

import org.apache.velocity.app.VelocityEngine;

import pl.compan.docusafe.core.AvailabilityManager;

/**
 * Util dla obiektu Velocity. 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class VelocityUtils implements Serializable
{
	/**
	 * Zwraca VelocityEngine. Ma mo�liwo�� ustawienia niekt�rych parametr�w globalnie
	 * @return
	 */
	public static VelocityEngine getEngine()
	{
		 VelocityEngine ve = new VelocityEngine();
         if(AvailabilityManager.isAvailable("velocity.nullLogSystem"))
         {
         	ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
         }
         
         return ve;
	}
}
