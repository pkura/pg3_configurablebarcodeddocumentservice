package pl.compan.docusafe.util;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTreeStyleVisitor.java,v 1.1.1.1 2003/12/02 12:46:26 administrator Exp $
 */
public interface HtmlTreeStyleVisitor
{
    /**
     * Metoda wywo�ywana jest dla ka�dego elementu drzewa. Zwracana
     * warto�� zostanie u�yta w miejsce tytu�u elementu pochodz�cego
     * z {@link HtmlTreeModel}. Daje to mo�liwo�� zmiany domy�lnej
     * nazwy klasy CSS, jak� maj� wszystkie odno�niki w drzewie.
     * <p>
     * Domy�lna nazwa klasy CSS pochodzi z instancji
     * {@link HtmlTreeTheme}.
     *
     * @param element Element drzewa.
     * @return Nazwa klasy CSS lub null (w�wczas u�yta zostanie nazwa
     * klasy zwracana przez HtmlTreeTheme).
     */
    String getStyleClass(Object element);
}
