package pl.compan.docusafe.util;

import org.directwebremoting.util.Base64;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.crypto.CryptographicFactory;
import pl.compan.docusafe.parametrization.pika.PikaLogic;

/**
 * 
 * Klasa zawieraj�ca podstawowe funkcje operuj�ce na za��cznikach
 *
 */
public class AttachmentUtils
{
	private final static Logger log = LoggerFactory.getLogger(AttachmentUtils.class);
	/**
	 * Koduje id za��cznika aby mo�na by�o go doda� parametru w urlu
	 * @param attachmentId
	 * @return
	 */
	public static String encodeAnonymousParam(Long attachmentId)
	{
		String result = null;
		
		try
		{

				CryptographicFactory factory = new CryptographicFactory();
				String KLUCZ = getKey();			 
				log.trace("szyfruje wartosc {} kluczem {} dlugosc {}", attachmentId.toString(), KLUCZ, KLUCZ.length());
				byte[] ss = factory.processBytes(attachmentId.toString().getBytes(), KLUCZ.getBytes(), true, 128);
				result = new String(Base64.encodeBase64(ss));
				
				log.trace("Zaszyfrowane w base64 to {}", result);
				
		}catch(Exception e){
			log.error("", e);
		}

		return result;
	}
	
	/**
	 * Dekoduje parametr i zwraca id za��cznika
	 * @param param
	 * @return
	 */
	public static Long decodeAnonymousParam(String param)
	{
		Long attachmentId = null;
		try
		{
			CryptographicFactory factory = new CryptographicFactory();
			byte[] sen = factory.processBytes(Base64.decodeBase64(param.getBytes()), getKey().getBytes(), false, 128);
			String result = new String(sen).trim();
			attachmentId = Long.valueOf(result);
			log.trace("Odszyfrowane to {}", attachmentId);
		
		}catch(Exception e){
			log.error("", e);
		}
		
		return attachmentId;
	}
	
	/**
	 * Zwraca Url do pobierania za��cznik�w przez anonimowych uzytkownik�w.
	 * @param attachmentId
	 * @return
	 */
	public static String getAnonymousUrl(Long attachmentId)
	{
		StringBuilder sb = new StringBuilder(Docusafe.getBaseUrl());
		sb.append("/viewserver/anonymonusViewer.action")
		  .append("?getAttachment=true&param=")
		  .append(encodeAnonymousParam(attachmentId));
		
		return sb.toString();
		
	}
	/**
	 * Zwraca klucz u�ywany do szyfrowania generowanych link�w do za��cznik�w
	 * @return
	 */
	private static String getKey()
	{
		return "KJ9L319TdKBKFHIM";
	}
}
