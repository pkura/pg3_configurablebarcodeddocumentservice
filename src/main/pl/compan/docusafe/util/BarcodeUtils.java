package pl.compan.docusafe.util;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;

/* User: Administrator, Date: 2005-07-07 16:39:04 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BarcodeUtils.java,v 1.4 2006/02/20 15:42:29 lk Exp $
 */
public abstract class BarcodeUtils
{
    public static final int MAX_LENGTH = 12;

    // kod kreskowy zawsze zaczyna si� od zera, je�eli w przysz�o�ci
    // kody zostan� wyd�u�one, b�d� si� zaczyna�y od innej cyfry
    // 0 oznacza kod dwunastocyfrowy

    public static final String PREFIX = "0";

    public static final char ID_PREFIX = '1';
    public static final char KO_IN_PREFIX = '2';
    public static final char KO_OUT_PREFIX = '3';

    public static String getIdBarcode(Document document) throws EdmException
    {
        if (document.getId() == null)
            throw new EdmException("Nie mo�na nada� kodu kreskowego dokumentowi bez ID");

        StringBuilder result = new StringBuilder(PREFIX);
        result.append(ID_PREFIX);

        int space = MAX_LENGTH - result.length();
        result.append(StringUtils.leftPad(document.getId().toString(), space, "0"));

        if (result.length() > MAX_LENGTH)
            throw new EdmException("Wygenerowany kod jest zbyt d�ugi (max="+MAX_LENGTH+")");

        return result.toString();
    }

    public static String getKoBarcode(InOfficeDocument document) throws EdmException
    {
        if (document.getId() == null)
            throw new EdmException("Nie mo�na nada� kodu kreskowego dokumentowi bez ID");

        return getKoBarcode(document, KO_IN_PREFIX);
    }
    
    public static String getKoBarcode(OfficeDocument document) throws EdmException
    {
        if (document.getId() == null)
            throw new EdmException("Nie mo�na nada� kodu kreskowego dokumentowi bez ID");
        if(document instanceof InOfficeDocument)
        	return getKoBarcode(document, KO_IN_PREFIX);
        else
        	return getKoBarcode(document, KO_OUT_PREFIX);
    }

    public static String getKoBarcode(OutOfficeDocument document) throws EdmException
    {
        if (document.getId() == null)
            throw new EdmException("Nie mo�na nada� kodu kreskowego dokumentowi bez ID");

        return getKoBarcode(document, KO_OUT_PREFIX);
    }

    private static String getKoBarcode(OfficeDocument document, char prefix) throws EdmException
    {
        if (document.getOfficeNumber() == null)
            //throw new EdmException("Dokument "+document.getId()+" nie ma numeru kancelaryjnego");
            return "";
        if (document.getOfficeNumberYear() == null)
            //throw new EdmException("Dokument "+document.getId()+" nie ma nadanego roku z dziennika");
            return "";

        StringBuilder result = new StringBuilder(PREFIX);
        result.append(prefix);

        int space = MAX_LENGTH - result.length();

        // 11 znak�w na kod
        String sid = document.getOfficeNumberYear() +
            StringUtils.leftPad(document.getOfficeNumber().toString(), space - 4, "0");
        // -4 dla roku

        result.append(sid);

        if (result.length() > MAX_LENGTH)
            throw new EdmException("Wygenerowany kod jest zbyt d�ugi (max="+MAX_LENGTH+")");

        return result.toString();
    }
}
