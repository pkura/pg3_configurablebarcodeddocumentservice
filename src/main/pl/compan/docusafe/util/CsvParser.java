package pl.compan.docusafe.util;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Dzia�aj�cy, przetestowany parser CSV
 *
 * link do "standardu" -> http://tools.ietf.org/html/rfc4180
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CsvParser {

    public static final char DELIMETER = ';';
    public static final char QUOTE ='"';
    public static final char ESCAPE = '\\';

    /**
     * Parsuje lini� u�ywaj�c ; do rozdzielania, " i \ do escapowania
     * (niezgodny z RFC4180)
     * @param line
     * @return
     */
    public static String[] parseLine(String line){
        boolean quoted = false;
        boolean escaped = false;
        List<String> ret = Lists.newArrayList();
        StringBuilder sb = new StringBuilder();
        char[] string = line.toCharArray();
        for(int i = 0; i < string.length; ++i){
            if(escaped){         //escape ma priorytet
                sb.append(string[i]);
                escaped = false;
                continue;
            }
            switch(string[i]){
                case DELIMETER:
                    if(quoted){
                        sb.append(DELIMETER);
                    } else {
                        ret.add(sb.toString());
                        sb.setLength(0);
                    }
                    break;
                case QUOTE:
                    if(quoted && string.length > i + 1 && string[i+1] == QUOTE){      //double quote -> quote
                        i++;
                        sb.append('"');
                    } else {
                        quoted = !quoted;
                    }
                    break;
                case ESCAPE:
                    escaped = true;
                    break;
                default:
                    sb.append(string[i]);
                    break;
            }
        }
        ret.add(sb.toString());
        return ret.toArray(new String[ret.size()]);
    }

//    public static String[] parseLine(String line, char delimeter, char quote, char escape){
//        boolean quoted;
//        boolean escaped;
//        int start;
//        int end;
//        StringBuilder sb = new StringBuilder();
//        char[] string = line.toCharArray();
//        for(int i = 0; i < string.length; ++i){
//
//        }
//
//    }

}
