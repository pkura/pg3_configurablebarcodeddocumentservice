package pl.compan.docusafe.util;

import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: SqlUtils.java,v 1.6 2010/01/06 12:16:07 tomekl Exp $
 */
public abstract class SqlUtils
{
    public static String escapeSql(String s)
    {
        if (s == null || s.indexOf('\'') < 0)
            return s;

        StringBuilder result = new StringBuilder((int) (s.length() * 1.3));
        CharacterIterator iter=new StringCharacterIterator(s);
        for (char c=iter.first(); c != CharacterIterator.DONE; c=iter.next())
        {
            if (c == '\'')
                result.append("\\'");
            else
                result.append(c);
        }
        return result.toString();
    }

    public static void bindParameters(PreparedStatement ps, List parameters,
                                List types)
        throws SQLException
    {
        for (int i=0; i< parameters.size(); i++)
        {
            Type type = (Type) types.get(i);
            Object parameter = parameters.get(i);

            if (type instanceof StringType)
            {
                ps.setString(i+1, (String) parameter);
            }
            else if (type instanceof IntegerType)
            {
                ps.setInt(i+1, ((Integer) parameter).intValue());
            }
            else if (type instanceof DateType)
            {
                ps.setDate(i+1, new java.sql.Date(((Date) parameter).getTime()));
            }
            else
            {
                throw new IllegalArgumentException("Nieznany typ parametru: "+type);
            }
        }
    }
    
    public static String questionMarks(int count)
	{
    	if(count < 1)
    		return "";
    		
		StringBuffer sb = new StringBuffer("?");
		for(int i=1;i<count;i++)
		{
			sb.append(",?");
		}
		return sb.toString();
	}
}

