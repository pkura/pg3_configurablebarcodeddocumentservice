package pl.compan.docusafe.util;

/**
 * Klasa zawierająca w sobie informacje prezentacyjne dotyczące
 * drzewa.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: HtmlTreeTheme.java,v 1.2 2008/10/23 14:35:38 pecet1 Exp $
 */
public class HtmlTreeTheme
{
    private HtmlImage expandedFolderImage;
    private HtmlImage collapsedFolderImage;

    private HtmlImage verticalLineImage;
    private HtmlImage blankImage;

    private HtmlImage lastChildlessControlImage;
    private HtmlImage middleChildlessControlImage;

    private HtmlImage lastCollapsedControlImage;
    private HtmlImage middleCollapsedControlImage;

    private HtmlImage lastExpandedControlImage;
    private HtmlImage middleExpandedControlImage;
    
    private HtmlImage userImage;

    private String linkClass;
    private String controlLinkClass;

    /**
     * Standardowy wygląd drzewa. Zawiera urle do ikonek używanych
     * przez eksploratora Windows.
     */
    public static final HtmlTreeTheme STANDARD = new HtmlTreeTheme();

    static
    {
        STANDARD.verticalLineImage = new HtmlImage("/img/vline.gif", 17, 17);
        STANDARD.blankImage = new HtmlImage("/img/blank.gif", 17, 17);
        STANDARD.expandedFolderImage = new HtmlImage("/img/folder-open.gif", 17, 17);
        STANDARD.collapsedFolderImage = new HtmlImage("/img/folder-closed.gif", 17, 17);
        STANDARD.lastChildlessControlImage = new HtmlImage("/img/childless-up.gif", 17, 17);
        STANDARD.middleChildlessControlImage = new HtmlImage("/img/childless-updown.gif", 17, 17);
        STANDARD.lastCollapsedControlImage = new HtmlImage("/img/plus-up.gif", 17, 17);
        STANDARD.middleCollapsedControlImage = new HtmlImage("/img/plus-updown.gif", 17, 17);
        STANDARD.lastExpandedControlImage = new HtmlImage("/img/minus-up.gif", 17, 17);
        STANDARD.middleExpandedControlImage = new HtmlImage("/img/minus-updown.gif", 17, 17);
        STANDARD.userImage = new HtmlImage("/img/folder-closed.gif", 17, 17);
        
        STANDARD.linkClass = "tree";
        STANDARD.controlLinkClass = "tree";
    }


    public HtmlImage getExpandedFolderImage()
    {
        return expandedFolderImage;
    }

    public HtmlImage getCollapsedFolderImage()
    {
        return collapsedFolderImage;
    }

    public HtmlImage getVerticalLineImage()
    {
        return verticalLineImage;
    }

    public HtmlImage getBlankImage()
    {
        return blankImage;
    }

    public HtmlImage getLastChildlessControlImage()
    {
        return lastChildlessControlImage;
    }

    public HtmlImage getMiddleChildlessControlImage()
    {
        return middleChildlessControlImage;
    }

    public HtmlImage getLastCollapsedControlImage()
    {
        return lastCollapsedControlImage;
    }

    public HtmlImage getMiddleCollapsedControlImage()
    {
        return middleCollapsedControlImage;
    }

    public HtmlImage getLastExpandedControlImage()
    {
        return lastExpandedControlImage;
    }

    public HtmlImage getMiddleExpandedControlImage()
    {
        return middleExpandedControlImage;
    }
    
    public HtmlImage getUserImage(){
    	return userImage;
    }

    /**
     * Nazwa klasy CSS odnośników związanych z tytułami elementów
     * drzewa.
     */
    public String getLinkClass()
    {
        return linkClass;
    }

    /**
     * Nazwa klasy odnośników związanych z obrazkami zwijającymi
     * i rozwijającymi elementu drzewa (domyślnie są to obrazki
     * przedstawiające znak '+' lub '-' w kwadracie).
     */
    public String getControlLinkClass()
    {
        return controlLinkClass;
    }
}
