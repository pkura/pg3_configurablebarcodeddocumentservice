package pl.compan.docusafe.util;

import org.joda.time.Period;
import org.joda.time.PeriodType;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.web.admin.BokAction;

import java.util.Calendar;
import java.util.Date;

/**
 * Klasa zawiera funkcje parsujace czas podane w stringu
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class TimeUtils
{
    private static final Logger log = LoggerFactory.getLogger(TimeUtils.class);
    
    /**
     * Parsuje nadmiar minut w godziny i zwraca stringa
     * @param time1
     * @return String
     */
    public static String parseMinuteToHours(String time1) throws StringIndexOutOfBoundsException, ArrayIndexOutOfBoundsException
    {
        if(!(time1 != null && time1.length() > 3))
        	throw new StringIndexOutOfBoundsException();
        
        String[] time1Array = time1.split(":");
        
        if(time1Array.length < 2)
        	throw new ArrayIndexOutOfBoundsException();
        
        return parseMinuteToHours(new Integer(time1Array[0]), new Integer(time1Array[1]));
    }
    
    /**
     * Parsuje nadmiar minut w godziny i zwraca stringa
     * @param hour int
     * @param minute int
     * @return String
     */
    public static String parseMinuteToHours(int hour, int minute)
    {
        final int maxMinute = 60;
        
        if(minute < 0)
        {
            minute = maxMinute + minute;
            hour--;
        }
        
        double addHour = Math.floor(minute/maxMinute);
        hour += addHour;
        minute = minute%maxMinute; 
        
        return TimeUtils.parseSmallValue(hour) + ":" + TimeUtils.parseSmallValue(minute);
    }
    
    /**
     * Zamienia x na Stringa "0x"
     */
    public static String parseSmallValue(int val)
    {
        if (val < 10)
          if(val < 0)
          {
            if(val <= -10)
              return String.valueOf(val);
            else
              return "-0"+ String.valueOf(Math.abs(val));
          }
          else
            return "0"+val;
        else
            return String.valueOf(val);
    }
    
   public static boolean isNegative(String time)
    {
        String[] timeArray = time.split(":");
        Integer val = 0;
        if(timeArray[0] != null)
        {
            val = new Integer(timeArray[0]);
        }

        if(val < 0)
            return true;
        else
            return false;
                       
    }
    /** 
     * sumuje czasy przechowywane w stringu i zwraca jako string
     * @param time1 String
     * @param time2 String
     * @return String
     * @exception EdmException
     */
    public static String summaryTimes(String time1, String time2) throws StringIndexOutOfBoundsException, ArrayIndexOutOfBoundsException
    {
        if(!(time1 != null && time2 !=null && time1.length() > 3 && time2.length() > 3))
        	throw new StringIndexOutOfBoundsException();
        
        String[] time1Array = time1.split(":");
        String[] time2Array = time2.split(":");
        
        if(time1Array.length < 2 || time2Array.length < 2)
        	throw new ArrayIndexOutOfBoundsException();
        
        Integer hour1 = new Integer(time1Array[0]);
        Integer hour2 = new Integer(time2Array[0]);
        Integer minute1 = new Integer(time1Array[1]);
        Integer minute2 = new Integer(time2Array[1]);
        
        Integer summaryHour = hour1 + hour2;
        Integer summaryMinute = minute1 + minute2;
        
        return TimeUtils.parseMinuteToHours(summaryHour, summaryMinute);
    }
    
    /** Sprawdza poprawno�� godziny w formacie 01:00
     * 
     */
    public static boolean checkStringTimes(String time1) throws StringIndexOutOfBoundsException, ArrayIndexOutOfBoundsException
    {
        if(!(time1 != null && time1.length() > 3))
        	throw new StringIndexOutOfBoundsException();
        
        String[] time1Array = time1.split(":");
        
        if(time1Array.length < 2)
        	throw new ArrayIndexOutOfBoundsException();
        
        return true;
    }
    
    /**
     * Odejmuje time1 od time2 i sprawdza czy jest wartosc ujemna jesli tak zwraca czas w postaci ujemnej np.
     * time1 = 00:00
     * time2 = 01:35
     * Wynik = -01:35
     * 
     * @param time1
     * @param time2
     * @return 
     */
    public static String substractTimes(String time1, String time2)
    {
        checkStringTimes(time1);
        checkStringTimes(time2);
        
        String[] time1Array = time1.split(":");
        String[] time2Array = time2.split(":");
        
        Calendar calendarO = DateUtils.getGregorianCalendar();
        Calendar calendarR = DateUtils.getGregorianCalendar();
        Calendar calendarstatic = DateUtils.getGregorianCalendar();
        
        calendarO.set(1970, 0, 1, Integer.parseInt(time1Array[0]), Integer.parseInt(time1Array[1]), 0);
        calendarR.set(1970, 0, 1, Integer.parseInt(time2Array[0]), Integer.parseInt(time2Array[1]), 0);
        calendarstatic.set(1970, 0, 1, 0, 0, 0);
        
        Date date = new Date((calendarO.getTimeInMillis() - calendarR.getTimeInMillis() - 60 * 60 * 1000));
        calendarO.setTime(date);
        
        if(calendarstatic.getTime().after(date))
        {
            Date date1 = new Date((calendarstatic.getTimeInMillis() - calendarO.getTimeInMillis() - 60*60*1000));
            calendarR.setTime(date1);
            int days = getRealTime(date1);
            int minute = calendarR.get(Calendar.MINUTE);
            int hour = days*24 - calendarR.get(Calendar.HOUR_OF_DAY);
            
            if((minute > 0) && (hour == 0))
                return "-"+parseSmallValue(hour) + ":" + parseSmallValue(minute);
                              
            return parseSmallValue(hour) + ":" + parseSmallValue(minute);
        }
        else
        {
            int days = getRealTime(date);
            int minute = calendarO.get(Calendar.MINUTE);
            int hour = days*24 + calendarO.get(Calendar.HOUR_OF_DAY);
            return parseSmallValue(hour) + ":" + parseSmallValue(minute);
        }
    }
    
    /**
     * Sprawdza czy time1 jest wiekszy od time2
     * @param time1
     * @param time2
     * @return 
     */
    public static boolean isGreater(String time1, String time2)
    {
        checkStringTimes(time1);
        checkStringTimes(time2);
        
        String[] time1Array = time1.split(":");
        String[] time2Array = time2.split(":");
        
        Calendar calendarO = DateUtils.getGregorianCalendar();
        Calendar calendarR = DateUtils.getGregorianCalendar();
        Calendar calendarstatic = DateUtils.getGregorianCalendar();
        
        calendarO.set(1970, 0, 1, Integer.parseInt(time1Array[0]), Integer.parseInt(time1Array[1]), 0);
        calendarR.set(1970, 0, 1, Integer.parseInt(time2Array[0]), Integer.parseInt(time2Array[1]), 0);
        calendarstatic.set(1970, 0, 1, 0, 0, 0);
        
        Date date = new Date((calendarO.getTimeInMillis() - calendarR.getTimeInMillis() - 60 * 60 * 1000));
        calendarO.setTime(date);
        
        if(calendarstatic.getTime().after(date) || (calendarstatic.getTime().equals(date)))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    /**
     * Zwraca r�nic� z daty od 1970-01-01
     * @param time
     * @return 
     */
    public static int getRealTime(Date time)
    {
        Calendar calendarstatic = DateUtils.getGregorianCalendar();
        calendarstatic.set(1970, 0, 1, 0, 0, 0);

        Calendar calendarO = DateUtils.getGregorianCalendar();
        calendarO.setTime(time);
        
        return DateUtils.substract(calendarstatic.getTime(), calendarO.getTime());
    }
    
    /**
     * Sprawdza granice czasu podane w stringu jesli sie nie zgadzaja wyrzuca blad. 
     * granice sa w przedziale 1 tego samego dnia
     * @param timeFrom
     * @param timeTo
     * @throws EdmException 
     */
    public static void checkTimeRange(String timeFrom, String timeTo) throws EdmException
    {
        if(!(timeFrom != null && timeTo !=null && timeFrom.length() > 3 && timeTo.length() > 3))
        	throw new EdmException("TwojaGodzinaJestBlednaPoprawnyFromatTo");
        
        String[] timeFromArray = timeFrom.split(":");
        String[] timeToArray = timeTo.split(":");
        
        if(timeFromArray.length < 2 || timeToArray.length < 2)
        	throw new EdmException("TwojaGodzinaJestBlednaPoprawnyFormatTo");
        
        Integer hourFrom = new Integer(timeFromArray[0]);
        Integer hourTo = new Integer(timeToArray[0]);
        Integer minutFrom = new Integer(timeFromArray[1]);
        Integer minutTo = new Integer(timeToArray[1]);
        
        if((hourFrom.equals(hourTo)) && (minutFrom.equals(minutTo)))
            throw new EdmException("GodzinyNieMogaBycTakieSame");
        //walidacja godzin
        if(hourFrom > 24 || hourFrom < 0 || hourTo > 24 || hourTo < 0)
                throw new EdmException("GodzinaMaFormatwPrzedziale");
        //walidacja minut
        if(minutFrom > 60 || minutFrom < 0 || minutTo > 60 || minutFrom < 0)
                throw new EdmException("MinutaMaFormatwPrzedziale");
        //godzinaOd musi byc wieksza
        if(hourFrom > hourTo || (hourTo.equals(hourFrom) && minutTo < minutFrom))
                throw new EdmException("PoleGodzinaOdMusiBycWieksze");
    }
    
    
    /**
     * Tworzy tablic� string�w z pelnymi dniami ostatni element jest reszt�
     */
    public static String[] createChargedOvertimes(String toCharge)
    {
        final double hourInDay = 24;

        checkStringTimes(toCharge);
        
        String[] time1Array = toCharge.split(":");
        String hour = time1Array[0];
        String minute = time1Array[1];
        
        int max = ((Double)Math.floor(Double.parseDouble(hour)/hourInDay)).intValue();
        String[] overtimesToCharge = new String[max+1];
        
        for(int i = 0; i <  max; i++)
            overtimesToCharge[i] = "24:00";
        
        Double hourRest = (Double)Double.parseDouble(hour)%hourInDay;
        overtimesToCharge[max] = hourRest.intValue() + ":" + minute;
        
        return overtimesToCharge;
    }

    private static StringManager sm = GlobalPreferences.loadPropertiesFile(BokAction.class.getPackage().getName(), null);

    /**
     * @return "2d 2h 2m 2s" pomijane s� pirwsze cz�ony w przypadku gdy s� r�wne 0
     */
    public static String getTimeAsString(Date dateBefore, Date dateAfter) {
        return getTimeAsString(dateBefore.getTime(), dateAfter.getTime());
    }

    /**
     * @return "2d 2h 2m 2s" pomijane s� pirwsze cz�ony w przypadku gdy s� r�wne 0
     */
    public static String getTimeAsString(long timeBefore, long timeAfter) {
        return getTimeAsString(new Period(timeBefore, timeAfter, PeriodType.dayTime()));
    }

    public static String getTimeAsString(Period period) {
        return getDayTimePeriodAsString(period);
    }

    /**
     * @return "2h 2m 2s" pomijane s� pirwsze cz�ony w przypadku gdy s� r�wne 0
     */
    public static String getTimeAsString(long timeInMillis) {
        return getTimeAsString(new Period(timeInMillis, PeriodType.dayTime()));
    }

    private static String getDayTimePeriodAsString(Period period) {
        StringBuilder builder = new StringBuilder();
        append(builder, period.getDays(), "timeFieldDays");
        append(builder, period.getHours(), "timeFieldHours");
        append(builder, period.getMinutes(), "timeFieldMinutes");
        append(builder, period.getSeconds(), "timeFieldSeconds");
        //append(builder, period.getMillis(), "timeFieldMilliseconds");
        if (builder.length()==0)
            builder.append("0 ").append(sm.getString("timeFieldSeconds"));
        return builder.toString();
    }

    private static void append(StringBuilder builder, int value, String stringManagerKey) {
        if (value > 0) {
            if (builder.length() > 0)
                builder.append(' ');
            builder.append(value).append(' ').append(sm.getString(stringManagerKey));
        }
    }

}
