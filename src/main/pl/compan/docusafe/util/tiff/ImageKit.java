package pl.compan.docusafe.util.tiff;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.awt.image.renderable.ParameterBlock;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
import javax.servlet.http.HttpServletRequest;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.LogFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.events.EventFactory;
import pl.compan.docusafe.util.ImageUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MimetypesFileTypeMap;
import pl.compan.docusafe.webwork.FormFile;

import com.asprise.util.tiff.TIFFReader;
import com.asprise.util.tiff.TIFFWriter;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.opensymphony.webwork.ServletActionContext;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

import java.util.concurrent.atomic.AtomicInteger;
 
public class ImageKit 
{
	private static Logger log = LoggerFactory.getLogger(ImageKit.class);
	public static final Integer IMPORT_KIND_GRAY_TIFF = 1;
	public static final Integer IMPORT_KIND_PDF = 2;
	
	private static AtomicInteger parseCount = new AtomicInteger(100);
	
	public static File CompressioTiff(float quality,TIFFReader tiffReader, Integer newW, Integer newH,int conversion,int compression)
	 {
		List<BufferedImage> biList = new ArrayList<BufferedImage>();
    	try
    	{
			BufferedImage bi;
			int i = tiffReader.countPages();
			for (int j = 0; j < i; j++)
			{
				RenderedImage ri = tiffReader.getPage(j);

	            bi = getBufferedImage(ri);
	            if(newH != null && newW != null)
	            	bi =  resize(bi, newW, newH);
				File outF =  File.createTempFile("tiffCon"+j, "jpg");//new File("c:/"+j+"MAR.jpg");
				OutputStream out = new FileOutputStream(outF);
				write(bi, quality, out);
				ImageIO.write(bi, "jpg", out);
				biList.add(ImageIO.read(outF));
				out.close();
				outF.delete();
								
			}
			BufferedImage[]biTab = new  BufferedImage[biList.size()];
			
			for (int j = 0; j < biTab.length; j++) 
			{
				biTab[j] = biList.get(j);
			}
			biList.clear();
			return writeTiff(conversion, compression, biTab); 
			//writeTiff(TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE, TIFFWriter.TIFF_COMPRESSION_GROUP4, biTab);
			
    	}
        catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
	} 

		
	 public static File writeTiff(int tiffConversion, int tiffCompression,BufferedImage[] biTab)throws IOException
	 {
	     File output = File.createTempFile("tiffCon", ".tif");
	     TIFFWriter.createTIFFFromImages(
	         biTab,
	         tiffConversion, tiffCompression,
	         output
	     );
	     return output;
	 }
	 public static  boolean isA3(int width, int height)
	 {
		 return width>height;
	 }
	 
	 /**
	  * 
	  * @param tiff
	  * @return
	  * @throws IOException
	  * @author tlipka
	  * @Info Metoda specyficzna dla aegon'owego ircDrivera, dokument w formacie A3 do formatu A4
	  */
	 public static File tiffA3ToA4(File tiff) throws IOException
	 {
		 File returnFile = null;
		 Boolean allPagesA4 = true;
		 TIFFReader tiffReader = new TIFFReader(tiff);		 
		 
		 for(int i=0;i<tiffReader.countPages();i++)
		 {
			 if(isA3(tiffReader.getPage(i).getWidth(),tiffReader.getPage(i).getHeight()))
				 allPagesA4 = false;
		 }
		 
		 if(allPagesA4)
			 return tiff;
		 
		 ArrayList<BufferedImage> buffedImages = new ArrayList<BufferedImage>();
		 for(int i=0;i<tiffReader.countPages();i++)
		 {
			buffedImages.add(
					ImageUtils.getBufferedImage(
							(RenderedImage) tiffReader.getPage(i)	
					)
			);
		 }
		 
		 ArrayList<BufferedImage> buffedReadyImages = new ArrayList<BufferedImage>();
		 
		 BufferedImage a3First = null;
		 BufferedImage a3Last = null;
		 
		 boolean a3odd = false;
		 
		 for(BufferedImage img : buffedImages)
		 {
			 if(!isA3(img.getWidth(),img.getHeight()))
			 {
				 buffedReadyImages.add(img);
			 }
			 else
			 {
				 BufferedImage imgLeft = img.getSubimage(
						 0,0,img.getWidth()/2,img.getHeight()
				 );
				 BufferedImage imgRight = img.getSubimage(
								 img.getWidth()/2,0,img.getWidth()/2,img.getHeight() 
				 );
				 if (a3odd) {
					 a3odd = false;
					 buffedReadyImages.add(a3First);
					 buffedReadyImages.add(imgLeft);
					 buffedReadyImages.add(imgRight);
					 buffedReadyImages.add(a3Last);
				 } else {
					 a3odd = true;
					 a3Last = imgLeft;
					 a3First = imgRight;
				 }
				 
			 }
		 }
		 
		 returnFile = File.createTempFile("imageKit", "tiff");
		 returnFile.deleteOnExit();
		 
		 BufferedImage[] bia = buffedReadyImages.toArray(new BufferedImage[buffedReadyImages.size()]);
         
		 TIFFWriter.createTIFFFromImages(bia,returnFile);
		 
		 return returnFile;
	 }
	 
	 
	 public static void saveInSpecifiedFormat(ArrayList biList, String path,boolean toA4) throws EOFException
	{
 
		  
		try {
						
			int a4Pages =0;
			for(int i=0; i<biList.size();i++)
			{
				RenderedImage img= (RenderedImage) biList.get(i);
				if(toA4&& isA3(img.getWidth(),img.getHeight()))
				{
					a4Pages++;
				}
				a4Pages++;
			}
			int pageCounter=0;
			BufferedImage[]biTab = new  BufferedImage[a4Pages];
			boolean firstA3 = true;
			for(int i=0; i<biList.size();i++)
			{
		
					RenderedImage img= (RenderedImage) biList.get(i);
					BufferedImage bufferedImg = ImageUtils.getBufferedImage(img);
					
					if(toA4 && isA3(img.getWidth(),img.getHeight()))
					{
						/*if(bufferedImg.getWidth()>bufferedImg.getHeight())
						{*/
							if(firstA3)
							{
						    	
						    	RenderedImage imgNext=null;
						    	if(i<biList.size()-1)
						    		imgNext = (RenderedImage) biList.get(i+1);
						    	if(imgNext!=null && isA3(imgNext.getWidth(),imgNext.getHeight())){
						    		biTab[pageCounter+3]=bufferedImg.getSubimage(0,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
									biTab[pageCounter]=bufferedImg.getSubimage(bufferedImg.getWidth()/2,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
						    	}
						    	else{
						    		biTab[pageCounter+1]=bufferedImg.getSubimage(0,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
						    		biTab[pageCounter]=bufferedImg.getSubimage(bufferedImg.getWidth()/2,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
						    		pageCounter+=2;
						    	}
								
							}
							else
							{
								biTab[pageCounter+1]=bufferedImg.getSubimage(0,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
								biTab[pageCounter+2]=bufferedImg.getSubimage(bufferedImg.getWidth()/2,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
								pageCounter+=4;
							}
							
							firstA3 = !firstA3;
						//}
						/*else
						{ 

							AffineTransform tx = new AffineTransform();
						    tx.rotate(-Math.PI/2,bufferedImg.getWidth()/2,bufferedImg.getHeight()/2);
						    int translx=(bufferedImg.getHeight()+bufferedImg.getWidth())/2-img.getHeight();
						    int transly=-bufferedImg.getWidth()+(bufferedImg.getHeight()+bufferedImg.getWidth())/2;
						    tx.translate(-translx,transly);
						    AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
						    bufferedImg = op.filter(bufferedImg, null);
														 
						    if(firstA3)
							{
						    	
						    	RenderedImage imgNext=null;
						    	if(i<biList.size()-1)
						    		imgNext = (RenderedImage) biList.get(i+1);
						    	if(imgNext!=null && isA3(imgNext.getWidth(),imgNext.getHeight())){
						    		biTab[pageCounter+3]=bufferedImg.getSubimage(0,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
									biTab[pageCounter]=bufferedImg.getSubimage(bufferedImg.getWidth()/2,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
						    	}
						    	else{
						    		biTab[pageCounter]=bufferedImg.getSubimage(0,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
						    		biTab[pageCounter+1]=bufferedImg.getSubimage(bufferedImg.getWidth()/2,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
						    		pageCounter+=2;
						    	} 
								 
							}
							else
							{
								biTab[pageCounter+2]=bufferedImg.getSubimage(0,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
								biTab[pageCounter+1]=bufferedImg.getSubimage(bufferedImg.getWidth()/2,0,bufferedImg.getWidth()/2,bufferedImg.getHeight());
								pageCounter+=4;
							}
							
							firstA3 = !firstA3;
							//biTab[pageCounter++]=bufferedImg.getSubimage(0,0,img.getWidth(),img.getHeight()/2);
							//biTab[pageCounter++]=bufferedImg.getSubimage(0,img.getHeight()/2,img.getWidth(),img.getHeight()/2);
						}
						*/
					}
					else{
						
						firstA3 = true;
						biTab[pageCounter++]=bufferedImg;
					}  
				
			}
			String filePath =path;
			if(filePath.endsWith(".tif"))
				filePath= filePath.substring(0, filePath.length()-4);
			
			TIFFWriter.createTIFFFromImages(biTab,new File(filePath+".tif"));
			 
		}
		catch (IOException e) {
			LogFactory.getLog("eprint").debug("", e);
		}
	}
	public static void write(BufferedImage image, float quality, OutputStream out) throws IOException
    {
        ImageWriter writer = ImageIO.getImageWritersBySuffix("jpg").next();
        ImageOutputStream ios = ImageIO.createImageOutputStream(out);
        writer.setOutput(ios);
        ImageWriteParam param = writer.getDefaultWriteParam();
        if (quality >= 0) {
            param.setCompressionMode(ImageWriteParam.MODE_DEFAULT);
            param.setCompressionQuality(quality);
        }
        writer.write(null, new IIOImage(image, null, null), param);
        ios.close();
        writer.dispose();
    }
		    
    public static BufferedImage resize(BufferedImage img, int newW, int newH)
    {  
		    int w = img.getWidth();  
		    int h = img.getHeight();  
		    BufferedImage dimg = new BufferedImage(newW, newH, img.getType());  
		    Graphics2D g = dimg.createGraphics();  
		    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);  
		    g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);  
		    
		    g.dispose();  
		    return dimg;  
    }  
		    
    public static BufferedImage getBufferedImage(RenderedImage ri)
    {
        if (ri instanceof BufferedImage)
            return (BufferedImage) ri;

        ColorModel cm = ri.getColorModel();
        WritableRaster raster = cm.createCompatibleWritableRaster(ri.getWidth(), ri.getHeight());
        BufferedImage bi = new BufferedImage(cm, raster,cm.isAlphaPremultiplied(), null);
        return bi;
    }
    
    /***
     * Dzieli za��czniki na strony (obs�uguje obrazy i pdf) zapisuje je w katalogu tempFile
     * Zwraca mape Sciezka do strony,Nazwa
     * @param multiFiles
     * @param tempFile
     * @return
     * @throws Exception
     */
    public static Map<String,String> splitAttachments(List<FormFile> multiFiles,String tempFile) throws Exception
    {
    	return splitAttachments(multiFiles, tempFile, false);
    }
    public static Map<String,String> splitAttachments(List<FormFile> multiFiles,String tempFile, Boolean generateNumber) throws Exception
    {
        Map<String,String> files = new TreeMap<String, String>();
    	SeekableStream s = null;
    	String sesId = null;
        if (multiFiles != null && multiFiles.size()>0)
        {
        	int j = 1;
        	
        	if(generateNumber)
        	{
        		Random r = new Random(54355);
        		sesId = ""+parseCount.getAndIncrement() +""+ Math.abs(r.nextLong()) +""+ new Date().getTime();        		
        	}
        	else
        	{
        		HttpServletRequest r = ServletActionContext.getRequest();
                sesId = r.getRequestedSessionId()+ new Date().getTime();
        	}
        		
        	
        	for (Iterator iterator = multiFiles.iterator(); iterator.hasNext();) 
        	{
        		
        		int startJ = j;
				FormFile formFile = (FormFile) iterator.next();
				String mime = MimetypesFileTypeMap.getInstance().getContentType(formFile.getFile().getName());
				log.info("Typ pliku do podzialu: " + mime);
				if(mime.startsWith("image/tiff"))
				{
                    s = new FileSeekableStream(formFile.getFile());
                    ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, null);
                    Map<String,String> filesTmp = new TreeMap<String, String>();                    
                    if (dec != null)
                    {
                    	log.info("Tniemy plik image/tiff posiadajacy stron: " + dec.getNumPages());
                        for (int i = 1; i <= dec.getNumPages(); i++)
                        {                        	
                            files.put(tempFile+"\\" + sesId + String.format("%04d", j), "Strona nr."+j);
                            filesTmp.put(tempFile+"\\" + sesId + String.format("%04d", j), "Strona nr."+j);
                            j++;
                        }
                        if (generateNumber) 
                        {
                        	splitTIFF(dec, filesTmp);
						}
                        else
                        {
	                        Thread thread = new Thread(new SplitTIFF(dec,filesTmp));
	                        thread.start();
                        }
                    }
				}
				else if(ImageUtils.canConvertToTiff(mime))
				{
                    String fileName = tempFile+"/" + sesId + String.format("%04d", j);
					files.put(fileName, "Strona nr."+j);
                    j++;                    
                    String repl = fileName.replace("\\","/");
                    File tmp = new File(repl+".png");
                    FileOutputStream os = new FileOutputStream(tmp);
                    FileInputStream stream = new FileInputStream(formFile.getFile());
                    byte[] buf = new byte[2048];
                    int count;
                    while ((count = stream.read(buf)) > 0)
                    {
                        os.write(buf, 0, count);
                    }
                    os.close();
				}
				else if(mime.endsWith("application/pdf"))
				{
					RandomAccessFile raf;
	    			raf = new RandomAccessFile(formFile.getFile(), "r");
	    			FileChannel channel = raf.getChannel();
	    			ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
	    			PDFFile pdffile = null;
	    			try
	    			{
	    				pdffile = new PDFFile(buf);	    	
	    			}
	    			catch (com.sun.pdfview.PDFParseException e) 
	    			{
	    				//jesli nie udalo sie odczytac pliku, bo np jest zle sformatowany to tworzymy nowego 
	    				channel.close();
	    				raf.close();
	    				raf = null;
	    				channel = null;
	    				PdfReader reader = new PdfReader(formFile.getFile().getAbsolutePath());
						int n = reader.getNumberOfPages();
						int i = 0;         
						com.lowagie.text.Document document = new com.lowagie.text.Document(reader.getPageSizeWithRotation(1));
						File outFile = File.createTempFile(sesId+formFile.getFile().getName(), "pdf");
						PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
						document.open();
						while ( i < n ) {
						    
						    PdfImportedPage page = writer.getImportedPage(reader, ++i);
						    writer.addPage(page);
						    
						}
						document.close();
						writer.close();
		    			raf = new RandomAccessFile(outFile, "r");
		    			channel = raf.getChannel();
		    			buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
		    			pdffile = new PDFFile(buf);	
					}
	    			
	    			List<String> filesTmp = new ArrayList<String>();
	    			int num=pdffile.getNumPages();
	    			for(int i=0;i<num;i++)
	    			{
	    				 files.put(tempFile+"/" + sesId + String.format("%04d", j), "Strona nr."+j);
                         filesTmp.add(tempFile+"/" + sesId + String.format("%04d", j));
                         j++;
	    			}
	    			if (generateNumber) 
                    {
                    	splitPdf(pdffile,filesTmp,raf,channel);
					}
                    else
                    {
		    			Thread thread = new Thread(new SplitPdf(pdffile,filesTmp,raf,channel));
	                    thread.start();   
                    }
                    buf.clear();
				}
				else
				{
					throw new EdmException("Nie mo�na sformatowa� pliku "+ formFile.getFile().getName()+" do tiff.");
				}
				//zapisuje orginal do folderu orginal
				File orginalF = new File(tempFile+"/orginal");
				orginalF.mkdirs();
				File tmp = new File(tempFile+"/orginal/"+sesId+"-"+startJ+"-"+j+"_"+formFile.getFile().getName());
                FileOutputStream os = new FileOutputStream(tmp);
                FileInputStream stream = new FileInputStream(formFile.getFile());
                byte[] buf = new byte[1024];
                int count;
                while ((count = stream.read(buf)) > 0)
                {
                    os.write(buf, 0, count);
                }
                os.close();
        	}
        }
    	return files;
    }
    
    private static void splitPdf(PDFFile pdffile, List<String> files, RandomAccessFile raf, FileChannel channel) 
    {
			try
			{
				int num=pdffile.getNumPages();
				for(int i=1;i<=num;i++)
				{
					PDFPage page = pdffile.getPage(i);		
					
					int width, height;
					float ratio = 1;
					if(page.getWidth()<1500)
						ratio = 1500/page.getWidth();
					width=new Double(page.getWidth()*(ratio)).intValue();
					height=new Double(page.getHeight()*(ratio)).intValue();
					
					
					BufferedImage im = (BufferedImage)page.getImage(width,height,null,null,true,true);
					String element = files.get(i-1);
                    String repl = element.replace("\\","/");
                    File tmp = new File(repl+".png");
                    FileOutputStream os = new FileOutputStream(tmp);
                    ParameterBlock pb = new ParameterBlock();
                    pb.addSource(im);
                    pb.add(os);
                    pb.add("png");
                    RenderedOp op = JAI.create("encode", pb);
                    forceLoad(op);
                    op.dispose();
                    os.close();
					//ImageIO.write(img, "png", new File(repl+".png"));
				}				
				channel.close();
				raf.close();
				
				pdffile = null;
				files = null;
				raf = null;
				channel = null;				
			} 
			catch (IOException e) 
			{
				LoggerFactory.getLogger(ImageKit.class).error("",e);
			}
		
    }
    
    public static void splitTIFF(ImageDecoder dec,Map<String,String> files)
    {
    	try
    	{
	    	String format = "png";
	    	Set<String> set = files.keySet();
	        int i=0;
	        for (Iterator<String> iter = set.iterator(); iter.hasNext();)
	        {
	            String element = (String) iter.next();
	            String repl = element.replace("\\","/");
	            File tmp = new File(repl+"."+format);
	            RenderedImage im = dec.decodeAsRenderedImage(i);
	            FileOutputStream os = new FileOutputStream(tmp);
	            ParameterBlock pb = new ParameterBlock();
	            pb.addSource(im);
	            pb.add(os);
	            pb.add(format);
	            RenderedOp op = JAI.create("encode", pb);
	            forceLoad(op);
	            op.dispose();
	            os.close();
	            i++;
	        }
		} 
		catch (IOException e) 
		{
			LoggerFactory.getLogger(ImageKit.class).error("",e);
		}
    }
    
    private static class SplitPdf implements Runnable
    {
		private PDFFile pdffile;
    	private List<String> files;
    	private RandomAccessFile raf;
    	private FileChannel channel;
    	
    	public SplitPdf(PDFFile pdffile, List<String> files, RandomAccessFile raf, FileChannel channel) {
			super();
			this.pdffile = pdffile;
			this.files = files;
			this.raf = raf;
			this.channel = channel;
		}
    	
		public void run() 
		{
			try
			{
				int num=pdffile.getNumPages();
				for(int i=1;i<=num;i++)
				{
					PDFPage page = pdffile.getPage(i);		
					
					int width, height;
					float ratio = 1;
					if(page.getWidth()<1500)
						ratio = 1500/page.getWidth();
					width=new Double(page.getWidth()*(ratio)).intValue();
					height=new Double(page.getHeight()*(ratio)).intValue();
					
					
					BufferedImage im = (BufferedImage)page.getImage(width,height,null,null,true,true);
					String element = files.get(i-1);
                    String repl = element.replace("\\","/");
                    File tmp = new File(repl+".png");
                    FileOutputStream os = new FileOutputStream(tmp);
                    ParameterBlock pb = new ParameterBlock();
                    pb.addSource(im);
                    pb.add(os);
                    pb.add("png");
                    RenderedOp op = JAI.create("encode", pb);
                    forceLoad(op);
                    op.dispose();
                    os.close();
					//ImageIO.write(img, "png", new File(repl+".png"));
				}				
				channel.close();
				raf.close();
				
				this.pdffile = null;
				this.files = null;
				this.raf = null;
				this.channel = null;				
			} 
			catch (IOException e) 
			{
				LoggerFactory.getLogger(ImageKit.class).error("",e);
			}
		}
    }
   
    
    
    private static class SplitTIFF implements Runnable
    {
        ImageDecoder dec;
        private Map<String,String> files;
        final String format = "png";
        
        public SplitTIFF(ImageDecoder d,Map<String,String> f)
        {
               this.dec = d;
               this.files = f;
        }
        public void run()
        {
            try
            {    
                Set<String> set = files.keySet();
                int i=0;
                for (Iterator<String> iter = set.iterator(); iter.hasNext();)
                {
                    String element = (String) iter.next();
                    String repl = element.replace("\\","/");
                    File tmp = new File(repl+"."+format);
                    RenderedImage im = dec.decodeAsRenderedImage(i);
                    FileOutputStream os = new FileOutputStream(tmp);
                    ParameterBlock pb = new ParameterBlock();
                    pb.addSource(im);
                    pb.add(os);
                    pb.add(format);
                    RenderedOp op = JAI.create("encode", pb);
                    forceLoad(op);
                    op.dispose();
                    os.close();
                    i++;
                }
            }
            catch (IOException e)
            {
            	LoggerFactory.getLogger(ImageKit.class).error("",e);
            }           
        }
    }
    
    private static void forceLoad(RenderedImage op)
    {
        if (op instanceof RenderedOp)
        {
            ((RenderedOp) op).getRendering();
        }

        int minX = op.getMinTileX();
        int minY = op.getMinTileY();
        int maxX = minX + op.getNumXTiles();
        int maxY = minY + op.getNumYTiles();

        for (int y = minY; y < maxY; y++)
        {
            for (int x = minX; x < maxX; x++)
            {
                op.getTile(x, y);
            }
        }
    }
    
    /***
     *  Dodaje event dodajacy zalacznik z podanych stron do dokumentu/zalacznika
     * @param pages 
     * @param obj ({@link Document} lub {@link Attachment})
     * @throws Exception
     */
    public static void addAttachments(String[] pages, Object obj,Integer kind, Boolean attachLastRevision) throws Exception
    {
    	 addAttachments(pages,obj,kind,attachLastRevision);
    }

    /**
     * Dodaje podzielone tiffy do <code>obj</code>
     * @param pages
     * @param obj Mo�e by� {@link Document}, tablica {@link Document} lub {@link Attachment}
     * @param kind
     * @throws Exception
     */
    public static void addAttachments(String[] pages, Object obj,Integer kind) throws Exception
    {
    	if(kind == null)
    		kind = IMPORT_KIND_GRAY_TIFF;
    	if(obj instanceof Document)
    	{
            Document doc = (Document) obj;
            List<Long> docIds = Arrays.asList(doc.getId());
            addAttachments(pages, docIds, kind);
    	}
        else if(obj instanceof Document[])
        {
            Document[] docs = (Document[]) obj;
            List<Long> docIds = Lists.newArrayList();
            for(Document doc : docs) {
                docIds.add(doc.getId());
            }
            addAttachments(pages, docIds, kind);
        }
    	else if(obj instanceof Attachment)
    	{
    		addRevision(pages, ((Attachment)obj).getId(),kind,false);
    	}
    	else
    	{
    		throw new EdmException("B��d dodania za��cznika");
    	}
    }

    /***
     *  Dodaje event dodajacy zalacznik z podanych stron do dokumentu
     * @param pages
     * @param docId
     * @param kind 
     * @throws Exception
     */
    public static void addAttachments(String[] pages, List<Long> docId, Integer kind) throws Exception
    {

    	PreparedStatement ps = null;
    	PreparedStatement ps1 = null;
    	ResultSet rs = null;
		try
		{
			Long importId = null;
			ps = DSApi.context().prepareStatement("select dl_import_attachment_id.nextval from dual");
			rs = ps.executeQuery();
			if(rs.next())
				importId = rs.getLong(1);
			else
				throw new EdmException("B��d zapisu");
            if(!DSApi.context().isTransactionOpen())
			    DSApi.context().begin();
	    	for (int i = 0; i < pages.length; i++) 
			{
	    		
				ps1 = DSApi.context().prepareStatement("insert into dl_import_attachment(document_id,attachment_name,import_id,username,importKind)values(?,?,?,?,?)");
				ps1.setLong(1, docId.get(0));
				ps1.setString(2, pages[i]);
				ps1.setLong(3, importId);
				ps1.setString(4, DSApi.context().getPrincipalName());
				ps1.setInt(5, kind);
				ps1.execute();
				DSApi.context().closeStatement(ps1);
			}
	    	//DSApi.context().begin();
            HandlerParameters params = new HandlerParameters(docId, importId);
            String eventParams = new Gson().toJson(params);
            EventFactory.registerEvent("immediateTrigger", "AddAttachmentsToDocument", eventParams, null, new Date());
	    	DSApi.context().commit();	    	
		}
		catch (Exception e) 
		{
			throw new EdmException(e.getMessage(),e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);		
			DSApi.context().closeStatement(ps1);	
		}
	}
    
    /**
     * Dodaje nowa wersje zalacznika z podanych stron (Sciezki do obrazow), uruchamia event dodajacy wersje
     * @param pages
     * @param attachmentId
     * @param kind 
     * @throws Exception
     */
    public static void addRevision(String[] pages, Long attachmentId, Integer kind, Boolean attachFiles) throws Exception
    {
    	PreparedStatement ps = null;
    	PreparedStatement ps1 = null;
    	ResultSet rs = null;
		try
		{
			Long importId = null;
			ps = DSApi.context().prepareStatement("select dl_import_attachment_id.nextval from dual");
			rs = ps.executeQuery();
			if(rs.next())
				importId = rs.getLong(1);
			else
				throw new EdmException("B��d zapisu");
			DSApi.context().begin();
	    	for (int i = 0; i < pages.length; i++)
			{	    			    		
				ps1 = DSApi.context().prepareStatement("insert into dl_import_attachment(attachment_id,attachment_name,import_id,username,importKind)values(?,?,?,?,?)");
				ps1.setLong(1, attachmentId);
				ps1.setString(2, pages[i]);
				ps1.setLong(3, importId);
				ps1.setString(4, DSApi.context().getPrincipalName());
				ps1.setInt(5, kind);
				ps1.execute();
				DSApi.context().closeStatement(ps1);
			}	    
	    	EventFactory.registerEvent("immediateTrigger", "AddRevisionToAttachment", attachmentId+"|"+importId+"|"+attachFiles, null, new Date());
	    	DSApi.context().commit();
		}
		catch (Exception e) 
		{
			throw new EdmException(e.getMessage(),e);
		}
		finally
		{
			DbUtils.closeQuietly(rs);
			DSApi.context().closeStatement(ps);
			DSApi.context().closeStatement(ps1);
		}
	}
    
    public static File createTiffFromPages(List<String> pages) throws IOException
	{
			Tiff tiff = new Tiff();
			for (String page : pages)
			{
				String repl = page.replace("\\", "/");
				log.info("Wczytuje strone tiff " + repl);
				if (repl != null && repl.length() > 0)
				{
					File ft = new File(repl + ".png");
					System.out.println(ft.getAbsolutePath());
					if (ft.exists())
					{
						log.trace("Strona istnieje " + repl);
						tiff.add(ft, "image/png");
					}
					else
					{
						throw new IOException("Brak strony do generowania za��cznika o nazwie: " + page);
					}
				}
			}

			File output = null;
			output = tiff.write(TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE, TIFFWriter.TIFF_COMPRESSION_GROUP4);
			return output;
	}

    public static class HandlerParameters {
        public HandlerParameters() {}
        public HandlerParameters(List<Long> documentId, Long importId) {
            this.documentId = documentId;
            this.importId = importId;
        }
        private List<Long> documentId;
        private Long importId;

        public List<Long> getDocumentId() {
            return documentId;
        }

        public Long getImportId() {
            return importId;
        }
    }
}

