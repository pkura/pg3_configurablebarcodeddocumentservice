package pl.compan.docusafe.util.tiff;

import com.asprise.util.tiff.TIFFReader;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
/* User: Administrator, Date: 2005-11-08 13:15:59 */

/**
 * Klasa stanowi kolekcj� plik�w graficznych.  Rozwi�zuje
 * ona problem z klas� TIFFWriter, kt�ra wymaga, aby wszystkie
 * pliki dodawane do wielostronicowego tiffa by�y przekazane
 * jednocze�nie w tablicy obiekt�w BufferedImage.  Dla wi�kszych
 * plik�w oznacza to du�e zu�ycie pami�ci.
 * <p>
 * Pliki dodawane do tej klasy s� reprezentowane jako obiekty
 * BufferedImageProxy, kt�re �aduj� obraz przy pierwszym wywo�aniu
 * dowolnej metody.  Przed ka�d� inicjalizacj� nowego obrazu,
 * poprzednio u�ywany obraz jest zwalniany, w ten spos�b w pami�ci
 * znajduje si� jednocze�nie tylko jeden obraz.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DisposableBufferedImageList.java,v 1.5 2006/02/20 15:42:33 lk Exp $
 */
public class DisposableBufferedImageList
{
    private List proxies = new LinkedList();

    private BufferedImageProxy currentProxy;

    public void add(File file, String mime) throws IOException
    {
        if (mime.startsWith("image/tiff"))
        {
            // sprawdzenie, czy plik ma wi�cej ni� jedn� stron�,
            // dodanie kilku obiekt�w BufferedImageProxy
            TIFFReader tiffReader = new TIFFReader(file);
            int pageCount = tiffReader.countPages();
            if (pageCount == 1)
            {
                proxies.add(new BufferedImageProxy(this, file, mime));
            }
            else
            {
                for (int i=0; i < pageCount; i++)
                {
                    proxies.add(new BufferedImageProxy(this, file, mime, i));
                }
            }
        }
        else
        {
            proxies.add(new BufferedImageProxy(this, file, mime));
        }
    }

    /**
     * Ka�dy obiekt BufferedImageProxy wywo�uje t� metod�
     * w metodzie initProxy() - w tym momencie poprzedni
     * obiekt BufferedImageProxy mo�e zosta� zwolniony.
     */
    void notifyProxyInitialized(BufferedImageProxy proxy)
    {
        if (currentProxy != null)
        {
            currentProxy.dispose();
            currentProxy = null;
        }
        currentProxy = proxy;
    }

    public BufferedImage[] toArray()
    {
        return (BufferedImage[]) proxies.toArray(new BufferedImage[proxies.size()]);
    }
}
