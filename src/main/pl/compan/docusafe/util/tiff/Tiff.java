package pl.compan.docusafe.util.tiff;

import java.io.File;
import java.io.IOException;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.LoggerFactory;

import com.asprise.util.tiff.TIFFWriter;

/**
 * Klasa tworzy wielostronicowy plik tiff na podstawie listy
 * przekazanych plik�w graficznych.  W danym momencie do pami�ci
 * za�adowany jest tylko jeden plik graficzny.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Tiff.java,v 1.8 2009/09/15 11:09:31 mariuszk Exp $
 */
public class Tiff
{
    private DisposableBufferedImageList list = new DisposableBufferedImageList();

    public void add(File file, String mime) throws IOException
    {
        list.add(file, mime);
    }

    /***
     * Jesli w adds jest tiffConversion.toGrayscale = true to i tak
     * skonwertuje do skali szarosci ignorujac parametr tiffConversion
     * @param tiffConversion
     * @param tiffCompression
     * @return
     * @throws IOException
     */
    public File write(int tiffConversion, int tiffCompression) throws IOException
    {
    	if("true".equals(Docusafe.getAdditionProperty("tiffConversion.toGrayscale")))
    	{
    		tiffConversion = TIFFWriter.TIFF_CONVERSION_TO_GRAY;
    		tiffCompression = TIFFWriter.TIFF_COMPRESSION_PACKBITS;
    	}
    	if("true".equals(Docusafe.getAdditionProperty("tiff.reverseColorDuringConversion"))) 
    	{
    		TIFFWriter.reverseColorDuringConversion = true;
    	}
    	
        File output = File.createTempFile("docusafe_", ".tif");
        TIFFWriter.createTIFFFromImages(
            list.toArray(),
            tiffConversion, tiffCompression,
            output
        );
        return output;
    }
}
