package pl.compan.docusafe.util;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Constants.java,v 1.1.1.1 2003/12/02 12:46:26 administrator Exp $
 */
public final class Constants
{
    public static final String Package;
    static
    {
        Package = Constants.class.getPackage().getName();
    }
}
