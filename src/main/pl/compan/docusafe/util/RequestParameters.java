package pl.compan.docusafe.util;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Klasa przechowuj�ca zbi�r parametr�w (analogicznie do HttpServletRequest).
 * Parametry mog� by� modyfikowane oraz pobrane w postaci ci�gu zakodowanego
 * w Base64.
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: RequestParameters.java,v 1.3 2006/02/20 15:42:31 lk Exp $
 */
public class RequestParameters
{
    private String[] declaredParameters;
    private Map parameters = new HashMap();

    public RequestParameters(String[] declaredParameters)
    {
        Arrays.sort(declaredParameters);
        this.declaredParameters = declaredParameters;
    }

    /**
     * Dodaje parametry pobrane z ci�gu zakodowanego w Base64.
     * @param base64
     */
    public void updateBase64(String base64)
    {
        String upackedEx = base64 != null ?
            new String(Base64.decode(base64.getBytes())) : "";

        String[] pairs = StringUtils.split(upackedEx, "&");

        if (pairs != null && pairs.length > 0)
        {
            for (int i=0, n=pairs.length; i < n; i++)
            {
                String[] pv = StringUtils.split(pairs[i], "=", 2);
                if (pv != null && pv.length == 2)
                {
                    String param = pv[0];
                    String value = pv[1];

                    addParameterValue(param, value);
                }
            }
        }
    }

    /**
     * Zwraca warto�� parametru lub null. Je�eli parametr mia�
     * kilka warto�ci, zwracana jest pierwsza z nich.
     */
    public String getParameter(String name)
    {
        String[] values = (String[]) parameters.get(name);
        if (values != null && values.length > 0)
        {
            return values[0];
        }
        return null;
    }

    public String[] getParameterValues(String name)
    {
        return (String[]) parameters.get(name);
    }

    /**
     * Zwraca warto�� parametru jako liczb� typu Long. Je�eli
     * parametr jest nieobecny lub podczas jego parsowania
     * wyst�pi b��d, zwracana jest warto�� null.
     */
    public Long getLong(String name)
    {
        String value = getParameter(name);
        if (value == null)
            return null;

        try
        {
            return new Long(Long.parseLong(value));
        }
        catch (NumberFormatException e)
        {
            //return new Long(0);
            return null;
        }
    }

    public void setParameter(String name, String value)
    {
        if (Arrays.binarySearch(declaredParameters, name) < 0)
            throw new IllegalArgumentException("Nieznana nazwa parametru: "+name);

        parameters.put(name, new String[] { value });
    }

    public void addParameterValue(String name, String value)
    {
        if (Arrays.binarySearch(declaredParameters, name) < 0)
            throw new IllegalArgumentException("Nieznana nazwa parametru: "+name);

        String[] values = (String[]) parameters.get(name);

        if (values != null)
        {
            String[] tmp = new String[values.length+1];
            System.arraycopy(values, 0, tmp, 0, values.length);
            tmp[tmp.length-1] = value;
            parameters.put(name, tmp);
        }
        else
        {
            setParameter(name, value);
        }
    }

    /**
     * Zwraca wszystkie parametry zawarte w tym obiekcie jako ci�g
     * zakodowany w Base64.
     * <p>
     * Uwaga: je�eli warto�� zwracana przez t� funkcj� ma by� u�yta
     * jako parametr w wywo�aniu typu GET, powinna zosta� dodatkowo
     * zakodowana przez URLEncoder.encode(), poniewa� mo�e zawiera�
     * znaki '='.
     */
    public String getBase64()
    {
        StringBuilder string = new StringBuilder();

        boolean first = true;
        for (Iterator iter=parameters.keySet().iterator(); iter.hasNext(); )
        {
            String key = (String) iter.next();
            String[] values = (String[]) parameters.get(key);

            for (int i=0, n=values.length; i < n; i++)
            {
                if (!first)
                    string.append('&');
                string.append(key);
                string.append('=');
                string.append(values[i]);
            }

            first = false;
        }

        return new String(Base64.encode(string.toString().getBytes()));
    }
}
