/**
 * 
 */
package pl.compan.docusafe.util;

public class HtmlImage
{
    private String src;
    private int width;
    private int height;

    public HtmlImage(String src, int width, int height)
    {
        this.src = src;
        this.width = width;
        this.height = height;
    }

    public String getSrc()
    {
        return src;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
}