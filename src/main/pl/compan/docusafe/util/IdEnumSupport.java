package pl.compan.docusafe.util;

import com.google.common.base.Function;

import java.util.HashMap;
import java.util.Map;

/**
 * Klasa u�atwiaj�ca mapowanie enum�w po wybranym atrybucie
 * (zapisywanie warto�ci zwracanych przez ordinal() lub name() uznawane jest za z�� praktyk�)
 *
 * Taki enum musi dziedziczy� po IdEntity
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public final class IdEnumSupport<K, V extends Enum & IdEntity<K>> implements Function<K, V> {

    private final Map<K, V> map = new HashMap<K, V>();

    private IdEnumSupport(){}

    public static <K,V extends Enum & IdEntity<K>> IdEnumSupport<K,V> create(Class<V> clazz){
        IdEnumSupport<K,V> ret = new IdEnumSupport<K,V>();
        for(IdEntity<K> ent : clazz.getEnumConstants()){
            ret.map.put(ent.getId(), (V)ent);
        }
        return ret;
    }

    public V get(K id){
        return map.get(id);
    }

    @Override
    public V apply(K from) {
        return get(from);
    }
}
