package pl.compan.docusafe.util;

import java.io.File;
import java.io.FileOutputStream;
import java.text.MessageFormat;
import java.util.Properties;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.GlobalPreferences;

public class BundleStringManager extends StringManager 
{
	private static final Logger log = LoggerFactory.getLogger(BundleStringManager.class);
	private static Properties misingProperties;
	
	static
	{
		misingProperties = new Properties();
	}
	
	public BundleStringManager()
	{
		this.defaultBundle = GlobalPreferences.resourceBundleMap.get(Docusafe.getCurrentLanguage());
	}
	
	
	public String get(String key)
	{
		return getString(key);
	}

	
	public String getString(String key)
	{

		try
		{
			if (homeLocalStringsProperties.containsKey(key))
			{
				return (String) homeLocalStringsProperties.getProperty(key);
			}
			
			return this.defaultBundle.getString(key);
		}
		catch (Exception e) 
		{
			if(log.isTraceEnabled())
				log.trace(e.getMessage(),key);
			if(key == null)
				return "";
			if(!misingProperties.contains(String.valueOf(key)) && AvailabilityManager.isAvailable("dropMisingProperties"))
				misingProperties.put(key, key);
			
			return key;
		}
	}
	
	public String getString(String key, Object[] args)
	{
		return MessageFormat.format(this.getString(key), args);
	}
	
	public String getString(String key, Object arg)
	{
		return this.getString(key, new Object[] { arg });
	}
	
	public String getString(String key, Object arg1, Object arg2)
	{		
		return this.getString(key, new Object[] { arg1, arg2 });
	}

	public static void dropMissingProperties() 
	{
		LoggerFactory.getLogger("tomekl").debug("drop");
		try
		{
			misingProperties.store(new FileOutputStream(new File(Docusafe.getHome().getAbsolutePath()+"/"+"missing.properties")), "misingProperties");
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	
	
	/*public String getString(String key, Object... args)
	{
		return MessageFormat.format(this.getString(key), args);
	}*/
}
