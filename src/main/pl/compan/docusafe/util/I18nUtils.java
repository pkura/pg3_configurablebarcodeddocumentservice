package pl.compan.docusafe.util;

import pl.compan.docusafe.core.GlobalPreferences;

/**
 * Klasa ułatwiająca wypisywanie wielojęzycznych stringów,
 *
 * najlepiej importować statycznie
 *
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
public final class I18nUtils {
    private static final StringManager sm = GlobalPreferences.loadPropertiesFile(I18nUtils.class.getPackage().getName(),null);

    public static String string(String key){
        return sm.getString(key);
    }

}
