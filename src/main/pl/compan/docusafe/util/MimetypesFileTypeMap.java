package pl.compan.docusafe.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Klasa podaj�ca typ MIME pliku na podstawie jego nazwy. Korzysta
 * z {@link javax.activation.MimetypesFileTypeMap} do pobierania
 * typ�w MIME z pliku docusafe.mime.types znajduj�cego si� w CLASSPATH
 * w domy�lnym pakiecie. Je�eli typ MIME pliku jest nieznany, zwracana
 * jest warto�� pola {@link #DEFAULT_MIME_TYPE} (application/octet-stream).
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: MimetypesFileTypeMap.java,v 1.6 2010/07/29 13:28:29 pecet1 Exp $
 */
public class MimetypesFileTypeMap extends javax.activation.MimetypesFileTypeMap
{
    private static Properties mimeIcons = new Properties();
    private static MimetypesFileTypeMap instance = null;
    static
    {
        instance = new MimetypesFileTypeMap(MimetypesFileTypeMap.class.getClassLoader().
            getResourceAsStream("docusafe.mime.types"));
    }

    public static final String DEFAULT_MIME_TYPE = "application/octet-stream";

    public MimetypesFileTypeMap(InputStream inputStream)
    {
        super(inputStream);
        try
        {
            mimeIcons.load(MimetypesFileTypeMap.class.getClassLoader().
                getResourceAsStream("docusafe.mime.icons"));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static MimetypesFileTypeMap getInstance()
    {
        return instance;
    }

    /**
     * Zwraca typ MIME pliku na podstawie rozszerzenia jego
     * nazwy. Je�eli typ MIME pliku nie da si� okre�li�, zwracana
     * jest warto�� pola {@link #DEFAULT_MIME_TYPE}.
     * @param file
     * @return
     */
    public synchronized String getContentType(File file){
        return getContentType(file.getName());
    }

    /**
     * Zwraca typ MIME pliku na podstawie rozszerzenia jego
     * nazwy. Je�eli typ MIME pliku nie da si� okre�li�, zwracana
     * jest warto�� pola {@link #DEFAULT_MIME_TYPE}.
     * @param filename
     * @return
     */
    public synchronized String getContentType(String filename)
    {
        //na razie wszystkie xml traktujemy tak samo
        if(filename.endsWith(".xml")){
            return "application/xml";
        } else if (filename.endsWith(".xls")) {
            return "application/vnd.ms-excel";
        } else if (filename.endsWith(".pdf")) {
            return "application/pdf";
        } else if (filename.endsWith(".ppt")) {
            return "application/vnd.ms-powerpoint";
        } else if (filename.endsWith(".odt")) {
            return "application/vnd.oasis.opendocument.text";
        } else {
            String contentType = super.getContentType(filename);
            if (contentType == null || contentType.length() == 0 ||
                contentType.trim().length() == 0)
                contentType = DEFAULT_MIME_TYPE;
            return contentType;
        }
    }

    public synchronized String getIcon(String mimeType)
    {
        if (mimeType == null)
            return mimeIcons.getProperty("*");
        String icon = mimeIcons.getProperty(mimeType);
        if (icon == null)
            icon = mimeIcons.getProperty("*");
        return icon;
    }
}
