package pl.compan.docusafe.util;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.compan.docusafe.core.*;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.base.ObjectPermission;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.dockinds.logic.AbstractDocumentLogic;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.jbpm4.Jbpm4Provider;
import pl.compan.docusafe.core.office.*;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;

import java.sql.SQLException;
import java.util.*;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

/**
 * Kopiowanie dokumentu i uruchomienie procesu
 * <p/>
 * TODO UWAGA Zrobione tylko dla wewnętrznych (dla wniosków o projekt międzynarodowy AMS)
 *
 * @author <a href="mailto:wiktor.ocet@docusafe.pl">Wiktor Ocet</a>
 */
public class DocumentReplicator {

    protected FieldsManager oldFm;
    protected Document oldDoc;
    protected DocumentKind oldDockind;

    List<String> errors = new ArrayList<String>();
    private static final Log log = LogFactory.getLog(DocumentReplicator.class);

    public static DocumentReplicator prepare(FieldsManager oldFm) throws EdmException {
        DSContextOpener opener = DSContextOpener.openAsUserIfNeed(DSContextOpener.SubjectSource.HttpServlet);
        DocumentReplicator documentReplicator = new DocumentReplicator(oldFm);
        opener.closeIfNeed();
        return documentReplicator;
    }

    protected DocumentReplicator(FieldsManager oldFm) throws EdmException {
        this.oldFm = oldFm;
        this.oldDoc = Document.find(oldFm.getDocumentId());
        this.oldDockind = oldFm.getDocumentKind();
    }

    public ReplicationResult copyDocument() throws EdmException {
        DSContextOpener opener = DSContextOpener.openAsUserIfNeed(DSContextOpener.SubjectSource.HttpServlet);
        Document newDoc = null;
        try {
            DSApi.context().begin();

            newDoc = createCopyOfDocument();
            DSApi.context().session().save(newDoc);

            copyDocumentValues(newDoc);
            DSApi.context().session().save(newDoc);

            finalizeInteralDocument(newDoc);
            copyAllAttachments(newDoc);
            setDocumentVersion(newDoc);
            bindToJournal(newDoc);
            DSApi.context().session().save(newDoc);

            startProcess(newDoc);

            printLabel(newDoc);
            TaskSnapshot.updateByDocument(newDoc);

            DSApi.context().commit();
        } catch (Exception e) {
            addError(e.getMessage());
            DSApi.context()._rollback();
            throw new EdmException(e);
        } finally {
            opener.closeIfNeed();
        }

        return new ReplicationResult(errors, newDoc);
    }

    private void startProcess(Document newDoc) throws EdmException {
        if (newDoc instanceof OfficeDocument) {
            OutOfficeDocument newOfficeDoc = ((OutOfficeDocument) newDoc);
            DocumentKind documentKind = newDoc.getDocumentKind();
            DocumentLogic logic = documentKind.logic();

            try {
                this.getClass().getDeclaredMethod("onStartProcess", OfficeDocument.class);
                logic.onStartProcess(newOfficeDoc);
                return;
            } catch (SecurityException e) {
            } catch (NoSuchMethodException e) {
            }

            if (Jbpm4Provider.getInstance().isJbpm4Supported()) {
                Map<String, Object> map = Maps.newHashMap();
                map.put(ASSIGN_USER_PARAM, oldDoc.getAuthor());
                map.put(ASSIGN_DIVISION_GUID_PARAM, oldDoc.getAutorDivision());
                documentKind.getDockindInfo().getProcessesDeclarations().onStart(newOfficeDoc, map);
            } else {
                logic.onStartProcess(newOfficeDoc);
            }
            return;
        }

        addError("Nie uruchomiono procesu");
    }

    private Document createCopyOfDocument() throws EdmException {
        Document newDoc = getNewDocument(oldDoc.getType());
        newDoc.setDocumentKind(oldDockind);

        //Document - not-null fields:
        //---------------------------
        //title
        //description
        //ctime
        //mtime
        //author
        //deleted
        //office
        //wparam
        //forceArchivePermissions

        String title = oldDoc.getTitle();
        newDoc.setTitle(title);
        String description = oldDoc.getDescription();
        newDoc.setDescription(description);
        //ctime auto
        //mtime auto while setTitle
        String author = oldDoc.getAuthor();
        newDoc.setAuthor(author);
        String authorDivision = oldDoc.getAutorDivision();
        newDoc.setAutorDivision(authorDivision);

        if (newDoc instanceof OfficeDocument && oldDoc instanceof OfficeDocument) {
            OutOfficeDocument newOfficeDoc = ((OutOfficeDocument) newDoc);

            String summary = ((OfficeDocument) oldDoc).getSummary();
            String barcode = ((OfficeDocument) oldDoc).getBarcode();

            newOfficeDoc.setSummary(summary);
            newOfficeDoc.setBarcode(barcode);

//            newOfficeDoc.setAssignedDivision(DSDivision.ROOT_GUID);
//            newOfficeDoc.setCurrentAssignmentAccepted(Boolean.FALSE);

//            String postalRegNumber = ((OfficeDocument) oldDoc).getPostalRegNumber();
//            Journal journal = ((OfficeDocument) oldDoc).getJournal();
//            newOfficeDoc.setPostalRegNumber(postalRegNumber);
//            newOfficeDoc.setJournal(journal);
        }

        if (newDoc instanceof OutOfficeDocument && oldDoc instanceof OutOfficeDocument) {
            OutOfficeDocument newOutOfficeDoc = ((OutOfficeDocument) newDoc);

            //OutOfficeDocument - not-null fields:
            //------------------------------------
            //summary
            //assignedDivision
            //creatingUser
            //cancelled
            //archived
            //internal

            newOutOfficeDoc.setCreatingUser(DSApi.context().getPrincipalName());
            newOutOfficeDoc.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
            newOutOfficeDoc.setDivisionGuid(DSDivision.ROOT_GUID);
            newOutOfficeDoc.setAssignedDivision(DSDivision.ROOT_GUID);
            newOutOfficeDoc.setSender(getCopyOfSender());

            String summary = ((OfficeDocument) oldDoc).getSummary();
            Boolean priority = ((OutOfficeDocument) oldDoc).getPriority();
            Boolean countryLetter = ((OutOfficeDocument) oldDoc).getCountryLetter();

            newOutOfficeDoc.setSummary(summary);
            newOutOfficeDoc.setPriority(priority);
            newOutOfficeDoc.setCountryLetter(countryLetter);
        }

        if (newDoc instanceof InOfficeDocument && oldDoc instanceof InOfficeDocument) {
            InOfficeDocument newInOfficeDoc = ((InOfficeDocument) newDoc);
            //todo

            //InOfficeDocument - not-null fields:
            //-----------------------------------
            //summary
            //assignedDivision
            //creatingUser
            //cancelled
            //archived
            //bok
            //replyUnnecessary
            //original

        }

        boolean inPackage = oldDoc.getInPackage();
        newDoc.setInPackage(inPackage);
        //*/

        updateDocumentBeforeCreate(newDoc);
        newDoc.create();
        return newDoc;
    }

    protected Sender getCopyOfSender() throws EdmException {
        if (oldDoc instanceof OutOfficeDocument) {
            Sender oldSender = ((OutOfficeDocument) oldDoc).getSender();

            Sender sender = new Sender();
            sender.setFirstname(oldSender.getFirstname());
            sender.setLastname(oldSender.getLastname());
            sender.setEmail(oldSender.getEmail());
            sender.setOrganization(oldSender.getOrganization());
            sender.setOrganizationDivision(oldSender.getOrganizationDivision());
            sender.setDictionaryType(Person.DICTIONARY_SENDER);
            sender.create();
            return sender;
        }
        return null;
    }

    protected static Sender createSender() throws EdmException {
        DSUser user = DSUser.getLoggedInUser();//findByUsername(DSApi.context().getPrincipalName());
        String personFirstName = user.getFirstname();
        String personLastName = user.getFirstname();
        Sender sender = new Sender();
        sender.setFirstname(personFirstName);
        sender.setLastname(personLastName);
        sender.setEmail(user.getEmail());
        sender.setOrganization(DSDivision.find(Iterables.getFirst(Lists.newArrayList(DSApi.context().getDSUser().getDivisionsGuid()), DSDivision.ROOT_GUID)).getName());
        sender.setOrganizationDivision(sender.getOrganization());
        sender.setDictionaryType(Person.DICTIONARY_SENDER);
        sender.create();
        return sender;
    }

    public static void finalizeInteralDocument(Document document) throws EdmException {
        if (document instanceof OutOfficeDocument)
            ((OutOfficeDocument) document).setInternal(true);

        if (document instanceof OfficeDocument) {
            try {
                java.util.Set<PermissionBean> perms = new java.util.HashSet<PermissionBean>();

                DSUser author = DSApi.context().getDSUser();
                String user = author.getName();
                String fullName = author.getLastname() + " " + author.getFirstname();

                perms.add(new PermissionBean(ObjectPermission.READ, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.READ_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.MODIFY, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.MODIFY_ATTACHMENTS, user, ObjectPermission.USER, fullName + " (" + user + ")"));
                perms.add(new PermissionBean(ObjectPermission.DELETE, user, ObjectPermission.USER, fullName + " (" + user + ")"));

                Set<PermissionBean> documentPermissions = DSApi.context().getDocumentPermissions(document);
                perms.addAll(documentPermissions);
                ((AbstractDocumentLogic) document.getDocumentKind().logic()).setUpPermission(document, perms);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }


    public Document getNewDocument(DocumentType docTyp) {
        switch (docTyp) {
            case INCOMING:
                return new InOfficeDocument();
            case OUTGOING:
                return new OutOfficeDocument();
            case INTERNAL:
                return new OutOfficeDocument();
            case ARCHIVE:
                return new Document("Opis", "Opis");

            case ORDER:
            case PLAIN:
            default:
                throw new IllegalArgumentException(docTyp.name());
        }
    }

    protected void copyAllAttachments(Document newDoc) throws EdmException {
        List<Attachment> oldAllAttachments = oldDoc.getAttachments();
        for (Attachment oldAttachment : oldAllAttachments) {
            Attachment attachment = oldAttachment.cloneObject();
            attachment.setDocument(newDoc);
            newDoc.createAttachment(attachment);
        }
    }

    private void setDocumentVersion(Document newDoc) throws EdmException {
        newDoc.setCzyAktualny(oldDoc.getCzyAktualny());
        newDoc.setCzyCzystopis(oldDoc.getCzyCzystopis());
        newDoc.setVersion(oldDoc.getVersion());
        newDoc.setVersionGuid(oldDoc.getVersionGuid());

        DSApi.context().session().save(newDoc);
    }

//    private void assignOfficeNumber(Document newDoc) throws EdmException {
//        if (newDoc instanceof OutOfficeDocument && ((OutOfficeDocument) newDoc).getOfficeNumber() == null
//                && oldDoc instanceof OutOfficeDocument && ((OutOfficeDocument) oldDoc).getOfficeNumber() != null) {
//            Date entryDate = new Date();
//            Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), newDoc.getId(), entryDate);
//            ((OutOfficeDocument) newDoc).bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
//        }
//    }

    protected void updateDocumentBeforeCreate(Document newDoc) throws EdmException { }

    private int getDwrType(DocumentType docTyp) {
        switch (docTyp) {
            case INCOMING:
                return DocumentLogic.TYPE_IN_OFFICE;
            case OUTGOING:
                return DocumentLogic.TYPE_OUT_OFFICE;
            case INTERNAL:
                return DocumentLogic.TYPE_INTERNAL_OFFICE;
            case ARCHIVE:
                return DocumentLogic.TYPE_ARCHIVE;

            case ORDER:
            case PLAIN:
            default:
                throw new IllegalArgumentException(docTyp.name());
        }
    }

    private void copyDocumentValues(Document newDoc) throws EdmException, SQLException {
        FieldsManager newFm = newDoc.getFieldsManager();
        Map<String, Object> newValues = new HashMap<String, Object>();

        putNewDocumentValues(newDoc, newValues);

        newFm.reloadValues(newValues);
        newDoc.getDocumentKind().setOnly(newDoc.getId(), newFm.getFieldValues());
    }

    protected void putNewDocumentValues(Document newDoc, Map<String, Object> newValues) throws EdmException, SQLException {
        Map<String, Object> oldFieldValues = oldFm.getFieldValues();
        List<Field> oldFields = oldFm.getFields();
        for (Field f : oldFields) {
            String cn = f.getCn();
            Object newVal = getDefaultCopyOfValue(newDoc, f, oldFieldValues.get(cn));
            newValues.put(cn, newVal);
        }
    }

    private Object getDefaultCopyOfValue(Document newDoc, Field field, Object oldValue) throws EdmException, SQLException {
        try{
            if (oldValue == null)
                return null;

            if (field.getColumn().equalsIgnoreCase(Field.ATTACHMENT))
                return getCopyOfAttachmentField(field, oldValue);
    //        if (field.getType().toLowerCase().startsWith("document"))
    //            return getCopyOfDocumentField(field, oldValue);
    //        if (field.getColumn().equalsIgnoreCase("*none*"))
    //            return getCopyOfNonColumnField(field, oldValue);
    //        if (!field.isPersistedInColumn())
    //            return getCopyOfNotPersistedInColumnField(field, oldValue);

            return getCopyOfValue(newDoc, field, oldValue);
        } catch(Exception e){
            addError(e.getMessage());
            return null;
        }
    }

    protected Object getCopyOfValue(Document newDoc, Field field, Object oldValue) throws EdmException, SQLException {
        return oldValue;
    }

//    private Object getCopyOfNonColumnField(Field field, Object oldValue) {
//        return null;
//    }
//
//    private Object getCopyOfDocumentField(Field field, Object oldValue) {
//        return null; //np. DocumentPerson
//    }
//
//    public Object getCopyOfNotPersistedInColumnField(Field field, Object copingValue) {
//        return null;
//    }

    protected Object getCopyOfAttachmentField(Field field, Object oldValue) throws EdmException {
        Attachment oldAttachment = Attachment.find((Long) oldValue);
        Attachment attachment = oldAttachment.cloneObject();
        return attachment.getId();
    }

    public static void bindToJournal(Document document) throws EdmException {
        if (document instanceof OfficeDocument)
            bindToJournal((OfficeDocument) document);
    }

    public static void bindToJournal(OfficeDocument document) throws EdmException {
        OutOfficeDocument doc = (OutOfficeDocument) document;
        Journal journal = Journal.getMainInternal();
        Long journalId = journal.getId();
        DSApi.context().session().flush();
        Date entryDate = GlobalPreferences.getCurrentDay();
        Integer sequenceId = Journal.TX_newEntry2(journalId, doc.getId(), entryDate);
        doc.bindToJournal(journalId, sequenceId);
        doc.setOfficeDate(entryDate);
    }

//    private void assignNewOfficeNumber(Document newDoc) throws EdmException {
//        if (newDoc instanceof OutOfficeDocument && ((OutOfficeDocument) newDoc).getOfficeNumber() != null) {
//            Date entryDate = new Date();
//            Integer sequenceId = Journal.TX_newEntry2(Journal.getMainOutgoing().getId(), newDoc.getId(), entryDate);
//            ((OutOfficeDocument) newDoc).setJournal(null);
//            ((OutOfficeDocument) newDoc).bindToJournal(Journal.getMainOutgoing().getId(), sequenceId);
//        }
//    }


    private void printLabel(Document newDoc) {
        try {
            newDoc.getDocumentKind().logic().printLabel(newDoc, getDwrType(newDoc.getType()), newDoc.getFieldsManager().getFieldValues());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private void addError(String msg) {
        errors.add(msg);
    }
    public static class ReplicationResult {
        List<String> errors = new ArrayList<String>();
        Document document;

        public ReplicationResult(List<String> errors, Document newDoc) {
            this.errors.addAll(errors);
            this.document = newDoc;
        }

        public void addError(String msg) {
            errors.add(msg);
        }

        public List<String> getErrors() {
            return errors;
        }

        public Document getDocument() {
            return document;
        }
    }
}
