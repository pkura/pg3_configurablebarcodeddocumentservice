package pl.compan.docusafe.util;

import org.dom4j.Element;
import org.dom4j.io.XMLWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: XmlUtils.java,v 1.5 2010/07/28 07:19:52 pecet1 Exp $
 */
public final class XmlUtils
{
    private XmlUtils(){}

    public static String escapeXml(String s)
    {
        if (s == null)
            return null;
        StringBuilder xml = new StringBuilder((int)(s.length()*1.2));
        CharacterIterator iter=new StringCharacterIterator(s);
        for (char c=iter.first(); c != CharacterIterator.DONE; c=iter.next())
        {
            switch (c)
            {
                    case '\'': xml.append("&apos;"); break;
                    case '"': xml.append("&quot;"); break;
                    case '<': xml.append("&lt;"); break;
                    case '>': xml.append("&gt;"); break;
                    case '&': xml.append("&amp;"); break;
                    default: xml.append(c);
            }
        }
        return xml.toString();
    }

    private static final Attributes EMPTY_ATTRS = new AttributesImpl();

    public static void writeElement(XMLWriter writer, String element, String text)
        throws SAXException
    {
        writer.startElement("", "", element, EMPTY_ATTRS);
        if (text != null && text.length() > 0)
            writer.characters(text.toCharArray(), 0, text.length());
        writer.endElement("", "", element);
    }

    /**
     * Wczytuje z danego elementu map� nazw (w poszczeg�lnych j�zykach).
     * @param element  w�ze� dokumentu XML zawieraj�cy nazwy j�zykowe
     * @param name  nazwa atrybutu j�zykowego (w tym elemencie), kt�ry nale�y wczyta�
     */
     public static Map<String,String> readLanguageValues(Element element, String name)
     {
         Map<String,String> values = new LinkedHashMap<String, String>();
         for (Locale locale : LocaleUtil.AVAILABLE_LOCALES)
         {
             values.put(locale.getLanguage(), "pl".equals(locale.getLanguage()) ?
             element.attributeValue(name) : element.attributeValue(name+"_"+locale.getLanguage()));
         }
         return values;
     }
}
