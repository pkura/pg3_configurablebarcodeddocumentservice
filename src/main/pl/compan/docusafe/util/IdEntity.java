package pl.compan.docusafe.util;

/**
 * Interfejs obiektów udostępniających ID pewnego typu
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
public interface IdEntity<T> {
    /** Zwraca ID - nigdy nie zwraca null */
    T getId();
}
