package pl.compan.docusafe.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * 
 * @author <a href="mailto:michal.manski@com-pan.pl">Michal Manski</a>
 */
public class PdfGenerator
{    
    public static final String HORIZONTAL = "horizontal";
    public static final String VERTICAL = "vertical";
    
    private static com.lowagie.text.Document generatePdfDocument(String name, int[] widths, List<String> columns, List<List<Object>> content, String orientation, File temp) throws DocumentException, IOException
    {
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            
            Font font = new Font(baseFont, 10);
            com.lowagie.text.Document pdfDoc =
                new com.lowagie.text.Document(VERTICAL.equals(orientation) ? PageSize.A4 : PageSize.A4.rotate());
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
            pdfDoc.open();
            PdfPTable table = new PdfPTable(widths.length);
            table.setWidths(widths);
            table.setWidthPercentage(100);

            // tworzenie nag��wka tabeli
            for (String column : columns)
                table.addCell(new Phrase(column, font));
            
            // tworzenie tre�ci tabeli
            for (List<Object> row : content)
            {
                for (Object object : row)
                    table.addCell(object != null ? object.toString() : "");
            }
            
            // dodawanie nazwy wydruku
            pdfDoc.add(new Phrase(name, font));
            // dodawanie tabeli
            pdfDoc.add(table);
            
            return pdfDoc;
    }
    
    public static File generate(File catalog, String name, String adnotation, int[] widths, List<String> columns, List<List<Object>> content, String orientation) throws EdmException
    {
        try
        {
            File temp = File.createTempFile("docusafe_", "_tmp", catalog);
            com.lowagie.text.Document pdfDoc = generatePdfDocument(name, widths, columns, content, orientation, temp);    
            File fontDir = new File(Docusafe.getHome(), "fonts");
            File arial = new File(fontDir, "arial.ttf");
            BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
            BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 8);
            pdfDoc.setFooter(new HeaderFooter(new Phrase(adnotation, font), true));
            
            pdfDoc.add(new Phrase(adnotation, font));
            
            pdfDoc.close();
            temp.deleteOnExit();//when JVM terminates
            return temp;
        }
        catch (DocumentException e)
        {
            throw new EdmException("B��d podczas generowania pliku PDF", e);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas generowania pliku PDF", e);
        }
    }
    
    
    public static File generate(String name, int[] widths, List<String> columns, List<List<Object>> content, String orientation) throws EdmException
    {
        try
        {
            File temp = File.createTempFile("docusafe_", "_tmp");
            com.lowagie.text.Document pdfDoc = generatePdfDocument(name, widths, columns, content, orientation, temp);        
            pdfDoc.close();       
            return temp;
        }
        catch (DocumentException e)
        {
            throw new EdmException("B��d podczas generowania pliku PDF", e);
        }
        catch (IOException e)
        {
            throw new EdmException("B��d podczas generowania pliku PDF", e);
        }
    }
}
