package pl.compan.docusafe.util;

import com.google.common.base.*;
import com.google.gson.*;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.webwork.DWRAction;
import org.jfree.util.Log;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.web.office.common.DwrEditorAction;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

/**
 * Klasa zawiera metody wspomagaj�ce formatowanie i parsowanie dat.
 *
 * <br></br><b>UWAGA!</b> Wszystkie objekty typu DateFormat nie akceptuj� daty innego formatu ni� okre�lony - {java.text.DateFormat.setLenient(false)}.
 * W przypadku niepoprawnego formatu wyrzuany jest wyj�tek {@link ParseException}.<br></br>
 *
 * TODO: zmieni� portableDateTimeFormat na portableDateTimeFormat
 * TODO: doda� portableTimeFormat i u�y� w zamykaniu dnia
 *
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: DateUtils.java,v 1.62 2010/08/05 15:57:15 mariuszk Exp $
 */
public class DateUtils
{
    public static final long SECOND = 1000L;
    public static final long MINUTE = 60 * SECOND;
    public static final long HOUR = 60 * MINUTE;
    public static final long DAY = 24 * HOUR;
    public static final long WEEK = 7 * DAY;
    public static final String[] JS_DATE_WITH_OPTIONAL_TIME = {"dd-MM-yyyy", "dd-MM-yyyy HH", "dd-MM-yyyy HH:mm", "dd-MM-yyyy HH:mm:ss"};

    /**
     * Format daty u�ywany w formularzach w ca�ej aplikacji.
     * Musi by� zgodny z warto�ci� sta�ej {@link #jsCalendarDateFormat}.
     */
    public static final DateFormat jsDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public static final DateFormat jsDateFormatYY = new SimpleDateFormat("dd-MM-yy");
    public static final DateFormat jsDateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    public static final DateFormat jsDateTimeWithSecondsFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    public static final DateFormat jsTimeFormat = new SimpleDateFormat("HH:mm");
    public static final Object jsApacheCommonsFormatLock = new Object();
    //2015-10-01T08:31:09.000Z
    public static final DateFormat axisDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    //2009-07-27T00:00:00
    public static final DateFormat simpleExportTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static final DateFormat epuapDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    /**
     * Format daty stosowany np. do exportu faktury kosztowej:
     * <b>yyyy-MM-dd+HH:mm</b>
     */
    public static final DateFormat exportDateFormat = new SimpleDateFormat("yyyy-MM-dd'+'HH:mm");
    /**
     * dd-MM-yyyy
     */
    public static final DateFormat commonDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public static final DateFormat commonDateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    public static final DateFormat commonTimeFormat = new SimpleDateFormat("HH:mm");
    
    public static final DateFormat folderDateFormat = new SimpleDateFormat("yyyyMMdd");
    public static final DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat mssqlDateFormat102 = new SimpleDateFormat("yyyy.MM.dd");
    public static final DateFormat commonDateFormatShort = new SimpleDateFormat("dd-MM-yy");
    public static final DateFormat commonDateTimeFormatShort = new SimpleDateFormat("dd-MM-yy HH:mm");
    public static final DateFormat sqlDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final DateFormat sqlDateTimeFormatNoSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final DateFormat sqlTimeFormat = new SimpleDateFormat("HH:mm:ss");

    public static final DateFormat yearMonthFormat = new SimpleDateFormat("yyyyMM");
    public static final DateFormat yearMonth_Format = new SimpleDateFormat("yyyy-MM");
    public static final DateFormat year_Format = new SimpleDateFormat("yyyy");
    public static final DateFormat polishMonthFormat = new SimpleDateFormat("MMMMM", new Locale("pl"));

    public static final DateFormat slashDateFormat = new SimpleDateFormat("yyyy/MM/dd");
    public static final DateFormat slashDateFormatShort = new SimpleDateFormat("yy/MM/dd");
    public static final DateFormat slashMinDate = new SimpleDateFormat("MM/yy");

    //"Only dates between January 1, 1753 and December 31, 9999 are accepted." - dla niekt�rych serwer�w
    public static final Date zeroDate = new GregorianCalendar(1753, GregorianCalendar.JANUARY, 1).getTime();

    public static final DateFormat commonDateTimeWithoutWhiteSpaceFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
    public static final DateFormat dateWithHoursWithoutDashFormat = new SimpleDateFormat("yyyyMMdd_HHmm");


    /**
     * Format daty, kt�ry powinien by� przekazywany w konstruktorze
     * kalendarza javascriptowego na stronach JSP. Musi by� zgodny
     * z warto�ci� sta�ej {@link #jsDateFormat}.
     */
    public static final String jsCalendarDateFormat = "%d-%m-%Y"; //"dd-mm-y";

    public static final DateFormat bipDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public static final DateFormat portableDateTimeFormat = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss.SSS'Z'");

    static
    {
        //****************************************
        //BEGIN - setLenient(false)
        //
        //java.text.DateFormat
        //public void setLenient(boolean lenient)
        //Specify whether or not date/time parsing is to be lenient. With lenient parsing, the parser may use heuristics
        //to interpret inputs that do not precisely match this object's format. With strict parsing, inputs must match
        //this object's format.
        //This method is equivalent to the following call.
        //getCalendar().setLenient(lenient)
        //This leniency value is overwritten by a call to setCalendar().
        //Parameters:
        //lenient - when true, parsing is lenient
        //
        //****************************************

        jsDateFormat.setLenient(false);
        jsDateFormatYY.setLenient(false);
        jsDateTimeFormat.setLenient(false);
        jsDateTimeWithSecondsFormat.setLenient(false);
        jsTimeFormat.setLenient(false);

        axisDateTime.setLenient(false);

        simpleExportTimeFormat.setLenient(false);
        epuapDateFormat.setLenient(false);

        commonDateFormat.setLenient(false);
        commonDateTimeFormat.setLenient(false);
        commonTimeFormat.setLenient(false);

        folderDateFormat.setLenient(false);
        sqlDateFormat.setLenient(false);
        mssqlDateFormat102.setLenient(false);
        commonDateFormatShort.setLenient(false);
        commonDateTimeFormatShort.setLenient(false);
        sqlDateTimeFormat.setLenient(false);
        sqlDateTimeFormatNoSeconds.setLenient(false);
        sqlTimeFormat.setLenient(false);

        yearMonthFormat.setLenient(false);
        yearMonth_Format.setLenient(false);
        year_Format.setLenient(false);
        polishMonthFormat.setLenient(false);

        slashDateFormat.setLenient(false);
        slashDateFormatShort.setLenient(false);
        slashMinDate.setLenient(false);

        commonDateTimeWithoutWhiteSpaceFormat.setLenient(false);
        bipDateFormat.setLenient(false);
        portableDateTimeFormat.setLenient(false);
        dateWithHoursWithoutDashFormat.setLenient(false);
        //**************************************
        //END - setLenient(false)
        //**************************************

        portableDateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    /**
     * Sama data. Domy�lna strefa czasowa, poniewa� interesuj�ce
     * s� tylko warto�ci roku, miesi�ca i dnia.
     */
    public static final DateFormat portableDateFormat = new SimpleDateFormat("dd-MM-yyyy'T'");

    public static final DateFormat ldapDateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    static
    {
        ldapDateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

     /** Kalendarz gregorianski */
    public static final Calendar _GREGORIAN_CALENDAR = GregorianCalendar.getInstance(TimeZone.getTimeZone("Europe/Warsaw"));
   
    
    /** zwraca kalendarz gregorianski z timezone w polsce*/
    public static Calendar getGregorianCalendar()
    {
        return GregorianCalendar.getInstance(TimeZone.getTimeZone("Europe/Warsaw"));
    }
    
    public static double hours(long time)
    {
        return (double) time / HOUR;
    }

    /**
     * Sprawdza, czy w przekazanych obiektach pola dnia, miesi�ca i roku
     * s� jednakowe. Godziny, minuty etc. s� ignorowane.
     * @param a
     * @param b
     */
    public static boolean datesEqual(Date a, Date b)
    {
        Calendar calendarA = Calendar.getInstance();
        Calendar calendarB = Calendar.getInstance();

        calendarA.setTime(a);
        calendarB.setTime(b);

        return calendarA.get(Calendar.DAY_OF_MONTH) == calendarB.get(Calendar.DAY_OF_MONTH) &&
            calendarA.get(Calendar.MONTH) == calendarB.get(Calendar.MONTH) &&
            calendarA.get(Calendar.YEAR) == calendarB.get(Calendar.YEAR);
    }
/**
 * Poprawia przekazany string z czasem to formatu hh:mm
 * zamini:
 * 1 -> 01:00,10->10:00,2:15->02:15
 * 
 * @param time
 * @return
 */
    public static String correctTime(String time)
    {
    	if(time == null )
    		return "00:00";
    	if(time.length() == 5)
    		return time;
    	if(time.length() == 1)
    	{
    		return "0"+time+":00";
    	}
    	if(time.length() == 2)
    	{
    		return time+":00";
    	}
    	if(time.length() == 4)
    	{
    		return "0"+time;
    	}
    	return time;
    }
    
	public static String sayDayName(Date d)
	{
		DateFormat f = new SimpleDateFormat("EEEE");
		try {
			return f.format(d);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String[] getDaysOfWeekMap()
	{
		 return new DateFormatSymbols(new Locale(Docusafe.getCurrentLanguage(),"","")).getWeekdays();
	}

    public static boolean beforeDay(Date a, Date b)
    {
        if (a == null)
            throw new NullPointerException("a");
        if (b == null)
            throw new NullPointerException("b");

        return getDay(a).before(getDay(b));
    }

    private static Date getDay(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        Calendar calDay = Calendar.getInstance();
        calDay.clear();
        cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        cal.set(Calendar.DATE, cal.get(Calendar.DATE));

        return calDay.getTime();
    }



    public static Date nullSafeParseJsDate(String sDate)
    {
        return sDate == null ? null : parseJsDate(sDate);
    }

    public static Date nullSafeParseJsDateTime(String sDateTime)
    {
        return sDateTime == null ? null : parseJsDateTime(sDateTime);
    }

    /**
     * Zwraca obiekt Date odpowiadaj�cy tekstowej reprezentacji zgodnej
     * z formatem {@link #jsDateFormat}. dd-MM-yyyy Je�eli przekazany parametr nie
     * mo�e by� poprawnie zinterpretowany, zwracana jest warto�� null.
     * @param sDate
     */
    public static Date parseJsDate(String sDate)
    {
        if (sDate == null)
            throw new NullPointerException("sDate");

        synchronized (jsDateFormat)
        {
            try
            {
                return jsDateFormat.parse(sDate);
            }
            catch (ParseException e)
            {
                return null;
            }
        }
    }

    public static Date parseDateFromYear(String sDate)
    {
    	if (sDate == null)
            throw new NullPointerException("sDate");
    	Integer year = Integer.parseInt(sDate);

    	Calendar cal = new GregorianCalendar(new Locale("pl","PL"));
    	cal.set(Calendar.YEAR, year);
    	cal.set(Calendar.MONTH, 0);
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	return cal.getTime();
    }
    public static  Date parseJsDateTime(String sDateTime)
    {
        if (sDateTime == null)
            throw new NullPointerException("sDateTime");

        synchronized (jsDateTimeFormat)
        {
            try
            {
                return jsDateTimeFormat.parse(sDateTime);
            }
            catch (ParseException e)
            {
                return null;
            }
        }
    }

    public static  Date parseJsDateTimeWithSeconds(String sDateTime)
    {
        if (sDateTime == null)
            throw new NullPointerException("sDateTime");

        synchronized (jsDateTimeWithSecondsFormat)
        {
            try
            {
                return jsDateTimeWithSecondsFormat.parse(sDateTime);
            }
            catch (ParseException e)
            {
                return null;
            }
        }
    }
    
    public static Date parseJsDateWithOptionalTime(String sDateTime) {
    	if(sDateTime == null)
    		throw new NullPointerException("sDateTime");
    	
    	synchronized (jsApacheCommonsFormatLock) {
    		try {
				return org.apache.commons.lang.time.DateUtils.parseDate(sDateTime, JS_DATE_WITH_OPTIONAL_TIME);
			} catch (ParseException e) {
				return null;
			}
		}
    }

    public static  Date parseSlashDate(String sDateTime)
    {
        if (sDateTime == null)
            throw new NullPointerException("sDateTime");

        synchronized (slashDateFormat)
        {
            try
            {
                return slashDateFormat.parse(sDateTime);
            }
            catch (ParseException e)
            {
                return null;
            }
        }
    }
    
    public static  Date parseSlashDateFormatShort(String sDateTime)
    {
        if (sDateTime == null)
            throw new NullPointerException("sDateTime");

        synchronized (slashDateFormatShort)
        {
            try
            {
                return slashDateFormatShort.parse(sDateTime);
            }
            catch (ParseException e)
            {
                return null;
            }
        }
    }

    public static  Date parseJsTime(String sTime)
    {
        if (sTime == null)
            throw new NullPointerException("sTime");

        synchronized (jsTimeFormat)
        {
            try
            {
                return jsTimeFormat.parse(sTime);
            }
            catch (ParseException e)
            {
                return null;
            }
        }
    }

    public static String formatJsDate(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (jsDateFormat)
        {
            return jsDateFormat.format(date);
        }
    }

    public static String formatFolderDate(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (folderDateFormat)
        {
            return folderDateFormat.format(date);
        }
    }

    public static String formatJsDateOrEmptyString(Date date)
    {
    	try
    	{
    		return formatJsDate(date);
    	}
    	catch(Exception e)
    	{
    		return "";
    	}
    }

    public static  String formatJsDateTimeWithSeconds(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (jsDateTimeWithSecondsFormat)
        {
            return jsDateTimeWithSecondsFormat.format(date);
        }
    }

    public static String formatJsDateYY(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (jsDateFormatYY)
        {
            return jsDateFormatYY.format(date);
        }
    }

    public static String formatJsTime(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (jsTimeFormat)
        {
            return jsTimeFormat.format(date);
        }
    }

    public static String formatJsDateTime(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (jsDateTimeFormat)
        {
            return jsDateTimeFormat.format(date);
        }
    }
    
    public static String formatSimpleExportNullableDateTime(Date date){
        return date == null ? null : simpleExportTimeFormat.format(date);
    }

    public static String formatAxisNullableDateTime(Date date){
        return date == null ? null : formatAxisDateTime(date);
    }

    public static String formatAxisDateTime(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (axisDateTime)
        {
            return axisDateTime.format(date);
        }
    }
    /** dd-MM-yyy */
    public static String formatCommonDate(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (commonDateFormat)
        {
            return commonDateFormat.format(date);
        }
    }

    public static String formatCommonTime(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (commonTimeFormat)
        {
            return commonTimeFormat.format(date);
        }
    }

    public static String formatCommonDateTime(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (commonDateTimeFormat)
        {
            return commonDateTimeFormat.format(date);
        }
    }

    public static String formatDateWithHoursWithoutDash (Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (dateWithHoursWithoutDashFormat)
        {
            return dateWithHoursWithoutDashFormat.format(date);
        }
    }

    public static String formatCommonDateTimeWithoutWhiteSpaces(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (commonDateTimeWithoutWhiteSpaceFormat)
        {
            return commonDateTimeWithoutWhiteSpaceFormat.format(date);
        }
    }

    public static String formatYearMonth(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (yearMonthFormat)
        {
            return yearMonthFormat.format(date);
        }
    }

    public static String formatYear_Month(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (yearMonth_Format)
        {
            return yearMonth_Format.format(date);
        }
    }

    public static String formatYear(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (year_Format)
        {
            return year_Format.format(date);
        }
    }

    public static String formatPolishMonth(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (polishMonthFormat)
        {
            return polishMonthFormat.format(date);
        }
    }

    public static String formatSlashDate(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (slashDateFormat)
        {
            return slashDateFormat.format(date);
        }
    }
    
    public static String formatSlashMinDate(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (slashDateFormat)
        {
            return slashMinDate.format(date);
        }
    }
    public static String formatBipDate(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (bipDateFormat)
        {
            return bipDateFormat.format(date);
        }
    }

    public static String formatLdapDateTime(Date date)
    {
        if (date == null)
            throw new NullPointerException("date");

        synchronized (ldapDateTimeFormat)
        {
            return ldapDateTimeFormat.format(date);
        }
    }

    public static Date parseLdapDateTime(String dateTime)
    {
        if (dateTime == null)
            throw new NullPointerException("dateTime");

        synchronized (ldapDateTimeFormat)
        {
            try
            {
                return ldapDateTimeFormat.parse(dateTime);
            }
            catch (ParseException e)
            {
                return null;
            }
        }
    }

    public static String formatPortableDateTime(Date date)
    {
        synchronized (portableDateTimeFormat)
        {
            return portableDateTimeFormat.format(date);
        }
    }

    /**
     * Formatuje sam� dat� w przeno�nym formacie (w strefie
     * czasowej GMT).
     * @param date
     */
    public static String formatPortableDate(Date date)
    {
        synchronized (portableDateFormat)
        {
            return portableDateFormat.format(date);
/*
            Calendar dateCal = Calendar.getInstance();
            dateCal.setTime(date);

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            cal.clear();
            cal.set(Calendar.YEAR, dateCal.get(Calendar.YEAR));
            cal.set(Calendar.MONTH, dateCal.get(Calendar.MONTH));
            cal.set(Calendar.DATE, dateCal.get(Calendar.DATE));

            return portableDateFormat.format(cal.getTime());
*/
        }
    }

    public static String formatSqlDate(Date date)
    {
        synchronized (sqlDateFormat)
        {
            return sqlDateFormat.format(date);
        }
    }

    public static String formatSqlDateTime(Date date)
    {
        synchronized (sqlDateTimeFormat)
        {
            return sqlDateTimeFormat.format(date);
        }
    }

    public static Date parsePortableDate(String sDate) throws EdmException
    {
        synchronized (portableDateFormat)
        {
            try
            {
                return portableDateFormat.parse(sDate);
            }
            catch (ParseException e)
            {
                throw new EdmException("Niepoprawny format daty: "+sDate, e);
            }
        }
    }

    public static java.util.Date nullSafeSqlDate(java.sql.Timestamp st)
    {
        return st != null ? new java.util.Date(st.getTime()) : null;
    }

    public static java.util.Date nullSafeSqlDate(java.sql.Date st)
    {
        return st != null ? new java.util.Date(st.getTime()) : null;
    }

    /**
     * Odejmuje dat� b od daty a, zwraca r�nic� w dniach.
     */
    public static int substract(final Date a, final Date b)
    {
        if (a == null)
            throw new NullPointerException("a");
        if (b == null)
            throw new NullPointerException("b");

        if (a == b || a.getTime() == b.getTime())
            return 0;

        boolean negative = b.after(a); // b > a, r�nica b�dzie ujemna
        int delta;

        // podczas oblicze� data calA powinna by� mniejsza ni� calB
        GregorianCalendar calA = new GregorianCalendar();
        calA.setTime(negative ? a : b);
        GregorianCalendar calB = new GregorianCalendar();
        calB.setTime(negative ? b : a);

        if (calA.get(Calendar.YEAR) == calB.get(Calendar.YEAR))
        {
            delta = calB.get(Calendar.DAY_OF_YEAR) - calA.get(Calendar.DAY_OF_YEAR);
        }
        else
        {
            delta = daysInYear(calA.get(Calendar.YEAR)) - calA.get(Calendar.DAY_OF_YEAR);
            delta += calB.get(Calendar.DAY_OF_YEAR);
            for (int y=calA.get(Calendar.YEAR)+1; y < calB.get(Calendar.YEAR); y++)
            {
                delta += daysInYear(y);
            }
        }

        return negative ? -delta : delta;
    }

    /**
     * Zwraca r�nic� mi�dzy datami w dniach.
     * @return a - b. Liczba bez znaku.
     */
    public static int difference(final Date a, final Date b)
    {
        return Math.abs(substract(a, b));
    }

    /**
     * Zwraca dat� reprezentuj�c� najp�niejszy mo�liwy do przedstawienia
     * czas danego dnia.
     * @param date
     */
    public static Date endOfDay(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);

        return cal.getTime();
    }

    public static Date startOfDay(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    /**
     * TODO: zaimplementowa� :-)
     */
    public static int getBusinessDays(int days)
    {
        return days;
    }

    public static Date plusDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static Date plusHours(Date date, int hours)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours);
        return cal.getTime();
    }

    public static Date plusMinutes(Date date, int minutes)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }

    public static Date plusBusinessDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, getBusinessDays(days));
        return cal.getTime();
    }

    public static Date getCurrentTime()
    {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    /**
     * Zwraca czas odpowiadaj�cy p�nocy rozpoczynaj�cej dany dzie�.
     * @param date
     * @param plusDays Liczba dni, kt�re nale�y doda� po ustawieniu
     * czasu na p�noc.
     */
    public static Date midnight(Date date, int plusDays)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.clear(Calendar.HOUR_OF_DAY);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.add(Calendar.DATE, plusDays);
        return cal.getTime();
    }

    public static Date nullSafeMidnight(Date date, int plusDays)
    {
        return date != null ? midnight(date, plusDays) : null;
    }

    private static int daysInYear(int year)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR);
    }

    public static Date mergeDateWithTime(Date date, Date time)
    {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date);
        cal2.setTime(time);
        cal1.set(Calendar.MILLISECOND, cal2.get(Calendar.MILLISECOND));
        cal1.set(Calendar.SECOND, cal2.get(Calendar.SECOND));
        cal1.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE));
        cal1.set(Calendar.HOUR_OF_DAY, cal2.get(Calendar.HOUR_OF_DAY));
        return cal1.getTime();
    }

    /**
     * Parsuje dat� rozpoznaj�c najpierw po d�ugo�ci roku czy data podana
     * jest w formacie yyyy-mm-dd czy dd-mm-yyyy.
     *
     * @param sDate
     * @return
     */
    public static Date parseDateAnyFormat(String sDate) throws ParseException
    {
        String[] split = sDate.split("-");
        if (split.length != 3)
            throw new ParseException("Niew�a�ciwy format daty : " + sDate, 0);
        if (split[0].length() == 4)
            return sqlDateFormat.parse(sDate);
        else if (split[2].length() == 4)
            return parseJsDate( sDate);
        else if (split[0].length() == 2 || split[2].length() == 2)
        	return commonDateFormatShort.parse(sDate);
        throw new ParseException("Niew�a�ciwy format daty : " + sDate, 0);
    }

    /**
     * Parsuje dat� sprawdza czy po dacie nie ma podanej godziny z minutami hh:mm 
     * pozniej rozpoznaj�c najpierw po d�ugo�ci roku czy data podana
     * jest w formacie yyyy-mm-dd czy dd-mm-yyyy.
     *
     * @param sDate
     * @return
     */
    public static Date parseDateTimeAnyFormat(String sDate) throws ParseException
    {
    	if(sDate.contains(":"))
    	{
            String[] split = sDate.split("-");
            if (split.length != 3)
                throw new ParseException("Niew�a�ciwy format daty", 0);
            if (split[0].length() == 4)
                return sqlDateTimeFormatNoSeconds.parse(sDate);
            else if (split[2].length() > 2)
                return jsDateTimeFormat.parse(sDate);
            else if (split[0].length() == 2 || split[2].length() == 2)
            	return commonDateTimeFormatShort.parse(sDate);
    	}
    	else
    	{
    		return parseDateAnyFormat(sDate);
    	}
    	throw new ParseException("Niew�a�ciwy format daty", 0);
    }

    /**
     * Zwraca rok w formacie yyyy
     * @param date przyjmowany format dd-mm-rrrr v rrrr-mm-dd
     * @return rok lub null jezeli podana data jest nieprawidlowa
     * @throws ParseException
     */
    public static Integer getYearJsDate(String date) throws ParseException
    {
    	String[] split = date.split("-");
    	 if (split[0].length() == 4)
    		 return Integer.parseInt(split[0]);
    	 if (split[2].length() == 4)
    		 return Integer.parseInt(split[2]);
    	 return null;
    }

    public static Integer parseJsYear(String date) {
    	try {
    		if (date == null || date.length() != 4)
    			return null;
    		return Integer.parseInt(date);
    	} catch (Exception ex) {
    		return null;
    	}
    }

    public static void main(String args[]) throws Exception
    {
        DateFormat df = new SimpleDateFormat("dd-MM-yy");
    }

    /**
     * Funckja sprawdza czy podana data w stringu jest pusta
     * standardowy if do walidacji.
     * @use DateUtils.nullSafeParseJsDate And StringUtils.isEmpty(date)
     * @param date String
     * @exception return false
     * @return boolean
     */
    public static boolean isEmptyDateString(String date) {
        try {
            Date dtDate = DateUtils.nullSafeParseJsDate(date);

            if (StringUtils.isEmpty(date) && dtDate == null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
    
    public static boolean isValidateDate(String date) {
    	
    	Log.info("isValidateDate dla stringa: " + date);
    	   if (date == null)
    		      return false;

    		    //set the format to use as a constructor argument
    		    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    		    
    		    if (date.trim().length() != dateFormat.toPattern().length())
    		      return false;

    		    dateFormat.setLenient(false);
    		    
    		    try {
    		      dateFormat.parse(date.trim());
    		    }
    		    catch (ParseException pe) {
    		      return false;
    		    }
    		    return true;
    }

    /**
     * Sprawdza czy pierwsza data jest wieksza od drugiej( Daty przekazywane w stringu)
     * @exception return false;
     * @param date1 String
     * @param date2 String
     * @return 
     */
    public static boolean isGreaterDate(String date1, String date2) {
        
        Date dtDate1 = DateUtils.nullSafeParseJsDate(date1);
        Date dtDate2 = DateUtils.nullSafeParseJsDate(date2);

        return DateUtils.isGreaterDate(dtDate1, dtDate2);
       
    }
    
    /**
     * Sprawdza czy pierwsza data jest wieksza od drugiej( Daty przekazywane w Date)
     * @exception return false;
     * @param date1 Date
     * @param date2 Date
     * @return 
     */
    public static boolean isGreaterDate(Date date1, Date date2){
         try {
            if (date1.after(date2)) 
                return true;
             else
                return false;
            
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * G��wna metoda do walidacji zakresu datu. Zabezpiecza nulle
     * i pozwalana zares tylko w przysz�o�ci
     * @param date
     */
    public static boolean validateFutureRange(Date from, Date to) throws EdmException{
        Preconditions.checkNotNull(from, "Data 'od' nie mo�e by� pusta");
        Preconditions.checkNotNull(to, "Data 'do' nie mo�e by� pusta");
        Preconditions.checkState(isGreaterDate(from, Calendar.getInstance().getTime()), "Data 'od' jest wczesniejsza od tera�niejszej lub jest pusta!");
        Preconditions.checkState(isGreaterDate(to, Calendar.getInstance().getTime()), "Data 'do' jest wczesniejsza od tera�niejszej lub jest pusta!");
        Preconditions.checkState(!isGreaterDate(from, to), "Data 'od' jest po�niejsza od Daty 'do'!");

        return true;
    }
    
    /**
     * Zwraca obecny rok
     */
    public static int getCurrentYear()
    {
        return GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
    }
    
    /**
     * Zwraca nazwe slowna miesiaca miesiace s� od 0-11
     * @param month
     * @return 
     */
    public static String getMonthName(int month)
    {
        Calendar calendar = _GREGORIAN_CALENDAR;
        calendar.set(GregorianCalendar.MONTH, month);
        
        return DateUtils.polishMonthFormat.format(calendar.getTime());
    }
    
    /**
     * Sprawdza czy data jest wieksza od dzisaj
     */
    public static boolean isGreaterFromToday(Date date)
    {
        Date today = DateUtils.getCurrentTime();
        
        return DateUtils.isGreaterDate(date, today);
    }
    
    /**
     * @param month miesiace od 0-11 jak w kalendarzu
     */
    public static GregorianCalendar getFirstDayOfMonth(int month)
    {
        return getFirstDayOfMonth(month, getCurrentYear());
    }
    
    /**
     * @param month miesiace od 0-11 jak w kalendarzu
     */
    public static GregorianCalendar getFirstDayOfMonth(int month, int year)
    {
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Warsaw"));
        int  day = 1;
        calendar.set(year, month, day);
        
        return calendar;
    }
    
    /**
     * Serializer daty dla gsona
     */
    public static JsonSerializer<Date> gsonTimestampSerializer = new JsonSerializer<Date>() {
		@Override
		public JsonElement serialize(Date date, Type type, JsonSerializationContext ctx) {
			if(date != null) {
				return new JsonPrimitive(date.getTime());
			}
			return null;
		}
	};
	
	/**
	 * Deserializer daty dla gsona
	 */
	public static JsonDeserializer<Date> gsonTimestampDeserializer = new JsonDeserializer<Date>() {
		@Override
		public Date deserialize(JsonElement el, Type type, JsonDeserializationContext ctx) throws JsonParseException {
			try {
				Date date = new Date(el.getAsLong());
				return date;
			} catch(Exception e) {
				throw new JsonParseException("Cannot convert date", e);
			}
		}
	};

    public static XMLGregorianCalendar convertToXMLGregorianCalendar(Date input) throws EdmException {
        if (input == null) {
            return null;
        }

        try {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(input);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException e) {
            throw new EdmException(e);
        }
    }

    ///Metoda  geHolidays(int year) zwraca
    // list� objektow typu Date z datami wszystkich swiat zdefiniowanych
    // w pliku adds.properties

    public static ArrayList<Date> getHolidays(int year) {

        ArrayList<Date> dates = new ArrayList<Date>();

        String[] SingleDates = Docusafe.getAdditionProperty(("holidays."+year)).split(",");

           for(String s: SingleDates) {

               String[] MonthDays = s.split("\\.");

               dates.add(new Date( (year-1900),(Integer.parseInt(MonthDays[0]))-1,(Integer.parseInt(MonthDays[1]))));

                }

        return dates;
    }


}
