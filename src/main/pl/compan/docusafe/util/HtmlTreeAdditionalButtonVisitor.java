package pl.compan.docusafe.util;

/**
 * Interface okre�laj�cy dodatkowe akcje, lub elementy do utworzenia na drzewie
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>.
 */
public interface HtmlTreeAdditionalButtonVisitor {

    /**
     * Czy element posiada akcje do zdefiniowania
     * @param element
     * @return
     */
    boolean hasAdditionalAction(Object element);

    /**
     * Dodatkowa akcja do wy�wietlenia przy elemencie drzewa
     * @param element
     * @param out
     * @return
     */
    void getAdditionalAction(Object element, StringBuilder out);

}
