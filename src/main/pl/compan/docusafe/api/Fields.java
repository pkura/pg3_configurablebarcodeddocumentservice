package pl.compan.docusafe.api;

import pl.compan.docusafe.core.EdmException;

import java.util.Map;

/**
 * Klasa udost�pniaj�ca atrybuty biznesowe do zapisu / odczytu
 *
 * Klasa �amie kilka regu� obiektu Map, - inaczej definiuje put (konwersje) co sprawia, �e
 * fa.put(key, value)
 * return fa.containsValue(value) mo�e zwr�ci� false
 *
 * Obiekty implementuj�ce nie musz� wykonywa� aktualizacji bazy danych przed committem
 * - korzystanie z Fields i FieldsManager jednocze�nie mo�e powodowa� trudne
 * do odtworzenia b��dy (podobnie jak korzystanie z wielu FieldsManager�w)
 *
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface Fields extends Map<String, Object> {

    /**
     * Ustawia dany klucz na warto�� reprezentowan� przez value, je�eli value
     * jest stringiem mo�liwa jest konwersja wg nast�puj�cych regu�:
     *
     * Typ liczbowy -> .valueOf(value.toString())
     *
     * Typ enum -> wyszukiwanie po cn (w przypadku nie znalezienia wyj�tek)
     *
     * @param key
     * @param value
     * @return
     */
    Object put(String key, Object value);

    /**
     *  Wyrzuca wyj�tek UnsupportedOperationException
     */
    void clear() throws UnsupportedOperationException;

    /**
     * Wyrzuca wyj�tek UnsupportedOperationException
     * zamiast usuwa� klucz nale�y ustawi� dan� warto�� na null
     * -> put(key, null)
     */
    Object remove(Object key) throws UnsupportedOperationException;

    /**
     * Zapisuje zmiany do obecnej sesji
     */
    void commitChanges() throws EdmException;

    /**
     * Zwraca obiekt (patrz klasa Field) zapisany w danym polu
     * @param key - cn (String) pola, kt�re jest zwracane
     * @return Object reprezentuj�cy warto�� pola lub null, je�li pod danym cn nic nie ma
     */
    Object get(Object key);

    /**
     * Zwraca obiekt oczekuj�c danej klasy
     * @param key - cn (String) pola
     * @throws ClassCastException je�li przekazana klasa jest z�a
     * @return obiekt danej klasy lub null
     */
    <T> T get(Object key, Class<T> clazz) throws ClassCastException;
}
