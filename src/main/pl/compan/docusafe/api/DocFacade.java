package pl.compan.docusafe.api;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.FieldsManager;


/**
 * Fasada u�atwiaj�ca dost�p do dokument�w 
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public interface DocFacade {

    /**
     * Zwraca id dokumentu
     * @return id dokumentu
     */
    long getId();

     /**
     * Zwraca dany dokument (mo�liwe lazy initialization - wymaga otwartego kontekstu)
     * @return instancja Document (nigdy null)
     * @throws pl.compan.docusafe.core.EdmException w przypadku op�nionej inicjalizacji
     */
    Document getDocument() throws EdmException;
    
    Document getDocument(boolean checkPermission) throws EdmException;

    /**
     * Zwraca klas� udost�pniaj�c� dost�p do atrybut�w biznesowych
     * @return instancja FieldAccess (nigdy null)
     * @throws EdmException w przypadku op�nionej inicjalizacji
     */
    Fields getFields() throws EdmException;

    /**
     * Zwraca dost�p do p�l poprzez stary FieldsManager, przydatne gdy trzeba korzysta� ze starych API
     * FieldsManager jest cache'owany i nie jest uaktualniany z Fields
     * @return
     * @throws EdmException
     */
    FieldsManager getFieldsManager() throws EdmException;

    /**
     * Zapisuje w sesji wszystkie zmiany
     */
    void saveChanges() throws EdmException;
}
