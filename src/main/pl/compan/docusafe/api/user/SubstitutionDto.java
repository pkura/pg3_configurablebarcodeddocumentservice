package pl.compan.docusafe.api.user;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import javax.xml.bind.annotation.*;
import java.util.Calendar;

/**
 * Obiekt reprezentujący zastępstwo
 */
@XmlRootElement(name = "substitution")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SubstitutionDto {
    /**
     * @var id zastępstwa
     */
    private Long id;
    /**
     * @var użytkownik zastępujący
     */
    private UserDto userSubstituting;
    /**
     * @var uzytkownik zastępowany
     */
    private UserDto userSubsituted;
    /**
     * @var początek
     */
    private DateTime start;
    /**
     * @var koniec
     */
    private DateTime end;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDto getUserSubstituting() {
        return userSubstituting;
    }

    public void setUserSubstituting(UserDto userSubstituting) {
        this.userSubstituting = userSubstituting;
    }

    public UserDto getUserSubsituted() {
        return userSubsituted;
    }

    public void setUserSubsituted(UserDto userSubsituted) {
        this.userSubsituted = userSubsituted;
    }

    // =========== start i end getters and setters ========

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("start")
    public String getStartIso(){
        return ISODateTimeFormat.dateTime().print(start);
    }

    public void setStartIso(String startIso){
        start = ISODateTimeFormat.dateTime().parseDateTime(startIso);
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("end")
    public String getEndIso(){
        return ISODateTimeFormat.dateTime().print(end);
    }

    public void setEndIso(String endIso){
        end = ISODateTimeFormat.dateTime().parseDateTime(endIso);
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonIgnore
    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonIgnore
    public DateTime getEnd() {
        return end;
    }

    @JsonIgnore
    public Calendar getStartTime(){
        return start.toGregorianCalendar();
    }

    public void setStartTime(Calendar start) {
        this.start = new DateTime(start);
    }

    @JsonIgnore
    public Calendar getEndTime() {
        return end.toGregorianCalendar();
    }

    public void setEndTime(Calendar endTime){
        this.end = new DateTime(endTime);
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }
}
