package pl.compan.docusafe.api.user;

import com.google.common.base.Optional;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListIds;
import pl.compan.docusafe.core.EdmException;

import java.util.List;

/**
 * Service for creating and listing users
 *
 * All methods need DSApi open context
 */
public interface UserService {

    List<UserDto> getAllUsers();

    /**
     * Returns user with selected username, method does not
     * return deleted users
     * @param name name of a user
     * @return user if found, absent optional otherwise
     */
    Optional<UserDto> getByNameOrExternalName(String name);

    /**
     * Returns user with selected id, method does not return
     * deleted users
     * @param id id of a user
     * @return user if found, absent optional otherwise
     */
    Optional<UserDto> getById(long id) ;

    /**
     * Creates new user
     * @param user user to be created
     * @return id of new user
     */
    long create(UserDto user);

    /**
     * Updates selected user
     * @param user user to be updated
     */
    void update(UserDto user);

    /**
     * Marks user as deleted
     * @param user user to be deleted
     */
    void delete(UserDto user) throws EdmException;

    /**
     * Zwraca dost�pne role w systemie
     * @return
     */
    List<KeyElement> getAvailableOfficeRoles() throws EdmException;

    /**
     * Lista r�l jakie b�d� zaktualizowane u�ytkownikowi
     * @param roles
     */
    void updateUserOfficeRoles(List<Long> roles, String user) throws EdmException;

    /**
     * Zwraca list� r�l usera
     * @param id
     * @return
     */
    List<KeyElement> getUserOfficeRoles(String id) throws EdmException;

    /**
     * Returns user with selected userId(id, name, externalName), method does not
     * return deleted users
     * @param userId of a user
     * @return user if found, absent optional otherwise
     */
	Optional<UserDto> getByIdOrNameOrExternalName(String userId);
}
