/**
 * Zawiera zbiór obiektów repezentujących dane instancje w wywoływanych żądaniach związanych z zarządzaniem uzytkownikami i działami. </br/>
 *  - uzytkownik <br/>
 *  - dział <br/>
 *   - zastepstwo <br/>
 * 
 */
package pl.compan.docusafe.api.user;
