package pl.compan.docusafe.api.user;

import com.google.common.base.Function;
import pl.compan.docusafe.util.IdEntity;
import pl.compan.docusafe.util.IdEnumSupport;

import javax.xml.bind.annotation.*;

/**
 * Typy element�w struktury organizacyjnej
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "type")
@XmlEnum
public enum DivisionType implements IdEntity<String>{

    @XmlEnumValue("DIVISION")
    DIVISION("division"),
    @XmlEnumValue("GROUP")
    GROUP("group"),
    @XmlEnumValue("POSITION")
    POSITION("position"),
    @XmlEnumValue("SWIMLANE")
    SWIMPLANE("swimlane");

    public final static Function<String, DivisionType> FROM_STRING
            = IdEnumSupport.create(DivisionType.class);

    private final String key;

    DivisionType(String key) {
        this.key = key;
    }

    public String value(){
        return name();
    }
    /**
     * Returns key stored in database
     * @return
     */
    public String getId() {
        return key;
    }

    public static DivisionType fromValue(String v){
        return valueOf(v);
    }
}
