package pl.compan.docusafe.api.user.office;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import pl.compan.docusafe.core.office.CaseStatus;
import pl.compan.docusafe.core.office.OfficeCase;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Calendar;
import java.util.Date;

/**
 * Obiekt spraw wystawiany na itnerfejsie
 */
@XmlRootElement(name = "case")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OfficeCaseDto {

    private Long id;
    private String documentId;
    private String title;
    private String description;
    private String officeId;
    private Long portfolioId;
    /** @var @see CasePriority.id*/
    private Integer priority;
    private DateTime openDate;
    private DateTime finishDate;
    private boolean closed;
    private String status;
    private String author;
    private String clerk;
    private DateTime ctime;
    private Integer sequenceId;

    private Long precedent;

    public OfficeCaseDto() {

    }

    /**
     * Tworzy obiekt DTo z obiektu hibernatowego
     * @param officeCase
     */
    public OfficeCaseDto(OfficeCase officeCase){
        this.id = officeCase.getId();
        this.title = officeCase.getTitle();
        this.description = officeCase.getDescription();
        this.officeId = officeCase.getOfficeId();
        this.openDate = new DateTime(officeCase.getOpenDate().getTime());
        this.finishDate = new DateTime(officeCase.getFinishDate().getTime());
        this.closed = officeCase.isClosed();
        this.status = officeCase.getStatus().getName();
        this.author = officeCase.getAuthor();
        this.clerk = officeCase.getClerk();
        this.ctime = new DateTime(officeCase.getCtime().getTime());
        this.sequenceId = officeCase.getSequenceId();
    }

    @JsonIgnore
    public Calendar getcTimeTime() {
        if(ctime == null)
            return null;
        return ctime.toGregorianCalendar();
    }

    public void setcTimeTime(Calendar endTime){
        this.ctime = new DateTime(endTime);
    }

    @JsonIgnore
    public Calendar getOpenDateTime() {
        if(openDate == null)
            return null;
        return openDate.toGregorianCalendar();
    }

    public void setOpenDateTime(Calendar endTime){
        this.openDate = new DateTime(endTime);
    }

    @JsonIgnore
    public Calendar getFinishDateTime() {
        if(finishDate == null)
            return null;
        return finishDate.toGregorianCalendar();
    }

    public void setFinishDateTime(Calendar endTime){
        this.finishDate = new DateTime(endTime);
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonIgnore
    public DateTime getOpenDate() {
        return openDate;
    }
    public void setOpenDate(DateTime openDate) {
        this.openDate = openDate;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("openDate")
    public String getOpenDateIso(){
        return ISODateTimeFormat.dateTime().print(openDate);
    }
    public void setOpenDateIso(String openIso){
        openDate = ISODateTimeFormat.dateTime().parseDateTime(openIso);
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonIgnore
    public DateTime getFinishDate() {
        return finishDate;
    }
    public void setFinishDate(DateTime finishDate) {
        this.finishDate = finishDate;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("finishDate")
    public String getFinishDateIso(){
        return ISODateTimeFormat.dateTime().print(finishDate);
    }
    public void setFinishDateIso(String finishIso){
        finishDate = ISODateTimeFormat.dateTime().parseDateTime(finishIso);
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getClerk() {
        return clerk;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonIgnore
    public DateTime getCtime() {
        return ctime;
    }
    public void setCtime(DateTime ctime) {
        this.ctime = ctime;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("ctime")
    public String getCtimeIso(){
        return ISODateTimeFormat.dateTime().print(ctime);
    }

    public void setCtimeIso(String ctimeIso){
        ctime = ISODateTimeFormat.dateTime().parseDateTime(ctimeIso);
    }





    public Integer getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(Integer sequenceId) {
        this.sequenceId = sequenceId;
    }


    @Override
    public String toString() {
        return "OfficeCaseDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", officeId='" + officeId + '\'' +
                ", openDate=" + openDate +
                ", finishDate=" + finishDate +
                ", closed=" + closed +
                ", status='" + status + '\'' +
                ", author='" + author + '\'' +
                ", clerk='" + clerk + '\'' +
                ", ctime=" + ctime +
                ", sequenceId=" + sequenceId +
                '}';
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Long getPortfolioId() {
        return portfolioId;
    }

    public void setPortfolioId(Long portfolioId) {
        this.portfolioId = portfolioId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPrecedent() {
        return precedent;
    }

    public void setPrecedent(Long precedent) {
        this.precedent = precedent;
    }
}
