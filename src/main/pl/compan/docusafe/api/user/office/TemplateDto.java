package pl.compan.docusafe.api.user.office;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Obiekt Szablon�w na interfejsie
 */
@XmlRootElement
public class TemplateDto {
    private String templateName;
    private Boolean isUserTemplate;

    public Boolean getIsUserTemplate() {
        return isUserTemplate;
    }

    public void setIsUserTemplate(Boolean isUserTemplate) {
        this.isUserTemplate = isUserTemplate;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }


}
