/**
 * 
 */
package pl.compan.docusafe.api.user.office;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchResponse;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.rest.views.DocumentKindView;
import pl.compan.docusafe.rest.views.DocumentView;

/**
 * Obiekt reprezetuj�cy list� zada� u�ytkownik�w
 */
@XmlRootElement(name="task-list")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)

public class TaskList {

	private List<Task> items;

	public TaskList() {
		this.items = new ArrayList<Task>();
	}
	
	public TaskList(List<Map<String, String>> items) {
		this.items = new ArrayList<Task>();
		for (Map<String, String> element : items) {
			this.items.add(new Task(element));
		}
	}
	
	@XmlElement(name = "task")
	public List<Task> getItems() {
		return this.items;
	}
}
