package pl.compan.docusafe.api.user.office;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.OfficeCase;

import java.sql.SQLException;
import java.util.List;

/**
 * Serwis dla spraw. DocumentId jest Stringiem gdyby serwis zewn�trzny identyfikowa� dokumenty inaczej ni�
 * poprzez id
 */
public interface OfficeCaseService {

    /**
     * zwraca sprawy u�ytkownika
     * @param username
     * @return
     */
    List<OfficeCaseDto> getUserOfficeCase(String username);

    /**
     * zwraca sprawy w dziale
     * @param guid
     * @return
     */
    List<OfficeCaseDto> getDivisionOfficeCase(String guid);

    /**
     * zwraca sprawy dokumentu
     * @param documentId
     * @return
     */
    List<AssociativeDocumentStore.AssociativeDocumentBean> getDocumentOfficeCase(String documentId);

    /**
     * Zwraca sprawy
     * @param officeCaseId
     * @return
     */
    List<OfficeCaseDto> getOfficeCaseToOfficeCase(Long officeCaseId);
    /**
     * Tworzy spraw�
     * @return
     */
    boolean createOfficeCase(OfficeCaseDto officeCaseDto);

    /**
     * Dodaje dokument do sprawy
     *
     * @param documentId
     * @return
     */
    String addDocumentToOfficeCase(String documentId, Long officeCaseId, boolean reOpen) throws EdmException;

    /**
     * Usuwa dokument ze sprawy
     *
     * @param documentId
     * @return
     */
    String removeDocumentFromOfficeCase(String documentId, Long officeCaseId) throws EdmException;

    /**
     * Zwraca spraw�
     *
     * @param officeCaseId
     * @return
     */
    OfficeCaseDto getOfficeCase(String officeCaseId) throws Exception;


    List<OfficeFolderDto> getPortfolio(String guid) throws EdmException;

    /**
     * Zwraca dokumenty w sprawie
     * @param officeCaseId
     * @return
     */
    List<AssociativeDocumentStore.AssociativeDocumentBean> getDocuments(String officeCaseId) throws Exception;

    /**
     * Powi�zuje spraw� ze spraw�
     * @param officeCaseId
     * @param officeCaseAddId
     */
    void addOfficeCaseToOfficeCase(String officeCaseId, String officeCaseAddId) throws EdmException;

    /**
     * Usuwa spraw� ze sprawy
     * @param officeCaseId
     * @param officeCaseRemoveId
     */
    void removeOfficeCaseToOfficeCase(String officeCaseId, String officeCaseRemoveId) throws SQLException, EdmException;

    /**
     * Aktualizuje spraw�
     *
     * @param officeCaseDto
     * @return
     */
    String updateOfficeCase(OfficeCaseDto officeCaseDto) throws EdmException;

    /**
     * Usuwa spraw�
     *
     * @param officeCaseDto
     * @return
     */
    String deleteOfficeCase(String id, String reason) throws EdmException;

    /**
     * Zwrava histori� sprawy
     * @param id
     * @return
     */
    List<WorkHistoryBean> getCaseAudit(String id) throws EdmException;
}
