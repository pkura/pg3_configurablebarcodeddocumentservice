/**
 * 
 */
package pl.compan.docusafe.api.user.office;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Obiekt służy do konwertowania Integerów które są zwracane w metodach, tak aby
 * były obsługiwane przez protokoły xml oraz json.
 */
@XmlRootElement(name = "integer")
public class IntegerDto {

    private Integer item;

    public IntegerDto() {
	this.item = new Integer(0);
    }

    public IntegerDto(Integer item) {
	this.item = item;
    }

    @XmlElement(name = "value")
    public Integer getItem() {
	return this.item;
    }

    public void setItem(Integer item) {
	this.item = item;
    }
}
