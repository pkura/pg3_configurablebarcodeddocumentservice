/**
 * 
 */
package pl.compan.docusafe.api.user.office;

import java.util.ArrayList;
import java.util.List;


import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;



import org.codehaus.jackson.map.annotate.JsonSerialize;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchResponse;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.rest.views.DocumentKindView;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.api.user.office.WorkHistoryBean;

/**
 * Klasa reprezentuje obiekty list. Tak aby by�a mo�liwo�� skonwertowania list na xml oraz jsona
 */
@XmlRootElement(name="list")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@XmlSeeAlso({UserDto.class,DivisionDto.class,SubstitutionDto.class,DocumentView.class,DocumentKindView.class,
			 AssociativeDocumentStore.AssociativeDocumentBean.class,CasePriority.class,OfficeFolderDto.class,KeyElement.class,TemplateDto.class,
			 OfficeDocumentDto.class,OfficeCaseDto.class,DictionaryDto.class,FullTextSearchResponse.class,FieldView.class,
			 pl.compan.docusafe.rest.views.FieldView.class, WorkHistoryBean.class, ManualAssignmentDto.class})
public class ListDtos<T> {

	private List<T> items;

	public ListDtos() {
		this.items = new ArrayList<T>();
	}
	
	public ListDtos(List<T> items) {
		this.items = items;
	}
	
	@XmlAnyElement(lax=true)
	public List<T> getItems() {
		return this.items;
	}

    @Override
    public String toString() {
        return "ListDtos{" +
                "items=" + items +
                '}';
    }
}
