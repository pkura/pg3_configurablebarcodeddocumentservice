/**
 * 
 */
package pl.compan.docusafe.api.user.office;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Obiekt s�u�y do konwertowania Boolean�w kt�re s� zwracane w metodach, tak aby by�o obs�ugiwany protok� xml oraz json
 */
@XmlRootElement(name="boolean")
public class BooleanDto {

	private Boolean item;

	public BooleanDto() {
		this.item = new Boolean(false);
	}
	
	public BooleanDto(Boolean item) {
		this.item = item;
	}
	
	@XmlElement(name="value")
	public Boolean getItem() {
		return this.item;
	}
	
	public void setItem(Boolean item) {
		this.item = item;
	}
}
