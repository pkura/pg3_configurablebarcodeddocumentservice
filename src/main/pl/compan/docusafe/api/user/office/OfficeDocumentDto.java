package pl.compan.docusafe.api.user.office;

import com.beust.jcommander.internal.Maps;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

import static pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING;

/**
 * Klasa reprezentuj�ca obiekt dokumentu w API
 */
@XmlRootElement(name = "document")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OfficeDocumentDto extends CommonOfficeDto{
    /** @var rodzaj sortowania: false oznacza descending. Axis 2 nei wspiera enum�w dlatego tu nie dajemy */
    public Boolean ascending = Boolean.TRUE;

    public String sortingField;

    public String title;

    public String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAscending() {
        return ascending;
    }

    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }

    public String getSortingField() {
        return sortingField;
    }

    public void setSortingField(String sortingField) {
        this.sortingField = sortingField;
    }

    /**
     * Konwertuje na map�
     * @param documentKind
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public Map<String, FieldView> convertToMap() throws EdmException {
        Map<String, FieldView> fds = Maps.newHashMap();

        if(fieldValues.isEmpty())
            return fds;

        for(FieldView fieldView : fieldValues){
            fds.put(fieldView.getCn(), fieldView);
        }
        return fds;
    }
    /**
     * Konwertuje pola na liste fieldData.
     * @param documentKind
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public Map<String, Object> getDocumentFieldObjectMap(DocumentKind documentKind) throws EdmException {
        Map<String, Object> fds = Maps.newHashMap();

        if(fieldValues.isEmpty())
            return fds;

        for(FieldView fieldView : fieldValues){
//            Field f = documentKind.getFieldByCn(fieldView.getCn());
//            if(f == null) {
//                //fds.put("document_" + fieldView.getCn(), fieldView.getValue());
//                continue;
//            }
            //@TODO najprawdopodobniej do poprawy warto�ci ustawiane w object jest boolean to Boolean
            fds.put( fieldView.getCn(), fieldView.getValue());
        }
        return fds;
    }

    @Override
    public String toString() {
        return "OfficeDocumentDto{" +
                "dockindCn='" + dockindCn + '\'' +
                ", dictionaryCn='" + dictionaryCn + '\'' +
                ", fieldValues=" + fieldValues +
                ", limit='" + limit + '\'' +
                ", offset='" + offset + '\'' +
                '}';
    }
}
