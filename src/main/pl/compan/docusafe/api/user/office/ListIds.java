/**
 * 
 */
package pl.compan.docusafe.api.user.office;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Klasa reprezetuje list� z wartosciami liczbowy.
 */
@XmlRootElement(name="list-Ids")
public class ListIds {
	
	private List<Long> items;

	public ListIds() {
		this.items = new ArrayList<Long>();
	}
	
	public ListIds(List<Long> items) {
		this.items = items;
	}
	
//	@XmlElementWrapper(name="listids")
	@XmlElement(name="id")
	public List<Long> getItems() {
		return this.items;
	}
}
