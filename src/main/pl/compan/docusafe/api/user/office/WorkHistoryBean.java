package pl.compan.docusafe.api.user.office;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import pl.compan.docusafe.core.Audit;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.users.DSUser;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Calendar;
import java.util.Date;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@XmlRootElement(name = "audit")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class WorkHistoryBean {

    private Date date;
    private String author;
    private String content;

    public WorkHistoryBean(Audit entry) throws EdmException
    {
        this.date = entry.getCtime();
        this.author = DSUser.safeToFirstnameLastname(entry.getUsername());
        this.author = StringUtils.isBlank(this.author) ? entry.getUsername() : this.author;
        this.content = entry.getDescription();
    }

    public WorkHistoryBean() {}

	@XmlTransient
    @JsonIgnore
    public Date getDate()
    {
        return date;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("date")
    public String getDateIso(){
        return ISODateTimeFormat.dateTime().print(new DateTime(date));
    }

    @JsonIgnore
    public Calendar getEndTime() {
        return new DateTime(date).toGregorianCalendar();
    }

    public void setDateIso(String endIso){
        date = ISODateTimeFormat.dateTime().parseDateTime(endIso).toDate();
    }


    public void setEndTime(Calendar endTime){
        this.date = new DateTime(endTime).toDate();
    }

    public String getAuthor()
    {
        return author;
    }

    public String getContent()
    {
        return content;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
