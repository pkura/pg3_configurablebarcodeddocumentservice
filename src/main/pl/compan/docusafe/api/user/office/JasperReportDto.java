package pl.compan.docusafe.api.user.office;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import java.util.Calendar;

/**
 *
 * Obiekt reprezentujący encję raportu Jaspera
 */
@XmlRootElement(name = "jasper-report")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class JasperReportDto {

	private String reportType;
    private String report;
    private String userId;
    private DateTime start;
    private DateTime end;

    public JasperReportDto() {
	}

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonIgnore
    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonIgnore
    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("start")
    public String getStartIso(){
        return ISODateTimeFormat.dateTime().print(start);
    }

    public void setStartIso(String startIso){
        start = ISODateTimeFormat.dateTime().parseDateTime(startIso);
    }

    @XmlTransient
    //axis nie wspiera DateTime, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    @JsonProperty("end")
    public String getEndIso(){
        return ISODateTimeFormat.dateTime().print(end);
    }

    public void setEndIso(String endIso){
        end = ISODateTimeFormat.dateTime().parseDateTime(endIso);
    }

    @JsonIgnore
    public Calendar getStartTime(){
        return start.toGregorianCalendar();
    }

    public void setStartTime(Calendar start) {
        this.start = new DateTime(start);
    }

    @JsonIgnore
    public Calendar getEndTime() {
        return end.toGregorianCalendar();
    }

    public void setEndTime(Calendar endTime){
        this.end = new DateTime(endTime);
    }
}
