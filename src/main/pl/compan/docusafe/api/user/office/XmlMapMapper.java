package pl.compan.docusafe.api.user.office;


import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Class is used to wrapp element of Map in Rest tests to make able run tests 
 * for XML, when it must be in format XML, because jaxb2 dont support convert Map type.
 * 
 * @author <a href="mailto:przemyslaw.kura@docusafe.pl">Przemek Kura</a>
 *
 */
@XmlRootElement(name="param")
public class XmlMapMapper {

	private Map<String,Object> items;

	public XmlMapMapper() {
		this.items = new LinkedHashMap<String, Object>();
	}
	
	public XmlMapMapper(Map<String,Object> items) {
		this.items = items;
	}
	
	@XmlElementWrapper(name = "map")
	public Map<String,Object> getItems() {
		return this.items;
	}
}