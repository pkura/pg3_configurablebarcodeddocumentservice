package pl.compan.docusafe.api.user.office;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import pl.compan.docusafe.general.mapers.utils.strings.PolishStringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Obiekt s�uzy do Api wyszukiwania fullTextSearch
 */
@XmlRootElement(name = "fulltext-query")
@XmlSeeAlso(CommonOfficeDto.class)
@XmlAccessorType(XmlAccessType.FIELD)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class FullTextSearchDto extends CommonOfficeDto{
    protected String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = new PolishStringUtils().zamienPolskieZnaki(text);
    }

    @Override
    public String toString() {
        return "FullTextSearchDto{" +
                "text='" + text + '\'' +
                "limit='" + limit + '\'' +
                '}';
    }
}
