package pl.compan.docusafe.api.user.office;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.rest.views.RichDocumentView;

import java.util.List;

/**
 * Serwis dla document�w
 */
public interface DocumentService {

    /**
     * Walidator barkod�w
     * @param barcode
     * @return
     * @throws EdmException 
     */
    boolean validateBarcode(String algorithmName, String barcode) throws EdmException;

    /**
     * Wyszukuje dokument po barkodzie. Zwraca tylko identyfikator dokumentu
     *
     *
     *
     * @param barcode
     * @param check
     * @return
     */
    List<Long> findByBarcode(String barcode) throws EdmException;

    /**
     * Zwraca obiekt s�ownika dokumentu
     * @param documentId
     * @param dictionary
     * @param id
     * @return
     * @throws EdmException
     */
    List<FieldView> getDocumentDictionaryObject(String documentId, String dictionary, Long id) throws EdmException;

    /**
     * Wyszukuje obiekty s�ownika
     * @param dictionaryDtos
     * @return
     * @throws EdmException
     */
    List<DictionaryDto> searchDictionary(DictionaryDto dictionaryDtos) throws EdmException;

    /**
     * Dodaje obiekty s�ownika
     * @param dictionaryDtos
     * @return
     * @throws EdmException
     */
    List<DictionaryDto> add(DictionaryDto dictionaryDtos) throws EdmException;

    Boolean remove(DictionaryDto dictionaryDtos) throws EdmException;

    Boolean update(DictionaryDto dictionaryDtos) throws EdmException;

    List<OfficeDocumentDto> searchDocuments(OfficeDocumentDto dictionary) throws EdmException;

    List<RichDocumentView> fullTextSearchDocument(FullTextSearchDto text) throws Exception;

    String getDocumentAsXml(String documentId) throws EdmException;

    /**
     * Zwraca histori� pisma
     * @param documentId
     * @return
     * @throws EdmException
     * @throws Exception
     */
    List<WorkHistoryBean> getAudit(String documentId) throws EdmException, Exception;

    /**
     * Dodaje pismo do obserwowanych
     *
     * @param documentId
     * @return
     */
    void watch(String documentId) throws EdmException;

    /**
     * Usuwa pismo z obserwowanych
     *
     * @param documentId
     * @return
     */
    void unwatch(String documentId) throws EdmException;
}
