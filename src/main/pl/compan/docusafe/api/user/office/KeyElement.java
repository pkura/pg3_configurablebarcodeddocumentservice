package pl.compan.docusafe.api.user.office;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Element reprezerntujący klucz wartość
 */
@XmlRootElement(name = "key-element")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class KeyElement {

    private String id;
    private String value;

    public KeyElement() {
    }
    
    public KeyElement(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "KeyElement{" +
                "'" + id + '\'' +
                ", '" + value + '\'' +
                '}';
    }
}
