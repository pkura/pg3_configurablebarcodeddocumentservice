package pl.compan.docusafe.api.user.office;

import pl.compan.docusafe.core.EdmException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Interfejs dla akcji zwi�zanych z procesami
 */
public interface ProcessService {

    /**
     * Zwraca akcje na dokumencie zwi�zane z danym procesem
     * @param documentId
     */
    public List<KeyElement> getProcessActions(String documentId) throws EdmException;

    /**
     * wykonuje akcje procesow� na dokumencie
     * @param documentId
     * @param processAction
     */
    public void executeProcessAction(String documentId, String processAction) throws EdmException;

    /**
     * R�czna dekretacja
     * @param assigns
     * @param documentId
     */
    void manualMutliAssign(List<ManualAssignmentDto> assigns, String documentId) throws Exception;

    /**
     * Metoda zwraca list� id dokument�w kt�re posiadaja uruchomiony dany process
     * @param processName
     * @return
     */
    List<Long> getDocumentsForProcess(String processName) throws EdmException;

    /**
     * Metoda zwraca list� nazw proces�w powi�zanych z danym dokumentem
     *
     *
     * @param documentId
     * @return
     */
    List<KeyElement> getProcessForDocument(String documentId) throws EdmException;

    /**
     * Metoda zwraca diagram instancji procesu z zaznaczonym stanem dokumentu w procesie.
     *
     *
     * @param processInstanceId
     * @return
     */
    String getProcessInstance(String processInstanceId) throws IOException, Exception;

    /**
     * Zwraca definicj� procesu
     * @param processName
     * @return
     */
    String getProcessDefinition(CharSequence processName) throws IOException;

    /**
     * Laduje definicj� procesu
     * @param processName
     * @return
     */
    String loadProcessDefinition(String base64, String processName) throws Exception;
}
