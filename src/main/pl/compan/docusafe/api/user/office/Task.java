/**
 * 
 */
package pl.compan.docusafe.api.user.office;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchResponse;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.rest.views.DocumentKindView;
import pl.compan.docusafe.rest.views.DocumentView;

/**
 * Obiekt reprezentujący zadań
 */
@XmlRootElement(name="task")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Task {

	private Map<String,String> items;

	public Task() {
		this.items = new LinkedHashMap<String, String>();
	}
	
	public Task(Map<String,String> items) {
		this.items = items;
	}
	
	@XmlElementWrapper(name = "map")
	public Map<String,String> getItems() {
		return this.items;
	}
}
