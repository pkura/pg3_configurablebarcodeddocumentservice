package pl.compan.docusafe.api.user.office;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
@XmlRootElement(name = "manual-assign")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ManualAssignmentDto {
	
    /**
     * @var rodziaj celu dekretacji albo user albo dzial
     */
    private String targetName;
    /** @var rodzaj dekretacji true[EXECUTE] do realizacji, false[CC] - do wiadomosci */
    private Boolean kind;

    /** @var rodzaj celu true[U] u�ytkownik, false[D] dzia� */
    private Boolean targetKind;

    public ManualAssignmentDto() {
    }

    public ManualAssignmentDto(String targetName, Boolean kind, Boolean targetKind) {
        this.targetName = targetName;
        this.kind = kind;
        this.targetKind = targetKind;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public Boolean isKind() {
        return kind;
    }

    public void setKind(Boolean kind) {
        this.kind = kind;
    }

    public Boolean isTargetKind() {
        return targetKind;
    }

    public void setTargetKind(Boolean targetKind) {
        this.targetKind = targetKind;
    }

    @Override
    public String toString() {
        return "ManualAssignmentDto{" +
                "targetName='" + targetName + '\'' +
                ", kind=" + kind +
                ", targetKind=" + targetKind +
                '}';
    }
}
