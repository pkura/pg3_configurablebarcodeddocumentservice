package pl.compan.docusafe.api.user.office;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import pl.compan.docusafe.core.office.OfficeFolder;

/**
 * Obiekt reprezentujący foldery spraw
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@XmlRootElement(name = "office-folder")
public class OfficeFolderDto {

    private Long id;
    private String name;
    private String barcode;
    private String rwa;

    public OfficeFolderDto() {

    }

    public OfficeFolderDto(OfficeFolder f) {
        this.id = f.getId();
        this.name = f.getName();
        this.barcode = f.getBarcode();
        this.rwa = f.getRwa().getRwa();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getRwa() {
        return rwa;
    }

    public void setRwa(String rwa) {
        this.rwa = rwa;
    }
}
