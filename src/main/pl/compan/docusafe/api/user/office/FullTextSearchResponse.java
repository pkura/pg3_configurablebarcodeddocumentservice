package pl.compan.docusafe.api.user.office;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 */
@XmlRootElement(name = "fulltext-result")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class FullTextSearchResponse extends DictionaryDto{
    private String id;
    private String author;
    private String ctime;
    private String dockind_id;

    private FieldView fields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }

    public String getDockind_id() {
        return dockind_id;
    }

    public void setDockind_id(String dockind_id) {
        this.dockind_id = dockind_id;
    }

    public FieldView getFields() {
        return fields;
    }

    public void setFields(FieldView fields) {
        this.fields = fields;
    }
}
