/**
 * 
 */
package pl.compan.docusafe.api.user.office;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
  * Klasa reprezentująca obiekt textowy.
 */
@XmlRootElement(name="string")
@XmlAccessorType(XmlAccessType.FIELD)
public class StringDto {

	private String item;

	public StringDto() {
		this.item = new String();
	}
	
	public StringDto(String item) {
		this.item = item;
	}
	
	@XmlElement(name="value")
	public String getItem() {
		return this.item;
	}
}
