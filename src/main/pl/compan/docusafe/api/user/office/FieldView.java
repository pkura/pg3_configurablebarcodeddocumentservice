package pl.compan.docusafe.api.user.office;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import pl.compan.docusafe.core.dockinds.field.Field;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Obiekt reprezentujący pole
 */
@XmlRootElement(name = "field")
@XmlType(namespace="pl.compan.docusafe.api.user.office", name = "field")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class FieldView {
    private String cn;
    private String name;
    private String value;

    public FieldView() {
    }

    public FieldView(String cn, String name, String value) {
        this.cn = cn;
        this.name = name;
        this.value = value;
    }

    public FieldView(String cn, String value) {
        this.cn = cn;
        this.value = value;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "FieldView{" +
                "cn='" + cn + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
