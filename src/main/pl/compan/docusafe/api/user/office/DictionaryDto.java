package pl.compan.docusafe.api.user.office;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DockindInfoBean;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.EnumItem;
import pl.compan.docusafe.core.dockinds.field.Field;
import pl.compan.docusafe.core.users.DSUser;

import static pl.compan.docusafe.core.dockinds.dwr.Field.Type.*;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Map;

/**
 * Obiekt reprezentujący słowniki w usługach restowych
 */
@XmlRootElement(name = "dictionary")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DictionaryDto extends CommonOfficeDto{

    public final static Function<EnumItem, FieldView> FROM_ENUM_ITEMS = new Function<EnumItem,  FieldView>() {
        @Override
        public @Nullable
        FieldView apply(@Nullable EnumItem ei) {
            FieldView fv = new FieldView(ei.getCn(),ei.getTitle(),ei.getId().toString());
            return fv;
        }
    };
    private int limitInt;
    private int offsetInt;

    public DictionaryDto() {
    }

    public DictionaryDto(Map<String, Object> values) {
        fromMap(values);
    }

    public DictionaryDto(List<EnumItem> enumItems){
        fieldValues = Lists.transform(enumItems, FROM_ENUM_ITEMS);
    }
    public DictionaryDto(String dockindCn, String dictionaryCn, List<FieldView> fieldValues) {
        this.dockindCn = dockindCn;
        this.dictionaryCn = dictionaryCn;
        this.fieldValues = fieldValues;
    }

    @Override
    public String toString() {
        return "DictionaryDto{" +
                "dockindCn='" + dockindCn + '\'' +
                ", dictionaryCn='" + dictionaryCn + '\'' +
                ", fieldValues=" + fieldValues +
                ", limit='" + limit + '\'' +
                ", offset='" + offset + '\'' +
                '}';
    }

    public int getLimitInt() {
        return limitInt;
    }

    public int getOffsetInt() {
        return offsetInt;
    }
}
