package pl.compan.docusafe.api.user.office;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.util.CollectionUtils;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.core.dockinds.field.Field;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import static pl.compan.docusafe.core.dockinds.dwr.Field.Type.STRING;

/**
 * Obiekt abstrakcyjny, reprezetuj�cy elementy s�ownika dokument�w
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CommonOfficeDto {
    public static final int LIMIT = 50, OFFSET = 0;
    /** @var cn dockindu */
    protected String dockindCn;
    protected String dictionaryCn;

    protected List<FieldView> fieldValues = Lists.newLinkedList();

    public CommonOfficeDto() {
    }

    public CommonOfficeDto(Map<String, Object> values) {
        fromMap(values);
    }

    protected int limit;
    protected int offset;




    public String getDictionaryCn() {
        return dictionaryCn;
    }

    public void setDictionaryCn(String dictionaryCn) {
        this.dictionaryCn = dictionaryCn;
    }

    public List<FieldView> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(List<FieldView> fieldValues) {
        this.fieldValues = fieldValues;
    }

    @JsonIgnore
    public void setFieldValuesArray(FieldView[] fv) {
        this.fieldValues = Lists.newArrayList(fv);
    }

    @JsonIgnore
    public FieldView[] getFieldValuesArray(){
        return this.fieldValues.toArray(new FieldView[this.fieldValues.size()]);
    }

    public void addFieldValue(FieldView fv){
        fieldValues.add(fv);
    }

    public String getDockindCn() {
        return dockindCn;
    }

    public void setDockindCn(String dockindCn) {
        this.dockindCn = dockindCn;
    }

    /**
     * Konwertuje pola na liste fieldData u�ywane na dwr o akcji search, add, delete
     * @param documentKind
     * @return
     * @throws pl.compan.docusafe.core.EdmException
     */
    public Map<String, FieldData> getFieldDataMap(DocumentKind documentKind) throws EdmException {
        return getFieldDataMap(documentKind, false);
    }
    /**
     * Konwertuje pola na liste fieldData u�ywane na dwr o akcji search, add, delete
     * @param documentKind
     * @return
     * @throws EdmException
     */
    public Map<String, Object> getFieldObjectMap(DocumentKind documentKind, boolean addDictionaryCnToField) throws EdmException {
        Map<String, Object> fds = Maps.newHashMap();

        Field dictionaryFieldCn = documentKind.getFieldByCn(dictionaryCn);
        if(CollectionUtils.isEmpty(dictionaryFieldCn.getFields()))
            return fds;

        for(FieldView fieldView : fieldValues){
            if(fieldView.getCn().equalsIgnoreCase(DwrDictionaryBase.ID)){
                fds.put(fieldView.getCn(), new FieldData(STRING, fieldView.getValue()));
                continue;
            }
            Field f = documentKind.getDictionaryField(dictionaryCn, fieldView.getCn());

            String cn = addDictionaryCnToField ?
                      dictionaryCn + '_' + f.getCnWithoutLastUnderscore(dictionaryFieldCn.isMultiple())
                    : f.getCnWithoutLastUnderscore(dictionaryFieldCn.isMultiple());
            //@TODO najprawdopodobniej do poprawy warto�ci ustawiane w object jest boolean to Boolean
            fds.put(cn, fieldView.getValue());
        }
        return fds;
    }

    /**
     * Konwertuje pola na liste fieldData u�ywane na dwr o akcji search, add, delete
     * @param documentKind
     * @return
     * @throws EdmException
     */
    public Map<String, FieldData> getFieldDataMap(DocumentKind documentKind, boolean addDictionaryCnToField) throws EdmException {
        Map<String, FieldData> fds = Maps.newHashMap();

        Field dictionaryFieldCn = documentKind.getFieldByCn(dictionaryCn);
        if(CollectionUtils.isEmpty(dictionaryFieldCn.getFields()))
            return fds;

        for(FieldView fieldView : fieldValues){
            if(fieldView.getCn().equalsIgnoreCase(DwrDictionaryBase.ID)){
                fds.put(fieldView.getCn(), new FieldData(STRING, fieldView.getValue()));
                continue;
            }
            Field f = documentKind.getDictionaryField(dictionaryCn, fieldView.getCn());

            String cn = addDictionaryCnToField ?
                    dictionaryCn + '_' + f.getCnWithoutLastUnderscore(dictionaryFieldCn.isMultiple())
                    : f.getCnWithoutLastUnderscore(dictionaryFieldCn.isMultiple());
            //@TODO najprawdopodobniej do poprawy warto�ci ustawiane w object jest boolean to Boolean
            fds.put(cn, f.getFieldData(fieldView.getValue()));
        }
        return fds;
    }

    public void fromMap(Map<String,? extends Object> values) {
        for(Map.Entry<String,? extends Object> e : values.entrySet())
            fieldValues.add(new FieldView(e.getKey(), "", String.valueOf(e.getValue())));
    }

    @Override
    public String toString() {
        return "CommonOfficeDto{" +
                "dockindCn='" + dockindCn + '\'' +
                ", dictionaryCn='" + dictionaryCn + '\'' +
                ", fieldValues=" + fieldValues +
                ", limit='" + limit + '\'' +
                ", offset='" + offset + '\'' +
                '}';
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
