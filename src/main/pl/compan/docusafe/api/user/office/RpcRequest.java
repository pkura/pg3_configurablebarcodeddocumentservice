package pl.compan.docusafe.api.user.office;

import com.google.common.collect.Lists;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Obiekt requestu w protokole RPC
 */
@XmlRootElement(name = "rpc-request")
public class RpcRequest {

    /**
     * Nazwa methody jaka ma zosta� wywo�ana w RestRPCService
     */
    private String methodName;

    private List<String> params = Lists.newArrayList();


    public RpcRequest() {
    }


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

   // @JsonDeserialize(using = StringJsonDeserializer.class)
    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    public void addParam(String s){
        params.add(s);
    }

    @Override
    public String toString() {
        return "RpcRequest{" +
                "methodName='" + methodName + '\'' +
                ", params=" + params +
                '}';
    }
}
