package pl.compan.docusafe.api.user.office;

import com.google.common.base.Preconditions;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Zbi�r method w protokole RPC.
 */
// nazwa metody nie mo�e si� powtarza�
public enum RpcMethod {
	
//	DOCUMENT
	
    /**
     *  DOCUMENT_BARCODE_FIND ( Document.findByBarcode )
     */
    DOCUMENT_BARCODE_FIND("Document.findByBarcode", "/document/barcode={barcode}/find", GET, false),
	/**
     *  DOCUMENT_BARCODE_FIND_CHECK_BARCODE( Document.findByBarcode )
     */
    DOCUMENT_BARCODE_FIND_CHECK_BARCODE("Document.findByCheckedBarcode", "/document/barcode={barcode}/find/{check}/check", GET, false),
    /**
     *  DOCUMENT_FULL_TEXTSEARCH( Document.fullTextSearch )
     */
    DOCUMENT_FULL_TEXTSEARCH("Document.fullTextSearch","/document/fulltextsearch", POST, true),
    /**
     *  DOCUMENT_SEARCH_DICTIONARY( Document.searchDictionary )
     */	
	DOCUMENT_SEARCH_DICTIONARY("Document.searchDictionary","/document/dictionary/search", POST, true), 
	/**
     *  DOCUMENT_SEARCH( Document.search )
     */
    DOCUMENT_SEARCH("Document.search","/document/search", POST, true),
    /**
     *  DOCUMENT_VALIDATE_BARCODE( Document.validateBarcode )
     */
	DOCUMENT_VALIDATE_BARCODE("Document.validateBarcode","/document/barcode={barcode}/{algorithm}/validate", POST, false),
	/**
     *  DOCUMENT_ADD_DICTIONARY_ENTITY( Document.add.dictionary.entity )
     */
    DOCUMENT_ADD_DICTIONARY_ENTITY("Document.add.dictionary.entity","/document/dictionary/add", POST, true),
    /**
     *  DOCUMENT_GET_DOCUMENT_DICTIONARY( Document.get.document.dictionary )
     */
    DOCUMENT_GET_DOCUMENT_DICTIONARY("Document.get.document.dictionary",
			"/document/dictionary?documentId={documentId}&dictionary={dictionary}&dictionaryId={dictionaryId}",GET, false),
	/**
     *  DOCUMENT_GET_AS_XML( Document.getDocumentAsXml )
     */
	DOCUMENT_GET_AS_XML("Document.getDocumentAsXml","/document/xml/{documentId}",GET, false),
	/**
	 *  DOCUMENT_GET( Document.get )	
     */
	DOCUMENT_GET("Document.get","/document/{documentId}",GET, false),
	
    /**
     * DOCUMENT_GET_AUDIT( Document.getAudit )
     */
	DOCUMENT_GET_AUDIT("Document.getAudit","/document/{documentId}/audit",GET,false),
	
	/**
     *  DOCUMENT_UPDATE_DOCUMENT_DICTIONARY( Document.update.dictionary.entity )
     */
	DOCUMENT_UPDATE_DOCUMENT_DICTIONARY("Document.update.dictionary.entity","/document/dictionary/update", POST, true),
	/**
     *  DOCUMENT_REMOVE_DOCUMENT_DICTIONARY( Document.remove.dictionary.entity )
     */
	DOCUMENT_REMOVE_DOCUMENT_DICTIONARY("Document.remove.dictionary.entity","/document/dictionary/delete", POST, true),
	/**
	 * DOCUMENT_ADD_WATCH( Document.addWatch )
	 */
	DOCUMENT_ADD_WATCH("Document.addWatch","/document/{documentId}/watch", PUT, false),
	/**
	 * DOCUMENT_UN_WATCH( Document.unWatch )
	 */
	DOCUMENT_UN_WATCH("Document.unWatch","/document/{documentId}/unwatch", DELETE, false),
    
//	USER
	
	/**
     *  USER_FIND_ALL( User.getAll )
     */
	USER_FIND_ALL("User.getAll","/user",GET,false),
    /**
     *  ROLE_FIND_ALL( Roles.getAll )
     */
    ROLE_FIND_ALL("Roles.getAll","/roles/list",GET,false),
    /**
     *  USER_ROLE( User.getRoles )
     */
    USER_ROLE("User.getRoles","/user/{username}/roles",GET,false),

    /**
     *  USER_ROLE( User.getRoles )
     */
    USER_UPDATE_ROLE("User.updateRoles","/user/{username}/roles",POST,true),

	/**
     *  USER_CREATE( User.create )
     */
    USER_CREATE("User.create","/user",POST,true),
	/**
     *  USER_FIND_ONE( User.get )
     */
    USER_FIND_ONE("User.get","/user/{userId}",GET,false),
    /**
     *  USER_FIND_ONE( User.get )
     */
    USER_FIND_ONE_BY_NAME("User.getByName","/user/{userId}/name",GET,false),
    /**
     *  USER_UPDATE( User.update )
     */
    USER_UPDATE("User.update","/user/{userId}",PUT,true),
    /**
     *  USER_GET_DIVISIONS( User.getDivisions )
     */
	USER_GET_DIVISIONS("User.getDivisions","/user/{userId}/division",GET,false),
    /**
     *  USER_GET_SUBSTITUTIONS_BY_USER( User.getSubstitutionsByUser )
     */
    USER_GET_SUBSTITUTIONS_BY_USER("User.getSubstitutionsByUser","/user/{userId}/substitution",GET,false),
    /**
     *  USER_GET_SUBSTITUTE( User.getSubstitute )
     */
	USER_GET_SUBSTITUTE("User.getSubstitute","/user/{userId}/substitute",GET,false),
	
	/**
     *  USER_DELETE( User.delete )
     */
    USER_DELETE("User.delete","/user/{userId}",DELETE,false),
	
//	TASK_LIST
	
	/**
     *  TASK_LIST_USER( TaskList.get )
     */
    TASK_LIST_USER("TaskList.get","/tasklist/{username}/user/{offset}/offset",GET,false), 
    
//  SUBSTITUTION  
    
    /**
     * SUBSTITUTION_CREATE( Substitution.create )
     */
    SUBSTITUTION_CREATE("Substitution.create","/substitution",POST,true), 
	
    /**
     * SUBSTITUTION_GET_BY_ID( Substitution.getById )
     */
    SUBSTITUTION_GET_BY_ID("Substitution.getById","/substitution/{substitutionId}",GET,false),

    /**
     * SUBSTITUTION_GET_ALL( Substitution.getAllCurrent )
     */
	SUBSTITUTION_GET_ALL_CURRENT("Substitution.getAllCurrent","/substitution",GET,false),

	/**
	 * SUBSTITUTION_GET_CURRENT( Substitution.getAllCurrentAtTime )
	 */
	SUBSTITUTION_GET_CURRENT_AT_TIME("Substitution.getAllCurrentAtTime","/substitution?time={time}",GET,false),	
	
	/**
	 * SUBSTITUTION_DELETE( Substitution.delete )
	 */
    SUBSTITUTION_DELETE("Substitution.delete","/substitution/{substitutionId}",DELETE,false), 
    
//  DIVISION
    
    /**
     *  DIVISION_CREATE( Division.create )
     */
    DIVISION_CREATE("Division.create","/division",POST,true),
    /**
     *  DIVISION_GET( Division.get )
     */
    DIVISION_GET("Division.get","/division/{divisionId}",GET,false),
    /**
     *  DIVISION_ADD_USER( Division.add.user )
     */
    DIVISION_ADD_USER("Division.add.user","/division/{divisionId}/user",POST,true), 
    /**
     *  DIVISION_GET_USERS( Division.getUsers)
     */
	DIVISION_GET_USERS("Division.getUsers","/division/{divisionId}/user",GET,false),
	/**
     *  DIVISION_GET_SUBDIVISIONS( Division.getSubdivisions )
     */
	DIVISION_GET_SUBDIVISIONS("Division.getSubdivisions","/division/{divisionId}/division",GET,false),
	/**
     *  DIVISION_GET_ALL( Division.getAll )
     */
	DIVISION_GET_ALL("Division.getAll","/division",GET,false),
	/**
     *  DIVISION_GET_BY_GUID( Division.getByGuid)
     */
	DIVISION_GET_BY_GUID("Division.getByGuid","/division?guid={guid}",GET,false),		
	/**
     *  DIVISION_UPDATE( Division.update)
     */
    DIVISION_UPDATE("Division.update","/division/{divisionId}",PUT,true),
    /**
     *  DIVISION_DELETE( Division.delete )
     */
    DIVISION_DELETE("Division.delete","/division/{divisionId}",DELETE,false),

    /**
     * DIVISION_REMOVE_USER("Division.remove.user")
     */
    DIVISION_REMOVE_USER("Division.remove.user", "/division/{divisionId}/user/{userId}", DELETE, false),
    
    
//  OFFICECASE
    
    /**
     * OFFICECASE_CREATE( OfficeCase.create )
     */
	OFFICECASE_CREATE("OfficeCase.create","/officecase/create",POST,true),
    /**
     * OFFICECASE_GET_USER( OfficeCase.get.user )
     */
    OFFICECASE_GET_USER("OfficeCase.get.user","/officecase/{username}/user",GET,false),
    /**
	 * OFFICECASE_ADD( OfficeCase.addDocumentToOfficeCase )
	 */
	OFFICECASE_ADD("OfficeCase.addDocumentToOfficeCase","/officecase/{documentId}/document/{officeCaseId}/case/add",PUT,false),
	/**
     * OFFICECASE_GET( OfficeCase.get )
     */
    OFFICECASE_GET("OfficeCase.get","/officecase/{officeCaseId}/case",GET,false), 
    /**
	 * OFFICECASE_GET_DOCUMENT( OfficeCase.get.document )
	 */
    OFFICECASE_GET_DOCUMENT("OfficeCase.get.document","/officecase/{documentId}/document",GET,false),

    /**
     * OFFICECASE_REMOVE_DOCUMENT( OfficeCase.removeDocumentFromCase )
     */
	OFFICECASE_REMOVE_DOCUMENT("OfficeCase.removeDocumentFromCase","/officecase/{documentId}/document/{officeCaseId}/case/remove",DELETE,false),
	   
    /**
     * OFFICECASE_GET_ALL_OFFICECASES( OfficeCase.getOfficeCaseToOfficeCase )
     */
	OFFICECASE_GET_ALL_OFFICECASES("OfficeCase.getOfficeCaseToOfficeCase","/officecase/{officeCaseId}/getcases",GET,false), 
	
	/**
	 * OFFICECASE_GET_ALL_OFFICECASES_FROM_DIVISION( OfficeCase.getDivisionOfficeCase )
	 */
	OFFICECASE_GET_ALL_OFFICECASES_FROM_DIVISION("OfficeCase.getDivisionOfficeCase","/officecase/division?guid={guid}",GET,false), 
	
	/**
	 * OFFICECASE_GET_ALL_PRIORITIES_FOR_CASE( OfficeCase.getCasePriority )
	 */
	OFFICECASE_GET_ALL_PRIORITIES_FOR_CASE("OfficeCase.getCasePriority","/officecase/priority",GET,false), 
	
	/**
	 * OFFICECASE_GET_PORTFOLIO( OfficeCase.get.portfolio)
	 */
    OFFICECASE_GET_PORTFOLIO("OfficeCase.get.portfolio","/officecase/portfolio/{guid}",GET,false),
    
    /**
	 * OFFICECASE_GET_DOCUMENTS( OfficeCase.getDocuments)
	 */
    OFFICECASE_GET_DOCUMENTS("OfficeCase.getDocuments","/officecase/{officeCaseId}/documents/get",GET,false),
    
    /**
     * OFFICECASE_GET_AUDIT( OfficeCase.getAudit )
     */
    OFFICECASE_GET_AUDIT("OfficeCase.getAudit","/officecase/{id}/audit",GET,false),
    
    /**
     * OFFICECASE_UPDATE( OfficeCase.update )
     */
    OFFICECASE_UPDATE("OfficeCase.update","/officecase/update",POST,true),
    
    /**
     * OFFICECASE_DELETE( OfficeCase.delete )
     */
    OFFICECASE_DELETE("OfficeCase.delete","/officecase/remove",POST,true),
    
    /**
     * OFFICECASE_ADD_TO_OFFICECASE( OfficeCase.addOfficeCase )
     */
    OFFICECASE_ADD_TO_OFFICECASE("OfficeCase.addOfficeCase","/officecase/{officeCaseId}/officecase/{officeCaseAddId}/add",PUT,false),
    
    /**
     * OFFICECASE_REMOVE_FROM_OFFICECASE( OfficeCase.removeOfficeCase ) 
     */
    OFFICECASE_REMOVE_FROM_OFFICECASE("OfficeCase.removeOfficeCase","/officecase/{officeCaseId}/officecase/{officeCaseRemoveId}/remove",DELETE,false),
	
//	AUTHENTICATION
	
	/**
	 * AUTHENTICATION_LOGIN( Auth.login )
	 */
	AUTHENTICATION_LOGIN("Auth.login","/login?username={username}&password={password}",POST,false),
	
	/**
	 * AUTHENTICATION_LOGIN_CAS( Auth.loginCAS )
	 */
	AUTHENTICATION_LOGIN_CAS("Auth.loginCAS","/login-cas?t={ticket}",POST,false),
	
	/**
	 * AUTHENTICATION_LOGOUT("Auth.logout")
	 */
	AUTHENTICATION_LOGOUT("Auth.logout","/logout",POST,false),
	
//	XES
	
	/**
	 * XES_GET_LOG_FOR_DOCUMENT( Xes.getLogByDocumentId )
	 */
	XES_GET_LOG_FOR_DOCUMENT("Xes.getLogByDocumentId","/xeslog/{documentId}/document",GET,false),
	/**
	 * XES_GET_LOG_FOR_USER( Xes.getLogByUserId" )
	 */
	XES_GET_LOG_FOR_USER("Xes.getLogByUserId","/xeslog/{userId}/user?time={time}",GET,false),
	
	/**
	 * XES_GET_LOG_FOR_USER_START_END_TIME( Xes.getLogByUserId" )
	 */
	XES_GET_LOG_FOR_USER_START_END_TIME("Xes.getLogByUserIdWithTime","/xeslog/{userId}/userRange?starttime={starttime}&endtime={endtime}",GET,false),

	
//	JASPER
	
	/**
	 * JASPER_ADD_REPORT( Jasper.addReport )
	 */
	JASPER_ADD_REPORT("Jasper.addReport","/jasper-report",PUT,true),
	
	/**
	 * JASPER_GET_REPORT_LIST( Jasper.getReportLists )
	 */
	JASPER_GET_REPORT_LIST("Jasper.getReportLists","/jasper-report",GET,false),
	
	/**
	 * JASPER_GET_REPORT_TYPE( Jasper.getReportType )
	 */
	JASPER_GET_REPORT_TYPE("Jasper.getReportType","/jasper-report/typntne",GET,false),
	
	//	CONVERTER
	
	/**
	 * CONVERTER_ATTACHMENT_TO_PDF( Converter.convertAttachmentAsPdf )
	 */
	CONVERTER_ATTACHMENT_TO_PDF("Converter.convertAttachmentAsPdf","/converter/{attachmentId}",PUT,false), 
	
	//  GRAPH
	
	/**
	 * GRAPH_GET_FOR_DOCUMENT( Graph.getGraphByDocumentId )
	 */
	GRAPH_GET_FOR_DOCUMENT("Graph.getGraphByDocumentId","/graphml/{documentId}/document",GET,false),
	
	//  DOT
	
	/**
	 * DOT_GET_FOR_DOCUMENT( Dot.getDotByDocumentId )
	 */
	DOT_GET_FOR_DOCUMENT("Dot.getDotByDocumentId","/dot/{documentId}/document",GET,false),
	
	// PROCESS
	
	/**
	 * PROCESS_GET_ACTIONS( Process.getProcessActions )
	 */
	PROCESS_GET_ACTIONS("Process.getProcessActions","/process/{documentId}/document/actions",GET,false), 
	
	/**
	 * PROCESS_EXECUTE_ACTION( Process.executeProcessAction )
	 */
	PROCESS_EXECUTE_ACTION("Process.executeProcessAction","/process/{documentId}/documentId/{processAction}/execute",POST,false),
	
	/**
	 * PROCESS_EXECUTE_ACTION( Process.assignMultiManualAssignment )
	 */
	PROCESS_MULTI_MANUAL_ASSIGNMENT("Process.assignMultiManualAssignment","/process/{documentId}/manual-assign",POST,true),
	/**
	 * PROCESS_GET_DOCUMENT_PROCESSES( Process.getProcessForDocument )
	 */
	PROCESS_GET_DOCUMENT_PROCESSES("Process.getProcessForDocument","/document/{documentId}/processes",GET,false), 
	/**
	 * PROCESS_GET_PROCESS_DOCUMENTS( Process.getDocumentsForProcess )
	 */
	PROCESS_GET_PROCESS_DOCUMENTS("Process.getDocumentsForProcess","/process/{processName}/documents",GET,false), 
	/**
	 * PROCESS_GET_PROCESS_DEFINITIONS( Process.getProcessDefinition )
	 */
	PROCESS_GET_PROCESS_DEFINITIONS("Process.getProcessDefinition","/process/{processName}/definition",GET,false), 
	/**
	 * PROCESS_GET_PROCESS_DIAGRAM( Process.getProcessInstanceDiagram )
	 */
	PROCESS_GET_PROCESS_DIAGRAM("Process.getProcessInstanceDiagram","/process/{processId}/diagram",GET,false),
	/**
	 * PROCESS_LOAD_PROCESS_DEFINITIONS( Process.loadProcessDefinition )
	 */
	PROCESS_LOAD_PROCESS_DEFINITIONS("Process.loadProcessDefinition","/process/{processName}/definition",POST,true),
	
	// TEMPLATE
	
	TEMPLATE_GET_FOR_DOCUMENT("Template.getTemplatesForDocument","/template/documentId={documentId}",GET,false),
	
	TEMPLATE_LOAD_FOR_DOCUMENT("Template.addTemplateForDocumentKind","/template/dockind={dockind}/{templateName:.+}",POST,true)
	
	;
    
    private String name;
    private String url;
    private RequestMethod requestMethod;
    private boolean hasRequestBody;

    RpcMethod(String name,String url, RequestMethod RequestMethod, boolean hasRequestBody) {
        this.name = name;
        this.url = url;
        this.requestMethod = RequestMethod;
        this.hasRequestBody = hasRequestBody;
    }

    public static RpcMethod findRpcMethod(String methodName){
        Preconditions.checkNotNull(methodName, "Nazwa methody nie mo�ebyc pusta");
        RpcMethod response = null;
        for(RpcMethod rm : RpcMethod.values()){
            if(rm.isNameEqual(methodName)){
                response = rm;
            }
        }

        return response;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @see getRequestMethod
     * @return
     */
    public HttpMethod getHttpMethod(){
        return HttpMethod.valueOf(requestMethod.name());
    }

    public RequestMethod getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(RequestMethod RequestMethod) {
        this.requestMethod = RequestMethod;
    }


    public boolean hasRequestBody() {
        return hasRequestBody;
    }

    public void setHasRequestBody(boolean hasRequestBody) {
        this.hasRequestBody = hasRequestBody;
    }

    public boolean isNameEqual(String nn){
        return nn.equals(this.getName());
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
