package pl.compan.docusafe.api.user;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Obiekt reprezentuj�cy element struktury organizacyjnej
 */
@XmlRootElement(name ="division")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DivisionDto {
    /** @var id dzia�u*/
    private Long id;

    /**
     * @var typ dzia�u
     */
    @XmlElement(required  =true)
    private DivisionType type;

    /**
     * @var guid dzia�u
     */
    private String guid;

    /**
     * @var id rodzica dzia�u
     */
    private Long parentId;

    /**
     * nazwa dzia�u
     */
    private String name;
    /**
     * @var kod dzia�u
     */
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlTransient
    //axis nie wspiera enum�w, adnotacja nie jest wspierana przez axisa2
    //potrzebny jest dodatkowy wpis w services.xml
    public DivisionType getType() {
        return type;
    }

    public void setType(DivisionType type) {
        this.type = type;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof DivisionDto)) return false;

        DivisionDto that = (DivisionDto) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (!guid.equals(that.guid)) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (type != that.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + type.hashCode();
        result = 31 * result + guid.hashCode();
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        return result;
    }
}
