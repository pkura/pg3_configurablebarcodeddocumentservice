package pl.compan.docusafe.api.user;

import com.google.common.base.Optional;
import org.joda.time.DateTime;
import pl.compan.docusafe.core.EdmException;

import java.util.List;

/**
 *  Serwis for querying and creating substitutions
 */
public interface SubstitutionService {

    /**
     * Returns all current active substitutions
     * @return list, may be empty
     */
    List<SubstitutionDto> getAllCurrentSubstitutions();

    /**
     * Returns substitutions that were active at certain time
     * @return list, may be empty
     */
    List<SubstitutionDto> getSubstitutionsAtTime(DateTime tim);

    /**
     * Returns current substitution of selected user
     * @param user user that is substituted
     * @return entry with current substitution or Optional.absent() if selected
     * user is not currently substituted
     */
    Optional<SubstitutionDto> getUserSubstitution(UserDto user);

    /**
     * Returns current substitutions filled by user
     * @param user user thath is substituting
     * @return list of
     */
    List<SubstitutionDto> getSubstitutedByUser(UserDto user);

    /**
     * Creates new substitution
     * @param substitution substitution that ends in future
     * @throws java.lang.IllegalArgumentException if substitution does not end
     * in future
     * @return id of substitution entry
     */
    long create(SubstitutionDto substitution) throws EdmException;

    /**
     * Returns substiturion by id
     * @param id id of substitution entry
     * @return entry or Optional.absent if entry not found or is cancelled
     */
    Optional<SubstitutionDto> getById(long id);

    /**
     * Cancels (ends) selected substitution
     * @param substitution substitution to be cancelled
     */
    void cancel(SubstitutionDto substitution);
}
