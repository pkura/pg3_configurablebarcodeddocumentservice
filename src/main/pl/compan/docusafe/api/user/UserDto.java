package pl.compan.docusafe.api.user;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import pl.compan.docusafe.core.users.DSUser;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 *  Dto dla user�w serializowane do JSON'a poprzez Jacksona, i wykorzystywane
 *  w WS
 */
@XmlRootElement(name = "user")
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public final class UserDto implements Serializable {
    //null przydatny tylko przy tworzeniu
    /**
     * @var id usera
     */
    private Long id;
    /**
     * @var imie
     */
    private String firstname;
    /**
     * @var nazwisko
     */
    private String lastname;
    /**
     * @var nazwa
     */
    private String username;

    /** @var has�o */
    private String password;

    /**
     * @var id z zewn�trznego systemu
     */
    private String personNumber;

    /**@var email
     *
     */
    private String email;

    public UserDto() {
    }

    public UserDto(DSUser user) {
        setFirstname(user.getFirstname());
        setLastname(user.getLastname());
        setId(user.getId());
        setUsername(user.getName());
        this.personNumber = user.getExternalName();
        this.email = user.getEmail();
    }

    /**
     * Checks if user is valid for saving
     */
    public void validate(){
        if(StringUtils.isBlank(firstname)){
            throw new IllegalArgumentException("invalid firstname");
        } else if (StringUtils.isBlank(lastname)){
            throw new IllegalArgumentException("invalid lastname");
        } else if (StringUtils.isBlank(username)){
            //|| !username.matches("^[a-zA-Z][a-zA-Z0-9]+$")
            throw new IllegalArgumentException("invalid username");
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String login) {
        this.username = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
