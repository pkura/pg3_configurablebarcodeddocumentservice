package pl.compan.docusafe.api.user;

import com.google.common.base.Optional;

import java.util.List;

/**
 * Service responsible for CRUD for divisions,
 * additionally allows CRUD for division-user mapping
 */
public interface DivisionService {
    /**
     * Returns all divisions that were not deleted (hidden)
     * @return all divisions
     */
    List<DivisionDto> getAllDivisions();

    /**
     * Returns all users in division with selected id, does not return users in subdivisions
     * of selected division
     * @param division id of division
     * @return list of users, may be empty
     */
    List<UserDto> getUsers(DivisionDto division);

    /**
     * Returns subdivisions of selected division
     * @param parentDivision selected parent division
     * @return list of subdivisions, may be empty
     */
    List<DivisionDto> getSubdivisions(DivisionDto parentDivision);

    /**
     * Adds selected user to division
     * @param division division
     * @param user user to add
     */
    void addUser(DivisionDto division, UserDto user);

    /**
     * Adds selected user to division
     * @param division division
     * @param user user to add
     */
    void addUser(String divisionGuid, String externalName);
    /**
     * Removes selected user from division
     * @param division division that should contain user
     * @param user user to be deleted from division
     * @return true if user was deleted, false otherwise
     */
    boolean removeUser(DivisionDto division, UserDto user);

    /**
     * Removes selected user from division
     * @param division division that should contain user
     * @param user user to be deleted from division
     * @return true if user was deleted, false otherwise
     */
    boolean removeUser(String divisionGuid, String externalName);

    /**
     * Returns divisions containing selected user
     * @param user selected user
     * @return list of divisions containing user, may be empty
     * @throws java.lang.NullPointerException when user is null
     */
    List<DivisionDto> getUserDivisions(UserDto user);

    /**
     * Returns division with selected guid, if it was not deleted (hidden)
     * @param guid selected guid
     * @return division, or Optional.absent() if such division does not exists, or was deleted
     * @throws java.lang.NullPointerException when guid is null
     */
    Optional<DivisionDto> getByGuid(String guid);

    /**
     * Return division with selected id, if it was not deleted (hidden)
     * @param divisionId id of idvision
     * @return division, or Optional.absent() if such division does not exists, or was deleted
     */
    Optional<DivisionDto> getById(long divisionId);

    /**
     * Creates new division
     * @param newDivision division to be created
     * @return id of created division
     * @throws java.lang.NullPointerException when newDivision is null
     * @throws java.lang.IllegalArgumentException if newDivision is not valid division
     */
    long create(DivisionDto newDivision);

    /**
     * Updates selected division
     * @param division division to be updated
     * @throws java.lang.NullPointerException if divisionDto is null
     * @throws java.lang.IllegalArgumentException if divisionDto is not valid division
     */
    void update(DivisionDto division);

    /**
     * Deletes (hides) selected division
     * @param division division to be deleted
     * @throws java.lang.NullPointerException when divisionDto is null
     * @throws java.lang.IllegalArgumentException if divisionDto is without id field
     */
    void delete(DivisionDto division);
}
