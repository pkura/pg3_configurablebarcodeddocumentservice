package pl.compan.docusafe.api;

import java.util.Map;
import java.util.Set;

public abstract class ActivityInfo implements Map<String,Object> {

    public static final String VARIABLES = "VARS";
    public static final String ACTIVITY_ID_KEY = "activity_id";
    public static final String TASK_NAME_KEY = "task_name";
    public static final String TASK_ASSIGNEE_USER_KEY = "task_assignee_user";
    public static final String CURRENT_USER_KEY = "current_user";
    public static final String TASK_ID_KEY = "task_id";

    public abstract String getTaskName();

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object put(String key, Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object remove(Object key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }
}