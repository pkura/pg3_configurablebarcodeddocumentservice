package pl.compan.docusafe.api;

import pl.compan.docusafe.core.dockinds.DockindScheme;
import pl.compan.docusafe.core.dockinds.field.SchemeField;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Fasada nad DockindScheme umożliwiająca m.in. wykorzystanie w Droolsach
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
public class SchemeFields implements Map<String, SchemeField> {

    private final static Logger LOG = LoggerFactory.getLogger(SchemeFields.class);
    private final DockindScheme ds;

    private SchemeFields(DockindScheme ds) {
        this.ds = ds;
    }

    public SchemeField get(Object string){
        SchemeField sf = ds.getSchemeFields().get(string);
        if(sf == null && string != null){
            sf = new SchemeField(string + "");
            ds.addSchemeField(sf);
        }
        return sf;
    }

    public static SchemeFields fromScheme(DockindScheme ds){
        return new SchemeFields(ds);
    }

    public int size() {
        return ds.getSchemeFields().size();
    }

    public boolean isEmpty() {
        return ds.getSchemeFields().isEmpty();
    }

    public boolean containsKey(Object key) {
        return true;
    }

    public boolean containsValue(Object value) {
        return ds.getSchemeFields().containsValue(value);
    }

//    public SchemeField get(Object key) {
//        return ds.getSchemeFields().get(key);
//    }

    public SchemeField put(String key, SchemeField value) {
        return ds.getSchemeFields().put(key, value);
    }

    public SchemeField remove(Object key) {
        return null;
    }

    public void putAll(Map<? extends String, ? extends SchemeField> t) {
        ds.getSchemeFields().putAll(t);
    }

    public void clear() {
    }

    public Set<String> keySet() {
        return ds.getSchemeFields().keySet();
    }

    public Collection<SchemeField> values() {
        return ds.getSchemeFields().values();
    }

    public Set<Entry<String, SchemeField>> entrySet() {
        return ds.getSchemeFields().entrySet();
    }
}