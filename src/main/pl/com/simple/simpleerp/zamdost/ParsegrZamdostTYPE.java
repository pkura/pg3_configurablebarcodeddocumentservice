//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.09.13 at 09:22:47 AM CEST 
//


package pl.com.simple.simpleerp.zamdost;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import pl.com.simple.simpleerp.*;



/**
 * <p>Java class for parsegr_zamdost_TYPE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="parsegr_zamdost_TYPE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="katparam_id" type="{http://www.simple.com.pl/SimpleErp}IDN_ID_TYPE"/>
 *         &lt;element name="parseg_id" type="{http://www.simple.com.pl/SimpleErp}ID_TYPE"/>
 *         &lt;element name="wartpar" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *               &lt;minLength value="0"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sprzczywyroz" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="zrodlo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "parsegr_zamdost_TYPE", propOrder = {

})
public class ParsegrZamdostTYPE {

    @XmlElement(name = "katparam_id", required = true)
    protected IDNIDTYPE katparamId;
    @XmlElement(name = "parseg_id", required = true)
    protected BigDecimal parsegId;
    protected String wartpar;
    protected BigDecimal sprzczywyroz;
    protected Integer zrodlo;

    /**
     * Gets the value of the katparamId property.
     * 
     * @return
     *     possible object is
     *     {@link IDNIDTYPE }
     *     
     */
    public IDNIDTYPE getKatparamId() {
        return katparamId;
    }

    /**
     * Sets the value of the katparamId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDNIDTYPE }
     *     
     */
    public void setKatparamId(IDNIDTYPE value) {
        this.katparamId = value;
    }

    /**
     * Gets the value of the parsegId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getParsegId() {
        return parsegId;
    }

    /**
     * Sets the value of the parsegId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setParsegId(BigDecimal value) {
        this.parsegId = value;
    }

    /**
     * Gets the value of the wartpar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWartpar() {
        return wartpar;
    }

    /**
     * Sets the value of the wartpar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWartpar(String value) {
        this.wartpar = value;
    }

    /**
     * Gets the value of the sprzczywyroz property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSprzczywyroz() {
        return sprzczywyroz;
    }

    /**
     * Sets the value of the sprzczywyroz property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSprzczywyroz(BigDecimal value) {
        this.sprzczywyroz = value;
    }

    /**
     * Gets the value of the zrodlo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getZrodlo() {
        return zrodlo;
    }

    /**
     * Sets the value of the zrodlo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setZrodlo(Integer value) {
        this.zrodlo = value;
    }

}
