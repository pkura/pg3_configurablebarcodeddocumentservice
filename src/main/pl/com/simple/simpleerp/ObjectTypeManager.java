package pl.com.simple.simpleerp;

//import pl.com.simple.simpleerp.dokzak.ObjectFactory;
//import pl.com.simple.simpleerp.zapdost.ObjectFactory;
import pl.com.simple.simpleerp.zapdost.ObjectFactory;
import pl.compan.docusafe.core.dockinds.dwr.EnumValues;
import pl.compan.docusafe.util.DateUtils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 10.10.13
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */
public class ObjectTypeManager {

    public static IDMIDTYPE createIDMIDTYPE(pl.com.simple.simpleerp.dokzak.ObjectFactory factory, EnumValues enumValue) {
        if(enumValue.getSelectedOptions().size()==0 || enumValue.getSelectedOptions().get(0).equals("") || enumValue.getSelectedOptions().get(0)==null)
            return null;

        IDMIDTYPE element = factory.createIDMIDTYPE();
        element.setType(IDENTITYNATURETYPE.N);
        element.setId(new BigDecimal(enumValue.getSelectedOptions().get(0)));
        return element;
    }

    public static IDMIDTYPE createIDMIDTYPE(pl.com.simple.simpleerp.dokzak.ObjectFactory factory, String idm) {
        if(idm.equals("") || idm==null)
            return null;

        IDMIDTYPE element = factory.createIDMIDTYPE();
        element.setType(IDENTITYNATURETYPE.T);
        element.setIdm(idm);
        return element;
    }

    public static IDMIDTYPE createIDMIDTYPE(pl.com.simple.simpleerp.dokzak.ObjectFactory factory, BigDecimal id) {
        if(id == null)
            return null;

        IDMIDTYPE element = factory.createIDMIDTYPE();
        element.setType(IDENTITYNATURETYPE.N);
        element.setId(id);
        return element;
    }

    public static IDSIDTYPE createIDSIDTYPE(pl.com.simple.simpleerp.dokzak.ObjectFactory factory, EnumValues enumValues) {
        if(enumValues.getSelectedOptions().get(0).equals("") || enumValues.getSelectedOptions().get(0) == null)
            return null;

        IDSIDTYPE idsidtype = factory.createIDSIDTYPE();
        idsidtype.setType(IDENTITYNATURETYPE.N);
        idsidtype.setId(new BigDecimal(enumValues.getSelectedOptions().get(0)));
        return idsidtype;
    }

    public static XMLGregorianCalendar convertToXMLGregorianCalendarDate(Date date) throws DatatypeConfigurationException {
        GregorianCalendar gregoryDate = new GregorianCalendar();
        DateUtils.exportDateFormat.format(date);
        gregoryDate.setTime(date);
        XMLGregorianCalendar xmlDate;
        xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregoryDate);
        return xmlDate;
    }

    public static IDNIDTYPE createIDNIDTYPE(pl.com.simple.simpleerp.dokzak.ObjectFactory fac, IDENTITYNATURETYPE type, BigDecimal id, String idn){
        IDNIDTYPE element = fac.createIDNIDTYPE();
        element.setType(type);
        element.setId(id);
        element.setIdn(idn);
        return element;
    }

    public static IDMIDTYPE createIDMIDTYPE(pl.com.simple.simpleerp.zapdost.ObjectFactory factory, EnumValues enumValue) {
        IDMIDTYPE element = factory.createIDMIDTYPE();
        element.setType(IDENTITYNATURETYPE.N);
        element.setId(new BigDecimal(enumValue.getSelectedOptions().get(0)));
        return element;
    }

    public static IDNIDTYPE createIDNIDTYPE(pl.com.simple.simpleerp.zapdost.ObjectFactory factory, EnumValues enumValue) {
        IDNIDTYPE element = factory.createIDNIDTYPE();
        element.setType(IDENTITYNATURETYPE.N);
        element.setId(new BigDecimal(enumValue.getSelectedOptions().get(0)));
        return element;
    }

    public static IDSIDTYPE createIDSIDTYPE(pl.com.simple.simpleerp.zapdost.ObjectFactory factory, EnumValues enumValues) {
        IDSIDTYPE idsidtype = factory.createIDSIDTYPE();
        idsidtype.setType(IDENTITYNATURETYPE.N);
        idsidtype.setId(new BigDecimal(enumValues.getSelectedOptions().get(0)));
        return idsidtype;
    }

    public static IDNIDTYPE createIDNIDTYPE(pl.com.simple.simpleerp.zapdost.ObjectFactory fac, IDENTITYNATURETYPE type, BigDecimal id, String idn){
        IDNIDTYPE element = fac.createIDNIDTYPE();
        element.setType(type);
        element.setId(id);
        element.setIdn(idn);
        return element;
    }

    public static SYMBOLWALUTYIDTYPE createSYMBOLWALUTYIDTYPE(ObjectFactory factory, EnumValues enumV) {
        SYMBOLWALUTYIDTYPE symbolWaluty = factory.createSYMBOLWALUTYIDTYPE();
        symbolWaluty.setSymbolwaluty(enumV.getSelectedValue());
        symbolWaluty.setType(IDENTITYNATURETYPE.N);
        symbolWaluty.setId(new BigDecimal(enumV.getSelectedOptions().get(0)));
        return symbolWaluty;
    }

    public static SYMBOLWALUTYIDTYPE createSYMBOLWALUTYIDTYPE(ObjectFactory factory, String idm) {
        SYMBOLWALUTYIDTYPE symbolWaluty = factory.createSYMBOLWALUTYIDTYPE();
        symbolWaluty.setSymbolwaluty("PLN");
        symbolWaluty.setType(IDENTITYNATURETYPE.T);
//        symbolWaluty.setId(new BigDecimal(enumV.getSelectedOptions().get(0)));
        return symbolWaluty;
    }

    public static SYMBOLWALUTYIDTYPE createSYMBOLWALUTYIDTYPE(pl.com.simple.simpleerp.dokzak.ObjectFactory factory, String pln) {
        SYMBOLWALUTYIDTYPE symbolWaluty = factory.createSYMBOLWALUTYIDTYPE();
        symbolWaluty.setSymbolwaluty("PLN");
        symbolWaluty.setType(IDENTITYNATURETYPE.T);
//        symbolWaluty.setId(new BigDecimal(enumV.getSelectedOptions().get(0)));
        return symbolWaluty;
    }
}
