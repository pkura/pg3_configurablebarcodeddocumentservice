//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.09.18 at 03:35:49 PM CEST 
//


package pl.com.simple.simpleerp.zapdost;

import pl.com.simple.simpleerp.*;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pl.com.simple.simpleerp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Zapdost_QNAME = new QName("http://www.simple.com.pl/SimpleErp", "zapdost");
    private final static QName _IDNIDCTYPEId_QNAME = new QName("", "id");
    private final static QName _IDNIDCTYPEIdn_QNAME = new QName("", "idn");
    private final static QName _SYMBOLWALUTYIDCTYPESymbolwaluty_QNAME = new QName("", "symbolwaluty");
    private final static QName _IDMIDCTYPEIdm_QNAME = new QName("", "idm");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pl.com.simple.simpleerp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZapdostpozycjeTYPE }
     * 
     */
    public ZapdostpozycjeTYPE createZapdostpozycjeTYPE() {
        return new ZapdostpozycjeTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.IDSIDTYPE }
     * 
     */
    public IDSIDTYPE createIDSIDTYPE() {
        return new IDSIDTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.SysDokZalacznikTYPE }
     * 
     */
    public SysDokZalacznikTYPE createSysDokZalacznikTYPE() {
        return new SysDokZalacznikTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.IDNIDTYPE }
     * 
     */
    public IDNIDTYPE createIDNIDTYPE() {
        return new IDNIDTYPE();
    }

    /**
     * Create an instance of {@link ParsegrZapdostTYPE }
     * 
     */
    public ParsegrZapdostTYPE createParsegrZapdostTYPE() {
        return new ParsegrZapdostTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.SYMKODIDTYPE }
     * 
     */
    public SYMKODIDTYPE createSYMKODIDTYPE() {
        return new SYMKODIDTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.SYMBOLWALUTYIDCTYPE }
     * 
     */
    public SYMBOLWALUTYIDCTYPE createSYMBOLWALUTYIDCTYPE() {
        return new SYMBOLWALUTYIDCTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.IDMIDCTYPE }
     * 
     */
    public IDMIDCTYPE createIDMIDCTYPE() {
        return new IDMIDCTYPE();
    }

    /**
     * Create an instance of {@link ZapdostTYPE }
     * 
     */
    public ZapdostTYPE createZapdostTYPE() {
        return new ZapdostTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.RepWartoscTYPE }
     * 
     */
    public RepWartoscTYPE createRepWartoscTYPE() {
        return new RepWartoscTYPE();
    }

    /**
     * Create an instance of {@link ParsegrZapdostiTYPE }
     * 
     */
    public ParsegrZapdostiTYPE createParsegrZapdostiTYPE() {
        return new ParsegrZapdostiTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.IDNIDCTYPE }
     * 
     */
    public IDNIDCTYPE createIDNIDCTYPE() {
        return new IDNIDCTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.IDMIDTYPE }
     * 
     */
    public IDMIDTYPE createIDMIDTYPE() {
        return new IDMIDTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.EANIDTYPE }
     * 
     */
    public EANIDTYPE createEANIDTYPE() {
        return new EANIDTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.RepWartosciTYPE }
     * 
     */
    public RepWartosciTYPE createRepWartosciTYPE() {
        return new RepWartosciTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.SYMBOLWALUTYIDTYPE }
     * 
     */
    public SYMBOLWALUTYIDTYPE createSYMBOLWALUTYIDTYPE() {
        return new SYMBOLWALUTYIDTYPE();
    }

    /**
     * Create an instance of {@link pl.com.simple.simpleerp.SysDokZalacznikiTYPE }
     * 
     */
    public SysDokZalacznikiTYPE createSysDokZalacznikiTYPE() {
        return new SysDokZalacznikiTYPE();
    }

    /**
     * Create an instance of {@link ZapdostpozTYPE }
     * 
     */
    public ZapdostpozTYPE createZapdostpozTYPE() {
        return new ZapdostpozTYPE();
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link ZapdostTYPE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.simple.com.pl/SimpleErp", name = "zapdost")
    public JAXBElement<ZapdostTYPE> createZapdost(ZapdostTYPE value) {
        return new JAXBElement<ZapdostTYPE>(_Zapdost_QNAME, ZapdostTYPE.class, null, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link java.math.BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "id", scope = IDNIDCTYPE.class)
    public JAXBElement<BigDecimal> createIDNIDCTYPEId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IDNIDCTYPEId_QNAME, BigDecimal.class, IDNIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idn", scope = IDNIDCTYPE.class)
    public JAXBElement<String> createIDNIDCTYPEIdn(String value) {
        return new JAXBElement<String>(_IDNIDCTYPEIdn_QNAME, String.class, IDNIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link java.math.BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "id", scope = SYMBOLWALUTYIDCTYPE.class)
    public JAXBElement<BigDecimal> createSYMBOLWALUTYIDCTYPEId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IDNIDCTYPEId_QNAME, BigDecimal.class, SYMBOLWALUTYIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "symbolwaluty", scope = SYMBOLWALUTYIDCTYPE.class)
    public JAXBElement<String> createSYMBOLWALUTYIDCTYPESymbolwaluty(String value) {
        return new JAXBElement<String>(_SYMBOLWALUTYIDCTYPESymbolwaluty_QNAME, String.class, SYMBOLWALUTYIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link java.math.BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "id", scope = IDMIDCTYPE.class)
    public JAXBElement<BigDecimal> createIDMIDCTYPEId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IDNIDCTYPEId_QNAME, BigDecimal.class, IDMIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link javax.xml.bind.JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idm", scope = IDMIDCTYPE.class)
    public JAXBElement<String> createIDMIDCTYPEIdm(String value) {
        return new JAXBElement<String>(_IDMIDCTYPEIdm_QNAME, String.class, IDMIDCTYPE.class, value);
    }

}
