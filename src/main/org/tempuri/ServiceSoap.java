/**
 * ServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package org.tempuri;

public interface ServiceSoap extends java.rmi.Remote {
    public int reportDocStatus(long IM_DocID, javax.xml.rpc.holders.LongHolder DS_DocID, int errorCode, java.lang.String errorDescripton, java.lang.String authCode) throws java.rmi.RemoteException;
}
