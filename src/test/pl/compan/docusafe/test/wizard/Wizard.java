package pl.compan.docusafe.test.wizard;

import java.util.*;
import java.io.*;
/* User: Administrator, Date: 2005-09-02 13:13:46 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Wizard.java,v 1.2 2005/09/05 13:56:03 lk Exp $
 */
public class Wizard
{
    private List pages;
    private int currentPage;
    private int previousPage = -1;

    public Wizard(WizardDef wizardDef)
    {
        pages = new ArrayList(wizardDef.getPages().size());
        for (Iterator iter=wizardDef.getPages().iterator(); iter.hasNext(); )
        {
            PageDef pageDef = (PageDef) iter.next();
            pages.add(new WizardPage(pageDef.getId(), pageDef.getUrl(), pageDef.getAttributes()));
        }
    }

    public WizardPage getCurrentPage()
    {
        return (WizardPage) pages.get(currentPage);
    }

    /**
     * Ustawia kolejn� stron� wizarda jako bie��c�, o ile jest to
     * mo�liwe
     * @see #canAdvance()
     * @throws IllegalStateException Je�eli nie ma kolejnej strony.
     */
    public WizardPage advance()
    {
        if (currentPage >= pages.size())
            throw new IllegalStateException("Nie mo�na przej�� do kolejnej strony");

        for (int i=currentPage+1; i < pages.size(); i++)
        {
            WizardPage page = (WizardPage) pages.get(i);
            if (!page.isSkipped())
            {
                previousPage = currentPage;
                currentPage = i;
                return (WizardPage) pages.get(i);
            }
        }

        throw new IllegalStateException("Nie mo�na przej�� do kolejnej strony");
    }

    public WizardPage rewind()
    {
        if (currentPage == 0)
            throw new IllegalStateException("Nie mo�na cofn�� si� z pierwszej strony");

        for (int i=currentPage-1; i >= 0; i--)
        {
            WizardPage page = (WizardPage) pages.get(i);
            if (!page.isSkipped())
            {
                previousPage = currentPage;
                currentPage = i;
                return (WizardPage) pages.get(i);
            }
        }

        throw new IllegalStateException("Nie znaleziono aktywnej strony wcze�niejszej " +
            "ni� bie��ca");
    }

    public boolean canRewind()
    {
        if (currentPage == 0) return false;

        for (int i=currentPage-1; i >= 0; i--)
        {
            WizardPage page = (WizardPage) pages.get(i);
            if (!page.isSkipped())
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Zwraca poprzednio wy�wietlan� stron�.
     * @deprecated ?
     */
    public WizardPage getPreviousPage()
    {
        if (previousPage >= 0)
        {
            return (WizardPage) pages.get(previousPage);
        }

        return null;
    }

    /**
     * Oznacza wskazan� stron� jako nieaktywn�, co spowoduje jej pomini�cie
     * podczas przewijania stron funkcj� advance().  Nie mo�na wywo�a� tej
     * funkcji dla strony bie��cej lub takiej, kt�ra znajduje si� w kolejno�ci
     * przed stron� bie��c�.
     */
    public void skipPage(String id)
    {
        for (int i=0; i < pages.size(); i++)
        {
            WizardPage page = (WizardPage) pages.get(i);
            if (page.getId().equals(id))
            {
                if (i <= currentPage)
                    throw new IllegalArgumentException("Nie mo�na pomin�� strony bie��cej, " +
                        "lub takiej kt�ra wyst�puje wcze�niej ni� strona bie��ca");

                page.setSkipped(true);
                return;
            }
        }

        throw new IllegalArgumentException("Nieznane id strony: "+id);
    }

    /**
     * Zwraca true, je�eli wizard posiada przynajmniej jedn� aktywn�
     * stron�, do kt�rej mo�na przej�� metod� advance().
     */
    public boolean canAdvance()
    {
        if (currentPage >= pages.size())
            return false;

        for (int i=currentPage+1; i < pages.size(); i++)
        {
            WizardPage page = (WizardPage) pages.get(i);
            if (!page.isSkipped())
                return true;
        }

        return false;
    }

    public byte[] getState()
    {
        // zwraca bie��cy stan wizarda
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
            ObjectOutputStream os = new ObjectOutputStream(baos);
            os.writeInt(currentPage);
            os.writeInt(previousPage);

            // pomini�te strony
            List skippedPagePositions = new ArrayList(pages.size());
            for (int i=0; i < pages.size(); i++)
            {
                WizardPage page = (WizardPage) pages.get(i);
                if (page.isSkipped())
                    skippedPagePositions.add(new Integer(i));
            }

            os.writeInt(skippedPagePositions.size());
            for (Iterator iter=skippedPagePositions.iterator(); iter.hasNext(); )
            {
                os.writeInt(((Integer) iter.next()).intValue());
            }

            for (int i=0; i < pages.size(); i++)
            {
                os.writeObject(((WizardPage) pages.get(i)).getAttributes());
            }

            os.close();

            return baos.toByteArray();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void setState(byte[] state) // throws EdmException
    {
        try
        {
            ObjectInputStream is = new ObjectInputStream(new ByteArrayInputStream(state));
            currentPage = is.readInt();
            previousPage = is.readInt();
            int skippedCount = is.readInt();
            while (skippedCount-- > 0)
            {
                ((WizardPage) pages.get(is.readInt())).setSkipped(true);
            }
            for (int i=0; i < pages.size(); i++)
            {
                ((WizardPage) pages.get(i)).setAttributes((Map) is.readObject());
            }
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static void main(String[] args)
    {

        // doNext
        // wykonuj� walidacj�
        // if (!ldap) wizard.disablePage("ldap")
        // if (ok) wizard.nextPage() - zwraca false, je�eli nie ma kolejnej
        // result = wizard.getCurrentPage().getUrl()
    }
}
