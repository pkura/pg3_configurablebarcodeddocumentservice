package pl.compan.docusafe.test.wizard;

import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
/* User: Administrator, Date: 2005-09-02 13:13:12 */

/**
 * Odczytuje konfiguracj� wizard�w z pliku XML.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Wizards.java,v 1.3 2008/10/06 11:32:11 pecet4 Exp $
 */
public class Wizards
{
    private final Map wizardDefs = new HashMap();

    public static void main(String[] args) throws Exception
    {
        Wizards wizards = new Wizards();
        wizards.load(Wizard.class.getResourceAsStream("wizards.xml"));

        byte[] state = null;

        Wizard wizard = wizards.getWizard("setup");
        while (true)
        {
            if (wizard.getCurrentPage().getId().equals("ldap"))
            {
                wizard.getCurrentPage().addAttribute("atr1", "warto��1");
                wizard.skipPage("jdbc");
                state = wizard.getState();
            }
            if (wizard.canAdvance()) wizard.advance();
            else break;
        }

        Wizard w2 = wizards.getWizard("setup");
        w2.setState(state);
        while (true)
        {
            if (w2.canAdvance()) w2.advance();
            else break;
        }

        // fillform: wizard.setState()
        // doUpdate: if (ok) wizard.advance()
        // jsp -> hidden value=${wizard.stateAsBase64} - z now� bie��c� stron�
        // fillform: wizard.setState()
        // fillform: setResult(wizard.getCurrentPage().getId())
        //
    }

    /**
     * Tworzy now� instancj� wizarda na podstawie definicji.
     */
    public Wizard getWizard(String id)
    {
        WizardDef wizardDef = (WizardDef) wizardDefs.get(id);
        if (wizardDef == null)
            throw new IllegalArgumentException("Nie istnieje wizard "+id);

        return new Wizard(wizardDef);
    }

    public void load(InputStream is)
        throws ParserConfigurationException, SAXException, IOException
    {
        if (is == null)
            throw new NullPointerException("is");
        new Reader(new InputSource(is));
    }

    private class Reader extends DefaultHandler
    {
        private WizardDef wizardDef;
        private PageDef pageDef;

        public Reader(InputSource source)
            throws ParserConfigurationException, SAXException, IOException
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.newSAXParser().parse(source, this);
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
        {
            if (qName.equals("wizard"))
            {
                if (wizardDef != null)
                    throw new SAXException("Zagnie�d�one elementy wizard nie s� dozwolone");

                String id = attributes.getValue("id");
                if (id == null || id.length() == 0)
                    throw new SAXException("Identyfikator wizarda nie mo�e by� pusty");
                wizardDef = new WizardDef(id);
            }
            else if (qName.equals("page"))
            {
                if (wizardDef == null)
                    throw new SAXException("Element page mo�e by� u�yty wy��cznie wewn�trz " +
                        "elementu wizard");

                if (pageDef != null)
                    throw new SAXException("Zagnie�d�one elementy page nie s� dozwolone");

                String id = attributes.getValue("id");
                String url = attributes.getValue("url");

                if (id == null || id.length() == 0)
                    throw new SAXException("Identyfikator strony nie mo�e by� pusty");
                if (url == null || url.length() == 0)
                    throw new SAXException("Adres strony "+id+" nie mo�e by� pusty");

                pageDef = new PageDef(id, url);
            }
            else if (qName.equals("attribute"))
            {
                if (pageDef != null)
                {
                    String name = attributes.getValue("name");
                    String value = attributes.getValue("value");

                    if (name == null || name.length() == 0)
                        throw new SAXException("Atrybut strony musi mie� nazw�");
                    if (value == null)
                        throw new SAXException("Atrybut strony musi mie� warto��");

                    pageDef.addAttribute(name, value);
                }
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException
        {
            if (qName.equals("wizard"))
            {
                wizardDefs.put(wizardDef.getId(), wizardDef);
                wizardDef = null;
            }
            else if (qName.equals("page"))
            {
                wizardDef.addPage(pageDef);
                pageDef = null;
            }
        }
    }
}

class WizardDef
{
    private String id;
    private List pages = new LinkedList();

    public WizardDef(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void addPage(PageDef pageDef)
    {
        pages.add(pageDef);
    }

    public List getPages()
    {
        return pages;
    }
}

class PageDef
{
    private String id;
    private String url;
    private Map attributes;

    public PageDef(String id, String url)
    {
        this.id = id;
        this.url = url;
    }

    public String getId()
    {
        return id;
    }

    public String getUrl()
    {
        return url;
    }

    public void addAttribute(String name, String value)
    {
        attributes.put(name, value);
    }

    public Map getAttributes()
    {
        return attributes;
    }
}
