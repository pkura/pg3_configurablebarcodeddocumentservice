package pl.compan.docusafe.test.lucene;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import java.io.FileNotFoundException;
import java.io.File;
import java.io.IOException;
/* User: Administrator, Date: 2005-10-11 15:52:25 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Index1.java,v 1.2 2008/10/06 10:19:34 pecet4 Exp $
 */
public class Index1
{
    public static void main(String[] args) throws Exception
    {
        IndexWriter writer = new IndexWriter("index", new StandardAnalyzer(), true);

        indexDocs(writer, new File("c:/projects/4calc"));

        writer.optimize();
        writer.close();
    }

    public static void indexDocs(IndexWriter writer, File file)
        throws IOException
    {
        // do not try to index files that cannot be read
        if (file.canRead())
        {
            if (file.isDirectory())
            {
                String[] files = file.list();
                // an IO error could occur
                if (files != null)
                {
                    for (int i = 0; i < files.length; i++)
                    {
                        indexDocs(writer, new File(file, files[i]));
                    }
                }
            }
            else
            {
                try
                {
                    writer.addDocument(FileDocument.Document(file));
                }
                // at least on windows, some temporary files raise this exception with an "access denied" message
                // checking if the file can be read doesn't help
                catch (FileNotFoundException fnfe)
                {
                    ;
                }
            }
        }
    }
}