package pl.compan.docusafe.test.crc;

import java.util.zip.CRC32;
import java.util.Map;
import java.util.HashMap;
/* User: Administrator, Date: 2005-11-02 09:15:14 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Perf.java,v 1.2 2008/10/06 10:53:53 pecet4 Exp $
 */
public class Perf
{
    public static void main(String[] args)
    {
        String s1 = "~read~NW_OGOLNE~group~read~NW_SWIADCZENIOWE~group~read_att~NW_SWIADCZENIOWE~group";
        String s2 = "~read~NW_RAPORTY~group~read_att~NW_RAPORTY~group";
        String s3 = "~read~NW_OGOLNE~group~read_att~NW_OGOLNE~group";

        CRC32 crc = new CRC32();

        long time = System.currentTimeMillis();
        for (int i=0; i < 100000; i++)
        {
            crc.reset();
            crc.update(s1.getBytes());
            crc.reset();
            crc.update(s2.getBytes());
            crc.reset();
            crc.update(s3.getBytes());
        }

        Map cache = new HashMap();

        crc.reset();
        crc.update(s1.getBytes());
        cache.put(s1, new Long(crc.getValue()));

        crc.reset();
        crc.update(s2.getBytes());
        cache.put(s2, new Long(crc.getValue()));

        crc.reset();
        crc.update(s3.getBytes());
        cache.put(s3, new Long(crc.getValue()));

        time = System.currentTimeMillis();
        for (int i=0; i < 100000; i++)
        {
            cache.get(s1);
            cache.get(s2);
            cache.get(s3);
        }
    }
}
