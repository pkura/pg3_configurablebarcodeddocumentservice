package pl.compan.docusafe.test;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import java.util.Hashtable;

/* User: Administrator, Date: 2005-06-30 12:42:34 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: LdapSchema.java,v 1.2 2008/10/06 10:34:51 pecet4 Exp $
 */
public class LdapSchema
{
    public static void main(String[] args) throws Exception
    {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_PRINCIPAL, "cn=Directory Manager");
        env.put(Context.SECURITY_CREDENTIALS, "admin111");
        env.put(Context.PROVIDER_URL, "ldap://localhost");
        DirContext context = new InitialDirContext(env);

        DirContext schema = context.getSchema("");
        DirContext personSchema = (DirContext) schema.lookup("ClassDefinition/docusafePerson");

        Attributes attributes = personSchema.getAttributes("");
        Attribute may = attributes.get("MUST");
        NamingEnumeration e = may.getAll();
        while (e.hasMore())
        {
            String name = (String) e.next();
        }

/*
        Attributes attrs = new BasicAttributes(true);
        attrs.put("NUMERICOID", "fooClass-oid");
        attrs.put("NAME", "fooClass");
        attrs.put("DESC", "Klasa foo");
        attrs.put("SUP", "top");
        attrs.put("STRUCTURAL", "true");

        Attribute must = new BasicAttribute("MUST", "cn");
        must.add("objectclass");
        attrs.put(must);

        schema.createSubcontext("ClassDefinition/fooClass", attrs);
*/
    }
}
