package pl.compan.docusafe.test;

import java.sql.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: OraBlob.java,v 1.2 2008/10/06 10:51:36 pecet4 Exp $
 */
public class OraBlob
{
    public static void main(String[] args) throws Exception
    {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
            "jdbc:oracle:thin:@195.136.199.227:1521:TEST", "lk2", "lk2");
        conn.setAutoCommit(false);

        Statement st = conn.createStatement();
//        ResultSet rs = st.executeQuery("select contentdata from " +
//            "ds_attachment_revision where id = 1");

/*
        st.executeUpdate("insert into blobs (id, b) values (1, empty_blob())");

        ResultSet rs = st.executeQuery("select b from blobs where id=1 for update");
        if (!rs.next())
            throw new Exception("rs.next");

        oracle.sql.BLOB blob = (oracle.sql.BLOB) rs.getBlob(1);
        OutputStream out = blob.getBinaryOutputStream();

        byte[] buf = new byte[2048];
        int count;
        FileInputStream fis = new FileInputStream("d:\\ldap.zip");
        while ((count = fis.read(buf)) > 0)
        {
            out.write(buf, 0, count);
        }
        fis.close();
        out.close();
*/
        ResultSet rs = st.executeQuery("select b from blobs where id=1");
        if (!rs.next())
            throw new Exception("rs.next");

        Blob blob = rs.getBlob(1);

        byte[] buf = new byte[2048];
        int count;
        InputStream fis = blob.getBinaryStream();
        FileOutputStream out = new FileOutputStream("d:\\blob.zip");
        while ((count = fis.read(buf)) > 0)
        {
            out.write(buf, 0, count);
        }
        fis.close();
        out.close();

        conn.commit();
    }
}
