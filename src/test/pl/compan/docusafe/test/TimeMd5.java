package pl.compan.docusafe.test;

import java.security.MessageDigest;
/* User: Administrator, Date: 2005-09-23 14:31:00 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TimeMd5.java,v 1.2 2008/10/06 11:26:59 pecet4 Exp $
 */
public class TimeMd5
{
    public static final String s = "package pl.compan.docusafe.test;\n" +
        "\n" +
        "import java.security.MessageDigest;\n" +
        "/* User: Administrator, Date: 2005-09-23 14:31:00 */\n" +
        "\n" +
        "/**\n" +
        " * @author <a href=\"mailto:lukasz.kowalczyk@com-pan.pl\">Lukasz Kowalczyk</a>\n" +
        " * @version $Id: TimeMd5.java,v 1.2 2008/10/06 11:26:59 pecet4 Exp $\n" +
        " */\n" +
        "public class TimeMd5\n" +
        "{\n" +
        "    public static final String s = \"\"\n" +
        "    public static void main(String[] args)\n" +
        "    {\n" +
        "        MessageDigest md = MessageDigest.getInstance(\"md5\");\n" +
        "        md.update();\n" +
        "    }\n" +
        "}";

    public static void main(String[] args) throws Exception
    {
        for (int i=0; i < 10; i ++)
        {
            long time = System.currentTimeMillis();
            MessageDigest md = MessageDigest.getInstance("md5");
            md.update(s.getBytes());
            md.digest();
        }
    }
}
