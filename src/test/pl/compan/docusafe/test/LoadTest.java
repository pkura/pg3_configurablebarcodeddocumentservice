package pl.compan.docusafe.test;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.*;

/* User: Administrator, Date: 2005-04-20 16:10:31 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: LoadTest.java,v 1.3 2008/10/06 10:34:51 pecet4 Exp $
 */
public class LoadTest
{
    static long bm_per_document;
    static long bm_per_attachment;
    static long bm_per_revision;

    public static void main(String[] args) throws Exception
    {
        if (args.length == 0)
        {
            System.exit(1);
        }

        File f = new File(args[0]);
        if (!f.exists() || !f.isFile())
            throw new Exception(f+" nie istnieje lub nie jest plikiem");

        load(f);
        // timer();
    }

    public static void load(File f) throws Exception
    {
        Class.forName("org.firebirdsql.jdbc.FBDriver");
        Connection conn = DriverManager.getConnection("jdbc:firebirdsql:localhost:docusafe?lc_ctype=win1250", "sysdba", "masterkey");
        conn.setAutoCommit(false);

        Statement st = conn.createStatement();

        PreparedStatement ps = conn.prepareStatement("insert into ds_folder " +
            "(id, parent_id, title, hversion, ctime, author, attributes) values " +
            "(?, ?, ?, ?, ?, ?, 0)");

        List folderIds = new ArrayList(100);

        long parentId;

        StopTimer timeFolders = new StopTimer();
        timeFolders.start();

        for (int i=0; i < 10; i++)
        {
            parentId = 0;
            for (int j=0; j < 10; j++)
            {
                ResultSet rs = st.executeQuery("select gen_id(ds_folder_id, 1) from rdb$database");
                if (!rs.next())
                    throw new Exception("rs.next()");
                long id = rs.getLong(1);

                folderIds.add(new Long(id));

                ps.setLong(1, id);
                ps.setLong(2, parentId);
                ps.setString(3, "Folder "+(i+1 < 10 ? "0" : "") + (i+1)+"-"+(j+1));
                ps.setInt(4, 0);
                ps.setTimestamp(5, new java.sql.Timestamp(System.currentTimeMillis()));
                ps.setString(6, "admin");

                ps.executeUpdate();

                parentId = id;
            }
            timeFolders.mark();
        }

        timeFolders.stop(true);

        ps.close();
        ps = conn.prepareStatement("insert into ds_document (id, hversion, title, description," +
            "ctime, mtime, author, externalId, deleted, office, folder_id) "+
            "values (?, 0, ?, ?, current_timestamp, current_timestamp, 'admin', null, 0, 0, ?)");
        PreparedStatement psa = conn.prepareStatement("insert into ds_attachment "+
            "(id, title, description, ctime, author, deleted, document_id, posn) "+
            "values (?, ?, ?, current_timestamp, 'admin', 0, ?, ?)");
        PreparedStatement psr = conn.prepareStatement("insert into ds_attachment_revision " +
            "(id, revision, ctime, author, originalFilename, contentsize, mime, attachment_id, contentdata) " +
            "values (?, ?, current_timestamp, 'admin', ?, ?, ?, ?, ?)");

        final int documentPerFolder = 100;
        final int attachmentsPerDocument = 10;

        int countDocuments = 0;
        int countAttachments = 0;
        int countRevisions = 0;

        StopTimer timeAll = new StopTimer();
        timeAll.start();

        StopTimer timeDocument = new StopTimer(folderIds.size() * documentPerFolder);
        StopTimer timeAttachment = new StopTimer(folderIds.size() * documentPerFolder * attachmentsPerDocument);
        StopTimer timeRevision = new StopTimer(folderIds.size() * documentPerFolder * attachmentsPerDocument);

        //for (Iterator iter=folderIds.iterator(); iter.hasNext(); )
        for (int g=0; g < folderIds.size(); g++)
        {
            long folderId = ((Long) folderIds.get(g)).longValue();

            // 100 dokument�w w folderze
            for (int i=0; i < documentPerFolder; i++)
            {
                timeDocument.start();

                ResultSet rs = st.executeQuery("select gen_id(ds_document_id, 1) from rdb$database");
                if (!rs.next())
                    throw new Exception("rs.next()");
                long id = rs.getLong(1);

                ps.setLong(1, id);
                ps.setString(2, "Dokument "+id);
                ps.setString(3, "Dokument "+id);
                ps.setLong(4, folderId);

                ps.executeUpdate();

                countDocuments++;

                // 10 za��cznik�w
                for (int j=0; j < attachmentsPerDocument; j++)
                {
                    timeAttachment.start();

                    rs = st.executeQuery("select gen_id(ds_attachment_id, 1) from rdb$database");
                    if (!rs.next())
                        throw new Exception("rs.next()");
                    long aid = rs.getLong(1);

                    psa.setLong(1, aid);
                    psa.setString(2, "Za��cznik "+aid);
                    psa.setString(3, "Za��cznik "+aid);
                    psa.setLong(4, id);
                    psa.setInt(5, j);

                    psa.executeUpdate();

                    countAttachments++;

                    timeRevision.start();

                    rs = st.executeQuery("select gen_id(ds_attachment_revision_id, 1) from rdb$database");
                    if (!rs.next())
                        throw new Exception("rs.next()");
                    long rid = rs.getLong(1);

                    InputStream is = new BufferedInputStream(new FileInputStream(f));

                    psr.setLong(1, rid);
                    psr.setInt(2, 1);
                    psr.setString(3, f.getName());
                    psr.setInt(4, (int) f.length());
                    psr.setString(5, "application/"+(f.getName().substring(f.getName().indexOf('.')+1)));
                    psr.setLong(6, aid);
                    psr.setBinaryStream(7, is, (int) f.length());

                    psr.executeUpdate();

                    countRevisions++;

                    is.close();

                    timeRevision.mark();
                    timeAttachment.mark();
                }

                timeDocument.mark();
            }
        }

        timeDocument.stop(true);
        timeAttachment.stop(true);
        timeRevision.stop(true);
        timeAll.stop();

        File log = new File("benchmark.txt");

        PrintWriter out = new PrintWriter(new FileWriter(log));

        out.println("Czas �adowania folder�w: "+timeFolders.total());
        out.println("Czas �adowania dokument�w: "+timeAll.total()+" ("+countDocuments+")");
        out.println("Czas �adowania za��cznik�w: "+timeAttachment.total()+" ("+countAttachments+")");
        out.println("Czas �adowania wersji: "+timeRevision.total()+" ("+countRevisions+")");
        out.println("�redni czas �adowania folderu: "+timeFolders.mean());
        out.println("�redni czas �adowania dokumentu: "+timeDocument.mean());
        out.println("�redni czas �adowania za��cznika: "+timeAttachment.mean());
        out.println("�redni czas �adowania tre�ci: "+timeRevision.mean());

        out.print("Ca�kowity czas: "+(timeFolders.total() + timeAll.total()));

        out.close();

        conn.commit();
        conn.close();
    }

    /**
     * Stoper.  Umo�liwia pojedynczy pomiar czasu oraz pomiary kolejne.
     */
    static class StopTimer
    {
        private long[] times;
        private int timeIdx;

        private long total = -1;

        private int state;

        private static final int STARTED = 1;
        private static final int STOPPED = 2;

        public StopTimer()
        {
            times = new long[16 + 1];
        }

        public StopTimer(int size)
        {
            if (size <= 0)
                throw new IllegalArgumentException("size="+size);
            times = new long[size + 1];
        }

        public void reset()
        {
            if (state == STARTED)
                throw new IllegalStateException("Przed wywo�aniem reset() nale�y " +
                    "zatrzyma� stoper");
            total = -1;
            state = 0;
            timeIdx = 0;
        }

        public void start()
        {
            if (state == STARTED) return;

            markTime();
            state = STARTED;
        }

        /**
         * Zatrzymuje stoper.
         * @param discard Je�eli true, wywo�anie stop() nie jest liczone
         * jako koniec kolejnego pomiaru, chyba, �e po wywo�aniu start()
         * ani razu nie wywo�ano mark().
         */
        public void stop(boolean discard)
        {
            if (!discard || timeIdx < 2) markTime();
            state = STOPPED;
        }

        public void stop()
        {
            stop(false);
        }

        /**
         * Zapami�tuje czas, jaki up�yn�� pomi�dzy bie��cym, a poprzednim
         * wywo�aniem mark() (lub stop()), stoper nie jest zatrzymywany.
         */
        public void mark()
        {
            markTime();
        }

        private void markTime()
        {
            times[timeIdx++] = System.currentTimeMillis();
        }

        public long total()
        {
            if (state != STOPPED)
                throw new IllegalStateException("Przed wywo�aniem tej metody nale�y " +
                    "wywo�a� stop()");

            if (total == -1)
            {
                total = 0;
                for (int i=1; i < timeIdx; i++)
                {
                    total += (times[i] - times[i-1]);
                }
            }

            return total;
        }

        private double mean = -1;

        public double mean()
        {
            if (mean < 0)
            {
                mean = (double) total() / (timeIdx - 1);
            }
            return mean;
        }
    }

    public static void timer() throws Exception
    {
        StopTimer timer = new StopTimer();

        long[] sleeps = new long[] { 100, 100 };

        timer.start();
        for (int i=0; i < sleeps.length; i++)
        {
            Thread.sleep(sleeps[i]);
            timer.mark();
        }
        timer.stop(true);
    }
}
