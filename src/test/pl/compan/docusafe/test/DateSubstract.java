package pl.compan.docusafe.test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/* User: Administrator, Date: 2005-07-14 09:23:31 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DateSubstract.java,v 1.3 2008/10/06 09:44:10 pecet4 Exp $
 */
public class DateSubstract
{
    public static void main(String[] args)
    {
        Calendar a = Calendar.getInstance();
        Calendar b = Calendar.getInstance();

        b.add(Calendar.HOUR, 14);

        print(a, b);
        print(b, a);

        b.add(Calendar.DATE, 365-12);

        print(a, b);
        print(b, a);
    }

    private static void print(Calendar a, Calendar b)
    {
    }

    public static int substract(final Date a, final Date b)
    {
        if (a == null)
            throw new NullPointerException("a");
        if (b == null)
            throw new NullPointerException("b");

        if (a == b || a.getTime() == b.getTime())
            return 0;

        boolean negative = b.after(a);
        int delta;

        GregorianCalendar calA = new GregorianCalendar();
        calA.setTime(negative ? a : b);
        GregorianCalendar calB = new GregorianCalendar();
        calB.setTime(negative ? b : a);

        if (calA.get(Calendar.YEAR) == calB.get(Calendar.YEAR))
        {
            delta = calB.get(Calendar.DAY_OF_YEAR) - calA.get(Calendar.DAY_OF_YEAR);
        }
        else
        {
            delta = daysInYear(calA.get(Calendar.YEAR)) - calA.get(Calendar.DAY_OF_YEAR);
            delta += calB.get(Calendar.DAY_OF_YEAR);
            for (int y=calA.get(Calendar.YEAR)+1; y < calB.get(Calendar.YEAR); y++)
            {
                delta += daysInYear(y);
            }
        }

        //return negative ? -delta : delta;
        return delta;
    }

    private static int daysInYear(int year)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR);
    }
}
