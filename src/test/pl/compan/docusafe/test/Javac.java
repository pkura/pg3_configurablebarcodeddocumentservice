package pl.compan.docusafe.test;
/* User: Administrator, Date: 2005-10-24 11:01:53 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Javac.java,v 1.1 2005/10/24 14:57:10 lk Exp $
 */
public class Javac
{
    public static void main(String[] args)
    {
        com.sun.tools.javac.Main.compile(new String[] { "-source 1.6" });
    }
}
