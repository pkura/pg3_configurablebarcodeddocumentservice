package pl.compan.docusafe.test;

import com.asprise.util.jtwain.SourceManager;
import com.asprise.util.jtwain.Source;
import com.asprise.util.jtwain.JTwainException;
/* User: Administrator, Date: 2005-08-23 11:04:04 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: JTwain.java,v 1.2 2008/10/06 10:32:00 pecet4 Exp $
 */
public class JTwain
{
    public static void main(String[] args) throws Exception
    {
        SourceManager sm = SourceManager.instance();

        Source[] sources = sm.getAllSources();

        for (int i=0; i < sources.length; i++)
        {
        }

        SourceManager.closeSourceManager();

        sm = SourceManager.instance();
    }
}
