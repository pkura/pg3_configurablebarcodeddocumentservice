package pl.compan.docusafe.test.tiff;

import com.asprise.util.tiff.TIFFReader;
import com.asprise.util.tiff.TIFFWriter;

import java.util.List;
import java.util.LinkedList;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
/* User: Administrator, Date: 2005-11-08 12:06:30 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Asprise.java,v 1.3 2008/10/06 09:28:52 pecet4 Exp $
 */
public class Asprise
{
    public static void _main(String[] args) throws IOException
    {
        DisposableBufferedImageList list = new DisposableBufferedImageList();
        for (int i=0; i < args.length; i++)
        {
            list.add(new File(args[i]));
        }

        TIFFWriter.createTIFFFromImages(
            list.toArray(),
            TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE,
            TIFFWriter.TIFF_COMPRESSION_GROUP4,
            new File("output.tif"));
    }

    public static void main(String[] args) throws IOException
    {
        List list = new LinkedList();
        for (int i=0; i < args.length; i++)
        {
            list.add(TIFFWriter.getBufferedImageFromImage(Toolkit.getDefaultToolkit().createImage(args[i])));
        }

        TIFFWriter.createTIFFFromImages(
            (BufferedImage[]) list.toArray(new BufferedImage[list.size()]),
            TIFFWriter.TIFF_CONVERSION_TO_BLACK_WHITE,
            TIFFWriter.TIFF_COMPRESSION_GROUP4,
            new File("output.tif"));
    }
}
