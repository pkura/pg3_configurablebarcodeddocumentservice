package pl.compan.docusafe.test.tiff;

import java.util.List;
import java.util.LinkedList;
import java.awt.image.BufferedImage;
import java.io.File;
/* User: Administrator, Date: 2005-11-08 13:15:59 */

/**
 * Dodawane s� pliki graficzne, a na ich podstawie tworzone
 * obiekty BufferedImageProxy.
 * Proxy pomija wywo�anie getGraphics2D (zwraca mock-object).
 * Ka�de inne wywo�anie powoduje inicjalizacj� obiektu
 * image i delegowanie do niego metod.
 *
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: DisposableBufferedImageList.java,v 1.1 2005/11/09 16:10:59 lk Exp $
 */
public class DisposableBufferedImageList
{
    private List proxies = new LinkedList();

    private BufferedImageProxy currentProxy;

    public void add(File file)
    {
        proxies.add(new BufferedImageProxy(this, file));
    }

    /**
     * Ka�dy obiekt BufferedImageProxy wywo�uje t� metod�
     * w metodzie initProxy() - w tym momencie poprzedni
     * obiekt BufferedImageProxy mo�e zosta� zwolniony.
     */
    void notifyProxyInitialized(BufferedImageProxy proxy)
    {
        if (currentProxy != null)
        {
            currentProxy.dispose();
            currentProxy = null;
        }
        currentProxy = proxy;
    }

    public BufferedImage[] toArray()
    {
        return (BufferedImage[]) proxies.toArray(new BufferedImage[proxies.size()]);
    }
}
