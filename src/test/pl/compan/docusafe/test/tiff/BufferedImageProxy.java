package pl.compan.docusafe.test.tiff;

import java.awt.image.*;
import java.awt.*;
import java.util.Vector;
import java.io.File;

import com.asprise.util.tiff.TIFFWriter;
/* User: Administrator, Date: 2005-11-08 12:27:40 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: BufferedImageProxy.java,v 1.2 2005/11/10 15:50:08 lk Exp $
 */
class BufferedImageProxy extends BufferedImage
{
    private DisposableBufferedImageList list;
    private File file;

    private BufferedImage image;

    BufferedImageProxy(DisposableBufferedImageList list, File file)
    {
        // warto�ci przekazywane do klasy nadrz�dnej nie maj�
        // znaczenia, w�a�ciwe parametry b�dzie mia� obraz zawarty
        // w polu image
        super(1, 1, BufferedImage.TYPE_INT_RGB);
        this.list = list;
        this.file = file;
    }

    void dispose()
    {
        this.image = null;
    }

    private void initProxy()
    {
        if (image == null)
        {
            list.notifyProxyInitialized(this);

            Image im = Toolkit.getDefaultToolkit().createImage(file.getAbsolutePath());
            if (im instanceof BufferedImage)
            {
                this.image = (BufferedImage) im;
            }
            else
            {
                this.image = TIFFWriter.getBufferedImageFromImage(im);
            }
        }
    }

    public int getType()
    {
        initProxy();
        return image.getType();
    }

    public ColorModel getColorModel()
    {
        initProxy();
        return image.getColorModel();
    }

    public WritableRaster getRaster()
    {
        initProxy();
        return image.getRaster();
    }

    public WritableRaster getAlphaRaster()
    {
        initProxy();
        return image.getAlphaRaster();
    }

    public int getRGB(int x, int y)
    {
        initProxy();
        return image.getRGB(x, y);
    }

    public int[] getRGB(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize)
    {
        initProxy();
        return image.getRGB(startX, startY, w, h, rgbArray, offset, scansize);
    }

    public void setRGB(int x, int y, int rgb)
    {
        initProxy();
        image.setRGB(x, y, rgb);
    }

    public void setRGB(int startX, int startY, int w, int h, int[] rgbArray, int offset, int scansize)
    {
        initProxy();
        image.setRGB(startX, startY, w, h, rgbArray, offset, scansize);
    }

    public int getWidth()
    {
        initProxy();
        return image.getWidth();
    }

    public int getHeight()
    {
        initProxy();
        return image.getHeight();
    }

    public int getWidth(ImageObserver observer)
    {
        initProxy();
        return image.getWidth(observer);
    }

    public int getHeight(ImageObserver observer)
    {
        initProxy();
        return image.getHeight(observer);
    }

    public ImageProducer getSource()
    {
        initProxy();
        return image.getSource();
    }

    public Object getProperty(String name, ImageObserver observer)
    {
        initProxy();
        return image.getProperty(name, observer);
    }

    public Object getProperty(String name)
    {
        initProxy();
        return image.getProperty(name);
    }

    public void flush()
    {
        initProxy();
        image.flush();
    }

    public Graphics getGraphics()
    {
        initProxy();
        return image.getGraphics();
    }

    public Graphics2D createGraphics()
    {
/*
        initProxy();
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Graphics2DProxy.class);
        enhancer.setCallback(new Graphics2DProxyCallback());
        return (Graphics2D) enhancer.create(new Class[] { Graphics2D.class },
            new Object[] { image.createGraphics() });
*/
        return new NullGraphics2D();
    }

    public BufferedImage getSubimage(int x, int y, int w, int h)
    {
        initProxy();
        return image.getSubimage(x, y, w, h);
    }

    public boolean isAlphaPremultiplied()
    {
        initProxy();
        return image.isAlphaPremultiplied();
    }

    public void coerceData(boolean isAlphaPremultiplied)
    {
        initProxy();
        image.coerceData(isAlphaPremultiplied);
    }

    public String toString()
    {
        initProxy();
        return image.toString();
    }

    public Vector<RenderedImage> getSources()
    {
        initProxy();
        return image.getSources();
    }

    public String[] getPropertyNames()
    {
        initProxy();
        return image.getPropertyNames();
    }

    public int getMinX()
    {
        initProxy();
        return image.getMinX();
    }

    public int getMinY()
    {
        initProxy();
        return image.getMinY();
    }

    public SampleModel getSampleModel()
    {
        initProxy();
        return image.getSampleModel();
    }

    public int getNumXTiles()
    {
        initProxy();
        return image.getNumXTiles();
    }

    public int getNumYTiles()
    {
        initProxy();
        return image.getNumYTiles();
    }

    public int getMinTileX()
    {
        initProxy();
        return image.getMinTileX();
    }

    public int getMinTileY()
    {
        initProxy();
        return image.getMinTileY();
    }

    public int getTileWidth()
    {
        initProxy();
        return image.getTileWidth();
    }

    public int getTileHeight()
    {
        initProxy();
        return image.getTileHeight();
    }

    public int getTileGridXOffset()
    {
        initProxy();
        return image.getTileGridXOffset();
    }

    public int getTileGridYOffset()
    {
        initProxy();
        return image.getTileGridYOffset();
    }

    public Raster getTile(int tileX, int tileY)
    {
        initProxy();
        return image.getTile(tileX, tileY);
    }

    public Raster getData()
    {
        initProxy();
        return image.getData();
    }

    public Raster getData(Rectangle rect)
    {
        initProxy();
        return image.getData(rect);
    }

    public WritableRaster copyData(WritableRaster outRaster)
    {
        initProxy();
        return image.copyData(outRaster);
    }

    public void setData(Raster r)
    {
        initProxy();
        image.setData(r);
    }

    public void addTileObserver(TileObserver to)
    {
        initProxy();
        image.addTileObserver(to);
    }

    public void removeTileObserver(TileObserver to)
    {
        initProxy();
        image.removeTileObserver(to);
    }

    public boolean isTileWritable(int tileX, int tileY)
    {
        initProxy();
        return image.isTileWritable(tileX, tileY);
    }

    public Point[] getWritableTileIndices()
    {
        initProxy();
        return image.getWritableTileIndices();
    }

    public boolean hasTileWriters()
    {
        initProxy();
        return image.hasTileWriters();
    }

    public WritableRaster getWritableTile(int tileX, int tileY)
    {
        initProxy();
        return image.getWritableTile(tileX, tileY);
    }

    public void releaseWritableTile(int tileX, int tileY)
    {
        initProxy();
        image.releaseWritableTile(tileX, tileY);
    }

    public int getTransparency()
    {
        initProxy();
        return image.getTransparency();
    }

    public ImageCapabilities getCapabilities(GraphicsConfiguration gc)
    {
        initProxy();
        return image.getCapabilities(gc);
    }

    public Image getScaledInstance(int width, int height, int hints)
    {
        initProxy();
        return image.getScaledInstance(width, height, hints);
    }

    public void setAccelerationPriority(float priority)
    {
        initProxy();
        image.setAccelerationPriority(priority);
    }

    public float getAccelerationPriority()
    {
        initProxy();
        return image.getAccelerationPriority();
    }
}
