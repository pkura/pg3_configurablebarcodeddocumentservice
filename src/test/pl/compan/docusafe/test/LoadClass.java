package pl.compan.docusafe.test;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: LoadClass.java,v 1.2 2008/10/06 10:34:51 pecet4 Exp $
 */
public class LoadClass extends ClassLoader
{
    private InputStream is;

    public LoadClass(InputStream is)
    {
        this.is = is;
    }

    protected Class findClass(String name) throws ClassNotFoundException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream(2000);
        byte[] buffer = new byte[512];
        int count;
        try
        {
            while ((count = is.read(buffer)) > 0)
            {
                os.write(buffer, 0, count);
            }
        }
        catch (IOException e)
        {
            throw new ClassNotFoundException(e.getMessage(), e);
        }

        return defineClass("license", os.toByteArray(), 0, os.size());
    }

    public Class loadLicenseClass() throws ClassNotFoundException
    {
        return loadClass("$$$ $$$");
    }

    public static void main(String[] args) throws Exception
    {
        LoadClass cl = new LoadClass(new FileInputStream("c:/share/licencje/umz/license.class"));
        Class clazz = cl.loadLicenseClass();
        Object o = clazz.newInstance();
    }
}
