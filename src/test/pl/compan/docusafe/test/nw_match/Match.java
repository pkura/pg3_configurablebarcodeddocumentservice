package pl.compan.docusafe.test.nw_match;

import ognl.OgnlException;
import ognl.Ognl;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/* User: Administrator, Date: 2005-12-27 10:46:58 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Match.java,v 1.3 2008/10/06 10:40:31 pecet4 Exp $
 */
public class Match
{
    public static void main(String[] args) throws Exception
    {
        SignatureCoords coords = getSignatureCoords(args[0], "A4");
    }

    public static SignatureCoords getSignatureCoords(String NR_POLISY, String format)
        throws IOException
    {
        File coordsFile = new File("c:/docusafe/HOME", "nw-signature.config");
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(coordsFile)));
        List patterns = new ArrayList(32);
        String line;

        // format linii wzorca: WZORZEC,format,strona,lx,ly,rx,ry
        while ((line=reader.readLine()) != null)
        {
            if (line.trim().startsWith("#"))
                continue;
            String[] fields = line.split("\\s*,\\s*");
            if (fields.length >= 7 && fields[1].equalsIgnoreCase(format))
                patterns.add(line);
        }

        Collections.sort(patterns, new Comparator<String>()
        {
            public int compare(String pat1, String pat2)
            {
                int tmp;
                int len1 = (tmp=pat1.indexOf('*')) > 0 ? tmp : pat1.indexOf(',');
                int len2 = (tmp=pat2.indexOf('*')) > 0 ? tmp : pat2.indexOf(',');
                return len2 - len1;
            }
        });

        for (Iterator iter=patterns.iterator(); iter.hasNext(); )
        {
            String pattern = (String) iter.next();
            String[] fields = pattern.split("\\s*,\\s*");
            if (match(fields[0], NR_POLISY, fields.length > 7 ? fields[7] : null))
            {
                return new SignatureCoords(
                    Integer.parseInt(fields[2])-1,
                    parseUnitToPercent(fields[3], false),
                    parseUnitToPercent(fields[4], true),
                    parseUnitToPercent(fields[5], false),
                    parseUnitToPercent(fields[6], true),
                    0);
            }
        }

        return null;
    }

    private static int parseUnitToPercent(String value, boolean vertical)
    {
        if (value.endsWith("cm"))
        {
            String v = value.substring(0, value.length()-"cm".length()).trim();
            return (int) (Double.parseDouble(v) / (vertical ? 29.7 : 21.0) * 100.0);
        }
        else if (value.endsWith("%"))
        {
            String v = value.substring(0, value.length()-"%".length()).trim();
            return (int) Double.parseDouble(v);
        }
        else
        {
            return (int) Double.parseDouble(value);
        }
    }

    private static boolean match(String pattern, String NR_POLISY, String expr)
    {
        if (expr != null)
        {
            Matcher matcher = Pattern.compile("[A-Za-z]*([0-9]+).*").matcher(NR_POLISY);
            if (matcher.matches())
            {
                Map<String, Object> context = new HashMap<String, Object>();
                context.put("num", Long.parseLong(matcher.group(1)));
                try
                {
                    Object result = Ognl.getValue(expr, context, new Object());
                    if (result == null || !(result instanceof Boolean))
                        throw new IllegalArgumentException("Wyra�enie "+expr+" nie zwr�ci�o warto�ci " +
                            "lub zwr�cona warto�� nie by�a typu logicznego ("+result+")");
                    if (!(Boolean) result)
                        return false;
                }
                catch (OgnlException e)
                {
                    throw new IllegalArgumentException("B��d analizy wyra�enia: "+expr);
                }
            }
        }

        if (pattern.length() == 0)
            return false;
        int star;
        if ((star=pattern.indexOf('*')) < 0)
        {
            return NR_POLISY.startsWith(pattern);
        }
        else if (star > 0)
        {
            String prefix = pattern.substring(0, star);
            if (!NR_POLISY.startsWith(prefix))
                return false;
            String wildcard = pattern.substring(star);
            String cdr = NR_POLISY.substring(prefix.length());
            if (cdr.length() > 0 && wildcard.startsWith("*A") &&
                Character.isLetter(cdr.charAt(0)))
                return true;
            else if (cdr.length() > 0 && wildcard.startsWith("*0") &&
                Character.isDigit(cdr.charAt(0)))
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
    }

    private static class SignatureCoords
    {
        // numer strony (dla multitiffa) - od zera
        private int page;
        // pozioma wsp�rz�dna lewego g�rnego rogu, w procentach
        private int ulPercent;
        // pionowa wsp�rz�dna lewego g�rnego rogu, w procentach
        private int utPercent;
        private int blPercent;
        private int btPercent;
        // ile obrot�w o pi/2 wykona� (liczba ujemna lub dodatnia)
        private int pi2;

        public SignatureCoords(int page, int ulPercent, int utPercent, int blPercent, int btPercent, int pi2)
        {
            this.page = page;
            this.ulPercent = ulPercent;
            this.utPercent = utPercent;
            this.blPercent = blPercent;
            this.btPercent = btPercent;
            this.pi2 = pi2;
        }

        public int getWidthPercent()
        {
            return blPercent - ulPercent;
        }

        public int getHeightPercent()
        {
            return btPercent - utPercent;
        }

        public int getPage()
        {
            return page;
        }

        public int getUlPercent()
        {
            return ulPercent;
        }

        public int getUtPercent()
        {
            return utPercent;
        }

        public int getBlPercent()
        {
            return blPercent;
        }

        public int getBtPercent()
        {
            return btPercent;
        }

        public int getPi2()
        {
            return pi2;
        }
    }
}
