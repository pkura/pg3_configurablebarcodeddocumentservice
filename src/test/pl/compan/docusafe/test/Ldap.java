package pl.compan.docusafe.test;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Ldap.java,v 1.1 2005/04/07 14:51:09 lk Exp $
 */
public class Ldap
{
    public static void main(String[] args) throws Exception
    {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_PRINCIPAL, "cn=Directory Manager");
        env.put(Context.SECURITY_CREDENTIALS, "admin111");
        env.put(Context.PROVIDER_URL, "ldap://195.136.199.227:2000");

        DirContext context = new InitialDirContext(env);
        context.getAttributes("cn=admin, ou=People,dc=zbp.pl");
    }
}
