package pl.compan.docusafe.test;

import com.sun.media.jai.codec.*;
import com.sun.media.jai.codecimpl.TIFFImageDecoder;

import javax.media.jai.operator.AWTImageDescriptor;
import javax.media.jai.operator.FileStoreDescriptor;
import javax.media.jai.JAI;
import java.io.File;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.ParameterBlock;
import java.awt.*;

/* User: Administrator, Date: 2005-08-05 16:54:25 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MinimalJai.java,v 1.1 2005/08/05 15:24:59 lk Exp $
 */
public class MinimalJai
{
    public static void main(String[] args) throws Exception
    {
        SeekableStream s = new FileSeekableStream(new File(args[0]));
        ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, null);

        //TIFFImageDecoder dec = new TIFFImageDecoder(s, null);

        // AWTImageDescriptor.create(dec.decodeAsRenderedImage(0), null);

        RenderedImage rendered = dec.decodeAsRenderedImage(0);
        TIFFEncodeParam param = new TIFFEncodeParam();
        param.setCompression(TIFFEncodeParam.COMPRESSION_DEFLATE);
        ParameterBlock pb = new ParameterBlock();
        //pb.addSource(dec.decodeAsRenderedImage(i));

        File ifile = new File("ifile.tif");
        pb.addSource(rendered);
        pb.add(ifile.getAbsolutePath());
        pb.add("tiff");
        pb.add(param);
        //JAI.create("filestore", pb);
        FileStoreDescriptor.create(rendered, ifile.getAbsolutePath(), "tiff", param, null, null);

    }
}
