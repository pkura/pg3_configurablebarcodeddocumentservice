package pl.compan.docusafe.test;

import com.sun.jdi.Bootstrap;
import com.sun.jdi.VirtualMachineManager;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.event.*;
import com.sun.jdi.request.MethodEntryRequest;
import com.sun.jdi.request.MethodExitRequest;
import com.sun.jdi.connect.AttachingConnector;
import com.sun.jdi.connect.Connector;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
/* User: Administrator, Date: 2005-08-31 11:45:43 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Timing.java,v 1.1 2005/08/31 15:18:11 lk Exp $
 */
public class Timing
{
    public static void main(String[] args) throws Exception
    {
        final int port = Integer.parseInt(args[0]);

        VirtualMachineManager vmm = Bootstrap.virtualMachineManager();
        List lstConnectors = vmm.attachingConnectors();
        AttachingConnector ac = null;
        for (Iterator iter=lstConnectors.iterator(); iter.hasNext(); )
        {
            AttachingConnector acT = (AttachingConnector) iter.next();
            if ("com.sun.jdi.SocketAttach".equals(acT.name()))
            {
                ac = acT;
                break;
            }
        }

        if (ac == null)
            throw new RuntimeException("Nie znaleziono konektora dt_socket");

        Map mapArgs = ac.defaultArguments();
        for (Iterator iter=mapArgs.values().iterator(); iter.hasNext(); )
        {
            Connector.Argument arg = (Connector.Argument) iter.next();
            if (arg.name().equals("hostname"))
                arg.setValue("localhost");
            else if (arg.name().equals("port"))
                arg.setValue(""+port);
        }

        VirtualMachine vm = ac.attach(mapArgs);

        System.out.println("attached");


        System.out.println("events enabled");

        new EventEater(vm).start();

/*
        long tmStart = System.currentTimeMillis();
        while ((System.currentTimeMillis() - tmStart) < 60 * 1000L)
        {
            Thread.sleep(1000);
        }
*/

        System.out.println("entry enable");

        MethodEntryRequest reqMethodEntry = vm.eventRequestManager().createMethodEntryRequest();
        reqMethodEntry.addClassFilter("pl.compan.docusafe.*");
        reqMethodEntry.enable();

        MethodExitRequest reqMethodExit = vm.eventRequestManager().createMethodExitRequest();
        reqMethodExit.addClassFilter("pl.compan.docusafe.*");
        reqMethodExit.enable();
    }

    private static Stack methodStack = new Stack();

    private static void processMethodEntry(MethodEntryEvent event)
    {
//        String clazz = event.method().declaringType().name();
//        if (!clazz.startsWith("java") && !clazz.startsWith("sun.") &&
//            !clazz.startsWith("org.apache"))
//        System.out.println(clazz+"."+event.method().name());
        //event.method().declaringType().classObject().owningThread().
        methodStack.push(new Long(System.currentTimeMillis()));
    }

    private static void processMethodExit(MethodExitEvent event)
    {
        Long time = (Long) methodStack.pop();
        System.out.println((System.currentTimeMillis() - time.longValue()) +
            ": "+
            event.method().declaringType().name()+"."+
            event.method().name());
    }

    private static class EventEater extends Thread
    {
        private VirtualMachine vm;

        public EventEater(VirtualMachine vm)
        {
            this.vm = vm;
        }

        public void run()
        {
            EventSet se;

            try
            {
                while ((se = vm.eventQueue().remove()) != null)
                {
                    for (EventIterator ei=se.eventIterator(); ei.hasNext(); )
                    {
                        Event ev = ei.nextEvent();

                        if (ev instanceof MethodEntryEvent)
                        {
                            processMethodEntry((MethodEntryEvent) ev);
                        }
                        else if (ev instanceof MethodExitEvent)
                        {
                            processMethodExit((MethodExitEvent) ev);
                        }
                        vm.resume();
                    }
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
