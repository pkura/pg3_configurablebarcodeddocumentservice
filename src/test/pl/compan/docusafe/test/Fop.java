package pl.compan.docusafe.test;

import org.apache.fop.apps.Driver;
import org.apache.fop.apps.Options;
import org.apache.avalon.framework.logger.Log4JLogger;
import org.apache.log4j.Category;
import org.apache.log4j.BasicConfigurator;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Result;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileOutputStream;
import java.io.File;

/**
 * @author <a href="mailto:lukasz@wa.home.pl">Lukasz Kowalczyk</a>
 * @version $Id: Fop.java,v 1.1 2005/04/06 16:22:43 lk Exp $
 */
public class Fop
{
    public static void main(String[] args) throws Exception
    {
        new Options(new File("userconfig.xml"));

        Driver driver = new Driver();

        BasicConfigurator.configure();
        driver.setLogger(new Log4JLogger(Category.getInstance(Fop.class)));
        driver.setRenderer(Driver.RENDER_PDF);

        FileOutputStream os = new FileOutputStream("output.pdf");
        driver.setOutputStream(os);

        Source xslt = new StreamSource(new File("xslt.fo"));
        Transformer t = TransformerFactory.newInstance().newTransformer(xslt);

        Result res = new SAXResult(driver.getContentHandler());
        Source src = new StreamSource(new File("report.xml"));

        t.transform(src, res);

        os.close();
    }
}
