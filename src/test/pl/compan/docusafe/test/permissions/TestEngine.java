package pl.compan.docusafe.test.permissions;

import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.zip.CRC32;
/* User: Administrator, Date: 2005-10-28 16:26:01 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TestEngine.java,v 1.4 2008/10/06 11:27:00 pecet4 Exp $
 */
public class TestEngine
{
    private static Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection("jdbc:jtds:sqlserver://localhost", "nw", "nw");
    }

    public static void main(String[] args) throws Exception
    {
        Class.forName("net.sourceforge.jtds.jdbc.Driver");

        Connection conn = getConnection();
        conn.setAutoCommit(false);

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select max(id) from ds_document");
        rs.next();
        int maxId = rs.getInt(1);
        rs.close();

        long time = System.currentTimeMillis();

        StringBuilder sb = new StringBuilder();
        CRC32 crc = new CRC32();

        for (int i=1; i <= maxId; i++)
        {
            rs = st.executeQuery("select name, subject, subjecttype from ds_document_permission where " +
                "document_id = "+i+" order by name, subject, subjecttype");

            sb.setLength(0);

            while (rs.next())
            {
                sb.append('~');
                sb.append(rs.getString(1));
                sb.append('~');
                sb.append(rs.getString(2));
                sb.append('~');
                sb.append(rs.getString(3));
            }
            rs.close();

            crc.reset();
            crc.update(sb.toString().getBytes());

            // TODO: sprawdzi�, czy crc dla szukanych uprawnie� s� r�ne

            if (i % 1000 == 0)
            {
                double percent = (double) i / maxId;
                long elapsed = System.currentTimeMillis()-time;
            }
        }

        st.close();
        conn.close();
    }
}
