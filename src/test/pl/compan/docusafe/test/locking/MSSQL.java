package pl.compan.docusafe.test.locking;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
/* User: Administrator, Date: 2005-10-26 11:52:52 */

import org.apache.commons.logging.LogFactory;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MSSQL.java,v 1.3 2008/10/06 10:40:35 pecet4 Exp $
 */
public class MSSQL
{
    private static void update(final int value, boolean call)
        throws Exception
    {
        Connection conn = getConnection();
        conn.setAutoCommit(false);

        Statement st = conn.createStatement();

        st.executeUpdate("insert into v values ("+value+")");

        //st.execute("begin transaction");
        st.executeQuery("select * from dso_journal with (rowlock xlock) where id=1");

        // w�tek value wykonuje si� do momentu select with lock
        // drugi w�tek uruchamia si�, robi select i czeka na pierwszy
        // pierwszy w�tek uaktualnia swoj� warto�� i robi co trzeba

        if (call)
        {
            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        update(value+10, false);
                    }
                    catch (Exception e)
                    {
                    	LogFactory.getLog("eprint").debug("", e);
                    }
                }
            }).start();
        }

        Thread.sleep(2000);

        st.executeUpdate("update dso_journal set sequenceid="+value+" where id=1");

        conn.commit();
        conn.close();
    }

    private static Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection("jdbc:jtds:sqlserver://localhost", "nw", "nw");
    }

    public static void main(String[] args) throws Exception
    {
        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        update(200, true);
    }
}
