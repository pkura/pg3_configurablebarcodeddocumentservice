package pl.compan.docusafe.test.locking;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
/* User: Administrator, Date: 2005-10-26 11:52:52 */

import org.apache.commons.logging.LogFactory;

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: FB.java,v 1.2 2008/10/06 10:10:27 pecet4 Exp $
 */
public class FB
{
    private static void update(final int value, boolean call)
        throws Exception
    {
        Connection conn = getConnection();
        conn.setAutoCommit(false);

        Statement st = conn.createStatement();
        st.executeQuery("select * from dso_journal where id=0 for update with lock");

        // w�tek value wykonuje si� do momentu select with lock
        // drugi w�tek uruchamia si�, robi select i czeka na pierwszy
        // pierwszy w�tek uaktualnia swoj� warto�� i robi co trzeba

        if (call)
        {
            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        update(value+10, false);
                    }
                    catch (Exception e)
                    {
                    	LogFactory.getLog("eprint").debug("", e);
                    }
                }
            }).start();
        }

        Thread.sleep(2000);

        st.executeUpdate("update dso_journal set sequenceid="+value+" where id=0");

        st.executeUpdate("insert into v values ("+value+")");

        conn.commit();
        conn.close();
    }

    private static Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection("jdbc:firebirdsql:localhost:docusafe", "sysdba", "masterkey");
    }

    public static void main(String[] args) throws Exception
    {
        Class.forName("org.firebirdsql.jdbc.FBDriver");
        update(200, true);
    }
}
