package pl.compan.docusafe.test;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import java.io.FileInputStream;
import java.io.File;
import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/* User: Administrator, Date: 2005-08-08 13:11:44 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: RelayXml.java,v 1.2 2008/10/06 11:04:52 pecet4 Exp $
 */
public class RelayXml
{
    public static void main(String[] args) throws Exception
    {
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

        Handler handler = new Handler();
        parser.parse(new FileInputStream(args[0]), handler);

    }

    static class Handler extends DefaultHandler
    {
        private File imageFile;
        private int pages;
        private String port;
        private String rfaxid;
        private Calendar calTime = Calendar.getInstance();

        private int state;

        private static final int STATE_IMAGE = 1;
        private static final int STATE_PAGES = 2;
        private static final int STATE_PORT = 3;
        private static final int STATE_RFAXID = 4;
        private static final int STATE_TIME = 5;
        private static final int STATE_DATE = 6;

        private static final DateFormat dformat = new SimpleDateFormat("yyyy/MM/dd");
        private static final DateFormat tformat = new SimpleDateFormat("HH:mm:ss");

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
        {
            if ("ImageFilePath".equals(qName))
                state = STATE_IMAGE;
            else if ("Port".equals(qName))
                state = STATE_PORT;
            else if ("RemoteID".equals(qName))
                state = STATE_RFAXID;
            else if ("SubmitDate".equals(qName))
                state = STATE_DATE;
            else if ("SubmitTime".equals(qName))
                state = STATE_TIME;
            else if ("TotalPages".equals(qName))
                state = STATE_PAGES;
            else
                state = 0;
        }

        public void characters(char ch[], int start, int length) throws SAXException
        {
            switch (state)
            {
                case STATE_IMAGE:
                    imageFile = new File(new String(ch, start, length));
                    break;
                case STATE_PORT:
                    port = new String(ch, start, length);
                    break;
                case STATE_RFAXID:
                    rfaxid = new String(ch, start, length);
                    break;
                case STATE_DATE:
                    synchronized (dformat)
                    {
                        try
                        {
                            Calendar tmp = Calendar.getInstance();
                            tmp.setTime(dformat.parse(new String(ch, start, length)));
                            calTime.set(Calendar.YEAR, tmp.get(Calendar.YEAR));
                            calTime.set(Calendar.MONTH, tmp.get(Calendar.MONTH));
                            calTime.set(Calendar.DAY_OF_MONTH, tmp.get(Calendar.DAY_OF_MONTH));
                        }
                        catch (ParseException e)
                        {
                        }
                    }
                    break;
                case STATE_TIME:
                    synchronized (tformat)
                    {
                        try
                        {
                            Calendar tmp = Calendar.getInstance();
                            tmp.setTime(tformat.parse(new String(ch, start, length)));
                            calTime.set(Calendar.HOUR_OF_DAY, tmp.get(Calendar.HOUR_OF_DAY));
                            calTime.set(Calendar.MINUTE, tmp.get(Calendar.MINUTE));
                            calTime.set(Calendar.SECOND, tmp.get(Calendar.SECOND));
                        }
                        catch (ParseException e)
                        {
                        }
                    }
                    break;
                case STATE_PAGES:
                    try
                    {
                        pages = Integer.parseInt(new String(ch, start, length));
                    }
                    catch (NumberFormatException e)
                    {
                    }
                    break;
            }
        }

        public File getImageFile()
        {
            return imageFile;
        }

        public int getPages()
        {
            return pages;
        }

        public String getPort()
        {
            return port;
        }

        public String getRfaxid()
        {
            return rfaxid;
        }

        public Date getTime()
        {
            return calTime.getTime();
        }
    }
}
