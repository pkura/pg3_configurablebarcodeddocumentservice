package pl.compan.docusafe.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/* User: Administrator, Date: 2005-06-22 11:50:31 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: MsSql.java,v 1.1 2005/06/22 09:47:22 lk Exp $
 */
public class MsSql
{
    static Connection conn;

    public static void main(String[] args) throws Exception
    {
        Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
        String url = "jdbc:microsoft:sqlserver://localhost:1433";
        conn = DriverManager.getConnection(url, "ds", "ds");
        Statement st = conn.createStatement();
        st.executeUpdate("use ds1");
    }
}
