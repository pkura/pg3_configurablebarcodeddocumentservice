package pl.compan.docusafe.test;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.codec.TiffImage;
import com.lowagie.text.pdf.codec.PngImage;
import com.lowagie.text.pdf.codec.GifImage;
import com.lowagie.text.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
/* User: Administrator, Date: 2005-08-25 12:48:31 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: PdfImage.java,v 1.3 2009/05/12 07:11:19 mariuszk Exp $
 */
public class PdfImage
{
    public static void main(String[] args) throws Exception
    {
        File temp = null;
        File fontDir = new File("c:/docusafe/HOME", "fonts");
        File arial = new File(fontDir, "arial.ttf");
        BaseFont baseFont = BaseFont.createFont(arial.getAbsolutePath(),
            BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        Font font = new Font(baseFont, 12);
        Font headingFont = new Font(baseFont, 14);

        temp = new File("image.pdf");
        com.lowagie.text.Document pdfDoc =
            new com.lowagie.text.Document();
        PdfWriter writer = PdfWriter.getInstance(pdfDoc, new FileOutputStream(temp));
        pdfDoc.open();

        PdfContentByte cb = writer.getDirectContent();

        float width = 500, height = 700;

        addImage(new File("c:/download/multi.gif"), null, pdfDoc, cb, width, height);

        pdfDoc.close();
    }

    private static boolean addImage(File file, String contentType,
                                 Document pdfDoc, PdfContentByte cb,
                                 float width, float height)
        throws IOException, DocumentException
    {
        String mime = contentType != null ? contentType.toLowerCase() : "";
        String ext = file.getName().toLowerCase();

        if (mime.indexOf("image/tif") >= 0 || ext.endsWith(".tif") || ext.endsWith(".tiff"))
        {
            addTiffImage(file, pdfDoc, cb, width, height);
            return true;
        }
        else if (mime.indexOf("image/png") >= 0 || ext.endsWith(".png"))
        {
            addPngImage(file, pdfDoc, cb, width, height);
            return true;
        }
        else if (mime.indexOf("image/gif") >= 0 || ext.endsWith(".gif"))
        {
            addGifImage(file, pdfDoc, cb, width, height);
            return true;
        }

        return false;
    }

    private static void addTiffImage(File file, Document pdfDoc, PdfContentByte cb,
                                      float width, float height)
        throws IOException, DocumentException
    {
        RandomAccessFileOrArray raf = new RandomAccessFileOrArray(file.getAbsolutePath());
        int pages = TiffImage.getNumberOfPages(raf);
        for (int i=0; i < pages; i++)
        {
            Image image = TiffImage.getTiffImage(raf, i+1);

            float iwidth = image.plainWidth();
            float iheight = image.plainHeight();
            //boolean doubley = false;

            if (iheight < iwidth)
            {
                iheight *= 2;
                //doubley = true;
            }

            float sx = iwidth > width ? width / iwidth : 1.0f;
            float sy = iheight > height ? height / iheight : 1.0f;

            float scale = Math.min(sx, sy);

            if (iwidth > width || iheight > height)
            {
                //image.scaleToFit(width, height);
                image.scaleAbsolute(scale * iwidth, scale * iheight);
            }

            image.setAbsolutePosition(20, 20);
            cb.addImage(image);
            pdfDoc.newPage();
        }
    }

    private static void addPngImage(File file, Document pdfDoc, PdfContentByte cb,
                                      float width, float height)
        throws IOException, DocumentException
    {
        Image image = PngImage.getImage(file.getAbsolutePath());
        image.scaleToFit(width, height);
        image.setAbsolutePosition(20, 20);
        cb.addImage(image);
        pdfDoc.newPage();
    }

    public static void addGifImage(File file, Document pdfDoc, PdfContentByte cb,
                                      float width, float height)
        throws IOException, DocumentException
    {
        Image image = new GifImage(file.getAbsolutePath()).getImage(1);
        image.scaleToFit(width, height);
        image.setAbsolutePosition(20, 20);
        cb.addImage(image);
        pdfDoc.newPage();
    }
}
