package pl.compan.docusafe.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
/* User: Administrator, Date: 2005-10-07 16:01:53 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: InsertBenchmark.java,v 1.2 2008/10/06 10:26:25 pecet4 Exp $
 */
public class InsertBenchmark
{
    public static void main(String[] args) throws Exception
    {
        Class.forName("org.firebirdsql.jdbc.FBDriver");
        Connection conn = DriverManager.getConnection("jdbc:firebirdsql:localhost:docusafe?lc_ctype=win1250", "sysdba", "masterkey");
        conn.setAutoCommit(false);

        Statement st = conn.createStatement();

        long time = System.currentTimeMillis();
        for (int i=0; i < 100000; i++)
        {
            st.executeUpdate("insert into s values ("+i+")");
        }

        conn.commit();
    }
}
