package pl.compan.docusafe.test;

import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
/* User: Administrator, Date: 2005-08-12 10:14:15 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Enroll.java,v 1.2 2008/10/06 10:10:02 pecet4 Exp $
 */
public class Enroll
{
    public static void main(String[] args) throws Exception
    {
        InputStream stream = new FileInputStream(args[0]);
        byte[] buf = new byte[2048];
        int count = stream.read(buf);
        byte[] data = new byte[count];
        System.arraycopy(buf, 0, data, 0, count);
        data = Base64.decode(data);
        PKCS10CertificationRequest pkcs10 = new PKCS10CertificationRequest(data);
    }
}
