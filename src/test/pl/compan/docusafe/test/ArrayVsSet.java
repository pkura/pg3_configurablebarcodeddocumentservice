package pl.compan.docusafe.test;

import java.util.*;

/* User: Administrator, Date: 2005-07-08 09:52:11 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: ArrayVsSet.java,v 1.4 2008/10/06 09:28:51 pecet4 Exp $
 */
public class ArrayVsSet
{
    private static Random rand = new Random();

    public static void main(String[] args)
    {
        final int size = Integer.parseInt(args[0]);


        Set hash = new HashSet();
        Set tree = new TreeSet();
        String[] values = new String[size];
        String[] array = new String[size];
        for(int i=0; i < size; i++)
        {
            String tmp = nextValue(30);
            hash.add(tmp);
            tree.add(tmp);
            values[i] = tmp;
            array[i] = tmp;
        }



        Arrays.sort(array);

        long hashTotal = 0, treeTotal = 0, arrayTotal = 0;

        final int TESTS = 100000000;
        // testy
        for (int i=0; i < TESTS; i++)
        {
            int pick = rand.nextInt(size);
            String value = values[pick];

            long time = System.currentTimeMillis();
            hash.contains(value);
            hashTotal += (System.currentTimeMillis() - time);

            time = System.currentTimeMillis();
            tree.contains(value);
            treeTotal += (System.currentTimeMillis() - time);

            time = System.currentTimeMillis();
            Arrays.binarySearch(array, value);
            arrayTotal += (System.currentTimeMillis() - time);
        }
    }

    private static String nextValue(int length)
    {
        StringBuilder result = new StringBuilder(length);
        int sizeleft = length;
        while (result.length() < sizeleft)
        {
            String tmp = String.valueOf(Math.abs(rand.nextLong()));
            if (tmp.length() < (sizeleft - result.length()))
            {
                result.append(tmp);
            }
            else
            {
                result.append(tmp.substring(0, sizeleft - result.length()));
            }
        }
        return result.toString();
    }
}
