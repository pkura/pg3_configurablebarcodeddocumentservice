package pl.compan.docusafe.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

/* User: Administrator, Date: 2005-07-14 14:38:16 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: TestConcurrency.java,v 1.2 2008/10/06 11:26:59 pecet4 Exp $
 */
public abstract class TestConcurrency
{
    static void test(String url, String user, String pass) throws Exception
    {
        Connection c1 = DriverManager.getConnection(url, user, pass);
        c1.setAutoCommit(false);
        c1.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

        Connection c2 = DriverManager.getConnection(url, user, pass);
        c2.setAutoCommit(false);
        c2.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

        Statement st1 = c1.createStatement();
        int rows1 = st1.executeUpdate("update test set seq=2 where seq=1 and id=1");

        int seq = 2;
        Statement st2 = c1.createStatement();
        int rows2 = st2.executeUpdate("update test set seq="+seq+" where seq=1 and id=1");
        while (rows2 == 0)
        {
            c2.rollback();
            ResultSet rs = st2.executeQuery("select seq from test where id=1");
            rs.next();
            seq = rs.getInt(1);
            rows2 = st2.executeUpdate("update test set seq="+(seq+1)+" where seq="+seq+" and id=1");
        }

        c1.commit();

        c2.commit();
    }

    public static void main(String[] args) throws Exception
    {
        Class.forName("org.firebirdsql.jdbc.FBDriver");
        Class.forName("net.sourceforge.jtds.jdbc.Driver");

        //test("jdbc:firebirdsql:localhost:docusafe", "sysdba", "masterkey");
        test("jdbc:jtds:sqlserver://localhost", "nw", "nw");
    }
}
