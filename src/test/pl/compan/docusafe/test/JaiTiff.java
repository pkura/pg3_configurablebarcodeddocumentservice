package pl.compan.docusafe.test;

import com.sun.media.jai.codec.*;
import com.sun.media.jai.codecimpl.TIFFImage;
import com.sun.media.jai.codecimpl.TIFFImageDecoder;

import javax.media.jai.NullOpImage;
import javax.media.jai.OpImage;
import javax.media.jai.JAI;
import javax.media.jai.RenderedOp;
import javax.media.jai.operator.AWTImageDescriptor;
import javax.media.jai.operator.PNGDescriptor;
import javax.media.jai.operator.FileStoreDescriptor;
import javax.imageio.ImageIO;
import java.util.List;
import java.util.ArrayList;
import java.awt.image.RenderedImage;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.ParameterBlock;
import java.awt.color.ColorSpace;
import java.io.File;
import java.io.FileOutputStream;

import sun.net.www.content.image.png;

/* User: Administrator, Date: 2005-08-04 10:12:59 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: JaiTiff.java,v 1.7 2008/10/06 10:32:00 pecet4 Exp $
 */
public class JaiTiff
{
    public static void main(String[] args) throws Exception
    {
        File tifFile = new File("c:/docusafe/xfax/RFX000000088.TIF");
        //File tifFile = new File("c:/docusafe/xfax/RFX000000054.TIF");

/*
        RenderedOp op = JAI.create("fileload", tifFile.getAbsolutePath());
        String[] names = op.getPropertyNames();
        for (int i=0; i < names.length; i++)
        {
        }

        TIFFDirectory dir = (TIFFDirectory) op.getProperty("tiff_directory");
        TIFFField[] fields = dir.getFields();
        for (int i=0; i < fields.length; i++)
        {
            String f;
            switch (fields[i].getType())
            {
                case TIFFField.TIFF_ASCII: f = fields[i].getAsString(0); break;
                case TIFFField.TIFF_BYTE: f = ""+fields[i].getAsInt(0); break;
                case TIFFField.TIFF_DOUBLE: f = ""+fields[i].getAsDouble(0); break;
                case TIFFField.TIFF_SHORT: f = ""+fields[i].getAsInt(0); break;
                case TIFFField.TIFF_LONG: f = ""+fields[i].getAsLong(0); break;
                default: f = "_";
            }
        }
*/

        //TIFFDecodeParam param = new TIFFDecodeParam();
        SeekableStream s = new FileSeekableStream(tifFile);
        ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, null);

/*
        if (dec.getNumPages() > 1)
        {
            List imageFiles = new ArrayList(dec.getNumPages());
            for (int i=0; i < dec.getNumPages(); i++)
            {
                RenderedImage image =
                    new NullOpImage(dec.decodeAsRenderedImage(i),
                                    null,
                                    null,
                                    OpImage.OP_IO_BOUND);
                File tmp = new File("image"+(i+1)+".tif");
                ImageIO.write(image, "tiff", tmp);
                imageFiles.add(tmp);
            }
        }
*/
        for (int i=0; i < dec.getNumPages(); i++)
        {
//            RenderedOp source = JAI.create("AWTImage",this.image);
//            source = JAI.create ("Invert",source,null);
//            BufferedImage image = source.getAsBufferedImage();
//            BufferedImage outputImage=new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_BYTE_BINARY);
//            outputImage.createGraphics().drawImage(image, 0, 0, null);

            RenderedImage rendered = dec.decodeAsRenderedImage(i);

            // rendered.getData().
/*
            BufferedImage source =
            BufferedImage outputImage = new BufferedImage(source.getWidth(null), source.getHeight(null), BufferedImage.TYPE_BYTE_BINARY);
            outputImage.createGraphics().drawImage(source, 0, 0, null);
*/

            ParameterBlock bpb = new ParameterBlock();
            bpb.addSource(rendered);
            bpb.add(new Double(0.1));
            rendered = JAI.create("binarize", bpb);

            // 204x96 dpi
/*
            if (rendered.getWidth() > rendered.getHeight())
            {
                ParameterBlock pb = new ParameterBlock();
                pb.addSource(rendered);
                pb.add(new Float(1.0));
                pb.add(new Float(2.0));
                pb.add(new Float(0.0));
                pb.add(new Float(0.0));
                rendered = JAI.create("scale", pb);
            }
*/


            TIFFEncodeParam param = new TIFFEncodeParam();
            param.setCompression(TIFFEncodeParam.COMPRESSION_GROUP4);
            File tmp = new File("image"+(i+1)+".tif");
            FileOutputStream os = new FileOutputStream(tmp);
            //SeekableStream ss = new FileSeekableStream(tmp);

            ParameterBlock pb = new ParameterBlock();
            //pb.addSource(AWTImageDescriptor.create(rendered, null));
            pb.addSource(rendered);
            pb.add(os);
            pb.add("tiff");
            pb.add(param);
            RenderedOp op = JAI.create("encode", pb);



            forceLoad(op);
            op.dispose();
            os.close();
        }
        s.close();
    }

    private static void forceLoad(RenderedImage op)
    {
        // wymuszanie zakończenia operacji
        // http://java.sun.com/products/java-media/jai/forDevelopers/jaifaq.html#deferred

        if (op instanceof RenderedOp)
        {
            ((RenderedOp) op).getRendering();
        }

        int minX = op.getMinTileX();
        int minY = op.getMinTileY();
        int maxX = minX + op.getNumXTiles();
        int maxY = minY + op.getNumYTiles();

        for (int y = minY; y < maxY; y++)
        {
            for (int x = minX; x < maxX; x++)
            {
                op.getTile(x, y);
            }
        }
    }

    private static String array(short[] array)
    {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i < array.length; i++)
        {
            sb.append(array[i]+", ");
        }
        return sb.toString();
    }
}
