package pl.compan.docusafe.test.clip;

import com.asprise.util.tiff.TIFFReader;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.ImageCodec;

import javax.media.jai.RenderedOp;
import javax.media.jai.JAI;
import java.io.File;
import java.io.FileOutputStream;
import java.awt.image.*;
import java.awt.image.renderable.ParameterBlock;
import java.util.Hashtable;
/* User: Administrator, Date: 2005-12-20 13:53:38 */

/**
 * @author <a href="mailto:lukasz.kowalczyk@com-pan.pl">Lukasz Kowalczyk</a>
 * @version $Id: Clip.java,v 1.1 2005/12/20 15:52:06 lk Exp $
 */
public class Clip
{
    public static void main(String[] args) throws Exception
    {
/*
        TIFFReader reader = new TIFFReader(new File(args[0]));

        RenderedImage im = reader.getPage(0);
        BufferedImage bim2 = getBufferedImage(im);
        BufferedImage bim = bim2.getSubimage(0, 0, 800, 800);
*/

        FileSeekableStream s = new FileSeekableStream(new File(args[0]));
        ImageDecoder dec = ImageCodec.createImageDecoder("tiff", s, null);

        RenderedImage im = dec.decodeAsRenderedImage(0);

        FileOutputStream os = new FileOutputStream(new File(args[1]));
        ParameterBlock pb = new ParameterBlock();
        pb.addSource(getBufferedImage(im).getSubimage(100, 100, 800, 800));
        pb.add(os);
        pb.add("jpeg");
        RenderedOp op = JAI.create("encode", pb);
        op.dispose();
        os.close();

    }

    private static BufferedImage getBufferedImage(RenderedImage ri)
    {
        if (ri instanceof BufferedImage)
            return (BufferedImage) ri;

        ColorModel cm = ri.getColorModel();
        WritableRaster raster = cm.createCompatibleWritableRaster(ri.getWidth(), ri.getHeight());
        Hashtable properties = new Hashtable();
        String[] keys = ri.getPropertyNames();
        if (keys!=null)
        {
            for (int i = 0; i < keys.length; i++)
            {
                properties.put(keys[i], ri.getProperty(keys[i]));
            }
        }
        BufferedImage bi = new BufferedImage(cm, raster,
            cm.isAlphaPremultiplied(), null);
        ri.copyData(raster);
        return bi;
    }
}
