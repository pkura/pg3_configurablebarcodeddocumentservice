#include "stdafx.h"
#include "dsnotifier.h"

/**
    Procedura obs�ugi okna dialogowego przyjmuj�cego parametry po��czenia
    z serwerem Docusafe.
    Parametr lParam musi zawiera� wska�nik do struktury CONNECTDLG_DATA.
*/
BOOL CALLBACK ConnectDlgProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
{
    HWND hWndOwner;
    RECT rcOwner, rcDlg, rc;
    static CONNECTDLG_DATA data;
    static PCONNECTDLG_DATA pdata;

    switch (msg)
    {
    case WM_INITDIALOG:
        // centrowanie dialogu
        hWndOwner = GetDesktopWindow();
        GetWindowRect(hWndOwner, &rcOwner);
        GetWindowRect(hDlg, &rcDlg);
        CopyRect(&rc, &rcOwner);

        OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top); 
        OffsetRect(&rc, -rc.left, -rc.top); 
        OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom); 
 
         // The new position is the sum of half the remaining 
         // space and the owner's original position. 
 
        SetWindowPos(hDlg, 
            HWND_TOP, 
            rcOwner.left + (rc.right / 2), 
            rcOwner.top + (rc.bottom / 2), 
            0, 0,          // ignores size arguments 
            SWP_NOSIZE); 

        pdata = (PCONNECTDLG_DATA) lParam;
        data = *pdata;
        
        // ustawienie maksymalnych d�ugo�ci przyjmowanych napis�w
        SendMessage(GetDlgItem(hDlg, IDC_URL), EM_SETLIMITTEXT, 
            sizeof(data.url)/sizeof(TCHAR) - 1, 0);
        SendMessage(GetDlgItem(hDlg, IDC_USERNAME), EM_SETLIMITTEXT,
            sizeof(data.username)/sizeof(TCHAR) - 1, 0);
        SendMessage(GetDlgItem(hDlg, IDC_PASSWORD), EM_SETLIMITTEXT,
            sizeof(data.password)/sizeof(TCHAR) - 1, 0);

        // ustawienie zapami�tanych warto�ci p�l
        SetDlgItemText(hDlg, IDC_URL, data.url);
        SetDlgItemText(hDlg, IDC_USERNAME, data.username);
        SetDlgItemText(hDlg, IDC_PASSWORD, data.password);

        // kursor ustawiany jest w pierwszym pustym polu lub w polu adresu,
        // je�eli wszystkie pola s� wype�nione
        if (lstrlen(data.url) == 0)
            SetFocus(GetDlgItem(hDlg, IDC_URL));
        else if (lstrlen(data.username) == 0)
            SetFocus(GetDlgItem(hDlg, IDC_USERNAME));
        else if (lstrlen(data.password) == 0)
            SetFocus(GetDlgItem(hDlg, IDC_PASSWORD));
        else
            SetFocus(GetDlgItem(hDlg, IDC_URL));

        return FALSE;

    case WM_COMMAND:
        switch (LOWORD(wParam))
        {
        case IDC_URL:
            if (HIWORD(wParam) == EN_UPDATE)
                GetDlgItemText(hDlg, IDC_URL, data.url, sizeof(data.url)/sizeof(TCHAR));
            break;
        case IDC_USERNAME:
            if (HIWORD(wParam)== EN_UPDATE)
                GetDlgItemText(hDlg, IDC_USERNAME, data.username, sizeof(data.username)/sizeof(TCHAR));
            break;
        case IDC_PASSWORD:
            if (HIWORD(wParam) == EN_UPDATE)
                GetDlgItemText(hDlg, IDC_PASSWORD, data.password, sizeof(data.password)/sizeof(TCHAR));
            break;
        case IDOK:
            *pdata = data;
            EndDialog(hDlg, TRUE);
            return TRUE;
        case IDCANCEL:
            EndDialog(hDlg, FALSE);
            return TRUE;
        }
        break;
    }
    return FALSE;
}

