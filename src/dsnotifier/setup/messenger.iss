; -- Example3.iss --
; Same as Example1.iss, but creates some registry entries too.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=Docusafe Messenger
AppVerName=Docusafe Messenger 1.0
DefaultDirName={pf}\Docusafe Messenger
DefaultGroupName=Docusafe Messenger
UninstallDisplayIcon={app}\messenger.exe
SourceDir=..\release w2k
OutputDir=.
AlwaysUsePersonalGroup=yes

[Files]
Source: "messenger.exe"; DestDir: "{app}"
; Source: "MyProg.hlp"; DestDir: "{app}"
; Source: "Readme.txt"; DestDir: "{app}"; Flags: isreadme

[Icons]
Name: "{group}\Docusafe Messenger"; Filename: "{app}\messenger.exe"
Name: "{userstartup}\Docusafe Messenger"; Filename: "{app}\messenger.exe"

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: pl; MessagesFile: "compiler:Languages\Polish.isl"

[Run]
Filename: "{app}\messenger.exe"; Description: "Uruchom aplikacj�"; Flags: postinstall nowait skipifsilent

; NOTE: Most apps do not need registry entries to be pre-created. If you
; don't know what the registry is or if you need to use it, then chances are
; you don't need a [Registry] section.

;[Registry]
; Start "Software\My Company\My Program" keys under HKEY_CURRENT_USER
; and HKEY_LOCAL_MACHINE. The flags tell it to always delete the
; "My Program" keys upon uninstall, and delete the "My Company" keys
; if there is nothing left in them.
;Root: HKCU; Subkey: "Software\My Company"; Flags: uninsdeletekeyifempty
;Root: HKCU; Subkey: "Software\My Company\My Program"; Flags: uninsdeletekey
;Root: HKLM; Subkey: "Software\My Company"; Flags: uninsdeletekeyifempty
;Root: HKLM; Subkey: "Software\My Company\My Program"; Flags: uninsdeletekey
;Root: HKLM; Subkey: "Software\My Company\My Program\Settings"; ValueType: string; ValueName: "Path"; ValueData: "{app}"
