#include "stdafx.h"
#include "dsnotifier.h"

void MsgBox(tstring message)
{
#if defined(DS_DEBUG)
    MessageBox(NULL, message.c_str(), TEXT("Informacja Docusafe"), MB_ICONINFORMATION);
#endif
}
