//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by dsnotifier.rc
//
#define IDC_MYICON                      2
#define IDD_DSNOTIFIER_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_DSNOTIFIER                  107
#define IDI_SMALL                       108
#define IDC_DSNOTIFIER                  109
#define IDR_MAINFRAME                   128
#define IDD_CONNECT                     129
#define IDI_CHECKING                    131
#define IDI_NEWTASKS                    133
#define IDI_CONNECT_ERROR               134
#define IDC_URL                         1000
#define IDC_USERNAME                    1001
#define IDC_PASSWORD                    1002
#define IDM_CONNECT                     32772
#define ID_MENU_OPEN                    32776
#define ID_MENU_OPTIONS                 32777
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
