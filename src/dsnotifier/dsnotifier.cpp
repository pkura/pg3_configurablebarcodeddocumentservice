// dsnotifier.cpp : Defines the entry point for the application.
//

/*
    Changelog:

    5/6/2006 �ukasz Kowalczyk. 
        Dla Nationwide w wypadku braku adresu aplikacji w rejestrze, przyjmowany 
        jest adres NATIONWIDE_URL zdefiniowany z customerspecific.h
        Dla Nationwide nie jest zapami�tywane has�o (trzeba je wpisa� przy ka�dym
        uruchomieniu aplikacji).
        W dialogu logowania kursor ustawiany jest w pierwszym pustym polu.
*/

#include "stdafx.h"
#include "dsnotifier.h"
#include "base64.h"
#include "customerspecific.h"
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
CONNECTDLG_DATA connectDlgData = { };
BOOL bPasswordStored = FALSE;
// ta zmienna jest uaktualniana w procedurze ShowConnectDialog
// i ma warto�� TRUE, gdy wy�wietlany jest dialog IDD_CONNECT
BOOL connectDlgShowing = FALSE;
// ikonki w trayu
HICON hIconChecking;
HICON hIconError;
HICON hIconNewTasks;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
HWND				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
BOOL                ReadConnectionData();
BOOL                StoreConnectionData();
BOOL                ShowConnectDialog(HINSTANCE hInst, HWND hWnd);
void                NotifyTasks(HWND hWnd, PTASK_DATA pData);
VOID                CALLBACK CheckTasksTimer(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);
void                OpenBrowser();
DWORD               GetDllVersion(LPCTSTR lpszDllName);
void                FlashIcon(HWND hWnd, BOOL bFlash);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;

    if (GetDllVersion(TEXT("shell32.dll")) < PACKVERSION(5, 0))
    {
        MessageBox(NULL, TEXT("Program wymaga Internet Explorera 5.0 lub nowszego"), TEXT("Docusafe"), MB_ICONERROR);
        return FALSE;
    }


    /*
    DLLVERSIONINFO dllVersionInfo;
    dllVersionInfo.cbSize = sizeof(DLLVERSIONINFO);
    DllGetVersion(&dllVersionInfo);
    */

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_DSNOTIFIER, szWindowClass, MAX_LOADSTRING);

    hIconChecking = LoadIcon(hInstance, (LPCTSTR)IDI_CHECKING);
    hIconError = LoadIcon(hInstance, (LPCTSTR)IDI_CONNECT_ERROR);
    hIconNewTasks = LoadIcon(hInstance, (LPCTSTR)IDI_NEWTASKS);

    ReadConnectionData();

	HWND hOtherInstance = FindWindow(szWindowClass, szTitle);
	// je�eli istnieje uruchomiona ju� instancja Messengera, jest ona
	// po cichu zamykana i otwierana jest nowa; przed zamkni�ciem pobierane
	// jest z niej has�o
    if (hOtherInstance)
    {
		int iPasswordLength = sizeof(connectDlgData.password)/sizeof(TCHAR);
		DWORD dwPid = 0;
		int iCopied;

		// alokacja bufora na stercie procesu drugiego Messengera
		// bufor zostanie wykorzystany na pobranie has�a

		GetWindowThreadProcessId(hOtherInstance, &dwPid);

		HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPid);
		
		LPVOID pvBuf = VirtualAllocEx(hProc, NULL, 
			iPasswordLength * sizeof(TCHAR),
			MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

		// zapytanie o has�o
		SendMessage(hOtherInstance, WM_GET_PASSWORD,  iPasswordLength, (LPARAM) pvBuf);
		
		ReadProcessMemory(hProc, pvBuf, connectDlgData.password, 
			iPasswordLength * sizeof(TCHAR),
			(SIZE_T*) &iCopied);
		VirtualFreeEx(hProc, pvBuf, 0, MEM_RELEASE);
		//MessageBox(NULL, connectDlgData.password, TEXT("tutaj haslo"), MB_ICONINFORMATION);

		// zamykanie drugiego Messengera
		SendMessage(hOtherInstance, WM_CLOSE, NULL, NULL);
    }

    MyRegisterClass(hInstance);

	// Perform application initialization:
    HWND hWnd = InitInstance (hInstance, nCmdShow);
	if (!hWnd) 
	{
		return FALSE;
	}

    // odczytanie danych po��czenia z rejestru
	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= 0; // CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_DSNOTIFIER);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL; //(LPCTSTR)IDC_DSNOTIFIER;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowEx(WS_EX_TOOLWINDOW, szWindowClass, szTitle, 0,
       CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return NULL;
   }

   ShowWindow(hWnd, SW_HIDE);
   UpdateWindow(hWnd);

   return hWnd;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	//PAINTSTRUCT ps;
	//HDC hdc;
    POINT pt;
    TASK_DATA taskData = {};
    PTASK_DATA pTaskData;

    static BOOL servicingNotification;
    static NOTIFYICONDATA nid = {};
    static HINSTANCE hInstance;
    static HMENU hTrayMenu;

	switch (message) 
	{
    case WM_CREATE:
        hInstance = ((LPCREATESTRUCT) lParam)->hInstance;

        // menu pokazywane w trayu
        hTrayMenu = LoadMenu(hInstance, szWindowClass);
        hTrayMenu = GetSubMenu(hTrayMenu, 0);

        // ikonka w trayu
        nid.cbSize = sizeof(NOTIFYICONDATA);
        nid.hWnd = hWnd;
        nid.uID = SHELL_ICON_UID;
        nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
        nid.hIcon = hIconChecking;
        nid.uCallbackMessage = WM_SHELL_MENU;
        {
            tstring msg(PROGRAM_NAME);
            size_t count = msg.copy(nid.szTip, sizeof(nid.szTip)/sizeof(TCHAR)-1, 0);
            nid.szTip[count] = '\0';
        }

        Shell_NotifyIcon(NIM_ADD, &nid);

        // je�eli nie znaleziono adresu aplikacji, nazwy u�ytkownika lub has�a, dialog b�dzie
        // pokazany przy starcie aplikacji
        while (lstrlen(connectDlgData.url) == 0 || lstrlen(connectDlgData.username) == 0 ||
            lstrlen(connectDlgData.password) == 0)
        {
            if (!ShowConnectDialog(hInst, hWnd))
                return -1;
        }

        {
            tstring msg(PROGRAM_NAME);
            msg.append(TEXT("\nSprawdzanie zada�"));
            size_t count = msg.copy(nid.szTip, sizeof(nid.szTip)/sizeof(TCHAR)-1, 0);
            nid.szTip[count] = '\0';
            nid.uFlags = NIF_TIP;
            Shell_NotifyIcon(NIM_MODIFY, &nid);
        }

        // je�eli CheckTasks poinformuje o braku autoryzacji, ponowne pokazanie dialogu

        while (!CheckTasks(hWnd, connectDlgData.url, connectDlgData.username, connectDlgData.password, &taskData) &&
            taskData.dwAuthError == TRUE)
        {
            if (!ShowConnectDialog(hInst, hWnd))
                return -1;
        }

        SendMessage(hWnd, WM_TASKS_NOTIFY, NULL, (LPARAM)&taskData);

        SetTimer(hWnd, TIMER_ID, TIMER_ELAPSE, CheckTasksTimer);

        return 0;

	case WM_GET_PASSWORD:
		{
			TCHAR* buf = (TCHAR*) lParam;
			int size = (int) wParam;
			lstrcpyn(buf, connectDlgData.password, size);
			return 0;
		}

    // komunikat wysy�any przez ikonk� w trayu
    case WM_SHELL_MENU:
        switch (lParam)
        {
        case WM_RBUTTONDOWN:
            // wy�wietlenie popup-menu
            FlashIcon(hWnd, FALSE);
            if (connectDlgShowing)
                break;
            GetCursorPos(&pt);
            SetForegroundWindow(hWnd);
            TrackPopupMenu(hTrayMenu, TPM_RIGHTBUTTON, pt.x, pt.y, 0, hWnd, NULL);
            PostMessage(hWnd, WM_NULL, 0, 0);
            break;
        case WM_LBUTTONDOWN:
            FlashIcon(hWnd, FALSE);
            break;
        case WM_LBUTTONDBLCLK:
            OpenBrowser();
            break;
        }
        return 0;

    case WM_TASKS_NOTIFY:
        if (!servicingNotification)
        {
#ifdef DS_TRACE
            MessageBox(hWnd, TEXT("Wywo�ywanie NotifyTasks"), NULL, MB_ICONERROR);
#endif
            servicingNotification = TRUE;
            pTaskData = (PTASK_DATA)lParam;
            NotifyTasks(hWnd, pTaskData);
            servicingNotification = FALSE;
        }
#ifdef DS_DEBUG
        else
        {
            MessageBox(hWnd, TEXT("servicing=TRUE"), NULL, MB_ICONERROR);
        }
#endif
        return 0;

    case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Parse the menu selections:
		switch (wmId)
		{
        case ID_MENU_OPTIONS:
            ShowConnectDialog(hInst, hWnd);
            break;
        case ID_MENU_OPEN:
            OpenBrowser();
            break;
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
            /*
        case IDM_CONNECT:
            if (DialogBoxParam(hInst, (LPCTSTR)IDD_CONNECT, hWnd, (DLGPROC)ConnectDlgProc, (LPARAM)&connectDlgData))
            {
                StoreConnectionData();
            }
            break;
            */
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_DESTROY:
        KillTimer(hWnd, TIMER_ID);
        // ten timer nie zawsze istnieje
        KillTimer(hWnd, FLASH_TIMER_ID);
        Shell_NotifyIcon(NIM_DELETE, &nid);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

VOID CALLBACK FlashIconTimer(HWND hWnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
    NOTIFYICONDATA nid = {};
    static BOOL bState = FALSE;

    // ikonka w trayu
    nid.cbSize = sizeof(NOTIFYICONDATA);
    nid.hWnd = hWnd;
    nid.uID = SHELL_ICON_UID;
    nid.uFlags = NIF_ICON;
    nid.hIcon = bState ? hIconChecking : hIconNewTasks;
    Shell_NotifyIcon(NIM_MODIFY, &nid);

    bState = !bState;
}

void FlashIcon(HWND hWnd, BOOL bFlash)
{
    static BOOL bFlashing = FALSE;

    if (bFlash == bFlashing)
        return;

    bFlashing = bFlash;

    if (bFlash)
    {
        SetTimer(hWnd, FLASH_TIMER_ID, FLASH_TIMER_ELAPSE, FlashIconTimer);
    }
    else
    {
        KillTimer(hWnd, FLASH_TIMER_ID);

        // ustawienie prawid�owej ikonki
        NOTIFYICONDATA nid = {};
        nid.cbSize = sizeof(NOTIFYICONDATA);
        nid.hWnd = hWnd;
        nid.uID = SHELL_ICON_UID;
        nid.uFlags = NIF_ICON;
        nid.hIcon = hIconChecking;
        Shell_NotifyIcon(NIM_MODIFY, &nid);
    }
}

// Message handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
	return FALSE;
}

/**
    Odczytuje z rejestru dane po��czenia i umieszcza je w zmiennej globalnej
    connectDlgData.
*/
BOOL ReadConnectionData()
{
    CONNECTDLG_DATA data;

    HKEY hkey;
    DWORD dwSize;
    DWORD dwType;
    DWORD dwPasswordStored;

    memset(&data, 0, sizeof(data));

    // najpierw pr�ba odczytu adresu aplikacji z HKEY_CLASSES_ROOT\software\com-pan.pl\docusafe\messenger
    if (RegOpenKeyEx(HKEY_CLASSES_ROOT, GLOBAL_REGISTRY_KEY, 0, KEY_READ, &hkey) == ERROR_SUCCESS)
    {
        dwSize = sizeof (data.url);
        if (RegQueryValueEx(hkey, TEXT("url"), 0, &dwType, (LPBYTE) data.url, &dwSize) != ERROR_SUCCESS)
            memset(&data.url, 0, sizeof(data.url));
        // czy has�o ma by� pami�tane/zapisywane - domy�lnie nie, warto�� musi by� typu DWORD
        dwSize = sizeof (dwPasswordStored);
        if (RegQueryValueEx(hkey, TEXT("passwordstored"), 0, &dwType, (LPBYTE) &dwPasswordStored, &dwSize) != ERROR_SUCCESS ||
            dwType != REG_DWORD)
        {
            dwPasswordStored = 0;
        }
        bPasswordStored = !!dwPasswordStored;
        RegCloseKey(hkey);
    }

    if (RegOpenKeyEx(HKEY_CURRENT_USER, REGISTRY_KEY, 0, KEY_READ, &hkey) == ERROR_SUCCESS)
    {
        // adres aplikacji jest odczytywany z HKEY_CURRENT_USER tylko wtedy, gdy nie znaleziono go
        // w HKEY_CLASSES_ROOT
        if (lstrlen(data.url) == 0)
        {
            dwSize = sizeof (data.url);
            if (RegQueryValueEx(hkey, TEXT("url"), 0, &dwType, (LPBYTE) data.url, &dwSize) != ERROR_SUCCESS)
            {
                data.url[0] = '\0';
            }
        }

        dwSize = sizeof (data.username);
        if (RegQueryValueEx(hkey, TEXT("username"), 0, &dwType, (LPBYTE) data.username, &dwSize) != ERROR_SUCCESS)
        {
            data.username[0] = '\0';
        }
        data.username[dwSize-1] = '\0';

        if (bPasswordStored)
        {
            dwSize = sizeof (data.password);
            if (RegQueryValueEx(hkey, TEXT("password"), 0, &dwType, (LPBYTE) data.password, &dwSize) != ERROR_SUCCESS)
            {
                data.password[0] = '\0';
            }
            data.password[dwSize-1] = '\0';
        }
    }

    RegCloseKey(hkey);

#ifdef NATIONWIDE
    if (lstrlen(data.url) == 0)
    {
        lstrcpyn(data.url, NATIONWIDE_URL, sizeof(NATIONWIDE_URL)/sizeof(TCHAR));
    }
#endif

    connectDlgData = data;

    return TRUE;
}

/**
    Zapisuje w rejestrze dane po��czenia zawarte w zmiennej globalnej
    connectDlgData.  Nie zapisuje has�a.
*/
BOOL StoreConnectionData()
{
    HKEY hkey;
    DWORD dwDisposition;

    RegCreateKeyEx(HKEY_CURRENT_USER, REGISTRY_KEY, 0, NULL,
        REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, &dwDisposition);

    RegSetValueEx(hkey, TEXT("url"), 0, REG_SZ, (LPBYTE) connectDlgData.url, 
        lstrlen(connectDlgData.url)*sizeof(TCHAR) + sizeof(TCHAR));
    RegSetValueEx(hkey, TEXT("username"), 0, REG_SZ, (LPBYTE) connectDlgData.username, 
        lstrlen(connectDlgData.username)*sizeof(TCHAR) + sizeof(TCHAR));

#ifndef NATIONWIDE
    if (bPasswordStored)
    {
        RegSetValueEx(hkey, TEXT("password"), 0, REG_SZ, (LPBYTE) connectDlgData.password, 
            lstrlen(connectDlgData.password)*sizeof(TCHAR) + sizeof(TCHAR));
    }
#endif

    RegCloseKey(hkey);

    return TRUE;
}

/**
    Wy�wietla dialog pytaj�cy o dane po��czenia.
*/
BOOL ShowConnectDialog(HINSTANCE hInst, HWND hWnd)
{
    if (connectDlgShowing)
        return FALSE;

    connectDlgShowing = TRUE;

    BOOL result = 
        (DialogBoxParam(hInst, (LPCTSTR)IDD_CONNECT, hWnd, (DLGPROC)ConnectDlgProc, (LPARAM)&connectDlgData) == IDOK);

    if (result)
        StoreConnectionData();

    connectDlgShowing = FALSE;

    return result;
}

void OpenBrowser()
{
    SHELLEXECUTEINFO ei = {};

    if (lstrlen(connectDlgData.url) > 0)
    {
        tstring addr;
		int password_len = lstrlen(connectDlgData.password);
		unsigned char* password_chars = new unsigned char[password_len];
		for (int i=0; i < password_len; i++)
		{
			password_chars[i] = (char) connectDlgData.password[i];
		}

        addr += connectDlgData.url;
        addr += TEXT("/login.jsp");
        addr += TEXT("?username=");
        addr += connectDlgData.username;
        addr += TEXT("&password64=");
		addr += base64_encode(password_chars, password_len);
	    ei.cbSize = sizeof(ei);
	    ei.lpVerb = TEXT("open");
	    //ei.lpFile = connectDlgData.url;
        ei.lpFile = addr.c_str();
	    ei.fMask  = SEE_MASK_NOCLOSEPROCESS;
	    ei.nShow  = SW_SHOWMAXIMIZED;

		delete[] password_chars;

	    ShellExecuteEx(&ei);
    }
}

/**
    Funkcja informuj�ca u�ytkownika o nowych zadaniach.  Powinna by� wywo�ywana
    jedynie z procedury obs�ugi okna.
*/
void NotifyTasks(HWND hWnd, PTASK_DATA pData)
{
    NOTIFYICONDATA nid = {};

    nid.cbSize = sizeof(NOTIFYICONDATA);
    nid.hWnd = hWnd;
    nid.uID = SHELL_ICON_UID;

    if (pData->dwError == ERROR_SUCCESS)
    {
        if (pData->bHasTasks)
        {
            // takie powiadomienie mo�na robi� tylko wtedy, gdy mo�na
            // stwierdzi�, czy pojawi�y si� NOWE zadania

            // informowanie o nowych zadaniach w formie baloon tip
            if (pData->bHasNewIncomingTasks || pData->bHasNewOutgoingTasks || pData->bHasNewInternalTasks)
            {
                // baloon tip tylko dla Windows >= 0x0500
                // TODO: wywali� st�d NIF_ICON
                // TODO: u�y� DllGetVersion

                FlashIcon(hWnd, TRUE);

                nid.uFlags = NIF_ICON | NIF_TIP;
                nid.hIcon = hIconNewTasks;
                tstring tip_msg(PROGRAM_NAME);
                tip_msg.append(TEXT("\nMasz nowe zadania"));
                size_t count = tip_msg.copy(nid.szTip, sizeof(nid.szTip)/sizeof(TCHAR)-1, 0);
                nid.szTip[count] = '\0';

                // if (>= 0x0500)

                nid.uFlags |= NIF_INFO;
                nid.uTimeout = 1000*5;
                nid.dwInfoFlags = NIIF_INFO;

                // przygotowanie komunikatu
                BOOL comma = FALSE;
                //tstring info_msg;
                tostringstream info_out;
                info_out << TEXT("Masz nowe zadania\n"); 
                if (pData->bHasNewIncomingTasks)
                {
                    comma = TRUE;
                    info_out << TEXT("przychodz�cych: ") << (int) pData->iNewIncomingCount;
                }
                if (pData->bHasNewInternalTasks)
                {
                    if (comma) info_out << TEXT(", ");
                    else comma = TRUE;
                    info_out << TEXT("wewn�trznych: ") << (int) pData->iNewInternalCount;
                }
                if (pData->bHasNewOutgoingTasks)
                {
                    if (comma) info_out << TEXT(", ");
                    else comma = TRUE;
                    info_out << TEXT("wychodz�cych: ") << (int) pData->iNewOutgoingCount;
                }

                count = info_out.str().copy(nid.szInfo, sizeof(nid.szInfo)/sizeof(TCHAR)-1, 0);
                nid.szInfo[count] = '\0';

                tstring title_msg;
                title_msg = TEXT("Docusafe");
                count = title_msg.copy(nid.szInfoTitle, sizeof(nid.szInfoTitle)/sizeof(TCHAR)-1, 0);
                nid.szInfoTitle[count] = '\0';

                // niezale�nie od baloon tipa zmiana ikony i tooltipa
            }
            else
            {
                nid.uFlags = NIF_ICON | NIF_TIP;
                nid.hIcon = hIconNewTasks;
                tstring msg(PROGRAM_NAME);
                msg.append(TEXT("\nPojawi�y si� nowe zadania"));
                size_t count = msg.copy(nid.szTip, sizeof(nid.szTip)/sizeof(TCHAR)-1, 0);
                nid.szTip[count] = '\0';
            }
        }
        else
        {
            FlashIcon(hWnd, FALSE);

            nid.uFlags = NIF_ICON | NIF_TIP;
            nid.hIcon = hIconChecking;
            tstring msg(PROGRAM_NAME);
            msg.append(TEXT("\nBrak nowych zada�"));
            size_t count = msg.copy(nid.szTip, sizeof(nid.szTip)/sizeof(TCHAR)-1, 0);
            nid.szTip[count] = '\0';
        }
    }
    else
    {
        nid.uFlags = NIF_ICON | NIF_TIP;
        nid.hIcon = hIconError;
        tstring msg(PROGRAM_NAME);
        msg.append(TEXT("\n"));
        if (lstrlen(pData->szErrorMessage) > 0)
        {
            tostringstream ss;
            if (pData->dwError != -1)
            {
                ss << pData->dwError;
                ss << ": ";
            }
            ss << pData->szErrorMessage;
            msg.append(ss.str());
        }
        else
        {
            msg.append(TEXT("B��d podczas sprawdzania zada�"));
        }
        size_t count = msg.copy(nid.szTip, sizeof(nid.szTip)/sizeof(TCHAR)-1, 0);
        nid.szTip[count] = '\0';
    }

    Shell_NotifyIcon(NIM_MODIFY, &nid);
}

VOID CALLBACK CheckTasksTimer(HWND hWnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
    TASK_DATA taskData;
    CheckTasks(hWnd, connectDlgData.url, connectDlgData.username, connectDlgData.password, &taskData);
    SendMessage(hWnd, WM_TASKS_NOTIFY, NULL, (LPARAM)&taskData);
}

// ms-help://MS.VSCC.2003/MS.MSDNQTR.2003FEB.1033/shellcc/platform/shell/programmersguide/versions.htm
DWORD GetDllVersion(LPCTSTR lpszDllName)
{
    HINSTANCE hinstDll;
    DWORD dwVersion = 0;

    /* For security purposes, LoadLibrary should be provided with a 
       fully-qualified path to the DLL. The lpszDllName variable should be
       tested to ensure that it is a fully qualified path before it is used. */
    hinstDll = LoadLibrary(lpszDllName);
	
    if(hinstDll)
    {
        DLLGETVERSIONPROC pDllGetVersion;
        pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, 
                          "DllGetVersion");

        /* Because some DLLs might not implement this function, you
        must test for it explicitly. Depending on the particular 
        DLL, the lack of a DllGetVersion function can be a useful
        indicator of the version. */

        if(pDllGetVersion)
        {
            DLLVERSIONINFO dvi;
            HRESULT hr;

            ZeroMemory(&dvi, sizeof(dvi));
            dvi.cbSize = sizeof(dvi);

            hr = (*pDllGetVersion)(&dvi);

            if(SUCCEEDED(hr))
            {
               dwVersion = PACKVERSION(dvi.dwMajorVersion, dvi.dwMinorVersion);
            }
        }

        FreeLibrary(hinstDll);
    }
    return dwVersion;
}
