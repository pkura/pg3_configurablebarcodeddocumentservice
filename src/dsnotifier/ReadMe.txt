TODO: centrowanie dialogu


Sprawdza, czy program jest uruchomiony
Nie mo�na wywo�a� menu, gdy otwarty jest dialog
Dok�adne komunikaty b��du (401, 404, inne)
Sprawdza wymagan� wersj� Internet Explorera przed rozpocz�ciem pracy.
Ochrona przed nadu�yciami (nie odczyta wi�cej ni� 100KB danych)


Pierwsze wywo�anie ConnectDlg:
- pobranie has�a do skutku (lub do Cancel)
- je�eli podano has�o - CheckTasks
- je�eli has�o nieprawid�owe - ponowne pokazanie dialogu

Kolejne wywo�anie ConnectDlg:
- aktualizuje dane po��czenia, nie wywo�uje CheckTasks

CheckTasks powinno pokazywa� dialog, gdy status 401?

========================================================================
    WIN32 APPLICATION : dsnotifier Project Overview
========================================================================

AppWizard has created this dsnotifier application for you.  
This file contains a summary of what you will find in each of the files that
make up your dsnotifier application.


dsnotifier.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

dsnotifier.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
AppWizard has created the following resources:

dsnotifier.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
    Visual C++.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Visual C++ reads and updates this file.
dsnotifier.ico
    This is an icon file, which is used as the application's icon (32x32).
    This icon is included by the main resource file dsnotifier.rc.

small.ico
    This is an icon file, which contains a smaller version (16x16)
    of the application's icon. This icon is included by the main resource
    file dsnotifier.rc.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named dsnotifier.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
