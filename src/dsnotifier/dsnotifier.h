#pragma once

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
    #define DS_DEBUG
#endif

// ms-help://MS.VSCC.2003/MS.MSDNQTR.2003FEB.1033/shellcc/platform/shell/programmersguide/versions.htm
#define PACKVERSION(major,minor) MAKELONG(minor,major)

#define USER_AGENT TEXT("Docusafe Messenger 1.0.1")
#define PROGRAM_NAME TEXT("Docusafe Messenger 1.0.1")
#define URL_SUFFIX TEXT("/webservice/notifier.action?v=1.0")
#define REGISTRY_KEY TEXT("Software\\com-pan.pl\\docusafe\\messenger")
#define GLOBAL_REGISTRY_KEY REGISTRY_KEY

#define SHELL_ICON_UID 1
#define TIMER_ID 1
#define FLASH_TIMER_ID 2
#define TIMER_ELAPSE 1000*70
#define FLASH_TIMER_ELAPSE 700

#define WM_SHELL_MENU WM_USER+1
#define WM_TASKS_NOTIFY WM_USER+2
#define WM_GET_PASSWORD WM_USER+3

typedef std::basic_string<TCHAR> tstring;
typedef std::basic_ostringstream<TCHAR> tostringstream;
typedef std::basic_istringstream<TCHAR> tistringstream;

typedef struct 
{
    TCHAR url[100];
    TCHAR username[63];
    TCHAR password[63];
}
CONNECTDLG_DATA, *PCONNECTDLG_DATA;

// na razie struktura ma jedno pole, bo nie istnieje
// wsparcie dla odczytu liczby zada�
typedef struct
{
    BOOL bHasTasks;
    size_t iIncomingCount;
    size_t iOutgoingCount;
    size_t iInternalCount;
    size_t iNewIncomingCount;
    size_t iNewOutgoingCount;
    size_t iNewInternalCount;
    BOOL bHasNewIncomingTasks;
    BOOL bHasNewOutgoingTasks;
    BOOL bHasNewInternalTasks;

    BOOL dwAuthError;
    DWORD dwError;
    TCHAR szErrorMessage[128];
}
TASK_DATA, *PTASK_DATA;

extern BOOL connectDlgShowing;
BOOL CALLBACK ConnectDlgProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam);
BOOL CheckTasks(HWND hWnd, TCHAR* url, TCHAR* username, TCHAR* password, PTASK_DATA pData);
void MsgBox(tstring message);
