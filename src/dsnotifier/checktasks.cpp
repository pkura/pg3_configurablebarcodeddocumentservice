#include "stdafx.h"
#include "dsnotifier.h"

using namespace std;

void UpdateError(PTASK_DATA pData, const TCHAR* = NULL, DWORD = ERROR_SUCCESS);

// TODO: zabezpieczenie przed nak�adaniem si� wywo�a� CheckTasks, mo�na to
// robi� w TimerProc

/**
    Funkcja sprawdzaj�ca liczb� zada� u�ytkownika.  Mo�e si� zako�czy�
    sukcesem (informuj�c o liczbie zada�) lub mo�e wyst�pi� b��d.
    Zwraca TRUE, je�eli uda�o si� sprawdzi� zadania, w przeciwnym
    razie FALSE, wtedy systemowy kod b��du znajduje si� w data.dwError, 
    a komunikat w data.szErrorMessage.
    Gdy b��d spowodowany jest nieprawid�owym has�em, parametr data.dwAuthError
    ma warto�� TRUE.

    TODO: zabezpieczy� si� przed wielokrotnym wywo�aniem, funkcja ma zmienne static

    Invalid CA:
    http://support.microsoft.com/?id=182888
    Async Wininet:
    http://www.codeproject.com/internet/asyncwininet.asp
*/
BOOL CheckTasks(HWND hWnd, TCHAR* url, TCHAR* username, TCHAR* password, PTASK_DATA pData)
{
    TCHAR hostname[80];
    TCHAR path[80];
    URL_COMPONENTS urlComponents = {};
    urlComponents.dwStructSize = sizeof(URL_COMPONENTS);
    urlComponents.lpszHostName = hostname;
    urlComponents.dwHostNameLength = sizeof(hostname)/sizeof(TCHAR) - 1;
    urlComponents.lpszUrlPath = path;
    urlComponents.dwUrlPathLength = sizeof(path)/sizeof(TCHAR) - 1;

    pData->dwError = ERROR_SUCCESS;
    pData->dwAuthError = FALSE;

    static set<tstring>* incomingTasks = new set<tstring>;
    static set<tstring>* outgoingTasks = new set<tstring>;
    static set<tstring>* internalTasks = new set<tstring>;

    const int MAX_SERVICE_STREAM_SIZE = 100*1024;

    memset(pData, 0, sizeof(TASK_DATA));
    
    if (!InternetCrackUrl(url, 0, 0, &urlComponents))
    {
        UpdateError(pData, TEXT("Adres aplikacji jest niepoprawny"));
        return FALSE;
    }

    if (urlComponents.nScheme != INTERNET_SCHEME_HTTP && urlComponents.nScheme != INTERNET_SCHEME_HTTPS)
    {
        UpdateError(pData, TEXT("Wymagany jest adres HTTP lub HTTPS"), -1);
        return FALSE;
    }

    BOOL ssl = urlComponents.nScheme == INTERNET_SCHEME_HTTPS;

    if (InternetAttemptConnect(0) != ERROR_SUCCESS)
    {
        UpdateError(pData);
        return FALSE;
    }

    if (!InternetCheckConnection((LPCTSTR) url, FLAG_ICC_FORCE_CONNECTION, 0))
    {
        UpdateError(pData);
        return FALSE;
    }

    HINTERNET hInternet = InternetOpen(USER_AGENT, INTERNET_OPEN_TYPE_PRECONFIG, 
        NULL, NULL, 0);
    if (hInternet == NULL)
    {
        UpdateError(pData);
        return FALSE;
    }

    HINTERNET hHandle = hInternet;

    hInternet = InternetConnect(hInternet, urlComponents.lpszHostName, urlComponents.nPort, 
        username, password, INTERNET_SERVICE_HTTP, 0, 0);
    if (hInternet == NULL)
    {
        UpdateError(pData);
        InternetCloseHandle(hHandle);
        return FALSE;
    }

    tstring urlPath;
    urlPath += path;
    urlPath += URL_SUFFIX;


    DWORD dwFlags = INTERNET_FLAG_RELOAD | INTERNET_FLAG_NO_CACHE_WRITE | 
        INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_NO_UI;
    if (ssl)
    {
        dwFlags |= INTERNET_FLAG_SECURE;
        dwFlags |= INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
        dwFlags |= INTERNET_FLAG_IGNORE_CERT_DATE_INVALID;
    }
    hInternet = HttpOpenRequest(hInternet, TEXT("GET"), urlPath.c_str(), NULL, NULL, NULL, dwFlags, NULL);
    if (hInternet == NULL)
    {
        UpdateError(pData);
        InternetCloseHandle(hHandle);
        return FALSE;
    }

    // milcz�ce ignorowanie nieznanego CA
    if (ssl)
    {
        DWORD dwFlags;
        DWORD dwBufferLen;
        InternetQueryOption(hInternet, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags, &dwBufferLen);
        dwFlags |= SECURITY_FLAG_IGNORE_UNKNOWN_CA;
        InternetSetOption(hInternet, INTERNET_OPTION_SECURITY_FLAGS, &dwFlags, sizeof(dwFlags));
    }

    if (!HttpSendRequest(hInternet, NULL, 0, 0, 0))
    {
#ifdef DS_TRACE
        {
            tostringstream msg;
            msg << "send request ";
            msg << GetLastError();
            MsgBox(msg.str());
        }
#endif
        UpdateError(pData);
        InternetCloseHandle(hHandle);
        return FALSE;
    }

    DWORD status;
    DWORD buflen = sizeof(status);
    if (!HttpQueryInfo(hInternet, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &status, &buflen, NULL))
    {
#ifdef DS_TRACE
        {
            tostringstream msg;
            msg << TEXT("B��d wywo�ania HttpQueryInfo: ") << GetLastError();
            MsgBox(msg.str());
        }
#endif
        InternetCloseHandle(hHandle);
        return FALSE;
    }

    if (status != 200)
    {
        pData->dwError = -1;

        if (status == 401)
        {
            pData->dwAuthError = TRUE;
            UpdateError(pData, TEXT("Nieprawid�owe has�o lub nazwa u�ytkownika"), -1);
        }
        else if (status == 404)
        {
            UpdateError(pData, TEXT("Podany adres aplikacji jest niepoprawny"), -1);
        }
        else
        {
            tostringstream ss;
            ss << TEXT("B��d HTTP ") << status;
            UpdateError(pData, ss.str().c_str(), -1);
#ifdef DS_TRACE
            MsgBox(ss.str());
#endif
            /*
            size_t count = ss.str().copy(pData->szErrorMessage, sizeof(pData->szErrorMessage)/sizeof(TCHAR) - 1, 0);
            pData->szErrorMessage[count] = '\0';
            */
        }

        InternetCloseHandle(hHandle);
        return FALSE;
    }

#ifdef DS_TRACE
    if (status != 200)
    {
        tostringstream ss;
        ss << "Status HTTP " << (int) status;
        MsgBox(ss.str());
    }
#endif

    // odczyt listy zada� z us�ugi Docusafe
    // TODO: ograniczenie rozmiaru
    tostringstream ssxml;
    char buffer[256] = "";
    DWORD bytesRead = 1; // pocz�tkowa warto�� > 0, aby p�tla zacz�a wykonanie
    DWORD length = 0;
    while (InternetReadFile(hInternet, (LPVOID) buffer, sizeof(buffer)-1, &bytesRead) && bytesRead > 0)
    {
        buffer[bytesRead] = '\0';
        ssxml << buffer;
        length += bytesRead;
        if (length > MAX_SERVICE_STREAM_SIZE)
            break;
    }

    // tymczasowe listy zada�, kt�re b�d� por�wnywane z zapami�tanymi
    set<tstring>* inTasks = new set<tstring>;
    set<tstring>* outTasks = new set<tstring>;
    set<tstring>* intTasks = new set<tstring>;

    // podzia� odczytanego tekstu na wiersze
    tstring xml = ssxml.str();
    tistringstream isxml(xml);
    tstring s;
    while (getline(isxml, s))
    {
        if (s.find(TEXT("in:")) == 0)
            inTasks->insert(s);
        else if (s.find(TEXT("out:")) == 0)
            outTasks->insert(s);
        else if (s.find(TEXT("int:")) == 0)
            intTasks->insert(s);
    }

    // dla ka�dej z trzech list zada� (przychodz�cych, wychodz�cych i wewn�trznych)
    // wykonywane jest sprawdzenie, czy pojawi�y si� nowe zadania; je�eli tak, uaktualniane
    // jest odpowiednie pole struktury TASK_DATA (bNewIncomingTasks, ...)

    if (*inTasks != *incomingTasks)
    {
        // kopia najnowszej listy zada�
        set<tstring> copy(*inTasks);
        // usuwam z kopii zadania odczytane w poprzednim wywo�aniu
        for (set<tstring>::iterator iter=incomingTasks->begin(); iter != incomingTasks->end(); ++iter)
        {
            copy.erase(*iter);
        }
        // je�eli pojawi�y si� zadania, kt�rych nie by�o na zapami�tanej li�cie,
        // zapisuj� to w TASK_DATA
        if (copy.size() > 0)
        {
            pData->bHasNewIncomingTasks = TRUE;
            pData->iNewIncomingCount = copy.size();
        }

        delete incomingTasks;
        incomingTasks = inTasks;
    }
    else
    {
        delete inTasks;
    }

    if (*outTasks != *outgoingTasks)
    {
        set<tstring> copy(*outTasks);
        for (set<tstring>::iterator iter=outgoingTasks->begin(); iter != outgoingTasks->end(); ++iter)
        {
            copy.erase(*iter);
        }
        if (copy.size() > 0)
        {
            pData->bHasNewOutgoingTasks = TRUE;
            pData->iNewOutgoingCount = copy.size();
        }

        delete outgoingTasks;
        outgoingTasks = outTasks;
    }
    else
    {
        delete outTasks;
    }

    if (*intTasks != *internalTasks)
    {
        set<tstring> copy(*intTasks);
        for (set<tstring>::iterator iter=internalTasks->begin(); iter != internalTasks->end(); ++iter)
        {
            copy.erase(*iter);
        }
        if (copy.size() > 0)
        {
            pData->bHasNewInternalTasks = TRUE;
            pData->iNewInternalCount = copy.size();
        }
        delete internalTasks;
        internalTasks = intTasks;
    }
    else
    {
        delete intTasks;
    }

    pData->iIncomingCount = incomingTasks->size();
    pData->iOutgoingCount = outgoingTasks->size();
    pData->iInternalCount = internalTasks->size();


#ifdef DS_TRACE
    MsgBox(ssxml.str());
#endif

    pData->bHasTasks = pData->iIncomingCount > 0 || pData->iOutgoingCount > 0 || pData->iInternalCount > 0;

    InternetCloseHandle(hHandle);

    return TRUE;
}

/**
    Aktualizuje pola dwError i szErrorMessage przekazanej struktury TASK_DATA.
*/
void UpdateError(PTASK_DATA pData, const TCHAR* customMsg, DWORD error)
{
    if (customMsg != NULL)
    {
        tstring msg(customMsg);
        size_t count = msg.copy(pData->szErrorMessage, sizeof(pData->szErrorMessage)/sizeof(TCHAR) - 1, 0);
        pData->szErrorMessage[count] = '\0';
        pData->dwError = (error != ERROR_SUCCESS) ? error : GetLastError();
    }
    else
    {
        pData->dwError = GetLastError();
        pData->szErrorMessage[0] = '\0';

        FormatMessage(
            FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            GetLastError(),
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
            (LPTSTR) pData->szErrorMessage,
            sizeof(pData->szErrorMessage),
            NULL);
    }
}
