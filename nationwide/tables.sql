-- ddl.sequence() dodaje na ko�cu w�asny �rednik

CREATE TABLE DS_PREFS_NODE (
    ID          int identity not null PRIMARY KEY,
    PARENT_ID   int,
    NAME        varchar(80) NOT NULL,
    USERNAME    varchar(62)
);

ALTER TABLE DS_PREFS_NODE ADD CONSTRAINT DS_PREFS_NODE_FK FOREIGN KEY (PARENT_ID)
REFERENCES DS_PREFS_NODE (ID);

CREATE TABLE DS_PREFS_VALUE (
    NODE_ID      int NOT NULL,
    PREF_KEY     varchar(80) NOT NULL,
    PREF_VALUE   text NOT NULL,
    USERNAME     varchar(62)
);

ALTER TABLE DS_PREFS_VALUE ADD CONSTRAINT DS_PREFS_VALUE_FK FOREIGN KEY (NODE_ID)
REFERENCES DS_PREFS_NODE (ID) ON DELETE CASCADE;

-- brak wsparcia dla sekwencji (DS_PREFS_NODE_ID);

CREATE TABLE DS_DOCTYPE (
    ID              numeric(19,0) identity not null,
    HVERSION        int NOT NULL,
    NAME            varchar(100) NOT NULL ,
    TABLENAME       varchar(20) NOT NULL,
    ENABLED         smallint NOT NULL,
    CONTENT         image
);

ALTER TABLE DS_DOCTYPE ADD CONSTRAINT DS_DOCTYPE_PK PRIMARY KEY (ID);
-- brak wsparcia dla sekwencji (DS_DOCTYPE_ID);

CREATE TABLE DS_ATTACHMENT (
    ID           numeric(19,0) identity not null,
    TITLE        varchar(254) NOT NULL ,
    DESCRIPTION  varchar(510) NOT NULL ,
    CTIME        datetime,
    AUTHOR       varchar(62) NOT NULL ,
    DOCUMENT_ID  numeric(19,0),
    POSN         int,
    DELETED      smallint NOT NULL,
    BARCODE      varchar(25),
    lparam       varchar(200),
    wparam       numeric(19,0)
);

ALTER TABLE DS_ATTACHMENT ADD CONSTRAINT DS_ATTACHMENT_PKEY PRIMARY KEY (ID);

-- brak wsparcia dla sekwencji (DS_ATTACHMENT_ID);

CREATE TABLE DS_ATTACHMENT_REVISION (
    ID                numeric(19,0) identity not null,
    REVISION          int NOT NULL,
    CTIME             datetime NOT NULL,
    AUTHOR            varchar(62) NOT NULL,
    ATTACHMENT_ID     numeric(19,0),
    ORIGINALFILENAME  varchar(510),
    CONTENTSIZE       int,
    MIME              varchar(62),
    CONTENTDATA       image,
    lparam            varchar(200),
    wparam            numeric(19,0)
);

ALTER TABLE DS_ATTACHMENT_REVISION ADD CONSTRAINT DS_ATTACHMENT_REVISION_PKEY PRIMARY KEY (ID);
alter table ds_attachment_revision add constraint ds_attachment_revision_fk foreign key (attachment_id) references ds_attachment(id);

-- brak wsparcia dla sekwencji (DS_ATTACHMENT_REVISION_ID);

CREATE TABLE DS_FOLDER (
    ID         numeric(19,0) identity not null,
    TITLE      varchar(254),
    PARENT_ID  numeric(19,0),
    HVERSION   int,
    CTIME      datetime NOT NULL,
    AUTHOR     varchar(62) NOT NULL,
    SYSTEMNAME varchar(32),
    ATTRIBUTES int NOT NULL,
    lparam       varchar(200),
    wparam       numeric(19,0)
);

ALTER TABLE DS_FOLDER ADD CONSTRAINT DS_FOLDER_PKEY PRIMARY KEY (ID);
-- brak wsparcia dla sekwencji (DS_FOLDER_ID);

CREATE TABLE DS_DOCUMENT (
    ID            numeric(19,0) identity not null,
    TITLE         varchar(254) NOT NULL,
    DESCRIPTION   varchar(510) NOT NULL,
    CTIME         datetime NOT NULL,
    MTIME         datetime NOT NULL,
    AUTHOR        varchar(62) NOT NULL,
    EXTERNALID    varchar(126),
    HVERSION      int NOT NULL,
    FOLDER_ID     numeric(19,0),
    DOCTYPE       numeric(19,0),
    OFFICE        smallint NOT NULL,
    DELETED       smallint NOT NULL,
    lparam        varchar(200),
    wparam        numeric(19,0)
);

ALTER TABLE DS_DOCUMENT ADD CONSTRAINT DS_DOCUMENT_PKEY PRIMARY KEY (ID);
alter table ds_document add constraint ds_document_fk foreign key (folder_id) references ds_folder(id);
ALTER TABLE DS_DOCUMENT ADD CONSTRAINT DS_DOCTYPE_FK FOREIGN KEY (DOCTYPE) REFERENCES DS_DOCTYPE (ID);
alter table ds_attachment add constraint ds_attachment_fk foreign key (document_id) references ds_document(id);

-- brak wsparcia dla sekwencji (DS_DOCUMENT_ID);

-- wyj�tkowo bez polskiego sortowania po USERNAME, bo klucz pierwotny
-- nie mo�e by� wtedy za�o�ony
create table ds_document_lock (
    document_id     numeric(19,0) NOT NULL,
    username        varchar(62) NOT NULL ,
    expiration      datetime,
    lockmode        varchar(20) not null
);

    -- brak obs�ugi CHECK dla baz innych ni� Firebird
    alter table ds_document_lock add constraint ds_document_lock_pk primary key (document_id, username, lockmode);


create table ds_document_watch (
    id              numeric(19,0) identity not null,
    document_id     numeric(19,0) NOT NULL,
    username        varchar(62) NOT NULL,
    watch_type      int not null
);

-- brak wsparcia dla sekwencji (DS_DOCUMENT_WATCH_ID);

create table ds_document_permission (
    document_id     numeric(19,0) NOT NULL,
    SUBJECT         varchar(62) NOT NULL,
    SUBJECTTYPE     varchar(14) NOT NULL,
    NAME            varchar(14) NOT NULL,
    NEGATIVE        smallint NOT NULL
);

create unique index ds_document_permission_pk on ds_document_permission (document_id, subject, subjecttype, name);
alter table ds_document_permission add constraint ds_document_permission_fk foreign key (document_id) references ds_document
on delete cascade;

create table ds_folder_permission (
    folder_id       numeric(19,0) NOT NULL,
    SUBJECT         varchar(62) NOT NULL,
    SUBJECTTYPE     varchar(14) NOT NULL,
    NAME            varchar(14) NOT NULL,
    NEGATIVE        smallint NOT NULL
);

create unique index ds_folder_permission_pk on ds_folder_permission (folder_id, subject, subjecttype, name);
alter table ds_folder_permission add constraint ds_folder_permission_fk foreign key (folder_id) references ds_folder
on delete cascade;


CREATE TABLE DS_REPORT (
    ID              numeric(19,0) identity not null,
    REPORTID        varchar(20) NOT NULL,
    VERSION         varchar(10) NOT NULL,
    TITLE           varchar(254) NOT NULL,
    CTIME           datetime NOT NULL,
    AUTHOR          varchar(62) NOT NULL,
    STATUS          varchar(20) NOT NULL,
    CONTENTDATA     image,
    LPARAM          varchar(200),
    WPARAM          numeric(19,0)
);

ALTER TABLE DS_REPORT ADD CONSTRAINT DS_REPORT_PK PRIMARY KEY (ID);
-- brak wsparcia dla sekwencji (DS_REPORT_ID);

CREATE TABLE DS_USER (
    ID              numeric(19,0) identity not null,
    NAME            varchar(62) NOT NULL,
    FIRSTNAME       varchar(62),
    LASTNAME        varchar(62),
    EMAIL           varchar(62),
    IDENTIFIER      varchar(20),
    LOGINDISABLED   smallint NOT NULL,
    USERROLES       varchar(80),
    LASTSUCCESSFULLOGIN         datetime,
    LASTUNSUCCESSFULLOGIN       datetime,
    SUBSTITUTEDFROM             datetime,
    SUBSTITUTEDTHROUGH          datetime,
    DELETED                     smallint NOT NULL,
    CERTIFICATELOGIN            smallint NOT NULL,
    LASTPASSWORDCHANGE          datetime,
    HASHEDPASSWORD  varchar(62),
    SUPERVISOR_ID   numeric(19,0),
    SUBSTITUTE_ID   numeric(19,0),
    SYSTEMUSER      smallint NOT NULL
);

ALTER TABLE DS_USER ADD CONSTRAINT DS_USER_PK PRIMARY KEY (ID);
CREATE UNIQUE INDEX DS_USER_NAME ON DS_USER (NAME);

-- brak wsparcia dla sekwencji (DS_USER_ID);

CREATE TABLE DS_USER_CERT (
    USER_ID         numeric(19,0) NOT NULL,
    POSN            smallint,
    CERTDATA        text,
    SIGNATURE       varchar(2048) NOT NULL
);
ALTER TABLE DS_USER_CERT ADD CONSTRAINT DS_USER_CERT_FK FOREIGN KEY (USER_ID)
REFERENCES DS_USER (ID);

CREATE TABLE DS_USER_CERTIFICATE (
    ID              numeric(19,0) identity not null,
    USERNAME        varchar(62) NOT NULL,
    ALIAS           varchar(70),
    ENCODEDCSR      text,
    ENCODEDCRT      text,
    CTIME           datetime NOT NULL,
    SIGNINGTIME     datetime,
    SIGNED          smallint NOT NULL
);

ALTER TABLE DS_USER_CERTIFICATE ADD CONSTRAINT DS_USER_CERTIFICATE_PK PRIMARY KEY (ID);

-- brak wsparcia dla sekwencji (DS_USER_CERTIFICATE_ID);


CREATE TABLE DS_DIVISION (
    ID              numeric(19,0) identity not null,
    GUID            varchar(62) NOT NULL,
    NAME            varchar(62) NOT NULL,
    CODE            varchar(20),
    DIVISIONTYPE    varchar(20) NOT NULL,
    PARENT_ID       numeric(19,0)
);

ALTER TABLE DS_DIVISION ADD CONSTRAINT DS_DIVISION_PK PRIMARY KEY (ID);
CREATE UNIQUE INDEX DS_DIVISION_GUID ON DS_DIVISION (GUID);
CREATE UNIQUE INDEX DS_DIVISION_NAME ON DS_DIVISION (PARENT_ID, NAME);

-- brak wsparcia dla sekwencji (DS_DIVISION_ID);

CREATE TABLE DS_USER_TO_DIVISION (
    USER_ID         numeric(19,0) NOT NULL,
    DIVISION_ID     numeric(19,0) NOT NULL
);

ALTER TABLE DS_USER_TO_DIVISION ADD CONSTRAINT DS_USER_TO_DIVISION_U FOREIGN KEY (USER_ID) REFERENCES DS_USER (ID);
ALTER TABLE DS_USER_TO_DIVISION ADD CONSTRAINT DS_USER_TO_DIVISION_D FOREIGN KEY (DIVISION_ID) REFERENCES DS_DIVISION (ID);
CREATE UNIQUE INDEX DS_USER_TO_DIVISION_UNIQ ON DS_USER_TO_DIVISION (USER_ID, DIVISION_ID);
