/**
    @param hmField - tablica warto�ci p�l, jakie zostan� zaproponowane w formularzu
 */
function execute(document, doctype, hmField)
{
    try
    {
        field = doctype.getFieldByCn('DATA_WPLYWU');
        if (hmField.get(field.getId()) == null)
        {
            hmField.put(field.getId(), document.getIncomingDate()); //document.getDocumentDate());
        }

        field = doctype.getFieldByCn('NR_POLISY');
        if (hmField.get(field.getId()) == null)
        {
            hmField.put(field.getId(), document.getTitle());
        }

        field = doctype.getFieldByCn('STATUS');
        if (hmField.get(field.getId()) == null)
        {
            hmField.put(field.getId(), field.getEnumItemByCn('PRZYJETY').getId());
        }
    }
    catch (x)
    {
        //throw x;
    }
}
