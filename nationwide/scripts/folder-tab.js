importPackage (Packages.java.lang);

function getFolderName(document, doctype, params)
{
	if (doctype == null)
    {
        System.out.println('Przekazane doctype: '+doctype);
		return null;
    }
   
	try
	{
		var f = doctype.getFieldByCn('NR_POLISY');
        var nr = params.get(f.getId());
        var path;
        if (nr != null)
        {
            nr = nr.toUpperCase();
            document.setTitle(nr);
            if (nr.length() > 7)
            {
                return nr.substring(0, 3) + '/' + nr.substring(0, 7) + '/' + nr;
            }
            else if (nr.length() > 3)
            {
                return nr.substring(0, 3) + '/' + nr;
            }
            else
            {
                return nr;
            }
        }
        else
        {
            System.out.println('Przekazany nr polisy: '+nr);
        }
	}
	catch (x)
	{
		System.out.println('Nie istnieje pole NR_POLISY: '+x+' (doctype='+doctype+')');
	}

    return null;
}
