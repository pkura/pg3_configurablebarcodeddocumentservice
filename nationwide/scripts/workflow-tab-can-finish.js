importPackage(java.lang);

function execute(document)
{
    try
    {
        // dokument musi by� zarchiwizowany, aby mo�na
        // by�o zako�czy� prac� z workflow
        return !document.getFolder().isSystem('office') &&
                !document.getFolder().isSystem('businessarchive')
    }
    catch (x) // FolderNotFoundException
    {
        throw x;
    }
}
