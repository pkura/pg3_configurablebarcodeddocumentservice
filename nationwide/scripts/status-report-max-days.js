// zwraca maksymaln� liczb� dni, przez kt�r� dokument o danym rodzaju
// mo�e przebywa� w danym statusie
// 0 oznacza, �e nie ma ogranicze�

/*
    @param rodzajCN - CN rodzaju dokumentu
    @param rodzajCLASS - klasa rodzaju dokumentu ("OGOLNE", "SWIADCZENIOWE", "RAPORTY")
    @param statusCN - CN statusu dokumentu; je�eli null, funkcja powinna zwr�ci� maksymalny
        czas przetwarzania dokumentu o przekazanym rodzaju i klasie
*/
function getMaxDays(rodzajCN, rodzajCLASS, statusCN)
{
    // nie okre�lono statusu, wi�c chodzi o maksymalny czas przetwarzania
    // dokumentu
    if (statusCN == null)
    {
        return 2;
    }
    // maksymalny czas przebywania dokumentu w przekazanym statusie
    else
    {
        switch (rodzajCN)
        {
            case 'AKT_ZGONU': return doc1(statusCN);
            case 'ANEKS_POLISY_GRUPOWEJ': return doc2(statusCN);
            // nierozpoznany typ dokumentu
            // default: return doc1(statusCN);
            default: return 1;
        }
    }
}

// jaki� typ dokumentu
function doc1(statusCN)
{
    switch (statusCN)
    {
        case 'W_REALIZACJI': return 2; // w realizacji
        default: return 0;
    }
}

function doc2(status)
{
    switch (status)
    {
        case 'W_REALIZACJI': return 3; // w realizacji
        default: return 0;
    }
}
