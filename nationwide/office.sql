-- kancelaria


create table dso_rwa (
    ID              int identity not null,
    digit1          char(1) not null,
    digit2          char(1),
    digit3          char(1),
    digit4          char(1),
    digit5          char(1),
    rwalevel        int not null,
    achome          varchar(5),
    acother         varchar(5),
    description     varchar(300) NOT NULL,
    remarks         varchar(512) ,
    lparam          varchar(200),
    wparam          numeric(19,0)
);

alter table dso_rwa add constraint dso_rwa_pk primary key (id);
-- brak wsparcia dla sekwencji (DSO_RWA_ID);

CREATE TABLE DSO_IN_DOCUMENT (
    ID                     numeric(19,0) not null PRIMARY KEY,
    DOCUMENTDATE           datetime,
    STAMPDATE              datetime,
    INCOMINGDATE           datetime,
    POSTALREGNUMBER        varchar(20),
    SENDER                 numeric(19,0),
    NUMATTACHMENTS         int,
    PACKAGEREMARKS         varchar(240) ,
    OTHERREMARKS           varchar(240) ,
    TRACKINGNUMBER         varchar(20),
    trackingpassword        varchar(20),
    SUBMITTOBIP            smallint,
    CASE_ID                numeric(19,0),
    CASEDOCUMENTID         varchar(84),
    LOCATION               varchar(60),
    KIND                   int,
    clerk                  varchar(62),
    ASSIGNEDDIVISION       varchar(62),
    CREATINGUSER           varchar(62) NOT NULL,
    STATUS                 int,
    OFFICENUMBER           int,
    OFFICENUMBERYEAR       int,
    BARCODE                 varchar(25),
    officenumbersymbol      varchar(20),
    referenceid             varchar(60),
    master_document_id      numeric(19,0),
    cancelled               smallint not null,
    cancellationreason      varchar(80),
    cancelledbyusername     varchar(62),
    currentassignmentguid   varchar(62),
    currentassignmentusername varchar(62),
    currentassignmentaccepted smallint,
    delivery                int,
    outgoingdelivery        int,
    summary                 varchar(80) NOT NULL,
    archived                smallint not null,
    journal_id              numeric(19,0),
    bok                     smallint not null
);


CREATE TABLE DSO_OUT_DOCUMENT (
    ID                      numeric(19,0) not null PRIMARY KEY,
    OFFICENUMBER            int,
    officenumbersymbol      varchar(20),
    OFFICENUMBERYEAR        int,
    DOCUMENTDATE            datetime,
    POSTALREGNUMBER         varchar(20),
    clerk                   varchar(62),
    ASSIGNEDDIVISION        varchar(62),
    CREATINGUSER            varchar(62) NOT NULL,
    INDOCUMENT              numeric(19,0),
    CASEDOCUMENTID          varchar(84),
    CASE_ID                 numeric(19,0),
    SENDER                  numeric(19,0),
    ZPODATE                 datetime,
    BARCODE                 varchar(25),
    cancelled               smallint not null,
    cancellationreason      varchar(80),
    prepstate               varchar(15),
    cancelledbyusername     varchar(62),
    currentassignmentguid   varchar(62),
    currentassignmentusername varchar(62),
    currentassignmentaccepted smallint,
    delivery                int,
    returnreason            int,
    returned                smallint not null,
    summary                 varchar(80) NOT NULL,
    archived                smallint not null,
    internaldocument        smallint not null,
    journal_id              numeric(19,0)
);

CREATE TABLE DSO_IN_DOCUMENT_STATUS (
    ID                      int identity not null PRIMARY KEY,
    POSN                    smallint NOT NULL,
    NAME                    varchar(40) NOT NULL
);

alter table dso_in_document add constraint dso_in_document_status_fk foreign key (status) references dso_in_document_status (id);

-- brak wsparcia dla sekwencji (DSO_IN_DOCUMENT_STATUS_ID);

CREATE TABLE DSO_IN_DOCUMENT_KIND (
    ID                      int identity not null PRIMARY KEY,
    POSN                    smallint NOT NULL,
    NAME                    varchar(40) NOT NULL,
    DAYS                    smallint
);

alter table dso_in_document add constraint dso_in_document_kind_fk foreign key (kind) references dso_in_document_kind (id);

-- brak wsparcia dla sekwencji (dso_in_document_kind_id);

create table DSO_IN_DOCUMENT_KIND_REQ_ATT (
	kind_id		int NOT NULL,
	posn		int,
	title		varchar(80) NOT NULL,
	sid		    varchar(32)
);
ALTER table DSO_IN_DOCUMENT_KIND_REQ_ATT ADD CONSTRAINT DSO_IN_DOC_KIND_REQ_ATT_FK FOREIGN KEY (KIND_ID) REFERENCES DSO_IN_DOCUMENT_KIND (ID);

create table dso_in_document_delivery (
    ID                      int identity not null PRIMARY KEY,
    POSN                    smallint NOT NULL,
    NAME                    varchar(40) NOT NULL
);

alter table dso_in_document add constraint dso_in_document_delivery_fk foreign key (delivery) references dso_in_document_delivery (id);
-- brak wsparcia dla sekwencji (dso_in_document_delivery_id);

create table dso_out_document_delivery (
    ID                      int identity not null PRIMARY KEY,
    POSN                    smallint NOT NULL,
    NAME                    varchar(40) NOT NULL
);

alter table dso_out_document add constraint dso_out_document_delivery_fk foreign key (delivery) references dso_out_document_delivery (id);
alter table dso_in_document add constraint dso_in_document_outdelivery_fk foreign key (outgoingdelivery) references dso_out_document_delivery (id);
-- brak wsparcia dla sekwencji (DSO_OUT_DOCUMENT_DELIVERY_ID);

create table dso_out_document_retreason (
    ID                      int identity not null PRIMARY KEY,
    POSN                    smallint NOT NULL,
    NAME                    varchar(40) NOT NULL
);

alter table dso_out_document add constraint dso_out_document_retreason_fk foreign key (returnreason) references dso_out_document_retreason (id);
-- brak wsparcia dla sekwencji (dso_out_document_retreason_id);

create table dso_document_audit (
    document_id         numeric(19,0) NOT NULL,
    posn                int NOT NULL,
    ctime               datetime NOT NULL,
    property            varchar(62) ,
    username            varchar(62) NOT NULL,
    description         varchar(200) NOT NULL,
    lparam              varchar(200),
    wparam              numeric(19,0)
);

alter table dso_document_audit add constraint dso_document_audit_fk1 foreign key (document_id) references ds_document (id);


create table dso_document_asgn_history (
    document_id         numeric(19,0) NOT NULL,
    posn                int NOT NULL,
    ctime               datetime NOT NULL,
    sourceuser          varchar(62) NOT NULL,
    targetuser          varchar(62),
    sourceguid          varchar(126) NOT NULL,
    targetguid          varchar(126) NOT NULL,
    objective           varchar(128) NOT NULL,
    accepted            smallint not null
);

alter table dso_document_asgn_history add constraint dso_document_asgn_history_fk foreign key (document_id) references ds_document(id);


CREATE TABLE DSO_DOCUMENT_REMARKS (
    DOCUMENT_ID  numeric(19,0) NOT NULL,
    POSN         int,
    CTIME        datetime,
    CONTENT      varchar(4000) NOT NULL,
    AUTHOR       varchar(62) NOT NULL
);

alter table DSO_DOCUMENT_REMARKS add constraint DSO_DOCUMENT_REMARKS_FK foreign key (document_id) references ds_document (id);


CREATE TABLE DSO_JOURNAL (
    ID           numeric(19,0) identity not null,
    HVERSION     int NOT NULL,
    DESCRIPTION  varchar(510) NOT NULL,
    OWNERGUID    varchar(126),
    JOURNALTYPE  varchar(62) NOT NULL,
    SEQUENCEID   int NOT NULL,
    SYMBOL       varchar(20),
    AUTHOR       varchar(62) NOT NULL,
    CTIME        datetime,
    archived      smallint not null,
    closed        smallint not null,
    cyear        int not null,
    lparam        varchar(200),
    wparam        numeric(19,0)
);

ALTER TABLE DSO_JOURNAL ADD CONSTRAINT DSO_JOURNAL_PKEY PRIMARY KEY (ID);
-- brak wsparcia dla sekwencji (DSO_JOURNAL_ID);

CREATE TABLE DSO_JOURNAL_ENTRY (
    DOCUMENTID  numeric(19,0) NOT NULL,
    SEQUENCEID  int NOT NULL,
    CTIME       datetime NOT NULL,
    entrydate   datetime not null,
    JOURNAL_ID  numeric(19,0) NOT NULL
);

create unique index DSO_JOURNAL_ENTRY_IDX1 on DSO_JOURNAL_ENTRY (DOCUMENTID,JOURNAL_ID);
alter table dso_journal_entry add constraint dso_journal_entry_fk foreign key (journal_id) references dso_journal(id);


CREATE TABLE DSO_PERSON (
    ID              numeric(19,0) identity not null,
    POSN            int,
    DISCRIMINATOR   varchar(20) NOT NULL,
    DOCUMENT_ID     numeric(19,0),
    DICTIONARYTYPE  varchar(10),
    ANONYMOUS       smallint NOT NULL,
    TITLE           varchar(30),
    FIRSTNAME       varchar(50),
    LASTNAME        varchar(50),
    ORGANIZATION    varchar(50),
    ORGANIZATIONDIVISION    varchar(50),
    STREET          varchar(50),
    ZIP             varchar(14),
    LOCATION        varchar(50),
    PESEL           varchar(11),
    NIP             varchar(15),
    REGON           varchar(15),
    COUNTRY         varchar(2),
    EMAIL           varchar(50),
    FAX             varchar(30),
    REMARKS         varchar(200),
    lparam          varchar(200),
    wparam          numeric(19,0)
);
alter table dso_person add constraint dso_person_pk primary key (id);
-- brak wsparcia dla sekwencji (DSO_PERSON_ID);

create table dso_role_usernames (
    role_id         numeric(19,0) not null,
    username        varchar(255) );

create table dso_role_divisions (
    role_id         numeric(19,0) not null,
    division_guid   varchar(255) );

create table dso_role_permissions (
    role_id         numeric(19,0) not null,
    permission_name varchar(255) );

create table dso_role (
    id      numeric(19,0) identity not null,
    name    varchar(126) NOT NULL,
    primary key (id));

alter table dso_role_usernames add constraint dso_role_usernames_fk foreign key (role_id) references dso_role (id);
alter table dso_role_divisions add constraint dso_role_divisions_fk foreign key (role_id) references dso_role (id);
alter table dso_role_permissions add constraint dso_role_permissions_fk foreign key (role_id) references dso_role (id);
-- brak wsparcia dla sekwencji (dso_role_id);

create table dso_case_status (
    id      int identity not null,
    posn    smallint NOT NULL,
    name    varchar(80) NOT NULL
);

alter table dso_case_status add constraint dso_case_status_pkey primary key (id);

create table dso_case_priority (
    id      int identity not null,
    posn    smallint NOT NULL,
    name    varchar(80) NOT NULL
);

-- brak wsparcia dla sekwencji (dso_case_priority_id);

alter table dso_case_priority add constraint dso_case_priority_pk primary key (id);


CREATE TABLE DSO_CONTAINER (
    -- wspolne pola sprawy i teczki
    ID              numeric(19,0) identity not null,
    PARENT_ID       numeric(19,0),
    DISCRIMINATOR   varchar(20) NOT NULL,
    OFFICEID        varchar(84) NOT NULL,
    OFFICEIDPREFIX  varchar(17),
    OFFICEIDMIDDLE  varchar(50) NOT NULL,
    OFFICEIDSUFFIX  varchar(17),
    DIVISIONGUID    varchar(128) NOT NULL ,
    CLERK           varchar(62) NOT NULL,
    AUTHOR          varchar(62) NOT NULL,
    CTIME           datetime NOT NULL,
    SEQUENCEID      int NOT NULL,
    ARCHIVED        smallint NOT NULL,
    RWA_ID          int NOT NULL,
    LPARAM          varchar(200),
    WPARAM          numeric(19,0),

    -- teczka
    NAME            varchar(128),
    BARCODE         varchar(25),

    -- sprawa
    CLOSED          smallint,
    CASE_YEAR       int,
    OPENDATE        datetime,
    FINISHDATE      datetime,
    DOCSEQUENCEID   int,
    TRACKINGNUMBER  varchar(20),
    TRACKINGPASSWORD varchar(20),
    PRIORITY_ID     int,
    STATUS_ID       int,
    PRECEDENT_ID    numeric(19,0)
);

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PK PRIMARY KEY (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PARENT FOREIGN KEY (PARENT_ID)
    REFERENCES DSO_CONTAINER (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_RWA FOREIGN KEY (RWA_ID)
    REFERENCES DSO_RWA (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PRIORITY FOREIGN KEY (PRIORITY_ID)
    REFERENCES DSO_CASE_PRIORITY (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_STATUS FOREIGN KEY (STATUS_ID)
    REFERENCES DSO_CASE_STATUS (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PRECEDENT FOREIGN KEY (PRECEDENT_ID)
    REFERENCES DSO_CONTAINER (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_POSITIVE CHECK (SEQUENCEID > 0);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_DISCRIMINATOR
    CHECK (DISCRIMINATOR = 'CASE' OR DISCRIMINATOR = 'FOLDER');

CREATE UNIQUE INDEX DSO_CONTAINER_SEQ ON DSO_CONTAINER (PARENT_ID, SEQUENCEID);
CREATE UNIQUE INDEX DSO_CONTAINER_OID ON DSO_CONTAINER (OFFICEID);

-- brak wsparcia dla sekwencji (DSO_CONTAINER_ID);

create table DSO_CONTAINER_AUDIT (
    CONTAINER_ID        numeric(19,0) NOT NULL,
    posn                int NOT NULL,
    ctime               datetime NOT NULL,
    property            varchar(62) ,
    username            varchar(62) NOT NULL,
    description         varchar(200) NOT NULL,
    lparam              varchar(200),
    wparam              numeric(19,0)
);

alter table DSO_CONTAINER_AUDIT add constraint DSO_CONTAINER_AUDIT_FK1 foreign key (container_id) references DSO_CONTAINER (id);

CREATE TABLE DSO_CONTAINER_REMARKS (
    CONTAINER_ID numeric(19,0) NOT NULL,
    POSN         int NOT NULL,
    CTIME        datetime NOT NULL,
    CONTENT      text NOT NULL,
    AUTHOR       varchar(62) NOT NULL
);

ALTER TABLE DSO_CONTAINER_REMARKS ADD CONSTRAINT DSO_CONTAINER_REMARKS_FK1 FOREIGN KEY (CONTAINER_ID) REFERENCES DSO_CONTAINER (ID);



create table dso_collective_assignment (
    id                  int identity not null,
    objective           varchar(100) NOT NULL,
    wfprocess           varchar(50)  NOT NULL
);

alter table dso_collective_assignment add constraint dso_collective_assignment_pk primary key (id);

-- brak wsparcia dla sekwencji (dso_collective_assignment_id);

create table dso_assignment_objective (
    ID                      int identity not null PRIMARY KEY,
    POSN                    smallint NOT NULL,
    NAME                    varchar(80) NOT NULL,
    OFFICESTATUS            varchar(20) NOT NULL,
    DOCUMENTTYPE            varchar(10) NOT NULL
);

-- brak wsparcia dla sekwencji (dso_assignment_objective_id);
-- brak wsparcia dla sekwencji (DSW_RESOURCE_ID);
-- brak wsparcia dla sekwencji (DSW_PROCESS_ID);
-- brak wsparcia dla sekwencji (DSW_ASSIGNMENT_ID);
-- brak wsparcia dla sekwencji (DSW_ACTIVITY_ID);
-- brak wsparcia dla sekwencji (DSW_DATATYPE_ID);
-- brak wsparcia dla sekwencji (DSW_PACKAGE_ID);

create table dsw_package (
    id          numeric(19,0) identity not null,
    packageid   varchar(100) not null,
    hashid      varchar(100) not null,
    xmlcontent  text
);
alter table dsw_package add constraint dsw_package_pk primary key (id);

create table dsw_resource (
    id          numeric(19,0) identity not null,
    res_key     varchar(62) NOT NULL,
    res_name    varchar(62),
    email       varchar(62)
);
alter table dsw_resource add constraint dsw_resource_pk primary key (id);
create unique index dsw_resource_key on dsw_resource (res_key);

create table dsw_participant_mapping (
    participant_id  varchar(60) NOT NULL,
    resource_id     numeric(19,0) not null
);
create unique index dsw_mapping_unique on dsw_participant_mapping (participant_id, resource_id);
alter table dsw_participant_mapping add constraint dsw_participant_mapping_res_fk foreign key (resource_id) references dsw_resource(id);

create table dsw_process (
    id          numeric(19,0) identity not null,
    name        varchar(200) NOT NULL,
    description varchar(200),
    state       varchar(30) not null,
    laststatetime datetime NOT NULL,
    process_key varchar(50) not null,
    packageid   varchar(50) not null,
    package_id  numeric(19,0) NOT NULL,
    processdefinitionid varchar(50) not null
);
alter table dsw_process add constraint dsw_process_pk primary key (id);
alter table dsw_process add constraint dsw_process_package_fk foreign key (package_id) references dsw_package(id);

create table dsw_activity (
    id          numeric(19,0) identity not null,
    state       varchar(30) not null,
    laststatetime datetime NOT NULL,
    activitydefinitionid varchar(60) not null,
    activity_key varchar(30) not null,
    name        varchar(200) NOT NULL,
    description varchar(200),
    process_id  numeric(19,0) NOT NULL
);
alter table dsw_activity add constraint dsw_activity_pk primary key (id);
alter table dsw_activity add constraint dsw_activity_fk foreign key (process_id) references dsw_process (id);

create table dsw_assignment (
    id          numeric(19,0) identity not null,
    accepted    smallint not null,
    activity_id numeric(19,0) NOT NULL,
    resource_id numeric(19,0) NOT NULL
);
alter table dsw_assignment add constraint dsw_assignment_pk primary key (id);
alter table dsw_assignment add constraint dsw_assignment_activity_fk foreign key (activity_id) references dsw_activity(id);
alter table dsw_assignment add constraint dsw_assignment_resource_fk foreign key (resource_id) references dsw_resource(id);

create table dsw_process_activities (
    process_id  numeric(19,0) NOT NULL,
    posn        int,
    activity_id numeric(19,0) NOT NULL
);
alter table dsw_process_activities add constraint dsw_process_activities_proc_fk foreign key (process_id) references dsw_process(id);
alter table dsw_process_activities add constraint dsw_process_activities_act_fk foreign key (activity_id) references dsw_activity(id);

create table dsw_datatype (
    id          numeric(19,0) identity not null,
    typename    varchar(30) not null,
    basictype   varchar(30),
    lowerindex  int,
    upperindex  int,
    membertype  numeric(19,0)
);
alter table dsw_datatype add constraint dsw_datatype_pk primary key (id);

create table dsw_datatype_enum_values (
    datatype_id numeric(19,0) NOT NULL,
    posn        int,
    enum_value  varchar(100)  NOT NULL
);
alter table dsw_datatype_enum_values add constraint dsw_datatype_enum_values_fk foreign key (datatype_id) references dsw_datatype(id);

create table dsw_process_context (
    process_id      numeric(19,0) NOT NULL,
    param_name      varchar(50) NOT NULL,
    param_value     varchar(2048)  NOT NULL,
    datatype_id     numeric(19,0) NOT NULL
);
alter table dsw_process_context add constraint dsw_process_context_fk foreign key (process_id) references dsw_process(id);
alter table dsw_process_context add constraint dsw_process_context_dt_fk foreign key (datatype_id) references dsw_datatype(id);

create table dsw_activity_context (
    activity_id     numeric(19,0) NOT NULL,
    param_name      varchar(50) NOT NULL,
    param_value     varchar(2048)  NOT NULL,
    datatype_id     numeric(19,0) NOT NULL
);
alter table dsw_activity_context add constraint dsw_activity_context_fk foreign key (activity_id) references dsw_activity(id);
alter table dsw_activity_context add constraint dsw_activity_context_dt_fk foreign key (datatype_id) references dsw_datatype(id);

