Dictionary = {
	getFieldsValues: function(dictionaryName, id, obj) {
		console.info('Dictionary.getFieldsValues', dictionaryName, id);

		setTimeout(function() {
			obj.callback(DictionaryValues[dictionaryName]);
		}, 1);
	},

	getFieldValue: function(dictionaryName, fieldCn, obj) {
		setTimeout(function() {
			obj.callback(FullDictionaryValues[dictionaryName][fieldCn]);
		}, 1);
	},

	getFieldsValuesList: function(dictionaryName, ids, obj) {
		console.info('Dictionary.getFieldsValuesList', dictionaryName, ids, DictionaryValuesList[dictionaryName]);

		setTimeout(function() {
			var ret = DictionaryValuesList[dictionaryName];
			
			if(ids == null || ids.length == 0 || ids[0] == '') {
				ret = null;
			} 

			if(dictionaryName == 'STAWKI_VAT' && DWR_STAWKI_VAT_ID_LIST.length == 0) {
				ret = null;
			}
			
			obj.callback(ret);
		}, 1);
	},

	getMultipleFieldsValues: function(dwrParams, dictionaryName, values, obj) {
		console.info('Dictionary.getMultipleFieldsValues', dictionaryName, MultipleFieldsValues[dictionaryName]);
		setTimeout(function() {
			obj.callback(MultipleFieldsValues[dictionaryName]);
		}, 1);
	},

	dwrSearch: function(dictionaryName, vals, pageSize, offset, obj) {
		console.info('dwrSearch', dictionaryName, vals, pageSize, offset);

		var id = Number(BaseField.unpackValues(vals[dictionaryName + '_ID']));
		var dicValues = DictionaryValuesList[dictionaryName];
		var found = [];

		for(var i = 0; i < dicValues.length; i ++) {
			if(dicValues[i][dictionaryName + '_ID'] === id) {
				var dicVal = dicValues[i];
				var newObj = {};
				for(var cn in dicVal) {
					if(dicVal[cn].selectedOptions) {
						newObj[cn] = Number(dicVal[cn].selectedOptions[0]);
					} else {
						newObj[cn] = dicVal[cn];
					}
				}
				newObj.id = id;
				found.push(newObj);
				break;
			}
		}
		console.info(found);

		setTimeout(function() {
			obj(found);
		}, 1);
	},

	dwrAdd: function(dictionaryName, vals, obj) {
		console.info('dwrAdd', dictionaryName, vals);
		tmp100 = vals;
		var lastId = Number(DWR_DEKRETACJA_KSIEG_ID_LIST[DWR_DEKRETACJA_KSIEG_ID_LIST.length-1]);
		lastId ++;
		DWR_DEKRETACJA_KSIEG_ID_LIST.push(lastId);

		var newVal = {};
		for(var fieldCn in vals) {
			var fieldVal = vals[fieldCn];
			newVal[fieldCn] = BaseField.unpackValues(fieldVal);
		}
		newVal[dictionaryName + '_ID'] = lastId;
		DictionaryValuesList[dictionaryName].push(newVal);

		setTimeout(function() {
			obj(lastId);
		}, 1);
	}
}

FullDictionaryValues = {
	'DEKRETACJA_KSIEG':
	{
		"DEKRETACJA_KSIEG_CUSTOMSAGENCYID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ITEMNO": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_LOKALIZACJA": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ACCEPTINGCENTRUMID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ANALYTICS_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_CONNECTED_TYPE_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ACCOUNTNUMBER": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_SUBGROUPID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_CLASS_TYPE_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_CENTRUMID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		}
	}
};

DictionaryValues = {
	'DEKRETACJA_KSIEG':
	{
		"DEKRETACJA_KSIEG_CUSTOMSAGENCYID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ITEMNO": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_LOKALIZACJA": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ACCEPTINGCENTRUMID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ANALYTICS_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_CONNECTED_TYPE_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_ACCOUNTNUMBER": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_SUBGROUPID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_CLASS_TYPE_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"DEKRETACJA_KSIEG_CENTRUMID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		}
	},

	'STAWKI_VAT':
	{
		"STAWKI_VAT_SUBGROUPID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_CUSTOMSAGENCYID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_LOKALIZACJA": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_CENTRUMID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_CONNECTED_TYPE_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_CLASS_TYPE_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_ACCEPTINGCENTRUMID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_ANALYTICS_ID": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_ITEMNO": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		},
		"STAWKI_VAT_ACCOUNTNUMBER": {
			"allOptions": [{
				"": "-- wybierz --"
			},
			{
				"1": "Jeden"
			}],
			"selectedId": "",
			"selectedOptions": [""],
			"selectedValue": ""
		}
	}
};

DictionaryValuesList = {
	DEKRETACJA_KSIEG: 
	[{
	"id": "1",
	"DEKRETACJA_KSIEG_ID": 1,
	"DEKRETACJA_KSIEG_CONNECTED_TYPE_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_CENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_ACCOUNTNUMBER": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_ACCEPTINGCENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_LOKALIZACJA": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_ITEMNO": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_CLASS_TYPE_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_SUBGROUPID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_CUSTOMSAGENCYID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_AMOUNT": 1,
	"DEKRETACJA_KSIEG_AMOUNTUSED": 0,
	"DEKRETACJA_KSIEG_GROUPID": 0,
	"DEKRETACJA_KSIEG_ANALYTICS_ID": {
		"allOptions": [{
			"": "--wybierz--"
		},
		{
			"0": "NIE DOTYCZY"
		}],
		"selectedId": "0",
		"selectedOptions": ["0"],
		"selectedValue": "NIE DOTYCZY"
	}
},
{
	"id": "2",
	"DEKRETACJA_KSIEG_ID": 2,
	"DEKRETACJA_KSIEG_CONNECTED_TYPE_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_CENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_ACCOUNTNUMBER": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_ACCEPTINGCENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_LOKALIZACJA": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_ITEMNO": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_CLASS_TYPE_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_SUBGROUPID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_CUSTOMSAGENCYID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	},
	"DEKRETACJA_KSIEG_AMOUNT": 200,
	"DEKRETACJA_KSIEG_AMOUNTUSED": 123,
	"DEKRETACJA_KSIEG_GROUPID": 1,
	"DEKRETACJA_KSIEG_ANALYTICS_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"2": "Dwa"
		}],
		"selectedId": "2",
		"selectedOptions": ["2"],
		"selectedValue": "Dwa"
	}
	}],

	'STAWKI_VAT':
	[
	{
	"id": "1",
	"STAWKI_VAT_ID": 1,			
	"STAWKI_VAT_SUBGROUPID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_CUSTOMSAGENCYID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_LOKALIZACJA": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_CENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_CONNECTED_TYPE_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_CLASS_TYPE_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_ACCEPTINGCENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_ANALYTICS_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_ITEMNO": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	},
	"STAWKI_VAT_ACCOUNTNUMBER": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Stawka jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Stawka jeden"
	}
	}
	]
};

MultipleFieldsValues = {
	'DEKRETACJA_KSIEG':
	{
	"DEKRETACJA_KSIEG_AMOUNTUSED": 0,
	"DEKRETACJA_KSIEG_ITEMNO": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_LOKALIZACJA": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_ACCEPTINGCENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_ANALYTICS_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_ACCOUNTNUMBER": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_AMOUNT": 0,
	"DEKRETACJA_KSIEG_SUBGROUPID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_CLASS_TYPE_ID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_CENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	},
	"DEKRETACJA_KSIEG_CUSTOMSAGENCYID": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Jeden"
		}],
		"selectedId": "1",
		"selectedOptions": ["1"],
		"selectedValue": "Jeden"
	}	
}
}