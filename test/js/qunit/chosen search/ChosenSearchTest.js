// ISO-8859-2

module('Chosen search');
asyncTest('Search with polish letters', function() {
	expect(14);
	// otwieramy chosen
	$j('#chosen_one_chzn').trigger('mousedown')

	setTimeout(function() {
		$j('.chzn-search > input[type=text]:first').focus().val('P').trigger('keyup');

		ok($j('#chosen_one_chzn_o_1').is(':hidden'), 'Option "�wi�cki" should be hidden');
		ok($j('#chosen_one_chzn_o_2').is(':hidden'), 'Option "Ma�a �wieczka" should be hidden');
		ok($j('#chosen_one_chzn_o_3:visible').hasClass('active-result'), 'Option "Pi��" should be visible');
		ok($j('#chosen_one_chzn_o_4:visible').hasClass('active-result'), 'Option "Pi�� ma�a" should be visible');
		start();
		stop();

		// chowamy
		$j('#chosen_one_chzn .chzn-drop li:visible:first').trigger('mouseup');
		
		// otwieramy chosen
		$j('#chosen_one_chzn').trigger('mousedown')

		setTimeout(function() {
			$j('.chzn-search > input[type=text]:first').focus().val('m').trigger('keyup');

			ok($j('#chosen_one_chzn_o_1').is(':hidden'), 'Option "�wi�cki" should be hidden');
			ok($j('#chosen_one_chzn_o_2:visible').hasClass('active-result'), 'Option "Ma�a �wieczka" should be visible');
			ok($j('#chosen_one_chzn_o_3').is(':hidden'), 'Option "Pi��" should be hidden');
			ok($j('#chosen_one_chzn_o_4:visible').hasClass('active-result'), 'Option "Pi�� ma�a" should be visible');

			equal($j('#chosen_one_chzn_o_2:visible').html().toLowerCase(), '<em>M</em>a�a �wieczka'.toLowerCase(), '"M" should be bolded in "Ma�a �wieczka"');
			equal($j('#chosen_one_chzn_o_4:visible').html().toLowerCase(), 'Pi�� <em>m</em>a�a'.toLowerCase(), '"m" should be bolded in "Pi�� ma�a"');
			start();
			stop();

			// chowamy
			$j('#chosen_one_chzn .chzn-drop li:visible:first').trigger('mouseup');
			
			// otwieramy chosen
			$j('#chosen_one_chzn').trigger('mousedown')

			setTimeout(function() {
				$j('.chzn-search > input[type=text]:first').focus().val('�').trigger('keyup');

				ok($j('#chosen_one_chzn_o_1:visible').hasClass('active-result'), 'Option "�wi�cki" should be visible');
				ok($j('#chosen_one_chzn_o_2:visible').hasClass('active-result'), 'Option "Ma�a �wieczka" should be visible');
				ok($j('#chosen_one_chzn_o_3').is(':hidden'), 'Option "Pi��" should be hidden');
				ok($j('#chosen_one_chzn_o_4').is(':hidden'), 'Option "Pi�� ma�a" should be hidden');
				start();

				// chowamy
				$j('#chosen_one_chzn .chzn-drop li:visible:first').trigger('mouseup');
				
			}, 200);
		}, 200);
	}, 200);
});

