DwrTest = {
	getFieldsList: function(dwrParams, obj) {
		// console.info('getFieldsList');
		setTimeout(function() {
			obj.callback(Fields);
		}, 1);
	},

	getFieldsValues: function(dwrParams, obj) {
		var values = {};
		
		setTimeout(function() {
			obj.callback(FieldsValues);
		}, 1);
	},

	setFieldsValues: function(values, dwrParams, obj) {
		var ret = Fields;

		setTimeout(function() {
			console.warn('setFieldsValues', ret);
			obj.callback(ret);
		}, 1);
	}
}

Fields = [{
	"type": "ENUM",
	"cn": "chosen_one",
	"label": "Chosen",
	"cssClass": null,
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": false,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": true,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a1",
	"readonly": false
}
];

FieldsValues = {
	'chosen_one': {
		'allOptions': [
			{'': '-- wybierz --'},
			{'1': '�wi�cki'},
			{'2': 'Ma�a �wieczka'},
			{'3': 'Pi��'},
			{'3': 'Pi�� ma�a'}
		],
		'selectedOptions': ['']			
	}
};