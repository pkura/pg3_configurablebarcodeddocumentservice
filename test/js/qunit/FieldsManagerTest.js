
// jedno żądanie ajaksowe
// |---500ms--->
asyncTest('Single ajax request - 500ms', function() {
	FieldsManager.Blocker.inc();

	// czy wykrył żądanie
	setTimeout(function() {
		strictEqual(FieldsManager.Blocker.getAjaxCounter(), 1, 'One ajax request present');
		start();
		stop();
	}, 1);

	// czy pola zablokowane
	setTimeout(function() {
		equal(FieldsManager.Blocker.isLocked(), true, 'Fields are locked');
		start();
		stop();
	}, DWR.BLOCK_TIMEOUT+50);

	// czy wykrył koniec żądania
	setTimeout(function() {
		FieldsManager.Blocker.dec();

		strictEqual(FieldsManager.Blocker.getAjaxCounter(), 0, 'No ajax requests presents');
		start();
		stop();
	}, 500);

	// czy odblokował pola
	setTimeout(function() {
		equal(FieldsManager.Blocker.isLocked(), false, 'Fields are unlocked');
		start();
	}, 500+DWR.RELEASE_TIMEOUT+50);
});

// dwa żądania po sobie - formatka nie powinna się zablokować
// |---50ms---> 20ms |---30ms--->
asyncTest('Two ajax requests in queue - no blocking', function() {
	FieldsManager.Blocker.inc();

	// czy wykrył żądanie
	setTimeout(function() {
		strictEqual(FieldsManager.Blocker.getAjaxCounter(), 1, 'One ajax request present');
		start();
		stop();
	}, 1);

	// czy wykrył koniec żądania
	setTimeout(function() {
		FieldsManager.Blocker.dec();

		strictEqual(FieldsManager.Blocker.getAjaxCounter(), 0, 'No ajax requests presents');
		start();
		stop();
	}, 50);

	// drugie żądanie
	setTimeout(function() {
		FieldsManager.Blocker.inc();
		strictEqual(FieldsManager.Blocker.getAjaxCounter(), 1, 'One ajax request present');
		start();
		stop();
	}, 70);

	// formatka nie powinna być zablokowana
	setTimeout(function() {
		equal(FieldsManager.Blocker.isLocked(), false, 'Fields are unlocked');
		start();
		stop();
	}, 90);

	// koniec drugiego żądania
	setTimeout(function() {
		FieldsManager.Blocker.dec();

		strictEqual(FieldsManager.Blocker.getAjaxCounter(), 0, 'No ajax requests presents');
		start();
	}, 100);
});

// losowe żądania
function getRandom(min, max) {
	return Math.round(Math.random() * (max - min) + min);
}
var reqCount = getRandom(3, 15);
asyncTest(reqCount + ' requests simultaneously', function() {
	var i, requests = [], req = null, lastReq = null;

	for(i = 0; i < reqCount; i ++) {
		// rodzielczość 100ms
		req = {
			start: Math.round(getRandom(0, 1000) / 100) * 100,
			length: Math.round(getRandom(100, 800) / 100) * 100,
			end: function() {
				return this.start + this.length;
			},
			running: false,
			runningTime: 0,
			tested: false
		};
		
		requests.push(req);
		if(lastReq == null) {
			lastReq = req;
		} else {
			if(req.end() > lastReq.end()) {
				lastReq = req;
			}
		}
	}

	for(i = 0; i < requests.length; i ++) {
		req = requests[i];
		(function(index) {
			setTimeout(function() {
				// początek żądania
				FieldsManager.Blocker.inc();
				requests[index].running = true;
			}, req.start);
		})(i);
				
		(function(index) {
			setTimeout(function() {
				// koniec żądania
				FieldsManager.Blocker.dec();
				requests[index].running = false;
			}, req.end());
		})(i);
	}

	// co 5ms sprawdzamy, które żądania są uruchomione i jak długo
	var hChecker = setInterval(function() {
		var i = 0, len = requests.length, req = null;
		for(i = 0; i < len; i ++) {
			req = requests[i];
			if(req.running === true) {
				req.runningTime += 5;
				// jeśli żądanie trwa dłużej niż BLOCK_TIMEOUT, to pola powinny być zablokowane - sprawdzamy tylko raz dla danego żądania
				if(req.tested === false && req.runningTime > DWR.BLOCK_TIMEOUT) {
					req.tested = true;
					equal(FieldsManager.Blocker.isLocked(), true, 'Fields are in locked state: start:' + req.start + 'ms length:' + req.length + 'ms');
					start();
					stop();
				}
			}
		}
	}, 25);

	setTimeout(function() {
		// w tym momencie wszystkie żądanie się zakończyły
		strictEqual(FieldsManager.Blocker.getAjaxCounter(), 0, 'All requests ended');
		start();
		stop();
	}, lastReq.end() + 50);

	setTimeout(function() {
		// w tym momencie formatka powinna być odblokowana
		equal(FieldsManager.Blocker.isLocked(), false, 'Fields are unlocked');
		start();
		
		// sprzątamy
		clearInterval(hChecker);
	}, lastReq.end() + 50 + DWR.RELEASE_TIMEOUT);
});