QUnit.config.reorder = false;

module('Refresh VAT dictionary');
asyncTest('Adding 2 items', function() {
	expect(3);

	// żeby js zdążył narysować
	setTimeout(function() {
		var counter = 2;

		setTimeout(function addItem() {
			// otwieramy popup
			$j('td[fieldcn=DWR_DEKRETACJA_KSIEG] .addItem').click();

			setTimeout(function() {
				// wybieramy pierwszą opcję z selekta
				$j('#DEKRETACJA_KSIEG_CONNECTED_TYPE_ID_POPUP_DEKRETACJA_KSIEG').val('1').change();

				setTimeout(function() {
					// klikamy "Dodaj"
					$j('#DEKRETACJA_KSIEGcreate-item-button').click();

					counter --;
					if(counter > 0) {
						setTimeout(addItem, 400);
					} else {
						setTimeout(function() {
							// klikamy "Nalicz podatek"
							$j('#DWR_NALICZENIE_PODATKU').click();
							setTimeout(function() {
								// powinny byc cztery wiersze
								var rows = $j('#dwrDictionaryTable_DWR_STAWKI_VAT tr[class^=multipleFieldsNumber_]').length;
								// equal(rows, 4, '4 rows should be present');
								

								setTimeout(removeLast, 100);
							}, 200);
						}, 100);
					}

				}, 10);
			}, 100);
		}, 400);

		function removeLast() {
			// usuwamy dwa wiersze
			$j('#dwrDictionaryTable_DWR_DEKRETACJA_KSIEG tr.multipleFieldsNumber_4 a.delete_dictionary_item').click();
			$j('#dwrDictionaryTable_DWR_DEKRETACJA_KSIEG tr.multipleFieldsNumber_3 a.delete_dictionary_item').click();

			// klikamy "Nalicz podatek"
			$j('#DWR_NALICZENIE_PODATKU').click();			

			setTimeout(function() {
				// powinny byc dwa wiersze
				var rows = $j('#dwrDictionaryTable_DWR_STAWKI_VAT tr[class^=multipleFieldsNumber_]').length;
				equal(rows, 2, '2 rows should be present');			

				setTimeout(removeAll, 100)	;
			}, 100);
		}

		function removeAll() {
			// usuwamy wszystkie wiersze
			$j('#dwrDictionaryTable_DWR_DEKRETACJA_KSIEG tr.multipleFieldsNumber_2 a.delete_dictionary_item').click();
			$j('#dwrDictionaryTable_DWR_DEKRETACJA_KSIEG tr.multipleFieldsNumber_1 a.delete_dictionary_item').click();		

			// klikamy "Nalicz podatek"
			$j('#DWR_NALICZENIE_PODATKU').click();	

			setTimeout(function() {
				// powinien być jeden pusty wiersz
				var rows = $j('#dwrDictionaryTable_DWR_STAWKI_VAT tr[class^=multipleFieldsNumber_]').length;
				equal(rows, 1, '1 row should be present');

				var str = '';
				$j('input[type=hidden][name^=STAWKI_VAT_]').each(function() {
					str += $j(this).val();
				});

				equal(str, '', 'Vat should be empty');
				start();

			}, 100);							
		}
	}, 400);
	
});