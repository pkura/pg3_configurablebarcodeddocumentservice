QUnit.config.reorder = false;

module('Hide empty');
test('Empty selects should be hidden', function() {
	expect(5);
	stop();

	setTimeout(function() {
		ok($j('label[for=SLOWNIK_SLOWNIK_Sty_1]').is(':visible') === false, "Label should be hidden");
		ok($j('#SLOWNIK_SLOWNIK_Sty_1_chzn').is(':visible') === false, "Chosen should be hidden");

		ok($j('label[for=SLOWNIK_Kw_2]').is(':visible') === true, "Label should be visible");
		ok($j('#SLOWNIK_Kw_3').is(':visible') === true, "Select should be visible");
		console.info('raz');
		start();
		stop();

		setTimeout(function() {
			// otwieramy plugin
			$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn').trigger('mousedown');
			$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn .chzn-drop li:first').trigger('mouseup');	

			$j('#SLOWNIK_CENTRUMID_2_chzn').trigger('mousedown');
			$j('#SLOWNIK_CENTRUMID_2_chzn .chzn-drop li:first').trigger('mouseup');		

			$j('#SLOWNIK_CENTRUMID_3_chzn').trigger('mousedown');
			$j('#SLOWNIK_CENTRUMID_3_chzn .chzn-drop li:first').trigger('mouseup');				

			ok(fieldsManager.validate(), 'All fields should be marked as valid');			
			start();
			console.info('dwa');						
		}, 100);
	}, 400);
});

var oldGetMultipleFieldsValues = Dictionary.getMultipleFieldsValues;
module('Set values', {
	setup: function() {
		Dictionary.getMultipleFieldsValues = function(dwrParams, dictionaryName, values, obj) {
			console.info('getMultipleFieldsValues2', dictionaryName, values);
			var val = {};

			if(values != null) {
				if(values.SLOWNIK_CENTRUMID != null && values.SLOWNIK_CENTRUMID.sender === true) {
					console.info('setting val');
					val.SLOWNIK_Sty = val.SLOWNIK_Mar = DictionaryValues.SLOWNIK_Lu;
				} else if(values.SLOWNIK_ACCOUNTNUMBER != null && values.SLOWNIK_ACCOUNTNUMBER.sender === true) {
					val.SLOWNIK_Sty = val.SLOWNIK_Mar = {allOptions: [], selectedOptions: []};
				}
			} 
				
			obj.callback(val);
		};
	},
	teardown: function() {
		Dictionary.getMultipleFieldsValues = oldGetMultipleFieldsValues;
	}
});
test('Setting and clearing values', function() {
	expect(5);
	stop();
	setTimeout(function() {
		// ustawiamy wartość pierwszego pola
		$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn').trigger('mousedown');
		$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn .chzn-drop li:first').trigger('mouseup');			

		setTimeout(function() {
			// pola powinny być widoczne
			ok($j('#SLOWNIK_SLOWNIK_Sty_1').is(':visible') === true, 'Select should be visible');
			ok($j('#SLOWNIK_SLOWNIK_Mar_1_chzn').is(':visible') === true, 'Chosen should be visible');
			start();
			stop();

			// ustawiamy wartość drugiego pola
			$j('#SLOWNIK_SLOWNIK_ACCOUNTNUMBER_1_chzn').trigger('mousedown');
			$j('#SLOWNIK_SLOWNIK_ACCOUNTNUMBER_1_chzn .chzn-drop li:first').trigger('mouseup');		

			setTimeout(function() {
				// pola powinny być schowane
				ok($j('#SLOWNIK_SLOWNIK_Sty_1').is(':visible') === false, 'Select should be hidden');
				ok($j('#SLOWNIK_SLOWNIK_Mar_1_chzn').is(':visible') === false, 'Chosen should be hidden');
				start();
				stop();

				setTimeout(function() {
					ok(fieldsManager.validate(), 'All fields should be marked as valid');			
					start();					
				}, 100);
			}, 100);

		}, 100);

		console.info('trzy');
	}, 100);
});
