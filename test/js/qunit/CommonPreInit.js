var $j = jQuery.noConflict();

$j.getScript = function() {
	return $j.Deferred().resolve().promise();
}

var DWR = {
	PAGE_SIZE: 10,
	DEFAULT_DATE_FORMAT: 'dd-mm-yyyy',
	DEFAULT_TIME_FORMAT: 'hh:MM:ss',
	DEFAULT_HOUR_FORMAT: 'hh',
	DEFAULT_HOUR_MINUTE_FORMAT: 'hh:MM',
	CKEDITOR_URL: 'INTERNAL',
	UPLOAD_URL: '/docusafe/dwr-upload.ajx',
	PLUPLOAD_FLASH_URL: '/docusafe/dwrdockind/plupload/plupload.flash.swf',
	PLUPLOAD_SILVERLIGHT_URL: '/docusafe/dwrdockind/plupload/plupload.silverlight.xap',
	BLOCKER: false,
	BLOCKER_STATS: false,
	BLOCK_TIMEOUT: 200,
	RELEASE_TIMEOUT: 400
};

var DWR_DATE_FORMAT_ID = new Object();
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT] = 1;
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT + ' ' + DWR.DEFAULT_TIME_FORMAT] = 2;
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT + ' ' + DWR.DEFAULT_HOUR_FORMAT] = 3;
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT + ' ' + DWR.DEFAULT_HOUR_MINUTE_FORMAT] = 4;

DWR.BLOCKER = true;


/* musi być globalna przed wczytaniem CKEditor */
CKEDITOR_BASEPATH = '/docusafe/dwrdockind/ckeditor/';

//tutaj wrzucam komunikaty w różnych językach, żeby były obsłużone z poziomu jsp
DWR.MSG = {
		DEFAULT_SELECT_LABEL:				'-- wybierz --',
		SAVE_WITH_TIMEOUT:					'Zapisz',
		DIC_ADD_NEXT:						'Dodaj następny element',
		DIC_DELETE_ELEMENT:					'Usuń',
		DIC_CLEAR_FIELDS:					'Wyczyść pola',
		DIC_SHOW_POPUP:						'Pokaż słownik',
		DIC_NEW_ITEM:						'nowy wpis',
		VALIDATOR_INCORRECT:				'Nieprawidłowa wartość',
		VALIDATOR_INCORRECT_NUMBER:			'Niepoprawna liczba',
		VALIDATOR_INCORRECT_AMOUNT:			'Niepoprawna kwota',
		VALIDATOR_INCORRECT_DATE:			'Niepoprawna data',
		VALIDATOR_INCORRECT_DAY:			'Niepoprawny dzień',
		VALIDATOR_INCORRECT_MONTH:			'Niepoprawny miesiąc',
		VALIDATOR_INCORRECT_YEAR:			'Niepoprawny rok',
		VALIDATOR_INCORRECT_TIME:			'Niepoprawna data',
		VALIDATOR_INCORRECT_HOUR:			'Niepoprawna godzina',
		VALIDATOR_INCORRECT_MINUTE:			'Niepoprawna minuta',
		VALIDATOR_INCORRECT_SECOND:			'Niepoprawna sekunda',
		RANGE_TOO_BIG_NUMBER:				'Zbyt duża liczba',
		RANGE_TOO_SMALL_NUMBER:				'Zbyt mała liczba',
		ENUM_MULTIPLE_SELECT:				'Wybierz',
		FIELD_REQUIRED:						'Pole obowiązkowe',
		DIC_POPUP_ADD:						'Dodaj',
		DIC_POPUP_SAVE:						'Zapisz',
		DIC_POPUP_CANCEL:					'Anuluj',
		DIC_POPUP_EDIT:						'Edytuj',
		DIC_POPUP_DELETE:					'Usuń',
		DIC_POPUP_CLEAR:					'Wyczyść',
		DIC_POPUP_SELECT:					'Wybierz',
		DIC_POPUP_ADD_TO_FORM:				'i umieść w formularzu',
		DIC_POPUP_MORE_RESULTS:				'Więcej wyników',
		DIC_POPUP_ITEM_SAVED: 				'Zapisano element.',
		DIC_POPUP_ITEM_ADDED: 				'Dodano element.',
		DIC_POPUP_ITEM_DELETED: 			'Usunięto element.',
		DIC_POPUP_ITEM_SAVED_FAILED: 		'Nie udało się zapisać elementu.',
		DIC_POPUP_ITEM_ADDED_FAILED: 		'Nie udało się dodać elementu.',
		DIC_POPUP_ITEM_DELETED_FAILED: 		'Nie udało się usunąć elementu.',
		DIC_POPUP_ITEM_ADDED_EMPTY_WARNING: 'Nie można dodać pustego elementu.',
		DIC_POPUP_ITEM_ADDED_NOT_VALID: 	'Formularz zawiera błędy.',
		DIC_POPUP_NO_RESULTS_FOUND:			'Nic nie znaleziono.',
		AUTOCOMPLETE_SEE_MORE_RESULTS:		'Zobacz więcej wyników',
		AUTOCOMPLETE_SHOWING_FIRST:			'Wyświetlono pierwszych',
		AUTOCOMPLETE_RESULTS:				'wyników',
		ATT_ADD_REVISION:					'Nowa wersja pliku',
		ATT_REVERT_REVISION:				'Anuluj',
		ATT_DOWNLOAD:						'Pobierz załącznik',
		ATT_DOWNLOAD_AS_PDF:				'Pobierz załącznik',
		ATT_VIEWSERVER:						'Wyświetl załącznik w przeglądarce',
		PLUPLOAD_FILE_NAME:                 'Nazwa pliku',
		PLUPLOAD_FILE_STATUS:               'Status',
		PLUPLOAD_FILE_SIZE:                 'Rozmiar',
		PLUPLOAD_ADD_FILES:                 'Dodaj pliki',
		PLUPLOAD_START_UPLOAD:              'Wyślij',
		PLUPLOAD_DRAG_HERE:                 'Upuść pliki tutaj',
		PLUPLOAD_SENDING:                   'Wysyłanie plików...',
		PLUPLOAD_COMPLETE:                  'Wysłano',
		PLUPLOAD_REMOVE:                    'Usuń',
		BLOCKER_LOADING:                    'Wczytywanie'
}


dwr = {
	engine: {
		setPreHook: function() {

		},
		setPostHook: function() {

		},

		setErrorHandler: function() {

		},

		setTextHtmlHandler: function() {

		}
	}
}

var dwrParams = {
    context: "dockind",
    action: "new",
    documentType: " ",
    dockind: " ",
    documentId: " ",
    docIds: " ",
    activity: " "
};

function fixResize() {

}

var ie = $j.browser.msie;
var ie6 = $j.browser.msie && (parseInt($j.browser.version) == 6) ? true : false;
var ie7 = $j.browser.msie && (parseInt($j.browser.version) == 7) ? true : false;
var ie8 = $j.browser.msie && (parseInt($j.browser.version) == 8) ? true : false;

if(!window.DEBUG) {
	console = {};
	console.info = console.log = console.warn = console.debug = function() {};
}
