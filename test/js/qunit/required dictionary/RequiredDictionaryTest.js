QUnit.config.reorder = false;

function copyFields(fields) {
	var ret = [], field = {};
	for(var i = 0; i < fields.length; i ++) {
		field = {};
		$j.extend(field, fields[i]);
		if(field.status == 'REMOVE') {
			field.status = 'ADD';
		}
		ret.push(field);
	}

	return ret;
}

(function() {
	var oldDwrTest = {};
	var fieldsValues = {};
	$j.extend(fieldsValues, FieldsValues);
	module('Required', {
		setup: function() {
			oldDwrTest.setFieldsValues = DwrTest.setFieldsValues;
			oldDwrTest.getFieldsValues = DwrTest.getFieldsValuesl
			
			DwrTest.setFieldsValues = function(values, dwrParams, obj) {
				var ret = null;
				if(values != null) {
					ret = copyFields(Fields);

					for(var cn in values) {
						fieldsValues[cn] = BaseField.unpackValues(values[cn]);
					}
				}


				setTimeout(function() {
					console.assert(ret != null, 'Fields values should not be null');
					obj.callback(ret);
				}, 1);
			};

			DwrTest.getFieldsValues = function(dwrParams, obj) {
				obj.callback(fieldsValues);
			};
		},

		teardown: function() {
			DwrTest.setFieldsValues = oldDwrTest.setFieldsValues;
			DwrTest.getFieldsValues = oldDwrTest.getFieldsValues;
		}
	});
	asyncTest('Empty dictionary', function() {
		expect(3);
		setTimeout(function() {
			$j('#obowiazkowy_obowiazkowy_nazwisko').val('Prus').trigger('change');
			ok(fieldsManager.validate(), 'Form should be valid');
			ok($j('#slownik_slownik_nazwisko').hasClass('invalid') === false, 'Field nazwisko should be valid');
			ok($j('label[for=slownik_slownik_nazwisko]').hasClass('invalid') === false, 'Label for field nazwisko should be valid');
			start();
		}, 100);
	});
	asyncTest('Type into required field', function() {
		expect(4);
		setTimeout(function() {
			$j('#slownik_slownik_nazwisko').val('M').trigger('change');
			setTimeout(function() {
				ok(fieldsManager.validate(), 'Form with required field should be valid');
				ok($j('#slownik_slownik_nazwisko').hasClass('invalid') === false, 'Field nazwisko should be valid');
				start(); stop();

				setTimeout(function() {
					$j('#slownik_slownik_nazwisko').val('').trigger('change');
					ok(fieldsManager.validate(), 'Empty form should be valid');
					ok($j('#slownik_slownik_nazwisko').hasClass('invalid') === false, 'Field nazwisko should be valid');
					start(); 				
				}, 100);
			}, 100);
		}, 100);
	});
	asyncTest('Type into non-required field', function() {
		expect(2);
		setTimeout(function() {
			$j('#slownik_slownik_imie').val('A').trigger('change');
			setTimeout(function() {
				ok(fieldsManager.validate() === false, 'Form should be invalid');
				ok($j('#slownik_slownik_nazwisko').hasClass('invalid') === true, 'Field nazwisko should be invalid');
				start(); 	
			}, 100);
		}, 100);
	});
})();
