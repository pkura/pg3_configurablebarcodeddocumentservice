DwrTest = {
	getFieldsList: function(dwrParams, obj) {
		// console.info('getFieldsList');
		setTimeout(function() {
			obj.callback(Fields);
		}, 1);
	},

	getFieldsValues: function(dwrParams, obj) {
		var values = {};
		
		setTimeout(function() {
			obj.callback(FieldsValues);
		}, 1);
	},

	setFieldsValues: function(values, dwrParams, obj) {
		var ret = Fields;

		setTimeout(function() {
			console.warn('setFieldsValues', ret);
			obj.callback(ret);
		}, 1);
	}
}

Fields = [{
	"type": "DICTIONARY",
	"cn": "slownik",
	"label": "Slownik",
	"cssClass": null,
	"status": "ADD",
	"submit": false,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": false,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": {
		"fields": [{
			"type": "STRING",
			"cn": "slownik_imie",
			"label": "Imie",
			"cssClass": null,
			"status": "ADD",
			"submit": false,
			"submitTimeout": 0,
			"column": 0,
			"fieldset": "slownik_fieldset",
			"kind": "DICTIONARY_FIELD",
			"validatorRegExp": null,
			"validatorMessage": null,
			"format": null,
			"required": false,
			"disabled": false,
			"info": "",
			"params": null,
			"hidden": false,
			"autocomplete": false,
			"autocompleteRegExp": null,
			"dictionary": null,
			"newItemMessage": "",
			"readonly": false
		},
		{
			"type": "STRING",
			"cn": "slownik_nazwisko",
			"label": "Nazwisko",
			"cssClass": null,
			"status": "ADD",
			"submit": false,
			"submitTimeout": 0,
			"column": 0,
			"fieldset": "slownik_fieldset",
			"kind": "DICTIONARY_FIELD",
			"validatorRegExp": null,
			"validatorMessage": null,
			"format": null,
			"required": true,
			"disabled": false,
			"info": "",
			"params": null,
			"hidden": false,
			"autocomplete": false,
			"autocompleteRegExp": null,
			"dictionary": null,
			"newItemMessage": "",
			"readonly": false
		}],
		"popUpFields": [],
		"name": "slownik",
		"visibleFieldsCns": ["slownik_imie",
		"slownik_nazwisko"],
		"searchableFieldsCns": ["slownik_"],
		"promptFieldsCns": ["slownik_"],
		"popUpButtons": ["doAdd",
		"doEdit",
		"doRemove",
		"doSelect",
		"doClear"],
		"tableView": false,
		"multiple": false,
		"cantUpdate": false,
		"onlyPopUpCreate": false,
		"dicButtons": ["showPopup",
		"doAdd",
		"doRemove",
		"doClear"]
	},
	"newItemMessage": "",
	"coords": "a1",
	"readonly": false
},
{
	"type": "DICTIONARY",
	"cn": "obowiazkowy",
	"label": "Slownik obowiazkowy",
	"cssClass": null,
	"status": "ADD",
	"submit": false,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": true,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": {
		"fields": [{
			"type": "STRING",
			"cn": "obowiazkowy_imie",
			"label": "Imie",
			"cssClass": null,
			"status": "ADD",
			"submit": false,
			"submitTimeout": 0,
			"column": 0,
			"fieldset": "obowiazkowy_fieldset",
			"kind": "DICTIONARY_FIELD",
			"validatorRegExp": null,
			"validatorMessage": null,
			"format": null,
			"required": false,
			"disabled": false,
			"info": "",
			"params": null,
			"hidden": false,
			"autocomplete": false,
			"autocompleteRegExp": null,
			"dictionary": null,
			"newItemMessage": "",
			"readonly": false
		},
		{
			"type": "STRING",
			"cn": "obowiazkowy_nazwisko",
			"label": "Nazwisko",
			"cssClass": null,
			"status": "ADD",
			"submit": false,
			"submitTimeout": 0,
			"column": 0,
			"fieldset": "obowiazkowy_fieldset",
			"kind": "DICTIONARY_FIELD",
			"validatorRegExp": null,
			"validatorMessage": null,
			"format": null,
			"required": true,
			"disabled": false,
			"info": "",
			"params": null,
			"hidden": false,
			"autocomplete": false,
			"autocompleteRegExp": null,
			"dictionary": null,
			"newItemMessage": "",
			"readonly": false
		}],
		"popUpFields": [],
		"name": "obowiazkowy",
		"visibleFieldsCns": ["obowiazkowy_imie", "obowiazkowy_nazwisko"],
		"searchableFieldsCns": [],
		"promptFieldsCns": [],
		"popUpButtons": ["doAdd",
		"doEdit",
		"doRemove",
		"doSelect",
		"doClear"],
		"tableView": false,
		"multiple": false,
		"cantUpdate": false,
		"onlyPopUpCreate": false,
		"dicButtons": ["showPopup",
		"doAdd",
		"doRemove",
		"doClear"]
	},
	"newItemMessage": "",
	"coords": "b1",
	"readonly": false

}];

FieldsValues = {
	'zalacznik_jeden': {
		'userSummary': 'Kamil Omelańczuk',
		'ctime': new Date(),
		'revisionMime': 'pdf',
		'attachmentId': 1,
		'revisionId': 1,
		'mimeAcceptable': true,
		'mimeXml': false
	},
	'zalacznik_trzy': {
		'userSummary': 'Kamil Omelańczuk',
		'ctime': new Date(),
		'revisionMime': 'pdf',
		'attachmentId': 3,
		'revisionId': 3,
		'mimeAcceptable': true,
		'mimeXml': false
	},
	'zalacznik_cztery': {
		'userSummary': 'Kamil Omelańczuk',
		'ctime': new Date(),
		'revisionMime': 'pdf',
		'attachmentId': 4,
		'revisionId': 4,
		'mimeAcceptable': true,
		'mimeXml': false
	}	
};