QUnit.config.reorder = false;

function copyFields(fields) {
	var ret = [], field = {};
	for(var i = 0; i < fields.length; i ++) {
		field = {};
		$j.extend(field, fields[i]);
		if(field.status == 'REMOVE') {
			field.status = 'ADD';
		}
		ret.push(field);
	}

	return ret;
}

(function() {
	var oldDwrTest = {};
	var fieldsValues = {};
	$j.extend(fieldsValues, FieldsValues);
	module('Required', {
		setup: function() {
			oldDwrTest.setFieldsValues = DwrTest.setFieldsValues;
			oldDwrTest.getFieldsValues = DwrTest.getFieldsValuesl
			
			DwrTest.setFieldsValues = function(values, dwrParams, obj) {
				var ret = null;
				if(values != null) {
					ret = copyFields(Fields);

					for(var cn in values) {
						fieldsValues[cn] = BaseField.unpackValues(values[cn]);
					}
				}


				setTimeout(function() {
					console.assert(ret != null, 'Fields values should not be null');
					obj.callback(ret);
				}, 1);
			};

			DwrTest.getFieldsValues = function(dwrParams, obj) {
				obj.callback(fieldsValues);
			};
		},

		teardown: function() {
			DwrTest.setFieldsValues = oldDwrTest.setFieldsValues;
			DwrTest.getFieldsValues = oldDwrTest.getFieldsValues;
		}
	});
	asyncTest('Attachment', function() {
		setTimeout(function() {
			ok(fieldsManager.validate(), 'Form should be valid');
			start();
		}, 100);
	});
})();
