DwrTest = {
	getFieldsList: function(dwrParams, obj) {
		// console.info('getFieldsList');
		setTimeout(function() {
			obj.callback(Fields);
		}, 1);
	},

	getFieldsValues: function(dwrParams, obj) {
		var values = {};
		
		setTimeout(function() {
			obj.callback(FieldsValues);
		}, 1);
	},

	setFieldsValues: function(values, dwrParams, obj) {
		var ret = Fields;

		setTimeout(function() {
			console.warn('setFieldsValues', ret);
			obj.callback(ret);
		}, 1);
	}
}

Fields = [{
	"type": "ATTACHMENT",
	"cn": "zalacznik_jeden",
	"label": "Załącznik jeden obowiązkowy",
	"cssClass": null,
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": true,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a1",
	"readonly": false
},
{
	"type": "ATTACHMENT",
	"cn": "zalacznik_dwa",
	"label": "Załącznik dwa nieobowiązkowy",
	"cssClass": null,
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": false,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a2",
	"readonly": false
},
{
	"type": "ATTACHMENT",
	"cn": "zalacznik_trzy",
	"label": "Załącznik trzy disabled",
	"cssClass": null,
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": true,
	"disabled": true,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a3",
	"readonly": false
},
{
	"type": "ATTACHMENT",
	"cn": "zalacznik_cztery",
	"label": "Załącznik cztery readonly",
	"cssClass": null,
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": true,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a4",
	"readonly": true
}
];

FieldsValues = {
	'zalacznik_jeden': {
		'userSummary': 'Kamil Omelańczuk',
		'ctime': new Date(),
		'revisionMime': 'pdf',
		'attachmentId': 1,
		'revisionId': 1,
		'mimeAcceptable': true,
		'mimeXml': false
	},
	'zalacznik_trzy': {
		'userSummary': 'Kamil Omelańczuk',
		'ctime': new Date(),
		'revisionMime': 'pdf',
		'attachmentId': 3,
		'revisionId': 3,
		'mimeAcceptable': true,
		'mimeXml': false
	},
	'zalacznik_cztery': {
		'userSummary': 'Kamil Omelańczuk',
		'ctime': new Date(),
		'revisionMime': 'pdf',
		'attachmentId': 4,
		'revisionId': 4,
		'mimeAcceptable': true,
		'mimeXml': false
	}	
};