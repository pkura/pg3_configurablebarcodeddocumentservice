Dictionary = {
	getFieldsValues: function(dictionaryName, id, obj) {
		console.info('Dictionary.getFieldsValues', dictionaryName, id);

		obj.callback(DictionaryValues);
	},

	getFieldValue: function(dictionaryName, fieldCn, obj) {
		obj.callback(FullDictionaryValues[dictionaryName][fieldCn]);
	},

	getFieldsValuesList: function(dictionaryName, ids, obj) {
		console.info('Dictionary.getFieldsValuesList', dictionaryName, ids, DictionaryValuesList);
		obj.callback(DictionaryValuesList);
	},

	getMultipleFieldsValues: function(dwrParams, dictionaryName, values, obj) {
		obj.callback({});
	},

	dwrSearch: function() {
		// console.info('search');
	}
}

FullDictionaryValues = {
	'SLOWNIK': {
		'SLOWNIK_CENTRUMID': {
			'allOptions': [
				{'': '-- wybierz --'},
				{'1': 'Pierwsza'},
				{'2': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit'}
			],
			'selectedOptions': ['2']
		},
		'SLOWNIK_ACCOUNTNUMBER': {
			'allOptions': [
				{'': '-- wybierz --'},
				{'1': 'Numer'},
				{'2': 'Konta'}
			],
			'selectedOptions': ['']			
		}
	}
};

DictionaryValues = {
	"SLOWNIK_CENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		}],
		"selectedId": "",
		"selectedOptions": [""],
		"selectedValue": ""
	},
	"SLOWNIK_ACCOUNTNUMBER": {
		"allOptions": [
		{
			"": "-- wybierz --"
		},
		{
			"3": "Wybrany"
		}],
		"selectedOptions": ["3"],
	}
};

DictionaryValuesList = [];

// tworzymy wartości
(function() {
	var i, fieldName, val, specVal, fullVal, specFullVal, j;

	for(i = 0; i < 1; i ++) {
		fieldName = 'SLOWNIK_' + dateFormat.i18n.pl.monthNames[i];
		val = {
			allOptions: [{
				'': '-- wybierz --'
			}],
			selectedOptions: [(i+1) + '']
		};

		specVal = {};
		specVal[(i+1) + ''] = (i+1) + '. ' + dateFormat.i18n.pl.monthNamesCase[i];
		val.allOptions.push(specVal);

		DictionaryValues[fieldName] = val;

		fulVal = {
			allOptions: [{
				'': '-- wybierz --'
			}],
			selectedOptions: ['']
		};

		for(j = 0; j < 400; j ++) {
			specFullVal = {};
			specFullVal[(j+1) + ''] = 'Opcja numer ' + (j+1);
			fulVal.allOptions.push(specFullVal);
		}

		FullDictionaryValues['SLOWNIK'][fieldName] = fulVal
	}

	for(i = 0; i < 1; i ++) {
		DictionaryValuesList.push(DictionaryValues);
	}
})();
