QUnit.config.reorder = false;

var OldDwrTest = {
	getFieldsValues: DwrTest.getFieldsValues,
	setFieldsValues: DwrTest.setFieldsValues
};

var VALUES = {
	'SUBMIT': {
		label: 'Ustaw pole na dwr',
		value: 'submit'
	}
};

module('Popup');
asyncTest('Resize popup', function() {
	expect(1);
	// otwieramy popup
	$j('#SHOW_POPUP_BUTTON').click()
	setTimeout(function() {
		// wybieramy druga - dluga opcje
		$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn').trigger('mousedown');
		setTimeout(function() {
			$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn .chzn-drop li:eq(1)').trigger('mouseup');

			setTimeout(function() {
				ok($j('.ui-dialog:visible:first').width() > 500, 'Popup width should be > 500 and is ' + $j('.ui-dialog:visible:first').width());
				start();

			}, 100);
			
		}, 100);
	}, 100);
});

/*
asyncTest('Field is not empty', function() {
	expect(1);
	$j('#SUBMIT').trigger('click');
	setTimeout(function() {
		ok(fieldsManager.validate() === true, 'Field is required and not empty -- allow save');
		start();
	}, 100);
});*/

