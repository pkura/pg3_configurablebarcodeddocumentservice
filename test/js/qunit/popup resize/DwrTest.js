DwrTest = {
	getFieldsList: function(dwrParams, obj) {
		// console.info('getFieldsList');
		setTimeout(function() {
			obj.callback(Fields);
		}, 1);
	},

	getFieldsValues: function(dwrParams, obj) {
		var values = {};
		
		setTimeout(function() {
			obj.callback(FieldsValues);
		}, 1);
	},

	setFieldsValues: function(values, dwrParams, obj) {
		var ret = Fields;

		setTimeout(function() {
			console.warn('setFieldsValues', ret);
			obj.callback(ret);
		}, 1);
	}
}

Fields = [{
	"type": "BUTTON",
	"cn": "SHOW_POPUP_BUTTON",
	"label": "",
	"cssClass": 'dwr-no-label',
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": true,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a1",
	"readonly": false,
	'popups': {
		'POPUP': {
			showpopupcn: 'POPUP',
			caption: 'Okienko'
		}
	}
},
{
	"type": "STRING",
	"cn": "STRING_FIELD",
	"label": "Pole tekstowe",
	"cssClass": '',
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": true,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a1",
	"readonly": false,
	'popups': {
		'POPUP': {
			popupcn: 'POPUP'
		}
	}
},
// slownik
{
		"autocomplete": false,
		"autocompleteRegExp": null,
		"autofocus": false,
		"cn": "DWR_SLOWNIK",
		"column": 0,
		"coords": "a1:g1",
		"cssClass": "dwr-disabled-higher-contrast dwr-horizontal-dic-title",
		"dictionary": {
			"autohideMultipleDictionary": false,
			"cantUpdate": false,
			"dicButtons": ["doAdd",
			"doClear",
			"doRemove"],
			"disabledSubmit": false,
			"fields": [{
				"autocomplete": false,
				"autocompleteRegExp": null,
				"autofocus": false,
				"cn": "SLOWNIK_ID_1",
				"column": 0,
				"coords": null,
				"cssClass": "",
				"dictionary": null,
				"disabled": false,
				"fieldset": "SLOWNIK_fieldset",
				"format": null,
				"hidden": true,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "DICTIONARY_FIELD",
				"label": "ID",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": false,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "INTEGER",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null
			},
			{
				"autocomplete": true,
				"autocompleteRegExp": "%",
				"autofocus": false,
				"cn": "SLOWNIK_CENTRUMID_1",
				"column": 0,
				"coords": null,
				"cssClass": "dwr-disabled-higher-contrast",
				"dictionary": null,
				"disabled": false,
				"fieldset": "SLOWNIK_fieldset",
				"format": null,
				"hidden": false,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "DICTIONARY_FIELD",
				"label": "Podmiot",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": true,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "ENUM",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null,
				"lazyEnumLoad": true
			},
			{
				"autocomplete": true,
				"autocompleteRegExp": "%",
				"autofocus": false,
				"cn": "SLOWNIK_ACCOUNTNUMBER_1",
				"column": 0,
				"coords": null,
				"cssClass": "dwr-disabled-higher-contrast",
				"dictionary": null,
				"disabled": true,
				"fieldset": "SLOWNIK_fieldset",
				"format": null,
				"hidden": false,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "DICTIONARY_FIELD",
				"label": "CPK",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": true,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "ENUM",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null,
				"lazyEnumLoad": true
			}],
			"jsPopupFunction": null,
			"multiple": true,
			"name": "SLOWNIK",
			"oldField": null,
			"onlyPopUpCreate": false,
			"popUpButtons": ["doAdd",
			"doEdit",
			"doRemove",
			"doSelect",
			"doClear"],
			"popUpFields": [{
				"autocomplete": false,
				"autocompleteRegExp": null,
				"autofocus": false,
				"cn": "SLOWNIK_ID_1",
				"column": 0,
				"coords": null,
				"cssClass": "",
				"dictionary": null,
				"disabled": false,
				"fieldset": "default",
				"format": null,
				"hidden": true,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "NORMAL",
				"label": "SLOWNIK_ID_1",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": false,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "INTEGER",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null
			}],
			"promptFieldsCns": [],
			"refreshMultipleDictionary": false,
			"searchableFieldsCns": [],
			"showDictionaryPopup": false,
			"tableView": true,
			"visibleFieldsCns": ["SLOWNIK_CENTRUMID_1",
			"SLOWNIK_ACCOUNTNUMBER_1"],
			"loadingMode": "ALL_AT_ONCE"
		},
		"disabled": false,
		"fieldset": "default",
		"format": null,
		"hidden": false,
		"hiddenOnDisabledEmpty": false,
		"info": null,
		"kind": "NORMAL",
		"label": "Pozycje budzetowe",
		"linkTarget": null,
		"listenersList": null,
		"multiple": false,
		"newItemMessage": "NowyWpis",
		"onchange": null,
		"params": null,
		"popups": {
			'POPUP': {
				popupcn: 'POPUP'
			}
		},
		"readonly": false,
		"required": true,
		"status": "ADD",
		"submit": true,
		"submitForm": null,
		"submitTimeout": 0,
		"type": "DICTIONARY",
		"validate": false,
		"validatorMessage": null,
		"validatorRegExp": null
	}
];

FieldsValues = {
	'SHOW_POPUP_BUTTON': {
		label: 'Popup',
		value: 'show_popup'
	}
};