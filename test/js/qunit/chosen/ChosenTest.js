QUnit.config.reorder = false;

module('Visibility');
asyncTest('Drop down visibility', function() {
	expect(1);
	
	$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn').trigger('mousedown');

	setTimeout(function() {
		// czy lista wyboru jest widoczna
		var left = Number($j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn .chzn-drop').offset().left);

		ok(left > 0, 'Position should be > 0 (left=' + left + ')');
		start();

		// chowamy
		$j('#SLOWNIK_SLOWNIK_CENTRUMID_1_chzn .chzn-drop li:first').trigger('mouseup');
	}, 200);
});

module('Wide select');
asyncTest('Drop down width', function() {
	expect(2);

	$j('#ZWYKLY_ZWYKLY_CENTRUMID_1_chzn').trigger('mousedown');

	setTimeout(function() {
		// czy lista wyboru nie chowa się z lewej strony
		var left = Number($j('#ZWYKLY_ZWYKLY_CENTRUMID_1_chzn .chzn-drop').offset().left);

		// czy lista wyboru nie chowa się z prawej strony
		var right = Number($j('#ZWYKLY_ZWYKLY_CENTRUMID_1_chzn .chzn-drop').offset().left + $j('#ZWYKLY_ZWYKLY_CENTRUMID_1_chzn .chzn-drop').outerWidth()),
			windowRight = Number($j(window).width());

		ok(left > 0, _.string.sprintf('Left should be > 0 (left = %d)', left));
		ok(right < windowRight, _.string.sprintf('Right should be < %d (right = %d)', windowRight, right));
		
		start();

		// chowamy
		$j('#ZWYKLY_ZWYKLY_CENTRUMID_1_chzn .chzn-drop li:first').trigger('mouseup');
	}, 200);
});

var OldDwrTest = {
	getFieldsValues: DwrTest.getFieldsValues,
	setFieldsValues: DwrTest.setFieldsValues
};
var VALUES = {
	"SWITCHABLE_SEL": {
		allOptions: [{"": "-- wybierz --"}, {"1": "Jeden"}, {"2": "Dwa"}],
		selectedOptions: ['']
	}
};
module('Readonly flag for autocomplete', {
	setup: function() {
		DwrTest.setFieldsValues = function(values, dwrParams, obj) {
			var clonedFields = _.clone(Fields, true);
			if(values.SWITCH_READONLY.sender === true) {
				var field = _.find(clonedFields, {cn: 'SWITCHABLE_SEL'});
				if(values.SWITCH_READONLY.booleanData == true) {
					// console.info('Readonly = true');
					field.readonly = true;
				} else {
					// console.info('Readonly = false');
					field.readonly = false;
				}
				field.status = 'CHANGED';
				VALUES.SWITCH_READONLY = values.SWITCH_READONLY.booleanData;
			} else if(values.SWITCHABLE_SEL.sender === true) {
				console.info(values.SWITCHABLE_SEL);
				VALUES.SWITCHABLE_SEL.selectedOptions = values.SWITCHABLE_SEL.enumValuesData.selectedOptions;
			}

			obj.callback(clonedFields);
		}
		DwrTest.getFieldsValues = function(dwrParams, obj) {
			var vals = _.clone(VALUES, true);
			obj.callback(vals);
		}
	},
	teardown: function() {
		DwrTest.setFieldsValues = OldDwrTest.setFieldsValues;
		DwrTest.getFieldsValues = OldDwrTest.getFieldsValues;
	}
});
asyncTest('Setting readonly=true', function() {
	expect(1);

	// zanaczamy na readonly
	$j('#SWITCH_READONLY').attr('checked', true).trigger('change');
	setTimeout(function() {
		$j('#SWITCHABLE_SEL_chzn').trigger('mousedown');

		setTimeout(function() {
			// lista nie powinna sie pojawic
			ok($j('#SWITCHABLE_SEL_chzn .chzn-drop').is(':visible') === false, 'Readonly select should not show list');
			start();
		}, 100);
	}, 100);
});
asyncTest('Reverting readonly=false', function() {
	expect(1);

	// zaznaczamy readonly = false
	$j('#SWITCH_READONLY').removeAttr('checked').trigger('change');
	setTimeout(function() {
		$j('#SWITCHABLE_SEL_chzn').trigger('mousedown');

		setTimeout(function() {
			// lista powinna sie pojawic
			ok($j('#SWITCHABLE_SEL_chzn .chzn-drop').is(':visible') === true, 'Readonly select should not show list');
			start();

			// ustawiamy pierwszą wartość
			$j('#SWITCHABLE_SEL_chzn .chzn-results li:first').trigger('mouseup');
		}, 100);
	}, 100);
})