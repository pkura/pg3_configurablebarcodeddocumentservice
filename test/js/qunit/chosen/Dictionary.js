Dictionary = {
	getFieldsValues: function(dictionaryName, id, obj) {
		// console.info('dicname', dictionaryName);

		obj.callback(DictionaryValues);
	},

	getFieldValue: function(dictionaryName, fieldCn, obj) {
		obj.callback(FullDictionaryValues[dictionaryName][fieldCn]);
	},

	getFieldsValuesList: function(dictionaryName, ids, obj) {
		// console.info('getFieldsValuesList');
		obj.callback(DictionaryValuesList);
	},

	getMultipleFieldsValues: function(dwrParams, dictionaryName, values, obj) {
		obj.callback({});
	},

	dwrSearch: function() {
		// console.info('search');
	}
}

FullDictionaryValues = {
	'SLOWNIK': {
		'SLOWNIK_CENTRUMID': {
			"allOptions":[{"":"-- wybierz --"},{"38":"UMO-2014/05/N/ST10/03703"},{"167":"102/U/2012"},{"144":"172/U/2014 - Nowe produkty (Meteofight)"},{"145":"172/U/2014 - AWOS"},{"146":"172/U/2014 - MAWS"},{"147":"172/U/2014 - Infrastruktura"},{"148":"172/U/2014 - Najem"},{"149":"172/U/2014 - Certyfikacja"},{"150":"172/U/2014 - Szkolenia"},{"151":"172/U/2014"},{"136":"173/U/2014 Oslona morska"},{"162":"174/U/2014"},{"163":"174/U/2014 - Szkolenia"},{"164":"174/U/2014 - Wody graniczne"},{"165":"174/U/2014 - Certyfikacja"},{"166":"174/U/2014 - Alerty"},{"168":"190/U/2009 Koszty operacyjne C.2.10"},{"169":"C.2.12 Uslugi konsultingowe Sogreah"},{"170":"C.2.6 Realizacja pakietu"},{"46":"203/OGa/2013"},{"41":"14/OR/2012 [206]"},{"43":"207/OGa/2013 BZP-272-011/WS/13/MSz"},{"44":"EMODNET-Chemistry [208]"},{"47":"209/OGa/2013"},{"48":"214/OGa/2013"},{"45":"EMODNET-Biology [215]"},{"49":"218/OGa/2013"},{"50":"219/OGa/2013"},{"51":"222/OGa/2013"},{"40":"283607 SeaDataNet II [258]"},{"55":"3/U/2012"},{"58":"3/U/2013"},{"54":"43/U/2011"},{"57":"15/U/2012"},{"59":"8/U/2013"},{"138":"0410/U/2005 H-SAF"},{"52":"434/U/2006"},{"60":"11/U/2013"},{"61":"34/U/2013"},{"53":"37/U/2011"},{"56":"14/U2012"},{"198":"Zlecenia drobne nauka"},{"153":"1320/u/2006+Aneks2"},{"154":"1346/u/2006+Aneks1"},{"130":"1434/u/2007-GM BARTER Aneks nr 12 i 3  "},{"31":"1444/s/u/2007"},{"135":"1445/s/u/2007-YP"},{"32":"1446/u/2007"},{"87":"1535/u/2008  "},{"133":"1545/B/GP2008 KOMPENSATA"},{"131":"1608/GM/09 "},{"139":"1637/U/2012"},{"193":"2060/U/2013"},{"159":"2060/U/2013 - Zakupy centralne"},{"160":"2060/U/2013 - Remonty centralne"},{"161":"2060/U/2013 - Limnologia"},{"199":"Zlecenia drobne PSHM"},{"140":"2341/s/u/2005-YP"},{"137":"2403/U/05 Aneks nr 1"},{"42":"Monitoring Baltyku V etap"},{"33":"2539/U/13 Monitoring skazen II etap"},{"34":"2540/U/13 Monitoring skazen III etap"},{"35":"2541/U/13 Monitoring skazen IV etap"},{"36":"2545/U/13 Monitoring tla III etap"},{"37":"2546/U/13 Monitoring tla IV etap"},{"39":"2547/U/13 Monitoring tla V etap"},{"142":"2554/U/2013 Monitoring ozonu"},{"143":"2555/U/2013 Monitoring ozonu"},{"179":"METEO RISK Platformy informatyczne systemu"},{"180":"METEO RISK Implementacja modeli prognozowania numerycznego"},{"181":"METEO RISK Promocja projektu"},{"182":"METEO RISK Szkolenia"},{"183":"METEO RISK Zarzadzanie projektem"},{"184":"METEO RISK Wklad wlasny"},{"185":"METEO RISK Koszty niekwalifikowane"},{"171":"SAMBAH LIFE+ "},{"172":"559/2009/Wn-50/OP-RE-LF/D DOTACJA "},{"186":"EULAKES WP1 Zarzadzanie i koordynacja projektu"},{"187":"EULAKES WP2 Komunikacja zarzadzanie rozpowszechnianie"},{"188":"EULAKES WP3 Wdrozenie istniejacych systemów monitoringu"},{"189":"EULAKES WP4 Wrazliwosc i ocena ryzyka"},{"190":"EULAKES WP5 akcje pilotazowe"},{"191":"EULAKES WP6 Nowy model Zarzadzania srodowiskowego"},{"192":"EULAKES Koszty niekwalifikowane"},{"174":"BALTRAD+ WP1 Zarzadzanie i administracja"},{"175":"BALTRAD+ WP2 Komunikacja i informacja"},{"176":"BALTRAD+ WP3 Realizacja inwestycji miedzynarodowych"},{"177":"BALTRAD+ WP4 Inwestycje pilotazowe i wdrozenie"},{"178":"BALTRAD+ Koszty niekwalifikowane"},{"173":"BALSAM"},{"200":"Zlecenia drobne inne"},{"201":"Zlecenia drobne TKZ"},{"63":"10/U/OPo/2011"},{"64":"12/U/2001/OWr aneks nr 7"},{"65":"5616/U/13/OWr"},{"66":"5620/U/13/OWr"},{"67":"5624/U/13/OWr"},{"68":"5625/U/13/OWr"},{"69":"5626/U/13/OWr"},{"70":"5654/U/12/OWr"},{"71":"5655/U/13/OWr"},{"72":"5658/U/13/OWr"},{"62":"58/IxP/dem/2012"},{"73":"5665/U/11/OWr"},{"74":"5673/U/13/OWr"},{"75":"5677/U/13/OWr"},{"76":"5678/U/13/OWr"},{"77":"5679/U/13/OWr"},{"78":"5680/U/13/OWr"},{"79":"5681/U/13/OWr"},{"80":"5682/U/13/OWr"},{"81":"5820/U/13/OWr Monitoring chemizmu"},{"82":"5820/U/13/OWr Monitoring chemizmu"},{"83":"5820/U/13/OWr Monitoring chemizmu"},{"84":"5830/U/10/OWr"},{"88":"6051/GM/09  "},{"89":"6056/GM/09  "},{"90":"6068/GM/09 Aneks nr 123456  i 7 (umowa zablokowana na potrzeby reklam)  "},{"91":"6078/GM/09  "},{"92":"6133/GP/2010-GM  "},{"96":"6138/GM/2010 Aneks nr 1  "},{"155":"6166/DM/2010"},{"104":"6212/GP/2010-YP Aneks nr 123"},{"93":"6227/GM/2010  "},{"156":"6248/PD/2010+Aneks1"},{"94":"6254/GM/2010  "},{"134":"6259/GP/2010-YP "},{"95":"6286/GM/2011  "},{"157":"7060/PD/2011+Aneks1"},{"132":"7097/GW/2012-GM "},{"97":"7107/GM/2012 "},{"99":"7123/GW/2012-GM  "},{"141":"7131/U/2012"},{"98":"7134/GM/2012-YP  "},{"103":"7139/GM/2012-YP Aneks nr 1 "},{"102":"7146/YP/2012 Aneks nr 1  "},{"101":"7147/YP/2012 Aneks nr 1  "},{"85":"7211/OPo/2012"},{"105":"7212/YP/2012"},{"86":"7213/OPo/2012"},{"152":"7218/PD/2013"},{"106":"7235/YP/2013 Aneks nr 1 "},{"107":"7238/YP/2013 (KOMPENSATA) "},{"108":"7245/YP/2013 (KOMPENSATA) "},{"110":"7247/2013 (KOMPENSATA) "},{"109":"7249/YP/2013 (KOMPENSATA) "},{"111":"7253/YP/2013 "},{"113":"7258/YP/2013 "},{"114":"7262/YP/2013"},{"112":"7263/YP/2013 "},{"115":"7271/YP/2013 "},{"118":"7273/YP/2013 "},{"124":"7275/YP/2013 "},{"120":"7277/YP/2013 "},{"116":"7278/YP/2013 "},{"117":"7279/YP/2013 "},{"128":"7281/YP/2013 "},{"119":"7283/YP/2013 "},{"121":"7285/YP/2013 "},{"125":"7286/YP/2013 "},{"127":"7287/YP/2013 "},{"126":"7289/YP/2013 "},{"122":"7290/YP/2013 "},{"123":"7291/YP/2013 "},{"129":"7292/YP/2013 "},{"158":"7296/PF/2013"},{"194":"Koszty zakladowe - 507"},{"195":"Koszty dzialalnosci bytowej - 534"},{"196":"Koszty dzialalnosci pomocniczej - 535"},{"197":"Koszty ogólnoinstytutowe - 550"}],
			/*'allOptions': [
				{'': '-- wybierz --'},
				{'1': 'Kamil'},
				{'2': 'Omelańczuk'}
			],*/

			'selectedOptions': ['']
		},
		'SLOWNIK_ACCOUNTNUMBER': {
			'allOptions': [
				{'': '-- wybierz --'},
				{'1': 'Numer'},
				{'2': 'Konta'}
			],
			'selectedOptions': ['']			
		}
	},
	'ZWYKLY': {
		'ZWYKLY_CENTRUMID': {
			'allOptions': [{'': '-- wybierz --'}, {'1': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna'}],
			'selectedOptions': ['']
		}
	}
};

DictionaryValues = {
	"SLOWNIK_CENTRUMID": {
		"allOptions": [{
			"": "-- wybierz --"
		}],
		"selectedId": "",
		"selectedOptions": [""],
		"selectedValue": ""
	},
	"SLOWNIK_ACCOUNTNUMBER": {
		"allOptions": [
		{
			"": "-- wybierz --"
		},
		{
			"3": "Wybrany"
		}],
		"selectedOptions": ["3"],
	},
	'ZWYKLY_CENTRUMID': {
		'allOptions': [{'': '-- wybierz --'}, 
			{'1': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna'},
			{'2': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci. Aenean nec lorem. In porttitor. Donec laoreet nonummy augue. Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy. Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien. Donec ut est in lectus consequat consequat. Etiam eget dui. Aliquam erat volutpat. Sed at lorem in nunc porta tristique. Proin nec augue. Quisque aliquam tempor magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc ac magna. Maecenas odio dolor, vulputate vel, auctor ac, accumsan id, felis. Pellentesque cursus sagittis felis. Pellentesque porttitor, velit lacinia egestas auctor, diam eros tempus arcu, nec vulputate augue magna vel risus. Cras non magna vel ante adipiscing rhoncus. Vivamus a mi. Morbi neque. Aliquam erat volutpat'}],
		'selectedOptions': ['']
	}
};

DictionaryValuesList = [];

// tworzymy wartości
(function() {
	var i, fieldName, val, specVal, fullVal, specFullVal, j;

	for(i = 0; i < 10; i ++) {
		fieldName = 'SLOWNIK_' + dateFormat.i18n.pl.monthNames[i];
		val = {
			allOptions: [{
				'': '-- wybierz --'
			}],
			selectedOptions: [(i+1) + '']
		};

		specVal = {};
		specVal[(i+1) + ''] = (i+1) + '. ' + dateFormat.i18n.pl.monthNamesCase[i];
		val.allOptions.push(specVal);

		DictionaryValues[fieldName] = val;

		fulVal = {
			allOptions: [{
				'': '-- wybierz --'
			}],
			selectedOptions: ['']
		};

		for(j = 0; j < 400; j ++) {
			specFullVal = {};
			specFullVal[(j+1) + ''] = 'Opcja numer ' + (j+1);
			fulVal.allOptions.push(specFullVal);
		}

		FullDictionaryValues['SLOWNIK'][fieldName] = fulVal
	}

	for(i = 0; i < 10; i ++) {
		DictionaryValuesList.push(DictionaryValues);
	}
})();
