QUnit.config.reorder = false;

var OldDwrTest = {
	getFieldsValues: DwrTest.getFieldsValues,
	setFieldsValues: DwrTest.setFieldsValues
};

var VALUES = {
	'SUBMIT': {
		label: 'Ustaw pole na dwr',
		value: 'submit'
	}
};

module('Required flag', {
	setup: function() {
		DwrTest.setFieldsValues = function(values, dwrParams, obj) {
			console.info('setFieldsValues', values);
			var clonedFields = _.clone(Fields, true);
			if(values.SWITCH_REQUIRED.sender === true) {
				var field = _.find(clonedFields, {cn: 'HTML_EDITOR'});
				if(values.SWITCH_REQUIRED.booleanData == true) {
					// console.info('required = true');
					field.required = true;
				} else {
					// console.info('required = false');
					field.required = false;
				}
				field.status = 'CHANGED';
				VALUES.SWITCH_REQUIRED = values.SWITCH_REQUIRED.booleanData;
			} else if (values.SUBMIT.sender === true) {
				VALUES.HTML_EDITOR = 'dwr';
			}

			obj.callback(clonedFields);
		}
		DwrTest.getFieldsValues = function(dwrParams, obj) {
			var vals = _.clone(VALUES, true);
			console.info(vals);
			obj.callback(vals);
		}
	},
	teardown: function() {
		/*DwrTest.setFieldsValues = OldDwrTest.setFieldsValues;
		DwrTest.getFieldsValues = OldDwrTest.getFieldsValues;*/
	}
});

asyncTest('Field is empty', function() {
	expect(1);
	setTimeout(function() {
		ok(fieldsManager.validate() === false, 'Field is required and empty -- dont allow save');
		start();
	}, 100);
});
asyncTest('Field is not empty', function() {
	expect(1);
	$j('#SUBMIT').trigger('click');
	setTimeout(function() {
		ok(fieldsManager.validate() === true, 'Field is required and not empty -- allow save');
		start();
	}, 100);
});

