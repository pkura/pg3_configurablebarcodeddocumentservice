DwrTest = {
	getFieldsList: function(dwrParams, obj) {
		// console.info('getFieldsList');
		setTimeout(function() {
			obj.callback(Fields);
		}, 1);
	},

	getFieldsValues: function(dwrParams, obj) {
		var values = {};
		
		setTimeout(function() {
			obj.callback(FieldsValues);
		}, 1);
	},

	setFieldsValues: function(values, dwrParams, obj) {
		var ret = Fields;

		setTimeout(function() {
			console.warn('setFieldsValues', ret);
			obj.callback(ret);
		}, 1);
	}
}

Fields = [{
	"type": "HTML_EDITOR",
	"cn": "HTML_EDITOR",
	"label": "Edytor HTML",
	"cssClass": null,
	"status": "ADD",
	"submit": true,
	"submitTimeout": 0,
	"column": 0,
	"fieldset": "default",
	"kind": "NORMAL",
	"validatorRegExp": null,
	"validatorMessage": null,
	"format": null,
	"required": true,
	"disabled": false,
	"info": "",
	"params": null,
	"hidden": false,
	"autocomplete": false,
	"autocompleteRegExp": null,
	"dictionary": null,
	"newItemMessage": "",
	"coords": "a1",
	"readonly": false
},
{
	"autocomplete": false,
	"autocompleteRegExp": "%",
	"autofocus": false,
	"cn": "SWITCH_REQUIRED",
	"column": 0,
	"coords": "a2",
	"cssClass": "",
	"dictionary": null,
	"disabled": false,
	"fieldset": "",
	"format": null,
	"hidden": false,
	"hiddenOnDisabledEmpty": false,
	"info": null,
	"kind": "NORMAL",
	"label": "Required",
	"linkTarget": null,
	"listenersList": null,
	"multiple": false,
	"newItemMessage": null,
	"onchange": null,
	"params": null,
	"popups": {
		
	},
	"readonly": false,
	"required": false,
	"status": "ADD",
	"submit": true,
	"submitForm": null,
	"submitTimeout": 0,
	"type": "BOOLEAN",
	"validate": false,
	"validatorMessage": null,
	"validatorRegExp": null,
	"lazyEnumLoad": false
},
{
	"autocomplete": false,
	"autocompleteRegExp": "%",
	"autofocus": false,
	"cn": "SUBMIT",
	"column": 0,
	"coords": "a3",
	"cssClass": "dwr-no-label",
	"dictionary": null,
	"disabled": false,
	"fieldset": "",
	"format": null,
	"hidden": false,
	"hiddenOnDisabledEmpty": false,
	"info": null,
	"kind": "NORMAL",
	"label": "Submit",
	"linkTarget": null,
	"listenersList": null,
	"multiple": false,
	"newItemMessage": null,
	"onchange": null,
	"params": null,
	"popups": {
		
	},
	"readonly": false,
	"required": false,
	"status": "ADD",
	"submit": true,
	"submitForm": null,
	"submitTimeout": 0,
	"type": "BUTTON",
	"validate": false,
	"validatorMessage": null,
	"validatorRegExp": null,
	"lazyEnumLoad": false
}
];

FieldsValues = {
	'SUBMIT': {
		label: 'Ustaw pole na dwr',
		value: 'submit'
	}
};