QUnit.config.reorder = false;

function copyFields(fields) {
	var ret = [], field = {};
	for(var i = 0; i < fields.length; i ++) {
		field = {};
		$j.extend(field, fields[i]);
		if(field.status == 'REMOVE') {
			field.status = 'ADD';
		}
		ret.push(field);
	}

	return ret;
}

(function() {
	var oldDwrTest = {};
	var fieldsValues = {};
	$j.extend(fieldsValues, FieldsValues);
	module('Grid toggle field', {
		setup: function() {
			oldDwrTest.setFieldsValues = DwrTest.setFieldsValues;
			oldDwrTest.getFieldsValues = DwrTest.getFieldsValues;
			
			DwrTest.setFieldsValues = function(values, dwrParams, obj) {
				var ret = null;
				if(values != null) {
					if(values.pole_jeden != null && values.pole_jeden.sender === true) {
						var val = values.pole_jeden.stringData;
						if(val == 'schowaj') {
							ret = copyFields(Fields);
							ret.length = 1;
						} else if(val == 'pokaz') {
							ret = copyFields(Fields);
						} else {
							ret = copyFields(Fields);
						}
					} 

					for(var cn in values) {
						fieldsValues[cn] = BaseField.unpackValues(values[cn]);
					}
				}


				setTimeout(function() {
					console.assert(ret != null, 'Fields values should not be null');
					obj.callback(ret);
				}, 1);
			};

			DwrTest.getFieldsValues = function(dwrParams, obj) {
				obj.callback(fieldsValues);
			};
		},

		teardown: function() {
			DwrTest.setFieldsValues = oldDwrTest.setFieldsValues;
			DwrTest.getFieldsValues = oldDwrTest.getFieldsValues;
		}
	});
	asyncTest('Hiding field', function() {
		expect(2);
		setTimeout(function() {
			$j('#pole_jeden').val('schowaj').trigger('change');
			setTimeout(function() {
				ok($j('#pole_jeden').is(':visible'), 'Field pole_jeden should be visible');
				ok($j('#pole_dwa').length === 0, 'Field pole_dwa should be hidden');
				start();
				console.info('jeden');
			}, 100);
		}, 100);
	});
	asyncTest('Showing field', function() {
		expect(2);
		setTimeout(function() {
			$j('#pole_jeden').val('pokaz').trigger('change');
			setTimeout(function() {
				ok($j('#pole_jeden').is(':visible'), 'Field pole_jeden should be visible');
				ok($j('#pole_dwa').is(':visible'), 'Field pole_dwa should be visible');
				start();
				console.info('dwa');
			}, 100);
		}, 100);
	});
	asyncTest('Colspan and rowspan', function() {
		// expect(3);

		function isCellPresent(coords) {
			var parsedCoords = Grid.parseCoords(coords);
			return $j('.' + parsedCoords.start.className).length === 1;
		}

		setTimeout(function() {
			// Tre��
			ok(isCellPresent('a4'), 'Tre��');
			// Kontrahent
			ok(isCellPresent('g1'), 'Kontrahent');
			// below kontrahent
			ok(isCellPresent('g6'), 'Cell G6');
			ok(isCellPresent('h6'), 'Cell H6');
			ok(isCellPresent('i6'), 'Cell I6');

			// row #5 should be present and empty
			ok($j('tr[index=5]').length === 1, 'Row #5 should be present');
			ok($j('tr[index=5]').children('td').length === 0, 'Row #5 should be empty');

			// merged area
			ok(isCellPresent('h7') !== true, 'Not cell H7');
			start();
		}, 100);
	});
})();
