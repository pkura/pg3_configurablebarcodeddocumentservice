QUnit.config.reorder = false;

module('Add and remove items');
asyncTest('Adding 9 items', function() {
	expect(1);

	for( var i = 0; i < 9; i ++) {
		$j('a.addItem').click();
	}
	
	setTimeout(function() {
		ok($j('.multipleFieldsNumber_10').length == 1, ' Row #10 is present');
		start();
	}, 100);
});

asyncTest('Deleteing item #9', function() {
	expect(1);
	$j('.multipleFieldsNumber_9 .delete_dictionary_item').click();
	setTimeout(function() {
		ok($j('.multipleFieldsNumber_10').length == 0, ' Row #10 renamed to #9');
		start();

	}, 100);
});

asyncTest('Adding item #10', function() {
	expect(1);
	$j('a.addItem').click();
	setTimeout(function() {
		ok($j('.multipleFieldsNumber_10').length == 1, ' Row #10 was added');
		start();

	}, 100);
});