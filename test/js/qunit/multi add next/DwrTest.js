DwrTest = {
	getFieldsList: function(dwrParams, obj) {
		// console.info('getFieldsList');
		obj.callback(Fields);
	},

	getFieldsValues: function(dwrParams, obj) {
		var values = {
			"DWR_SLOWNIK": DictionaryIdList,
			"DWR_DODAJ_NASTEPNY": {label: "Dodaj", value: "ADD", clicked: false}
		};
		console.warn('getFieldsValues', values);
		obj.callback(values);
	},

	setFieldsValues: function(values, dwrParams, obj) {
		console.info('setFieldsValues', values);

		if(values != null && values.DWR_DODAJ_NASTEPNY != null && values.DWR_DODAJ_NASTEPNY.sender === true) {
			buttonClick();
		}

		obj.callback(Fields);
	}
}

DictionaryIdList = [1];

Fields = [
	{
		"autocomplete": false,
		"autocompleteRegExp": null,
		"autofocus": false,
		"cn": "DWR_SLOWNIK",
		"column": 0,
		"coords": "a1:a1",
		"cssClass": "dwr-disabled-higher-contrast dwr-horizontal-dic-title",
		"dictionary": {
			"autohideMultipleDictionary": false,
			"cantUpdate": false,
			"dicButtons": ["doAdd",
			"doClear",
			"doRemove"],
			"disabledSubmit": false,
			"fields": [{
				"autocomplete": false,
				"autocompleteRegExp": null,
				"autofocus": false,
				"cn": "SLOWNIK_ID_1",
				"column": 0,
				"coords": null,
				"cssClass": "",
				"dictionary": null,
				"disabled": false,
				"fieldset": "SLOWNIK_fieldset",
				"format": null,
				"hidden": true,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "DICTIONARY_FIELD",
				"label": "ID",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": false,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "INTEGER",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null
			},
			{
				"autocomplete": true,
				"autocompleteRegExp": "%",
				"autofocus": false,
				"cn": "SLOWNIK_CENTRUMID_1",
				"column": 0,
				"coords": null,
				"cssClass": "dwr-disabled-higher-contrast",
				"dictionary": null,
				"disabled": false,
				"fieldset": "SLOWNIK_fieldset",
				"format": null,
				"hidden": false,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "DICTIONARY_FIELD",
				"label": "Podmiot",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": true,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "ENUM",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null,
				"lazyEnumLoad": true
			},
			{
				"autocomplete": true,
				"autocompleteRegExp": "%",
				"autofocus": false,
				"cn": "SLOWNIK_ACCOUNTNUMBER_1",
				"column": 0,
				"coords": null,
				"cssClass": "dwr-disabled-higher-contrast",
				"dictionary": null,
				"disabled": false,
				"fieldset": "SLOWNIK_fieldset",
				"format": null,
				"hidden": false,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "DICTIONARY_FIELD",
				"label": "CPK",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": true,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "ENUM",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null,
				"lazyEnumLoad": true
			}],
			"jsPopupFunction": null,
			"multiple": true,
			"name": "SLOWNIK",
			"oldField": null,
			"onlyPopUpCreate": false,
			"popUpButtons": ["doAdd",
			"doEdit",
			"doRemove",
			"doSelect",
			"doClear"],
			"popUpFields": [{
				"autocomplete": false,
				"autocompleteRegExp": null,
				"autofocus": false,
				"cn": "SLOWNIK_ID_1",
				"column": 0,
				"coords": null,
				"cssClass": "",
				"dictionary": null,
				"disabled": false,
				"fieldset": "default",
				"format": null,
				"hidden": true,
				"hiddenOnDisabledEmpty": false,
				"info": null,
				"kind": "NORMAL",
				"label": "SLOWNIK_ID_1",
				"linkTarget": null,
				"listenersList": null,
				"multiple": false,
				"newItemMessage": null,
				"onchange": null,
				"params": null,
				"popups": {
					
				},
				"readonly": false,
				"required": false,
				"status": "ADD",
				"submit": true,
				"submitForm": null,
				"submitTimeout": 0,
				"type": "INTEGER",
				"validate": false,
				"validatorMessage": null,
				"validatorRegExp": null
			}],
			"promptFieldsCns": [],
			"refreshMultipleDictionary": true,
			"searchableFieldsCns": [],
			"showDictionaryPopup": false,
			"tableView": true,
			"visibleFieldsCns": ["SLOWNIK_CENTRUMID_1",
			"SLOWNIK_ACCOUNTNUMBER_1"],
			"loadingMode": "ALL_AT_ONCE"
		},
		"disabled": false,
		"fieldset": "default",
		"format": null,
		"hidden": false,
		"hiddenOnDisabledEmpty": false,
		"info": null,
		"kind": "NORMAL",
		"label": "Pozycje budzetowe",
		"linkTarget": null,
		"listenersList": null,
		"multiple": false,
		"newItemMessage": "NowyWpis",
		"onchange": null,
		"params": null,
		"popups": {
			
		},
		"readonly": false,
		"required": true,
		"status": "ADD",
		"submit": true,
		"submitForm": null,
		"submitTimeout": 0,
		"type": "DICTIONARY",
		"validate": false,
		"validatorMessage": null,
		"validatorRegExp": null
	}
];


// dodajemy pola do slownika
(function() {
	var i, fieldName, field;

	for(i = 0; i < 1; i ++) {
		fieldName = 'SLOWNIK_' + dateFormat.i18n.pl.monthNames[i] + '_1';
		field = {
			"autocomplete": true,
			"autocompleteRegExp": "%",
			"autofocus": false,
			"cn": fieldName,
			"column": 0,
			"coords": null,
			"cssClass": "dwr-disabled-higher-contrast",
			"dictionary": null,
			"disabled": false,
			"fieldset": "SLOWNIK_fieldset",
			"format": null,
			"hidden": false,
			"hiddenOnDisabledEmpty": false,
			"info": null,
			"kind": "DICTIONARY_FIELD",
			"label": dateFormat.i18n.pl.monthNames[i+12],
			"linkTarget": null,
			"listenersList": null,
			"multiple": false,
			"newItemMessage": null,
			"onchange": null,
			"params": null,
			"popups": {
				
			},
			"readonly": false,
			"required": true,
			"status": "ADD",
			"submit": true,
			"submitForm": null,
			"submitTimeout": 0,
			"type": "ENUM",
			"validate": false,
			"validatorMessage": null,
			"validatorRegExp": null,
			"lazyEnumLoad": true
		};

		Fields[0].dictionary.fields.push(field);
		Fields[0].dictionary.visibleFieldsCns.push(fieldName);
	}
})();

// guzik
Fields.push(
{
	"autocomplete": false,
	"autocompleteRegExp": null,
	"autofocus": false,
	"cn": "DWR_DODAJ_NASTEPNY",
	"column": 0,
	"coords": "a2",
	"cssClass": "dwr-no-label",
	"disabled": false,
	"fieldset": "default",
	"format": null,
	"hidden": false,
	"hiddenOnDisabledEmpty": false,
	"info": null,
	"kind": "NORMAL",
	"label": "guzik",
	"linkTarget": null,
	"listenersList": null,
	"multiple": false,
	"newItemMessage": "NowyWpis",
	"onchange": null,
	"params": null,
	"popups": {
		
	},
	"readonly": false,
	"required": true,
	"status": "ADD",
	"submit": true,
	"submitForm": null,
	"submitTimeout": 0,
	"type": "BUTTON",
	"validate": false,
	"validatorMessage": null,
	"validatorRegExp": null
});
