var buttonClick = (function() {
	var id = 1;

	return function() {
		DictionaryIdList.push(++id);
		var len = DictionaryValuesList.length;
		var lastItem = DictionaryValuesList[len-1];
		var newItem = {};
		$j.extend(true, newItem, lastItem);


		console.info(newItem);
		newItem.SLOWNIK_Sty.allOptions[1]['1'] = 'Numer ' + id;
		DictionaryValuesList.push(newItem);
	}

})();

module('Add next');
test('Adding 2 items', function() {
	expect(4);
	stop();

	// żeby js zdążył narysować
	setTimeout(function() {
		$j('#DWR_DODAJ_NASTEPNY').click();

		setTimeout(function() {
			var jqChosen = $j('#SLOWNIK_Sty_2_chzn');
			equal(jqChosen.length, 1, "Second row created");
			equal($j('#SLOWNIK_Sty_2_chzn').text(), "Numer 2", "Second item label present");

			start();
			stop();
		}, 400);

		setTimeout(function() {
				$j('#DWR_DODAJ_NASTEPNY').click();

				jqChosen = $j('#SLOWNIK_Sty_3_chzn');
				equal(jqChosen.length, 1, "Third row created");
				equal($j('#SLOWNIK_Sty_3_chzn').text(), "Numer 3", "Third item label present");

				start();
			}, 800);		
	}, 400);
	
});