QUnit.config.reorder = false;

// sprawdzamy czy po kliknięciu wywoływuje się żądanie ajaksowe
window.oldGetFieldValue = Dictionary.getFieldValue;
Dictionary.getFieldValue = function() {
	console.info('new getFieldValue');
	var args = arguments;

	// czekamy 2000ms, żeby zasymulować ajax
	(function(ctx) {
		setTimeout(function() {
			oldGetFieldValue.apply(ctx, args);
		}, 1000);
	})(this);
}

var expected = {
	"type": "ENUM",
	"enumValuesData": {
		"allOptions": [{
			"": "-- wybierz --"
		},
		{
			"1": "Kamil"
		},
		{
			"2": "Omelańczuk"
		}],
		"selectedOptions": [""]
	}
};

module('First row');
test('Invoking request', function() {
	expect(1);
	stop();
	$j('#MPK_MPK_CENTRUMID_1_chzn').trigger('mousedown');

	// czy wczytał wartości
	setTimeout(function() {
		var actual = fieldsManager.fields.DWR_MPK.dictionaryFieldsManager.fields.MPK_CENTRUMID_1.getValue();
		deepEqual(actual.enumValuesData.allOptions, expected.enumValuesData.allOptions, "Returned values");
		start();
		// zamykamy
		$j('#MPK_MPK_CENTRUMID_1_chzn').click();
	}, 2000);
});

// dodaj następny
module('Second row');
test('Add next', function() {
	expect(2);
	stop();
	$j('a.addItem').click();

	// czekamy, żeby js zdążył narysować
	setTimeout(function() {
		ok($j('#MPK_CENTRUMID_2_chzn').length == 1, "Second row created");
		$j('#MPK_CENTRUMID_2_chzn').trigger('mousedown');

		// czy wczytał wartości
		setTimeout(function() {
			var actual = fieldsManager.fields.DWR_MPK.dictionaryFieldsManager.fields.MPK_CENTRUMID_2.getValue();
			deepEqual(actual.enumValuesData.allOptions, expected.enumValuesData.allOptions, "Returned value");	
			start();		

		// zamykamy
		$j('#MPK_CENTRUMID_2_chzn').click();			
		}, 2000);
	}, 1000);
});

//kliknięcie na disabled
module('Disabled item')
test('Disabled click', function() {
	expect(1);
	stop();
	$j('#MPK_MPK_ACCOUNTNUMBER_1_chzn').trigger('mousedown');
	setTimeout(function() {
		Dictionary.getFieldsValues('', '', {callback: function(data) {
			var actual = fieldsManager.fields.DWR_MPK.dictionaryFieldsManager.fields.MPK_ACCOUNTNUMBER_1.getValue().enumValuesData;
			var expected = data.MPK_ACCOUNTNUMBER;
			deepEqual(actual, expected, "Enum value should not change");
			start();		    
		}});
	}, 2000);
});