Dictionary = {
	getFieldsValues: function(dictionaryName, id, obj) {
		// console.info('dicname', dictionaryName);
		var vals = {
			"MPK_CENTRUMID": {
				"allOptions": [{
					"": "-- wybierz --"
				}],
				"selectedId": "",
				"selectedOptions": [""],
				"selectedValue": ""
			},
			"MPK_ACCOUNTNUMBER": {
				"allOptions": [
				{
					"": "-- wybierz --"
				},
				{
					"3": "Wybrany"
				}],
				"selectedOptions": ["3"],
			}
		};

		obj.callback(vals);
	},

	getFieldValue: function(dictionaryName, fieldCn, obj) {
		obj.callback(FullDictionaryValues[dictionaryName][fieldCn]);
	},

	getFieldsValuesList: function(dictionaryName, ids, obj) {
		// console.info('getFieldsValuesList');
		obj.callback({});
	},

	getMultipleFieldsValues: function(dwrParams, dictionaryName, values, obj) {
		obj.callback({});
	},

	dwrSearch: function() {
		// console.info('search');
	}
}

FullDictionaryValues = {
	'MPK': {
		'MPK_CENTRUMID': {
			'allOptions': [
				{'': '-- wybierz --'},
				{'1': 'Kamil'},
				{'2': 'Omelańczuk'}
			],
			'selectedOptions': ['']
		},
		'MPK_ACCOUNTNUMBER': {
			'allOptions': [
				{'': '-- wybierz --'},
				{'1': 'Numer'},
				{'2': 'Konta'}
			],
			'selectedOptions': ['']			
		}
	}
};