Testy dla dwr js z uzyciem grunt, qunit i phantomjs
1. Zainstaluj nodejs razem z npm
2. Uruchom "npm install" w katalogu, w którym jest ten plik (README.md)
3. Uruchom grunt - wszystkie testy powinny się uruchomić automatycznie

Jeśli chcesz uruchomić testy na Internet Explorer w wersji zainstalowanej na twoim systemie, to
1. Ściągnij Chutzpah http://chutzpah.codeplex.com/ - potrzebny jest też .NET v4
2. Rozpakuj do np.: c:\Chutzpah i dodaj do ścieżki systemowej (PATH)
3. Uruchom test-ie.bat w katalagu, w którym jest ten plik (README.md)
