module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		qunit: {
			all: ['qunit/**/*.htm'],
			'hide-empty-select': ['qunit/hide empty select/hide empty select.htm'],
			'refresh-multiple-dictionary': ['qunit/refresh multiple dictionary/refresh multiple dictionary.htm'],
			'chosen': ['qunit/chosen/chosen test.htm'],
			'html-editor': ['qunit/html editor/html editor.htm']
		},
		watch: {
			all: {
				files: ['../../web/dwrdockind/Field.js'],
				tasks: ['qunit']
			},
			'hide-empty-select': {
				files: ['../../web/dwrdockind/Field.js', '../../web/dwrdockind/FieldsManager.js', '../../web/dwrdockind/MultipleFieldsHelper.js',
					'../../web/dwrdockind/DictionaryPopup.js', 'qunit/multiple dictionary/*.*'],
				tasks: ['qunit:hide-empty-select', 'qunit:chosen']
			},
			'chosen': {
				files: ['../../web/dwrdockind/chosen.jquery.js'],
				tasks: ['qunit:hide-empty-select', 'qunit:chosen']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-qunit');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['qunit']);
}