package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.beust.jcommander.internal.Lists;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import edu.emory.mathcs.backport.java.util.Collections;
import pl.compan.docusafe.api.user.office.*;
import pl.compan.docusafe.core.certificates.ElectronicSignature;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.TestUtils;

public class RestProcessServiceTest extends RestAbstractRpcTest{

    static String cookie;

    public ObjectMapper mapper = new ObjectMapper();
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_JSON);
    
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Test
	public void testGetProcessActions() {
		log.debug("\tTest wyszukiwania przycisków procesowych dla documentu");

		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String, String> var = Collections.singletonMap("documentId", TestUtils.getTestProperty("rest.document.get"));

		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/process/{documentId}/document/actions"),
																  HttpMethod.GET,
																  requestEntity,
																  ListDtos.class,
																  var);

		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());

        ListDtos<KeyElement> list = responseEntity.getBody();
		assertFalse("Empty list of actions on document - ", CollectionUtils.isEmpty(list.getItems()));
	}

	@Test
	public void testExecuteProcessAction() {
		log.debug("\tTest wykonywanie akcji dla dokumentu");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String, String> vars = new LinkedHashMap<String, String>();
		vars.put("documentId", TestUtils.getTestProperty("rest.document.get"));
		vars.put("processAction", TestUtils.getTestProperty("rest.document.processAction"));

		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<Object> responseEntity = template.exchange(getUrl("/process/{documentId}/documentId/{processAction}/execute"), HttpMethod.POST,
																requestEntity, Object.class, vars);

		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertEquals("We espect ACCEPTED status code - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());

	}

    @Test
    public void testAssignMultiManualAssignment() throws Exception{
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);

        List<ManualAssignmentDto> elements = Lists.newArrayList();
        
        String targetName = TestUtils.getTestProperty("rest.document.process.target.name");
		Boolean kind = Boolean.valueOf(TestUtils.getTestProperty("rest.document.process.kind"));
		Boolean targetKind = Boolean.valueOf(TestUtils.getTestProperty("rest.document.process.kind.target"));
		elements.add(new ManualAssignmentDto(targetName ,kind , targetKind ));

        HttpEntity<String> requestEntity = new HttpEntity<String>(mapper.writeValueAsString(new ListDtos<ManualAssignmentDto>(elements)),headers);
        Map<String, String> vars = new LinkedHashMap<String, String>();
        vars.put("documentId", TestUtils.getTestProperty("rest.document.get"));

        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<Object> responseEntity = template.exchange(getUrl("/process/{documentId}/manual-assign"), HttpMethod.POST,
                requestEntity, Object.class, vars);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertEquals("We espect ACCEPTED status code - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());

    }

    @Test
    public void testGetDocumentProcesses() {
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);

        Map<String, String> vars = new LinkedHashMap<String, String>();
        vars.put("documentId", TestUtils.getTestProperty("rest.document.get"));
        //vars.put("documentId", "424");

        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/document/{documentId}/processes"), HttpMethod.GET,
                requestEntity, ListDtos.class, vars);

        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testGetProcessDocuments() {
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);

        Map<String, String> vars = new LinkedHashMap<String, String>();
        vars.put("processName", TestUtils.getTestProperty("rest.document.processName"));

        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<ListIds> responseEntity = template.exchange(getUrl("/process/{processName}/documents"), HttpMethod.GET,
                requestEntity, ListIds.class, vars);

        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testGetProcessDefinitions() {
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);

        Map<String, String> vars = new LinkedHashMap<String, String>();
        vars.put("processName", TestUtils.getTestProperty("rest.document.processName"));

        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<String> responseEntity = template.exchange(getUrl("/process/{processName}/definition"), HttpMethod.GET,
                requestEntity, String.class, vars);

        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testLoadProcessDefinitions() throws IOException {
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);

        Map<String, String> vars = new LinkedHashMap<String, String>();
        //actitviti wymaga rozszerzenia podczas ladowania
        vars.put("processName", TestUtils.getTestProperty("rest.document.processName") + ".bpmn");

        File f = new File(TestUtils.getTestProperty("rest.test.path") + "/zadanie.bpmn");
        String s = FileUtils.encodeByBase64(f);
        KeyElement ke = new KeyElement(null, s);
        HttpEntity<String> requestEntity = new HttpEntity<String>(mapper.writeValueAsString(ke),headers);
        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<String> responseEntity = template.exchange(getUrl("/process/{processName}/definition"), HttpMethod.POST,
                requestEntity, String.class, vars);

        assertNotNull("Empty response is null - ",responseEntity);
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testGetProcessDiagram() throws IOException {
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);

        Map<String, String> vars = new LinkedHashMap<String, String>();
        vars.put("processId", TestUtils.getTestProperty("rest.document.process.instance.id"));

        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<String> responseEntity = template.exchange(getUrl("/process/{processId}/diagram"), HttpMethod.GET,
                requestEntity, String.class, vars);

        byte[] encodedDocument = responseEntity.getBody().getBytes();
        byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

        File tempFile = new File(TestUtils.getTestProperty(TestUtils.REST_TEST_PATH) + "/process_" + vars.get("processId") + ".png");
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
    }

}
