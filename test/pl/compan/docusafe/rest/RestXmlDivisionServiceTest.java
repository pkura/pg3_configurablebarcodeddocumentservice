package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.hql.CollectionSubqueryFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import edu.emory.mathcs.backport.java.util.Collections;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * Test use the same id division in test function create | update | delete
 * @author kuras
 *
 */
public class RestXmlDivisionServiceTest extends RestAbstractRpcTest{

    static String cookie;

    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_XML);
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Test
	public void testCreate_NewDivision() throws JAXBException {
		log.debug("\tTest tworzenia nowego dzia�u");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        headers.setAccept(Arrays.asList(new MediaType(MediaType.TEXT_HTML.getType(), MediaType.TEXT_HTML.getSubtype(), Charset.forName("UTF-8"))));

		String nameOfDivision = TestUtils.getTestProperty("rest.api.division.create");
		DivisionType divisionType = DivisionType.valueOf(TestUtils.getTestProperty("rest.api.division.type"));
		String code = TestUtils.getTestProperty("rest.api.division.code");
		Long parentId = Long.valueOf(TestUtils.getTestProperty("rest.api.division.parentId"));
		
		DivisionDto divisionDto = createDivisionDto("xml", nameOfDivision , divisionType , code, parentId);
		String xmlDivision = MarshallerUtils.marshall(divisionDto);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlDivision ,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/division"),
																	HttpMethod.POST,
																	requestEntity,
																	String.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Status code must be CREATED - ",HttpStatus.CREATED, responseEntity.getStatusCode());
        
	}
	
	@Test
	public void testGetAllDivisions() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest pobierania wszystkich dzia��w");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/division"), HttpMethod.GET, requestEntity, ListDtos.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("There is no OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        List<DivisionDto> divisionDtos = responseEntity.getBody().getItems();
		assertFalse("Must not be null - ", CollectionUtils.isEmpty(divisionDtos));
        
	}

	@Test
	public void testGetDivisionById() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest pobierania dzia�u");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		String idDivision = TestUtils.getTestProperty("rest.api.division.get");
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<DivisionDto> responseEntity = template.exchange(getUrl("/division/{divisionId}"),
																	HttpMethod.GET,
																	requestEntity,
																	DivisionDto.class,
																	var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("There is no OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
	}

	@Test
	public void testGetDivisionByGuid() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest pobierania dzia�u dla guid rootdivision");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		String guid = TestUtils.getTestProperty("rest.api.division.guid");
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<DivisionDto> responseEntity = template.exchange(getUrl("/division?guid=" + guid),
																	HttpMethod.GET,
																	requestEntity,
																	DivisionDto.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("There is no OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        

	}

	@Test
	public void testGetUsersFromDivision() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest pobierania u�ytkownik�w dla dzia�u");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Long divisionId = new Long(TestUtils.getTestProperty("rest.api.division.get"));
		Map<String, Long> var = Collections.singletonMap("divisionId", divisionId );
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/division/{divisionId}/user"),
																	HttpMethod.GET,
																	requestEntity,
																	ListDtos.class,
																	var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("There is no OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());

	}

	@Test
	public void testAddUserToDivision() throws JAXBException {
		log.debug("\tTest dodawania u�ytkownika do dzia�u");
		
		// przygotowanie
		String idDivision = TestUtils.getTestProperty("rest.api.division.get");
		Long userId = new Long(TestUtils.getTestProperty("rest.api.division.userId"));
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        headers.setAccept(Arrays.asList(new MediaType(MediaType.TEXT_HTML.getType(), MediaType.TEXT_HTML.getSubtype(), Charset.forName("UTF-8"))));
		
		UserDto userDto = getUserById(cookie, userId.toString(),MediaType.APPLICATION_XML);
		String xmlRequest = MarshallerUtils.marshall(userDto);

		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest ,headers);
		Map<String, Long> var = Collections.singletonMap("divisionId", idDivision);
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/division/{divisionId}/user"),
																	HttpMethod.POST,
																	requestEntity,
																	String.class,
																	var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("No add user to division - ",HttpStatus.CREATED, responseEntity.getStatusCode());
        
	}


	@Test
	public void testRemoveUserFromDivision() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest usuwania u�ytkownika z dzia�u");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		String idDivision = TestUtils.getTestProperty("rest.api.division.get");
		Long userId = new Long(TestUtils.getTestProperty("rest.api.division.userId"));
		

		HttpEntity<String> requestEntity = new HttpEntity<String>(null ,headers);
		
		
		Map<String, Long> vars = new HashMap<String, Long>();
		vars.put("divisionId", new Long(idDivision)); 
		vars.put("userId", userId);
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<Object> responseEntity = template.exchange(getUrl("/division/{divisionId}/user/{userId}"),
																	HttpMethod.DELETE,
																	requestEntity,
																	Object.class,
																	vars);
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNull("Must be empty body response  - ",responseEntity.getBody());
        assertEquals("No remove user to division - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());
	}

	@Test
	public void testGetSubdivisions() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest pobierania wszystkich poddzia��w z dzia�u");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		Long divisionId = new Long(TestUtils.getTestProperty("rest.api.division.get"));
		HttpEntity<String> requestEntity = new HttpEntity<String>(null ,headers);
		
		Map<String, Long> var = Collections.singletonMap("divisionId", divisionId);
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/division/{divisionId}/division"),
																	HttpMethod.GET,
																	requestEntity,
																	ListDtos.class,
																	var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Status code must be OK - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        List<DivisionDto> divisionDtos = responseEntity.getBody().getItems();
        assertFalse("Must be not empty collections of divisions - ", CollectionUtils.isEmpty(divisionDtos));
	}

	@Test
	public void testUpdateDivision() throws JAXBException  {
		log.debug("\tTest aktualizowania dzia�u");
		
		// przygotowanie
		String idDivision = TestUtils.getTestProperty("rest.api.division.get");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		String nameOfDivision = TestUtils.getTestProperty("rest.api.division.update.name");
		Long parentId = new Long(TestUtils.getTestProperty("rest.api.division.parentId"));
		
		DivisionDto divisionDto = updateDivisionDto(idDivision, nameOfDivision, parentId);
		String xmlDivision = MarshallerUtils.marshall(divisionDto);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlDivision ,headers);
		Map<String,Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<Object> responseEntity = template.exchange(getUrl("/division/{divisionId}"),
																	HttpMethod.PUT,
																	requestEntity,
																	Object.class,
																	var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNull("Must be empty body response  - ",responseEntity.getBody());
        assertEquals("Status code must be NO_CONTENT - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());
	}

	@Test
	public void testDeleteDivision() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest usuwania dzia�u");
		
		// przygotowanie
		String idDivision = TestUtils.getTestProperty("rest.api.division.get");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String,Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<Object> responseEntity = template.exchange(getUrl("/division/{divisionId}"),
																	HttpMethod.DELETE,
																	requestEntity,
																	Object.class,
																	var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNull("Must be empty body response  - ",responseEntity.getBody());
        assertEquals("Status code must be NO_CONTENT - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());
        
	}


}
