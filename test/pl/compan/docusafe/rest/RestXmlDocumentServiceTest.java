package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.beust.jcommander.internal.Maps;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.poi.hssf.record.formula.functions.T;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;
import com.thoughtworks.xstream.core.util.Fields;

import edu.emory.mathcs.backport.java.util.Collections;
import pl.compan.docusafe.api.user.office.BooleanDto;
import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchDto;
import pl.compan.docusafe.api.user.office.FullTextSearchResponse;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.ListIds;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.api.user.office.WorkHistoryBean;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.rest.views.DocumentKindView;
import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

public class RestXmlDocumentServiceTest extends RestAbstractRpcTest {

    public static String cookie;
    
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_XML);
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	/**
	 * test do resta mobailnego
	 * @throws JAXBException
	 */
	
	@Test
	public void testGetDocument() throws JAXBException{
        String documentId = TestUtils.getTestProperty("rest.document.get");
		log.debug("\tTest pobierania dokumentu o id " + documentId);
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		String url = getUrl("/document/{documentId}");
        
		ResponseEntity<DocumentView> responseEntity = template.exchange(url, HttpMethod.GET, requestEntity, DocumentView.class, documentId);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
	}
	
	@Test
	public void testValidateBarcode_PG(){
		log.debug("\tTest sprawdzaj�cy poprawno�ci barcode dla algorytmu PG");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String, String> vars = new HashMap<String,String>();
		vars.put("barcode", TestUtils.getTestProperty("rest.document.barcode.number"));
		vars.put("algorithm", TestUtils.getTestProperty("rest.document.barcode.algorithm"));
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<BooleanDto> responseEntity = template.exchange(getUrl("/document/barcode={barcode}/{algorithm}/validate"),
																  HttpMethod.POST,
																  requestEntity,
																  BooleanDto.class,
																  vars);		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        Boolean isValid = responseEntity.getBody().getItem();
		assertTrue("Must be correct barcode - ", isValid);
        
	}

    @Test
	public void testValidateBarcode_NoValidation()  {
		log.debug("\tTest sprawdzaj�cy poprawno�ci barcode bez validacji");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String, String> vars = new HashMap<String,String>();
		vars.put("barcode", "lorem ipsum");
		vars.put("algorithm", "NO_VALIDATION");
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<BooleanDto> responseEntity = template.exchange(getUrl("/document/barcode={barcode}/{algorithm}/validate"),
																  HttpMethod.POST,
																  requestEntity,
																  BooleanDto.class,
																  vars);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        Boolean isValid = responseEntity.getBody().getItem();
		assertTrue("Must be correct barcode - ", isValid);
        
	}

    /**
     * Wyszukuje dokument po barkodzie i sprawdza poprawno�� barkodu
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testFindDocumentByBarcode_AreDocuments_WithBarcodeCheck(){
        log.debug("\tTest wyszukiwania dokumentu po barkodzie");

        // przygotowanie
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
        Map<String, String> var = Maps.newHashMap();
        var.put("barcode", TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_FINDBYBARCODE));
        var.put("check", "PG");

        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<ListIds> responseEntity = template.exchange(getUrl("/document/barcode={barcode}/find/{check}/check"),
                HttpMethod.GET,
                requestEntity,
                ListIds.class,
                var);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());

        ListIds list = responseEntity.getBody();
        assertFalse("Empty list of ids documents - ", CollectionUtils.isEmpty(list.getItems()));
    }

    /**
     * Testuje wyszukiwanie dokument�w bez sprawdzania poprawno�ci barkodu
     * @throws JAXBException 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
	@Test
	public void testFindDocumentByBarcode_AreDocuments() throws JAXBException{
		log.debug("\tTest wyszukiwania dokumentu po barkodzie");

		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String, String> var = Collections.singletonMap("barcode", TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_FINDBYBARCODE));

		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<ListIds> responseEntity = template.exchange(getUrl("/document/barcode={barcode}/find"),
																  HttpMethod.GET,
																  requestEntity,
																  ListIds.class,
																  var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());

        ListIds list = responseEntity.getBody();
		assertFalse("Empty list of ids documents - ", CollectionUtils.isEmpty(list.getItems()));
	}
	
	@Test
	public void testFindDocumentByBarcode_NoOneDocuments(){
		log.debug("Test wyszukiwania dokumentu po barkodzie (nie ma dokument�w)");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String, String> var = Collections.singletonMap("barcode", TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_FINDBYBARCODE_NO_DOC));
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListIds> responseEntity = template.exchange(getUrl("/document/barcode={barcode}/find"),
																  HttpMethod.GET,
																  requestEntity,
																  ListIds.class,
																  var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        ListIds list = responseEntity.getBody();
		assertTrue("Empty list of ids documents - ", CollectionUtils.isEmpty(list.getItems()));
        
	}


	
	@Test
	public void testGetDocumentAsXml() throws IOException{
		log.debug("Test pobierania dokumentu w formie xml");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
		
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
        String docId =  TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_AS_XML_ID);
		Map<String, String> var = Collections.singletonMap("documentId", docId);
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<String> responseEntity = template.exchange(getUrl("/document/xml/{documentId}"),
																  HttpMethod.GET,
																  requestEntity,
																  String.class,
																  var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        byte[] encodedDocument = responseEntity.getBody().getBytes();
        byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

        File tempFile = TestUtils.getFileInTestFolder("documentAsXml-"+ docId + ".xml");
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

        log.info("decoded Document xml " + decodedDocument.toString());
	}
	
	@Test
	public void testSearchDocument() throws JAXBException{
		log.debug("\tTest wyszukiwania dokument�w");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		String searchFieldName = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
		String dockindCn = TestUtils.getTestPropertyAsMap("rest.document.search.fields").keySet().iterator().next();
		String isSearchField = TestUtils.getTestPropertyAsMap("rest.document.search.fields").get(dockindCn);
   		int limit = Integer.valueOf(TestUtils.getTestProperty("rest.fulltextsearch.limit"));
   		int offset = Integer.valueOf(TestUtils.getTestProperty("rest.fulltextsearch.offset"));
		String sortingField = TestUtils.getTestProperty("rest.fulltextsearch.sorting.field");
   		
		OfficeDocumentDto officeDocumentDto = createOfficeDocumentDto(searchFieldName, dockindCn, isSearchField, limit, offset, sortingField);
		
		String xmlRequest = MarshallerUtils.marshall(officeDocumentDto);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/document/search"),
																  HttpMethod.POST,
																  requestEntity,
																  ListDtos.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        ListDtos<OfficeDocumentDto> list = responseEntity.getBody();
		assertFalse("Empty list of office documents - ", CollectionUtils.isEmpty(list.getItems()));
        
	}
	
	@Test
	public void testAddDictionaryToDictionariesDocument() throws JAXBException{
		log.debug("\tTest dodawania do s�ownika dokumentu");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		List<FieldView> fields = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_ADD);
        String dictionaryCn = TestUtils.getTestProperty("rest.document.dictionary.search.cn");
        String dockindCn = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");

        DictionaryDto dictionaryDto = createDictionaryDto(dockindCn,dictionaryCn,fields);
        String xmlRequest = MarshallerUtils.marshall(dictionaryDto);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest,headers);

		RestTemplate template = TestUtils.getRestTemplate();
        
		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/document/dictionary/add"), HttpMethod.POST,
																requestEntity, ListDtos.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        List<Map<String,List<Map<String, String>>>> ObjectFromXml = responseEntity.getBody().getItems();
		assertFalse("Empty dictionary! - ", CollectionUtils.isEmpty(ObjectFromXml));
		
	}

	@Test
	public void testSearchFullTextDocument() throws JAXBException{
		log.debug("\tTest wyszukiwania dokument�w po tre�ci dokumentu");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		FullTextSearchDto fts = createFullTextSeatchDto();
		
		String xmlRequset = MarshallerUtils.marshall(fts);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequset,headers);

		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/document/fulltextsearch"),
																  HttpMethod.POST,
																  requestEntity,
																  ListDtos.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        
	}
	
	@Test
	public void testGetDocumentDictionary() throws JAXBException{
        String documentId = TestUtils.getTestProperty(TestUtils.REST_GET_DOCUMENT_ID_DICTIONARY);
        String dictionatyCn = TestUtils.getTestProperty(TestUtils.REST_GET_DOCUMENT_DICTIONARY_CN);
        String dictionaryId = TestUtils.getTestProperty(TestUtils.REST_GET_DOCUMENT_DICTIONARY_ID);
		log.debug("\tTest pobierania s�ownika dla dokumentu o id " + documentId);
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);

		RestTemplate template = TestUtils.getRestTemplate();
		String url = getUrl("/document/dictionary?documentId={documentId}&dictionary={dictionary}&dictionaryId={dictionaryId}");
		
		ResponseEntity<ListDtos> responseEntity = template.exchange(url, HttpMethod.GET, requestEntity, ListDtos.class, documentId,dictionatyCn, dictionaryId);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        ListDtos<FieldView> list = responseEntity.getBody();
		assertFalse("Empty dictionary of office documents - ", CollectionUtils.isEmpty(list.getItems()));
        
	}

	@Test
	public void testSearchDictionary() throws JAXBException {
        String dictionaryCn = TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH_CN);
        String dockindCn = TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH_DOCKIND_CN);

        log.debug("\tTest wyszukiwania s�ownik�w dla typu s�ownika " + dictionaryCn);
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		DictionaryDto dictionaryDto = createDictionaryDto(dockindCn,
                        dictionaryCn,
                        TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH));
        String xmlRequest = MarshallerUtils.marshall(dictionaryDto);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest,headers);

		RestTemplate template = TestUtils.getRestTemplate();
        
		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/document/dictionary/search"), HttpMethod.POST, requestEntity, ListDtos.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        ListDtos<DictionaryDto> list = responseEntity.getBody();
		assertFalse("Empty dictionary of dictionaries - ", CollectionUtils.isEmpty(list.getItems()));
        
	}
	
	@Test
	public void testUpdateDocumentOnDictionary() throws JAXBException{
		log.debug("\tTest aktualizowania dokumentu na s�owniku");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		List<FieldView> fields = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_UPDATE);
        String idDictionary = TestUtils.getTestProperty("rest.get.document.dictionaryId");
        String dictionaryCn = TestUtils.getTestProperty("rest.document.dictionary.search.cn");
        String dockindCn = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
        
		fields.add(new FieldView("id",idDictionary));

		DictionaryDto dictionaryDto = createDictionaryDto(dockindCn, dictionaryCn, fields);
		
        String xmlRequest = MarshallerUtils.marshall(dictionaryDto);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest,headers);

		RestTemplate template = TestUtils.getRestTemplate();
        

		ResponseEntity<BooleanDto> responseEntity = template.exchange(getUrl("/document/dictionary/update"), HttpMethod.POST,
																requestEntity, BooleanDto.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
		assertTrue("Update is not done - ", responseEntity.getBody().getItem());
	}

	
	
	@Test
	public void testRemoveDocumentFromDictionary() throws JAXBException{
		log.debug("\tTest usuwania s�ownika dokumentu");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        String idDictionary= TestUtils.getTestProperty("rest.document.dictionary.delete");
		List<FieldView> fields = Lists.newArrayList(new FieldView("id",idDictionary ));
		DictionaryDto dictionaryDto = createDictionaryDto(fields);
        String xmlRequest = MarshallerUtils.marshall(dictionaryDto);
		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest,headers);

		RestTemplate template = TestUtils.getRestTemplate();
        
		ResponseEntity<BooleanDto> responseEntity = template.exchange(getUrl("/document/dictionary/delete"), HttpMethod.POST,
																requestEntity, BooleanDto.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("We espect OK status code - ",HttpStatus.OK, responseEntity.getStatusCode());
        
		assertTrue("Remove is not done - ", responseEntity.getBody().getItem());
        
	}	
	

    @Test
    public void testGetAudit() throws JAXBException{
        String id = TestUtils.getTestProperty("rest.document.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();
        
        Map<String, String> var = Collections.singletonMap("documentId", id);
        ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/document/{documentId}/audit"), HttpMethod.GET, requestEntity, ListDtos.class, var);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not fetch history of document - ",HttpStatus.OK, responseEntity.getStatusCode());

    }

    @Test
    public void testAddWatch(){
        String id = TestUtils.getTestProperty("rest.document.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();

        Map<String, String> var = Collections.singletonMap("documentId", id);
        ResponseEntity<Object> responseEntity = template.exchange(getUrl("/document/{documentId}/watch"), HttpMethod.PUT, requestEntity, Object.class, var);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not add document to watch - ",HttpStatus.CREATED, responseEntity.getStatusCode());

    }

    @Test
    public void testUnWatch(){
        String id = TestUtils.getTestProperty("rest.document.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();

        Map<String, String> var = Collections.singletonMap("documentId", id);
        ResponseEntity<Object> responseEntity = template.exchange(getUrl("/document/{documentId}/unwatch"), HttpMethod.DELETE, requestEntity, Object.class, var);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not unwatch document - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());

    }



}
