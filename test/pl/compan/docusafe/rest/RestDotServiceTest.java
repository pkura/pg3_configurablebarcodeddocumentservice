package pl.compan.docusafe.rest;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import edu.emory.mathcs.backport.java.util.Collections;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.*;

public class RestDotServiceTest {

    public static final Logger log = LoggerFactory.getLogger(RestDotServiceTest.class);

    public static String url, user, pass;
    public static final String SET_COOKIE="Set-Cookie";
    static String cookie;

    long beginTime, endTime;

    @BeforeClass
    public static void initTest() throws Exception {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        cookie = login();
    }

    @AfterClass
    public static void afterTest() throws Exception {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetLogByDocumentId() throws JsonGenerationException, JsonMappingException, IOException, EdmException {

        log.debug("Testing getLogByDocument method");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(new MediaType(MediaType.TEXT_HTML.getType(), MediaType.TEXT_HTML.getSubtype(), Charset.forName("UTF-8"))));

        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);

        RestTemplate template = TestUtils.getRestTemplate();
        String documentId = TestUtils.getTestProperty("dot.document.get");

        Map<String,String> var = Collections.singletonMap("documentId", documentId);

        beginTime = (Calendar.getInstance()).getTimeInMillis();
        ResponseEntity<String> responseEntity = template.exchange(getUrl("/dot/{documentId}/document"),
                HttpMethod.GET, requestEntity, String.class,var);
        endTime = (Calendar.getInstance()).getTimeInMillis();

        // testowanie
        assertNotNull("Empty response", responseEntity);
        assertNotNull("Empty body response", responseEntity.getBody());
        assertEquals("Wrong status code",HttpStatus.OK, responseEntity.getStatusCode());

        byte[] encodedDocument = responseEntity.getBody().getBytes();
        byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

        File tempFile = new File(TestUtils.getTestProperty(TestUtils.REST_TEST_PATH) + "/DOCUMENT_ID_"+ var.get("documentId") + ".txt");
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

        log.info("decoded document's logs xml {}", decodedDocument.toString());

        // konwertowanie
//		convertRequestResponseToJSON(requestEntity, responseEntity);

        //czas wykonania
        long executionTime = endTime - beginTime;

    }

//	-------------------------------------------------------------------------------------------------------------------------------

    private void convertRequestResponseToJSON(HttpEntity<?> requestEntity,
                                              ResponseEntity<?> responseEntity) throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        System.out.println("\nRequest\n");
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestEntity.getHeaders()));
        // if is string we must bind this String to Object
        if (requestEntity.getBody() == null){
            System.out.println("null");
        } else if (requestEntity.getBody().getClass() == String.class){
            String requestString = (String) requestEntity.getBody();
            Object json = mapper.readValue(requestString , Object.class);
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json));
        } else {
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestEntity.getBody()));
        }

        System.out.println("\nResponse\n");
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getHeaders()));
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getBody()));

    }

    private static String login() {
        System.out.println("Zalogowanie do systemu jako " + user + "\n");
        HttpHeaders headers = getJsonHeaders(null);

        RestTemplate template = TestUtils.getRestTemplate();

        HttpEntity<String> e = new HttpEntity<String>("",headers);
        ResponseEntity<String> loginResponse = template.postForEntity(
                getUrl("/login?username={username}&password={password}"), e, String.class, user, pass);

        return loginResponse.getHeaders().get(SET_COOKIE).iterator().next();
    }

    private static String getUrl(String uri) {
        return url + uri;
    }

    private static HttpHeaders getJsonHeaders(Object object) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        if(StringUtils.isNotEmpty(cookie)){
            headers.add("Cookie", cookie);
        }
        return headers;
    }

}
