package pl.compan.docusafe.rest;

import static junit.framework.Assert.*;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.core.dockinds.dwr.DwrDictionaryBase;
import pl.compan.docusafe.util.TestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.util.Calendar;

/**
 */
public class RestApiTest {

    public static final Logger log = LoggerFactory.getLogger(RestApiTest.class);

    public static String url, user, pass;
    ObjectMapper mapper = new ObjectMapper();
    public static final String SET_COOKIE="Set-Cookie";
    static String cookie;

	private static XLSParamWriter xlsParamWriter;

    @BeforeClass
    public static void  init(){
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        // login one time per test whole class
        cookie = login();
        
        xlsParamWriter = new XLSParamWriter();
//        try {
////    		xlsParamWriter.createColumnHeaders("Test PG");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
    }

	
    
    @AfterClass
    public static void  afterTest(){
        cookie = null; 
        logout();
    }

	@Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
    	    	
        log.debug("after - zapisywanie do pliku");
                 
//        xlsParamWriter.updateFile();
//		xlsParamWriter.autoWidth();
    }

    @Test
    public void testUsers() throws IOException{
    	
    	
    	System.err.println("\tTest pobrania wszystkich użytkowników");
    	xlsParamWriter.setActionName("Test pobrania wszystkich użytkowników");
    	log.debug("get all users");
        List userDtos = testGetAllUsers();       
        assertFalse("Brak użytkowników!", CollectionUtils.isEmpty(userDtos));
             
    }

    @Test
    public void testDivisions() throws IOException{
        log.debug("get all divisions");
        xlsParamWriter.setActionName("Test dywizji");
        List<Map<String,List<Map<String, String>>>> userDtos = getListsGET(getUrl("division"), "all divisions");
        assertFalse("Brak użytkowników!", CollectionUtils.isEmpty(userDtos));
    }

    @Test
    public void testSubstis() throws IOException{
        log.debug("get all substs");
        xlsParamWriter.setActionName("substytucji");
        List<Map<String,List<Map<String, String>>>> userDtos = getListsGET(getUrl("substitution"), "all substitution");
        assertTrue("Brak użytkowników!", CollectionUtils.isEmpty(userDtos));
    }

    private List<UserDto> testGetAllUsers() throws IOException {
        return getListsGET(getUrl("user"), "all users");
    }

    private  List getListsGET(String uri, String message) throws IOException {
   	
    	long beginTime, endTime;
        HttpHeaders headers = getJsonHeaders(cookie);
        RestTemplate template = TestUtils.getRestTemplate();

        System.err.println(headers.values());
        
        HttpEntity<String> e = new HttpEntity<String>(null,headers);
        
        beginTime = new Date().getTime();
        ResponseEntity<List> response = template.exchange(uri, HttpMethod.GET, e, List.class);
        endTime = new Date().getTime() - beginTime; 
        
        System.err.println(response.getHeaders());
        ObjectMapper mapper = new ObjectMapper();
        
        
        List fw = (List)response.getBody();
        
        try {
        	String jsonResponse = mapper.writeValueAsString(fw);
        	String jsonRequset = mapper.writeValueAsString(e);
//			System.err.println(Arrays.toString(jsonString.split(",")));
        	xlsParamWriter.setUrl(uri);
//            xlsParamWriter.setHeadersRequest(headers, jsonRequset);
            xlsParamWriter.setParamResp(jsonResponse);
//            xlsParamWriter.setResponse(fw);
            xlsParamWriter.setHttpMethod(HttpMethod.GET.toString());
            xlsParamWriter.setTime(String.valueOf(endTime) + "ms");
        	
    		
		} catch (JsonGenerationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        log.debug(message + " : "+ fw);
        return fw;
    }

    @Test
    public void testDictionaryObject() throws Exception {
        testDocumentDictionary(cookie,"302","SENDER", "246");
        testDocumentDictionary(cookie,"225","SENDER_KOPIA", "27");
        testDocumentDictionary(cookie,"302","RECIPIENT_HERE", "247");
    }


    @Test
    public void testDictionarySearch(){
        List<Map<String,List<Map<String, String>>>> list = dictionaryAction(List.class, "normal", "SENDER", "document/dictionary/search", HttpMethod.POST, new FieldView("FIRSTNAME", "", "Grze"));
        log.debug("SECOND request");
        List<Map<String,List<Map<String, String>>>>  list1 = dictionaryAction(List.class, "normal", "SENDER", "document/dictionary/search", HttpMethod.POST, new FieldView("FIRSTNAME", "", "Rad"));
        log.debug("email");
        List<Map<String,List<Map<String, String>>>>  list2 = dictionaryAction(List.class, "email", "SENDER_KOPIA", "document/dictionary/search", HttpMethod.POST, new FieldView("FIRSTNAME", "", "Rad"));
    }

    @Test
    public void testDictionaryEnum(){
        List l = dictionaryAction(List.class, "normal_out", "EPUAP", "document/dictionary/search", HttpMethod.POST);
    }

    @Test
    public void testDictionaryAdd(){
        log.info("adding to dictionary");
        List<Map<String,List<Map<String, String>>>>  list1 = dictionaryAction(List.class, "normal", "SENDER", "document/dictionary/add", HttpMethod.POST,
                new FieldView("FIRSTNAME", "Jan 5"),
                new FieldView("LASTNAME", "Kowalski"));
        String v = list1.iterator().next().get("fieldValues").iterator().next().get("value");
        log.info("searching added value");
        List list = dictionaryAction(List.class, "normal", "SENDER", "document/dictionary/search", HttpMethod.POST, new FieldView(DwrDictionaryBase.ID, v));
    }


    @Test
    public void testDictionaryDelete(){
        log.info("delete to dictionary");
        Boolean  list1 = dictionaryAction(Boolean.class, "normal", "SENDER", "document/dictionary/delete", HttpMethod.POST,
                new FieldView("id", "305"));
    }

    @Test
    public void testDictioanryUpdate(){
        log.info("update to dictionary");

        Boolean  list1 = dictionaryAction(Boolean.class, "normal", "SENDER", "document/dictionary/update", HttpMethod.POST,
                new FieldView("id", "306"),
                new FieldView("FIRSTNAME", "Jan 5 zaktualizowany"),
                new FieldView("LASTNAME", "Kowalski"));
    }
//*
    @Test
    public void testDocumentSearch(){
        log.info("search documents");

        OfficeDocumentDto dic = new OfficeDocumentDto();
        //dic.setDescription("sdgsdgds");
        documentAction(List.class, "normal", "SENDER", "document/search", HttpMethod.POST, dic, new FieldView("ANONIM", "true"));
    }

    @Test
    public void testFullTextSearch() throws IOException {
        log.info("fullTextSearch");

        HttpHeaders headers = getJsonHeaders(cookie);
        RestTemplate template = TestUtils.getRestTemplate();

        FullTextSearchDto fts = new FullTextSearchDto();
        fts.setText("pismo");
        fts.setLimit(50);

        log.error("request param: {}", mapper.writeValueAsString(fts));

        HttpEntity<String> e = new HttpEntity<String>(mapper.writeValueAsString(fts),headers);
        ResponseEntity<List> response = template.exchange(getUrl("document/fulltextsearch"), HttpMethod.POST, e, List.class);
        log.debug("full text Search: " + response.getBody());
    }

    @Test
    public void testTaskList() throws IOException {
        log.info("testTaskList");

        HttpHeaders headers = getJsonHeaders(cookie);
        RestTemplate template = TestUtils.getRestTemplate();

        HttpEntity<String> e = new HttpEntity<String>("",headers);
        ResponseEntity<List> response = template.exchange(getUrl("tasklist/{username}/user/0/offset"), HttpMethod.GET, e, List.class, "admin");

        log.debug("testTaskList: " + mapper.writeValueAsString(response.getBody()));
    }

    /**
     *
     * @param dockind
     * @param dictionaryCn
     * @param uri
     * @param fvs
     */
    private <T> T documentAction(Class<T> clazz, String dockind, String dictionaryCn, String uri, HttpMethod method, OfficeDocumentDto dic,  FieldView... fvs){
        HttpHeaders headers = getJsonHeaders(cookie);
        RestTemplate template = TestUtils.getRestTemplate();

        dic.setDockindCn(dockind);
        dic.setSortingField("ID");
        dic.setLimit(50);
        dic.setOffset(0);
        dic.setFieldValues(Lists.newArrayList(fvs));

        HttpEntity<OfficeDocumentDto> e = new HttpEntity<OfficeDocumentDto>(dic,headers);
        ResponseEntity<T> response = template.exchange(getUrl(uri), method, e, clazz);

        T fw = (T) response.getBody();
        log.debug("dictionary response: " + fw);
        return fw;
    }
    /**
     *
     * @param dockind
     * @param dictionaryCn
     * @param uri
     * @param fvs
     */
    private <T> T dictionaryAction(Class<T> clazz, String dockind, String dictionaryCn, String uri, HttpMethod method, FieldView... fvs){
        HttpHeaders headers = getJsonHeaders(cookie);
        RestTemplate template = TestUtils.getRestTemplate();

        DictionaryDto dic = new DictionaryDto();
        dic.setDictionaryCn(dictionaryCn);
        dic.setDockindCn(dockind);
        dic.setFieldValues(Lists.newArrayList(fvs));

        HttpEntity<DictionaryDto> e = new HttpEntity<DictionaryDto>(dic,headers);
        ResponseEntity<T> response = template.exchange(getUrl(uri), method, e, clazz);

        T fw = (T) response.getBody();
        log.debug("dictionary response: " + fw);
        return fw;
    }

    /** document/dictionary
     */
    private void testDocumentDictionary(String cookie, String docId, String dictionary, String dicId) {
        HttpHeaders headers = getJsonHeaders(cookie);
        RestTemplate template = TestUtils.getRestTemplate();

        HttpEntity<String> e = new HttpEntity<String>("",headers);
        String uri = getUrl("document/dictionary?documentId={docId}&dictionary={dictionary}&dictionaryId={dicId}");
        ResponseEntity<List> response = template.exchange(uri, HttpMethod.GET, e, List.class,
                docId,
                dictionary,
                dicId);

        List<FieldView> fw = (List<FieldView>)response.getBody();
        log.debug("dictionary response: " + fw);
    }

    private static String login() {
    	System.out.println("Zalogowanie do systemu jako " + user);
        HttpHeaders headers = getJsonHeaders(null);

        RestTemplate template = TestUtils.getRestTemplate();

        HttpEntity<String> e = new HttpEntity<String>("",headers);
        ResponseEntity<String> loginResponse = template.postForEntity(
                getUrl("login?username={username}&password={password}"), e, String.class, user, pass);

        return loginResponse.getHeaders().get(SET_COOKIE).iterator().next();
    }

    @Test
    public void testLoginCas(){
        System.out.println("logowanie cas" + user);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        RestTemplate template = TestUtils.getRestTemplate();

        HttpEntity<String> e = new HttpEntity<String>("",headers);
        ResponseEntity<String> loginResponse = template.postForEntity(
                "https://localhost:8844/cas/v1/tickets/username={username}&password={password}", e, String.class, user, pass);

        for(Map.Entry< String,List<String>> en : loginResponse.getHeaders().entrySet()){
            log.info("headers {} -> {}", en.getKey(), en.getValue());
        }
    }

    private static HttpHeaders getJsonHeaders(String cookie) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        if(StringUtils.isNotEmpty(cookie)){
            headers.add("Cookie", cookie);
        }
        return headers;

    }

    private static String getUrl(String uri){
        return url + '/' + uri;
    }
    
    private static void logout() {
		
	}
}
