package pl.compan.docusafe.rest.rpc;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;



import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcXmlDivisionTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_XML);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testXmlRpcDivisionCreate_NewDivision() throws IOException, JAXBException{
        log.debug("Testing method DIVISION_CREATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
        String nameOfDivision = TestUtils.getTestProperty("rest.api.division.create");
        String code = TestUtils.getTestProperty("rest.api.division.code");
		Long parentId = new Long(TestUtils.getTestProperty("rest.api.division.parentId"));
        
		DivisionDto divisionDto = createDivisionDto("xml", nameOfDivision, DivisionType.DIVISION, code, parentId);
        String paramRpcUrl = MarshallerUtils.marshall(divisionDto);
        log.info("xml param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_CREATE,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testXmlRpcDivisionGetById() throws IOException, JAXBException {
        log.debug("Testing method DIVISION_GET");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        		
        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String,Object> var = Collections.singletonMap("divisionId", (Object)Long.valueOf(idDivision));
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
        
        DivisionDto divisionDto = new DivisionDto();
        divisionDto = (DivisionDto) MarshallerUtils.unmarshall(divisionDto , response.getBody());
        
        assertNotNull("Division object Must be not null", divisionDto);
        
    }

	@Test
    public void testXmlRpcDivisionAddUser() throws IOException, JAXBException{
        log.debug("Testing method DIVISION_ADD_USER");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);

        String idUser = TestUtils.getTestProperty("rest.api.division.userId");
        UserDto userDto = getUserById(cookie, idUser,MediaType.APPLICATION_XML);

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");        
        Map<String,Object> var = Collections.singletonMap("divisionId", (Object)Long.valueOf(idDivision));
        XmlMapMapper map = new XmlMapMapper(var);
        

        String paramRpcUrl1 = MarshallerUtils.marshall(userDto);
        String paramRpcUrl2 = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl1);
        log.info("xml param url rpc " + paramRpcUrl2);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_ADD_USER,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());

    }


    @Test
    public void testXmlRpcDivisionGetUsers() throws  IOException, JAXBException {
        log.debug("Testing method DIVISION_GET_USERS");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String,Object> var = Collections.singletonMap("divisionId", (Object)Long.valueOf(idDivision));
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_USERS,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    

    @Test
    public void testXmlRpcDivisionGetSubdivisions() throws  IOException, JAXBException {
        log.debug("Testing method DIVISION_GET_SUBDIVISIONS");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        Long divisionId = new Long(TestUtils.getTestProperty("rest.api.division.get"));
        Map<String, Object> var = Collections.singletonMap("divisionId", (Object)divisionId);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_SUBDIVISIONS,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    
    @Test
    public void testXmlRpcDivisionGetAll() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method DIVISION_GET_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        
        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_ALL, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    
    @Test
    public void testXmlRpcDivisionGetByGuid() throws IOException, JAXBException {
        log.debug("Testing method DIVISION_GET_BY_GUID");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String guid = TestUtils.getTestProperty("rest.api.division.guid");
        Map<String, Object> var = Collections.singletonMap("guid", (Object)guid);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_BY_GUID,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    
    @Test
    public void testXmlRpcDivisionUpdate() throws IOException, JAXBException{
        log.debug("Testing method DIVISION_UPDATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String idDivisionUpdate = TestUtils.getTestProperty("rest.api.division.get");
        String nameOfDivision = TestUtils.getTestProperty("rest.api.division.create");
		Long parentId = new Long(TestUtils.getTestProperty("rest.api.division.parentId"));
		
		DivisionDto divisionDto = updateDivisionDto(idDivisionUpdate, nameOfDivision, parentId);
        Map<String, Object> var = Collections.singletonMap("divisionId", (Object)idDivisionUpdate);
        String paramRpcUrl1 = MarshallerUtils.marshall(divisionDto);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl2 = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl1);
        log.info("xml param url rpc " + paramRpcUrl2);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_UPDATE,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());

    }

    
    
  
    
    @Test
   	public void testXmlRpcDivisionDelete() throws IOException, JAXBException{
   		log.debug("Testing method DIVISION_DELETE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        
   		String idDivisionUpdate = TestUtils.getTestProperty("rest.api.division.get");
   		
		Map<String,Object> var = Collections.singletonMap("divisionId", (Object) idDivisionUpdate);
		 XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_DELETE,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertFalse("Must be false", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
   		
    }

    @Test
    public void testXmlRpcDivisionRemoveUser() throws IOException, JAXBException{
        log.debug("Testing method DIVISION_ADD_USER");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String idUser = TestUtils.getTestProperty("rest.api.division.userId");

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String, Object> var = Collections.singletonMap("divisionId", (Object)Long.valueOf(idDivision));
        Map<String, Object> var2 = Collections.singletonMap("userId", (Object)Long.valueOf(idUser));
        String paramRpcUrl1 = MarshallerUtils.marshall(new XmlMapMapper(var));
        String paramRpcUrl2 = MarshallerUtils.marshall(new XmlMapMapper(var2));
        log.info("xml param url rpc " + paramRpcUrl1);
        log.info("xml param url rpc " + paramRpcUrl2);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_REMOVE_USER,
                headers,paramRpcUrl2, paramRpcUrl1);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());

    }

}
