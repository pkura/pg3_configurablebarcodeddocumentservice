package pl.compan.docusafe.rest.rpc;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.JAXBException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;

import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcXmlDocumentTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_XML);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }
    
    @Test
    public void testXmlRpcFindDocumentByBarcode() throws IOException, JAXBException{
        log.info("Testing Method DOCUMENT_BARCODE_FIND");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML); 
        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        
		Map<String, Object> var = Collections.singletonMap("barcode", (Object)barcode);
		XmlMapMapper map = new XmlMapMapper(var);
		String paramRpc = MarshallerUtils.marshall(map);

        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_BARCODE_FIND, headers, paramRpc);
    }
    
    @Test
    public void testXmlRpcBarcodeValidation() throws IOException, JAXBException{
        log.info("findDocument test");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        
        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        String algorithm = TestUtils.getTestProperty("rest.document.barcode.algorithm");
        
		Map<String, Object> vars = new LinkedHashMap<String, Object>();
		vars.put("barcode", (Object)barcode);
		vars.put("algorithm", (Object)algorithm);
		XmlMapMapper map = new XmlMapMapper(vars);
        String paramRpc = MarshallerUtils.marshall(map);
        
		executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_VALIDATE_BARCODE, headers, paramRpc);
    }
    
    @Test
    public void testXmlRpcGetDocumentAsXml() throws IOException, JAXBException{
        log.info("findDocument test");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
        
        String docId =  TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_AS_XML_ID);
		Map<String, Object> var = Collections.singletonMap("documentId", (Object)docId);
		XmlMapMapper map = new XmlMapMapper(var);
		String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> responseEntity = executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_GET_AS_XML, headers, paramRpc);

        TestUtils.saveToFile(responseEntity, "documentAsXml-" + docId + "-xml-rpc.xml");
    }
    
    
    @Test
    public void testXmlRpcGetDocument() throws IOException, JAXBException{
        log.info("Testing method DOCUMENT_GET");
        
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        String docId =  TestUtils.getTestProperty("rest.document.get");
        
		Map<String, Object> var = Collections.singletonMap("documentId", (Object)docId);
		XmlMapMapper map = new XmlMapMapper(var);
		String paramRpc = MarshallerUtils.marshall(map);

        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_GET, headers, paramRpc);
    }
    
    @Test
   	public void testXmlRpcDocumentGetAudit() throws IOException, JAXBException{
   		log.debug("Testing method DOCUMENT_GET_AUDIT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        String docId =  TestUtils.getTestProperty("rest.document.get");
        
		Map<String, Object> var = Collections.singletonMap("documentId", (Object)docId);
		XmlMapMapper map = new XmlMapMapper(var);
		String paramRpc = MarshallerUtils.marshall(map);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_GET_AUDIT,
																		   headers,paramRpc);
          
		assertNotNull("Empty response - ",response);
        assertNotNull("Empty body response  - ",response.getBody());
        assertEquals("Do not fetch history of document - ",HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
    public void testXmlRpcFindDocumentByBarcode_MethodWithCheck() throws IOException, JAXBException{
        log.info("Testing method DOCUMENT_BARCODE_FIND_CHECK_BARCODE");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        String check = TestUtils.getTestProperty("rest.document.barcode.check");
        
        Map<String, Object> vars = new LinkedHashMap<String,Object>();
		vars.put("barcode", (Object)barcode);
        vars.put("check", (Object)check);
		XmlMapMapper map = new XmlMapMapper(vars);
		String paramRpc = MarshallerUtils.marshall(map );

        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_BARCODE_FIND_CHECK_BARCODE, headers, paramRpc);
    }
    
    @Test
    public void testXmlRpcFullTextSearch() throws IOException, JAXBException {
        log.info("testXmlRpcFullTextSearch");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        
        String text_FullTextSearch = TestUtils.getTestProperty("rest.fulltextsearch.text");
        String limit_FullTextSearch = TestUtils.getTestProperty("rest.fulltextsearch.limit");
        String offset_FullTextSearch = TestUtils.getTestProperty("rest.fulltextsearch.offset");
        
        FullTextSearchDto fts = new FullTextSearchDto();
        fts.setText(text_FullTextSearch);
		fts.setLimit(Integer.valueOf(limit_FullTextSearch));
		fts.setOffset(Integer.valueOf(offset_FullTextSearch));

        String paramBody = MarshallerUtils.marshall(fts);
        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_FULL_TEXTSEARCH, headers, paramBody);
    }
    
    @Test
    public void testXmlRpcSearchDocumentsByDictionary() throws IOException, JAXBException {
        log.info("Testing Method DOCUMENT_SEARCH_DICTIONARY");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        
        String dictionaryCn = TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH_CN);
        String dockindCn = TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH_DOCKIND_CN);
        List<FieldView> list = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH);

        FieldView[] fieldView = list.toArray(new FieldView[list.size()]);
		DictionaryDto dictionaryDto = createDictionaryDto(fieldView ,dockindCn,dictionaryCn);

        String paramBody = MarshallerUtils.marshall(dictionaryDto);
        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_SEARCH_DICTIONARY, headers, paramBody);
    }
    
    @Test
   	public void testXmlRpcDocumentSearch() throws IOException, JAXBException{
   		log.debug("Testing method DOCUMENT_SEARCH");

   		String searchFieldName = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
		String dockindCn = TestUtils.getTestPropertyAsMap("rest.document.search.fields").keySet().iterator().next();
		String isSearchField = TestUtils.getTestPropertyAsMap("rest.document.search.fields").get(dockindCn);
   		int limit = Integer.valueOf(TestUtils.getTestProperty("rest.fulltextsearch.limit"));
   		int offset = Integer.valueOf(TestUtils.getTestProperty("rest.fulltextsearch.offset"));
		String sortingField = TestUtils.getTestProperty("rest.fulltextsearch.sorting.field");
   		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		
		OfficeDocumentDto officeDocumentDto = createOfficeDocumentDto(searchFieldName, dockindCn, isSearchField, limit, offset, sortingField);
   		String paramRpcUrl = MarshallerUtils.marshall(officeDocumentDto);
		log.info("xml param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_SEARCH, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcDocumentAddDictionaryToDictionariesDocument() throws IOException, JAXBException{
   		log.debug("Testing method DOCUMENT_ADD_DICTIONARY_ENTITY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		
   		List<FieldView> fields = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_ADD);
		String dictionaryCn = TestUtils.getTestProperty("rest.document.dictionary.search.cn");
		String dockindCn = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
		
		DictionaryDto dictionaryDto = createDictionaryDto(dockindCn,dictionaryCn,fields);
   		String paramRpcUrl = MarshallerUtils.marshall(dictionaryDto);
		log.info("xml param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_ADD_DICTIONARY_ENTITY, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
//   		TypeReference<List<Map<String,Object>>> typeRef = new TypeReference<List<Map<String,Object>>>() {};
//   		List<Map<String,List<Map<String, String>>>> ObjectFromJson = mapper.readValue(response.getBody(), typeRef);
//   		idDictionary = ObjectFromJson.get(0).get("fieldValues").get(0).get("value");
   	}
    
    @Test
   	public void testXmlRpcDocumentGetDocumentDictionary() throws IOException, JAXBException{
   		log.debug("Testing method DOCUMENT_GET_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		
   		String documentId = TestUtils.getTestProperty("rest.get.documentId.dictionary");
   		String dictionary = TestUtils.getTestProperty("rest.get.document.dictionaryCn");
   		Long dictionaryId = new Long(TestUtils.getTestProperty("rest.get.document.dictionaryId"));
   		
   		Map<String, Object> vars = new LinkedHashMap<String, Object>();
   		vars.put("documentId", (Object)documentId);
   		vars.put("dictionary", (Object)dictionary);
   		vars.put("dictionaryId", (Object)dictionaryId);
   		XmlMapMapper map = new XmlMapMapper(vars);
		String paramRpcUrl = MarshallerUtils.marshall(map );
		log.info("xml param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_GET_DOCUMENT_DICTIONARY,
																		   headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   	}
    
    @Test
   	public void testXmlRpcDocumentUpdateDocumentDictionary() throws IOException, JAXBException{
   		log.debug("Testing method DOCUMENT_UPDATE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		
   		List<FieldView> fields = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_UPDATE);
        String idDictionary = TestUtils.getTestProperty("rest.get.document.dictionaryId");
        String dictionaryCn = TestUtils.getTestProperty("rest.document.dictionary.search.cn");
        String dockindCn = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
        
		fields.add(new FieldView("id",idDictionary));

		DictionaryDto dictionaryDto = createDictionaryDto(dockindCn, dictionaryCn, fields);
		
   		String paramRpcUrl = MarshallerUtils.marshall(dictionaryDto);
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_UPDATE_DOCUMENT_DICTIONARY,headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcDocumentRemoveDocumentDictionary() throws IOException, JAXBException{
   		log.debug("Testing method DOCUMENT_REMOVE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		
   		String idDictionary = TestUtils.getTestProperty("rest.document.dictionary.delete");
		
        List<FieldView> fields = Lists.newArrayList(new FieldView("id",idDictionary));
		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = MarshallerUtils.marshall(dictionaryDto);
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_REMOVE_DOCUMENT_DICTIONARY,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
    public void testXmlRpcAddWatch() throws IOException, JAXBException{
        log.info("Testing method DOCUMENT_ADD_WATCH");
        
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        String id = TestUtils.getTestProperty("rest.document.get");
        
        Map<String, Object> var = Collections.singletonMap("documentId", (Object)id);
		String paramRpc = MarshallerUtils.marshall(new XmlMapMapper(var));

        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_ADD_WATCH, headers, paramRpc);
    }
    
    @Test
    public void testXmlRpcUnWatch() throws IOException, JAXBException{
        log.info("Testing method DOCUMENT_UN_WATCH");
        
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        String id = TestUtils.getTestProperty("rest.document.get");
        
        Map<String, Object> var = Collections.singletonMap("documentId", (Object)id);
		String paramRpc = MarshallerUtils.marshall(new XmlMapMapper(var));

        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_UN_WATCH, headers, paramRpc);
    }
}
