package pl.compan.docusafe.rest.rpc;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;

import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcJsonDocumentTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }
    
    @Test
    public void testJsonRpcFindDocumentByBarcode() throws IOException{
        log.info("Testing Method DOCUMENT_BARCODE_FIND");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML); 
        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        
		Map<String, String> var = Collections.singletonMap("barcode", barcode);
		String paramRpc = mapper.writeValueAsString(var);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_BARCODE_FIND, headers, paramRpc);
    }
    
    @Test
    public void testJsonRpcBarcodeValidation() throws IOException{
        log.info("findDocument test");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        
        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        String algorithm = TestUtils.getTestProperty("rest.document.barcode.algorithm");
        
		Map<String, String> var1 = Collections.singletonMap("barcode", barcode);
		Map<String, String> var2 = Collections.singletonMap("algorithm", algorithm);
		String paramRpc1 = mapper.writeValueAsString(var1);
		String paramRpc2 = mapper.writeValueAsString(var2);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_VALIDATE_BARCODE, headers, paramRpc1,paramRpc2);
    }
    
    @Test
    public void testJsonRpcGetDocumentAsXml() throws IOException{
        log.info("findDocument test");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        
        String docId =  TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_AS_XML_ID);
		Map<String, String> var = Collections.singletonMap("documentId", docId);
		
		String paramRpc = mapper.writeValueAsString(var);

        ResponseEntity<String> responseEntity = executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_GET_AS_XML, headers, paramRpc);

        TestUtils.saveToFile(responseEntity, "documentAsXml-" + docId + "-json-rpc.xml");
    }
    
    
    @Test
    public void testJsonRpcGetDocument() throws IOException{
        log.info("Testing method DOCUMENT_GET");
        
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        String docId =  TestUtils.getTestProperty("rest.document.get");
        
		Map<String, String> var = Collections.singletonMap("documentId", docId);
		String paramRpc = mapper.writeValueAsString(var);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_GET, headers, paramRpc);
    }
    
    @Test
   	public void testJsonRpcDocumentGetAudit() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_GET_AUDIT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String docId =  TestUtils.getTestProperty("rest.document.get");
        
		Map<String, String> var = Collections.singletonMap("documentId", docId);
		String paramRpc = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpc);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_GET_AUDIT,
																		   headers,paramRpc);
          
		assertNotNull("Empty response - ",response);
        assertNotNull("Empty body response  - ",response.getBody());
        assertEquals("Do not fetch history of case - ",HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
    public void testJsonRpcFindDocumentByBarcode_MethodWithCheck() throws IOException{
        log.info("Testing method DOCUMENT_BARCODE_FIND_CHECK_BARCODE");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        String check = TestUtils.getTestProperty("rest.document.barcode.check");
        
        Map<String, String> vars = new LinkedHashMap<String,String>();
		vars.put("barcode", barcode);
        vars.put("check", check);
		String paramRpc = mapper.writeValueAsString(vars);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_BARCODE_FIND_CHECK_BARCODE, headers, paramRpc);
    }
    
    @Test
    public void testJsonRpcFullTextSearch() throws IOException {
        log.info("testJsonRpcFullTextSearch");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        
        String text_FullTextSearch = TestUtils.getTestProperty("rest.fulltextsearch.text");
        String limit_FullTextSearch = TestUtils.getTestProperty("rest.fulltextsearch.limit");
        String offset_FullTextSearch = TestUtils.getTestProperty("rest.fulltextsearch.offset");
        
        FullTextSearchDto fts = new FullTextSearchDto();
        fts.setText(text_FullTextSearch);
		fts.setLimit(Integer.valueOf(limit_FullTextSearch));
		fts.setOffset(Integer.valueOf(offset_FullTextSearch));

        String paramBody = mapper.writeValueAsString(fts);
        executeRPCJsonRequest(RpcMethod.DOCUMENT_FULL_TEXTSEARCH, headers, paramBody);
    }
    
    @Test
    public void testJsonRpcSearchDocumentsByDictionary() throws IOException {
        log.info("Testing Method DOCUMENT_SEARCH_DICTIONARY");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        
        String dictionaryCn = TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH_CN);
        String dockindCn = TestUtils.getTestProperty(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH_DOCKIND_CN);
        List<FieldView> list = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_SEARCH);

        FieldView[] fieldView = list.toArray(new FieldView[list.size()]);
		DictionaryDto dictionaryDto = createDictionaryDto(fieldView ,dockindCn,dictionaryCn);

        String paramBody = mapper.writeValueAsString(dictionaryDto);
        executeRPCJsonRequest(RpcMethod.DOCUMENT_SEARCH_DICTIONARY, headers, paramBody);
    }
    
    @Test
   	public void testJsonRpcDocumentSearch() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_SEARCH");

   		String searchFieldName = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
		String dockindCn = TestUtils.getTestPropertyAsMap("rest.document.search.fields").keySet().iterator().next();
		String isSearchField = TestUtils.getTestPropertyAsMap("rest.document.search.fields").get(dockindCn);
   		int limit = Integer.valueOf(TestUtils.getTestProperty("rest.fulltextsearch.limit"));
   		int offset = Integer.valueOf(TestUtils.getTestProperty("rest.fulltextsearch.offset"));
		String sortingField = TestUtils.getTestProperty("rest.fulltextsearch.sorting.field");
   		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		
		OfficeDocumentDto officeDocumentDto = createOfficeDocumentDto(searchFieldName, dockindCn, isSearchField, limit, offset, sortingField);
   		String paramRpcUrl = mapper.writeValueAsString(officeDocumentDto);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_SEARCH, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcDocumentAddDictionaryToDictionariesDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_ADD_DICTIONARY_ENTITY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   				
		List<FieldView> fields = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_ADD);
		String dictionaryCn = TestUtils.getTestProperty("rest.document.dictionary.search.cn");
		String dockindCn = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
		
		DictionaryDto dictionaryDto = createDictionaryDto(dockindCn,dictionaryCn,fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_ADD_DICTIONARY_ENTITY, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
//   		TypeReference<List<Map<String,Object>>> typeRef = new TypeReference<List<Map<String,Object>>>() {};
//   		List<Map<String,List<Map<String, String>>>> ObjectFromJson = mapper.readValue(response.getBody(), typeRef);
//   		idDictionary = ObjectFromJson.get(0).get("fieldValues").get(0).get("value");
   	}
    
    @Test
   	public void testJsonRpcDocumentGetDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_GET_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		
   		String documentId = TestUtils.getTestProperty("rest.get.documentId.dictionary");
   		String dictionary = TestUtils.getTestProperty("rest.get.document.dictionaryCn");
   		String dictionaryId = TestUtils.getTestProperty("rest.get.document.dictionaryId");
   		
   		Map<String, String> var1 = Collections.singletonMap("documentId", documentId);
   		Map<String, String> var2 = Collections.singletonMap("dictionary", dictionary);
   		Map<String, Long> var3 = Collections.singletonMap("dictionaryId", new Long(dictionaryId));
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
   		String paramRpcUrl3 = mapper.writeValueAsString(var3);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		log.info("json param url rpc " + paramRpcUrl3);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_GET_DOCUMENT_DICTIONARY,
																		   headers, paramRpcUrl1, paramRpcUrl2, paramRpcUrl3);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   	}
    
    @Test
   	public void testJsonRpcDocumentUpdateDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_UPDATE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		
   		List<FieldView> fields = TestUtils.getTestFieldView(TestUtils.REST_DOCUMENT_DICTIONARY_UPDATE);
        String idDictionary = TestUtils.getTestProperty("rest.get.document.dictionaryId");
        String dictionaryCn = TestUtils.getTestProperty("rest.document.dictionary.search.cn");
        String dockindCn = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
        
		fields.add(new FieldView("id",idDictionary));

		DictionaryDto dictionaryDto = createDictionaryDto(dockindCn, dictionaryCn, fields);
		
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_UPDATE_DOCUMENT_DICTIONARY,headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcDocumentRemoveDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_REMOVE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String idDictionary = TestUtils.getTestProperty("rest.document.dictionary.delete");
		
        List<FieldView> fields = Lists.newArrayList(new FieldView("id",idDictionary));
		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_REMOVE_DOCUMENT_DICTIONARY,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
    public void testJsonRpcAddWatch() throws IOException{
        log.info("Testing method DOCUMENT_ADD_WATCH");
        
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        String id = TestUtils.getTestProperty("rest.document.get");
        
        Map<String, String> var = Collections.singletonMap("documentId", id);
		String paramRpc = mapper.writeValueAsString(var);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_ADD_WATCH, headers, paramRpc);
    }
    
    @Test
    public void testJsonRpcUnWatch() throws IOException{
        log.info("Testing method DOCUMENT_UN_WATCH");
        
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        String id = TestUtils.getTestProperty("rest.document.get");
        
        Map<String, String> var = Collections.singletonMap("documentId", id);
		String paramRpc = mapper.writeValueAsString(var);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_UN_WATCH, headers, paramRpc);
    }
}
