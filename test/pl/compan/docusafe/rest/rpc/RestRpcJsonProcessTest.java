package pl.compan.docusafe.rest.rpc;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import com.beust.jcommander.internal.Lists;

import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.ManualAssignmentDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.TestUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 03.08.14
 */
public class RestRpcJsonProcessTest extends RestAbstractRpcTest{
    public static String cookie;

    @BeforeClass
    public static void  init(){
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void  afterTest(){
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
   	public void testJsonRpcGetProcessActions() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method TASK_LIST_USER");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

   		Map<String, String> var = Collections.singletonMap("documentId", TestUtils.getTestProperty("rest.document.get"));

		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_GET_ACTIONS, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcExecuteProcessAction() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method PROCESS_EXECUTE_ACTION");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

   		Map<String, String> vars = new LinkedHashMap<String, String>();
		vars.put("documentId", TestUtils.getTestProperty("rest.document.get"));
		vars.put("processAction", TestUtils.getTestProperty("rest.document.processAction"));

		String paramRpcUrl = mapper.writeValueAsString(vars);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_EXECUTE_ACTION, headers, paramRpcUrl);
          
		assertNotNull("Empty response - ",response);
        assertEquals("We espect ACCEPTED status code - ",HttpStatus.ACCEPTED, response.getStatusCode());
   	}
    
    @Test
   	public void testAssignMultiManualAssignment() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method PROCESS_MULTI_MANUAL_ASSIGNMENT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

   		Map<String, String> vars = new LinkedHashMap<String, String>();
		vars.put("documentId", TestUtils.getTestProperty("rest.document.get"));
		
		List<ManualAssignmentDto> elements = Lists.newArrayList();
        String targetName = TestUtils.getTestProperty("rest.document.process.target.name");
		Boolean kind = Boolean.valueOf(TestUtils.getTestProperty("rest.document.process.kind"));
		Boolean targetKind = Boolean.valueOf(TestUtils.getTestProperty("rest.document.process.kind.target"));;
		elements.add(new ManualAssignmentDto(targetName ,kind , targetKind ));

		String paramRpcUrl = mapper.writeValueAsString(vars);
		String paramBody = mapper.writeValueAsString(new ListDtos<ManualAssignmentDto>(elements));
		log.info("json param url rpc " + paramRpcUrl);
		log.info("json param url rpc " + paramBody);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_MULTI_MANUAL_ASSIGNMENT, headers, paramBody,paramRpcUrl);
          
		assertNotNull("Empty response - ",response);
        assertEquals("We espect ACCEPTED status code - ",HttpStatus.ACCEPTED, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcGetDocumentProcesses() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method PROCESS_GET_DOCUMENT_PROCESSES");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

   		Map<String, String> var = Collections.singletonMap("documentId", TestUtils.getTestProperty("rest.document.get"));

		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_GET_DOCUMENT_PROCESSES, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcGetProcessDocuments() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method PROCESS_GET_PROCESS_DOCUMENTS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

   		Map<String, String> var = new LinkedHashMap<String, String>();
        var.put("processName", TestUtils.getTestProperty("rest.document.processName"));

		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_GET_PROCESS_DOCUMENTS, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcGetProcessDefinitions() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method PROCESS_GET_PROCESS_DOCUMENTS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);

   		Map<String, String> var = new LinkedHashMap<String, String>();
        var.put("processName", TestUtils.getTestProperty("rest.document.processName"));

		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_GET_PROCESS_DEFINITIONS, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcGetProcessDiagram() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method PROCESS_GET_PROCESS_DIAGRAM");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);

   		Map<String, String> var = new LinkedHashMap<String, String>();
        var.put("processId", TestUtils.getTestProperty("rest.document.process.instance.id"));

		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_GET_PROCESS_DIAGRAM, headers, paramRpcUrl);
          
		byte[] encodedDocument = response.getBody().getBytes();
        byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

        File tempFile = new File(TestUtils.getTestProperty(TestUtils.REST_TEST_PATH) + "/process_" + var.get("processId") + ".png");
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

        assertNotNull("Empty response - ",response);
        assertNotNull("Empty body response  - ",response.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcLoadProcessDefinitions() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method PROCESS_LOAD_PROCESS_DEFINITIONS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

   		Map<String, String> var = new LinkedHashMap<String, String>();
        var.put("processName", TestUtils.getTestProperty("rest.document.processName") + ".bpmn");

        File f = new File(TestUtils.getTestProperty("rest.test.path") + "/zadanie.bpmn");
        String s = FileUtils.encodeByBase64(f);
        KeyElement ke = new KeyElement(null, s);

        String paramRpcUrl = mapper.writeValueAsString(var);
		String bodyRpc = mapper.writeValueAsString(ke);
		log.info("json param url rpc " + paramRpcUrl);
		log.info("json rpc " + bodyRpc);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.PROCESS_LOAD_PROCESS_DEFINITIONS, headers, bodyRpc, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}

}
