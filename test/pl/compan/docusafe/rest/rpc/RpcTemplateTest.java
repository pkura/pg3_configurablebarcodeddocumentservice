package pl.compan.docusafe.rest.rpc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RpcTemplateTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }
    
    
    @Test
   	public void testGetAvailableTemplateForDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String documentId = TestUtils.getTestProperty("rest.document.get");
   		
		Map<String, String> var = Collections.singletonMap("documentId", documentId);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.TEMPLATE_GET_FOR_DOCUMENT,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);

   		
    }
    
    @Test
    public void testAddTemplate() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method TEMPLATE_LOAD_FOR_DOCUMENT");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON, "text", MediaType.TEXT_HTML);
        String dockinds = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
        String templateNames = TestUtils.getTestProperty("rest.api.template.name");
        File file = TestUtils.getFileInTestFolder("test2.rtf");
        String encodedDocument = FileUtils.encodeByBase64(file);
        
		Map<String,String> dockind = Collections.singletonMap("dockind", dockinds);
		Map<String,String> templateName = Collections.singletonMap("templateName:.+", templateNames);
		Map<String,String> fileString = Collections.singletonMap("file", encodedDocument);
        
        // Message Converters
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new FormHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new MappingJacksonHttpMessageConverter());

        // RestTemplate
        RestTemplate template = TestUtils.getRestTemplate();
        template.setMessageConverters(messageConverters);
		
   		String paramRpcUrl1 = mapper.writeValueAsString(dockind);
   		String paramRpcUrl2 = mapper.writeValueAsString(templateName);
   		String paramRpc = mapper.writeValueAsString(fileString);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		log.info("json param url rpc " + paramRpc);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.TEMPLATE_LOAD_FOR_DOCUMENT,
                headers,paramRpc,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);

    }
    
    
}
