package pl.compan.docusafe.rest.rpc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcJsonOfficeCaseTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testJsonRpcOfficeCaseCreate() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method OFFICECASE_CREATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        
        String clerk = TestUtils.getTestProperty("rest.api.officecase.create.clerk");
		String title = TestUtils.getTestProperty("rest.api.officecase.create.title");
		String description = TestUtils.getTestProperty("rest.api.officecase.create.description");
		Integer priority = Integer.parseInt(TestUtils.getTestProperty("rest.api.officecase.create.priority"));
		
		Calendar calendar = TestUtils.getTestPropertyGetDataTime("rest.api.officecase.iso.time");
        String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());
		
		String documentId = TestUtils.getTestProperty("rest.api.officecase.create.documentId");
		Long portfolioId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.create.portfolioId"));
		
		OfficeCaseDto officeCaseDto = createOfficeCaseDto(clerk, title, description, priority, dateTime, documentId, portfolioId);
        String paramBody = mapper.writeValueAsString(officeCaseDto);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_CREATE,
                headers,paramBody);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGetUser() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_USER");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String userName = TestUtils.getTestProperty("rest.api.officecase.user.case");
   		
		Map<String, String> var = Collections.singletonMap("username", userName);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_USER,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
    public void testJsonRpcOfficeCaseAddDocument() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method OFFICECASE_ADD");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        String documentId = TestUtils.getTestProperty("rest.api.officecase.documentId.addTo");
        String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
        
		Map<String,String> var1 = Collections.singletonMap("documentId", documentId);
		Map<String,String> var2 = Collections.singletonMap("officeCaseId", officeCaseId);
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl1);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_ADD,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGet() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);  		
   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
   		
		Map<String,String> var = Collections.singletonMap("officeCaseId", officeCaseId);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGetDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_DOCUMENT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String documentId = TestUtils.getTestProperty("rest.api.officecase.get.documentId");
   		
		Map<String, String> var = Collections.singletonMap("documentId", documentId );
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_DOCUMENT,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseRemoveDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_REMOVE_DOCUMENT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON, "text", MediaType.TEXT_HTML);
   		String documentId = TestUtils.getTestProperty("rest.api.officecase.get.documentId");
   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
   		
		Map<String, String> var1 = Collections.singletonMap("documentId", documentId );
		Map<String, String> var2 = Collections.singletonMap("officeCaseId", officeCaseId );
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_REMOVE_DOCUMENT,
																		   headers,paramRpcUrl1,paramRpcUrl2);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
   
    
    @Test
   	public void testJsonRpcOfficeCaseGet_OfficeCaseToOfficeCase() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_ALL_OFFICECASES");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);  		
   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
   		
		Map<String,String> var = Collections.singletonMap("officeCaseId", officeCaseId);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_ALL_OFFICECASES,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGet_FromDivision() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_ALL_OFFICECASES_FROM_DIVISION");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON, "json", MediaType.TEXT_HTML);  		
   		String guid = TestUtils.getTestProperty("rest.api.officecase.guid.case");
   		
		Map<String,String> var = Collections.singletonMap("guid", guid);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_ALL_OFFICECASES_FROM_DIVISION,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGet_Priorities() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_ALL_PRIORITIES_FOR_CASE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);  		
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_ALL_PRIORITIES_FOR_CASE,
																		   headers);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
 
    
    @Test
   	public void testJsonRpcOfficeCaseGetPortfolio() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_PORTFOLIO");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String guid = TestUtils.getTestProperty("rest.api.officecase.guid.case");
   		
		Map<String,String> var = Collections.singletonMap("guid", guid);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_PORTFOLIO,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGetDocuments() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_DOCUMENTS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
   		
		Map<String,String> var = Collections.singletonMap("officeCaseId", officeCaseId);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_DOCUMENTS,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGetAudit() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_AUDIT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
   		
		Map<String,String> var = Collections.singletonMap("id", id);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_AUDIT,
																		   headers,paramRpcUrl);
          
		assertNotNull("Empty response - ",response);
        assertNotNull("Empty body response  - ",response.getBody());
        assertEquals("Do not fetch history of case - ",HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
    public void testJsonRpcOfficeCaseUpdate() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method OFFICECASE_UPDATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        
        String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
		String clerk = TestUtils.getTestProperty("rest.api.officecase.create.clerk");
		String title = TestUtils.getTestProperty("rest.api.officecase.create.title");
		String description = TestUtils.getTestProperty("rest.api.officecase.create.description");
		Integer priority = Integer.parseInt(TestUtils.getTestProperty("rest.api.officecase.create.priority"));
		
		Calendar calendar = TestUtils.getTestPropertyGetDataTime("rest.api.officecase.iso.time");
        String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());
		
		String documentId = TestUtils.getTestProperty("rest.api.officecase.create.documentId");
		Long portfolioId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.create.portfolioId"));
		OfficeCaseDto officeCaseDto = createOfficeCaseDto(clerk, title, description, priority, dateTime, documentId, portfolioId);
        
        officeCaseDto.setId(Long.valueOf(id));
        
        String paramBody = mapper.writeValueAsString(officeCaseDto);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_UPDATE,
                headers,paramBody);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcAddOfficeCaseToOfficeCase() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method OFFICECASE_ADD_TO_OFFICECASE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));
        Long officeCaseAddId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.addId"));
        
		Map<String,Long> var1 = Collections.singletonMap("officeCaseId", officeCaseId);
		Map<String,Long> var2 = Collections.singletonMap("officeCaseAddId", officeCaseAddId);
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl1);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_ADD_TO_OFFICECASE,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcRemoveOfficeCaseFromOfficeCase() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method OFFICECASE_REMOVE_FROM_OFFICECASE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));
        Long officeCaseRemoveId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.removeId"));
        
		Map<String,Long> var1 = Collections.singletonMap("officeCaseId", officeCaseId);
		Map<String,Long> var2 = Collections.singletonMap("officeCaseRemoveId", officeCaseRemoveId);
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl1);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_REMOVE_FROM_OFFICECASE,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcOfficeCaseDelete() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method OFFICECASE_DELETE");

        String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
        String reason = TestUtils.getTestProperty("rest.api.officecase.delete.reason");;
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        KeyElement keyElement = new KeyElement(id, reason);
        
        String paramBody = mapper.writeValueAsString(keyElement);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_DELETE,
                headers,paramBody);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }
    
}
