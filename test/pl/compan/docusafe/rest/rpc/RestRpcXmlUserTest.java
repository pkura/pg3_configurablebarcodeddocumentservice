package pl.compan.docusafe.rest.rpc;
import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.google.common.collect.Lists;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.ListIds;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcXmlUserTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_XML);
    }

    @AfterClass
    public static void  afterTest(){
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testXmlRpcGetAllUsers() throws  IOException {
        log.debug("Testing method USER_FIND_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_FIND_ALL, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testXmlRpcGetAllRoles() throws IOException {
        log.debug("Testing method ROLE_FIND_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.ROLE_FIND_ALL, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testXmlRpcGetUserRoles() throws IOException, JAXBException {
        log.debug("Testing method ROLE_FIND_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String userName = TestUtils.getTestProperty("rest.api.user.get");
        Map<String, Object> var = Collections.singletonMap("username", (Object)userName);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_ROLE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testXmlRpcGetUser_ByName() throws IOException, JAXBException{
    	log.debug("Testing method USER_FIND_ONE");

        String userName = TestUtils.getTestProperty("rest.api.user.get.name");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        Map<String, Object> var = Collections.singletonMap("userId", (Object)userName);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_FIND_ONE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }


    @Test
    public void testXmlRpcGetUser() throws IOException, JAXBException{
        log.debug("Testing method USER_FIND_ONE");

        String userId = TestUtils.getTestProperty("rest.api.user.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        Map<String, Object> var = Collections.singletonMap("userId", (Object)userId);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_FIND_ONE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testXmlRpcCreateUser_New() throws JAXBException, IOException {
        log.debug("Testing method USER_CREATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
        UserDto userDto = new UserDto();
        userDto.setFirstname(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE) + "rpcXml");
        userDto.setLastname(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcXml");
        userDto.setUsername(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcXml");
        userDto.setPassword(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcXml");
        String paramRpc = MarshallerUtils.marshall(userDto);
        log.info("Xml param rpc " + paramRpc);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_CREATE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }
    
    @Test
    public void testXmlRpcUpdateUser() throws IOException, JAXBException{
        log.debug("Testing method USER_UPDATE");
        String userId = TestUtils.getTestProperty("rest.api.user.get");

        String idUserCreated = userId;
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        UserDto userDto = new UserDto();
        userDto.setId(Long.valueOf(idUserCreated));
        userDto.setFirstname("updatefirstnameRPC");
        userDto.setLastname("updatelastaneRPC");
        userDto.setUsername("updateusernameRPC");
        String paramRpc = MarshallerUtils.marshall(userDto);
        log.info("xml param rpc " + paramRpc);

        Map<String,Object> var = Collections.singletonMap("userId", (Object)idUserCreated);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_UPDATE, headers, paramRpc, paramRpcUrl);

        assertNotNull("Must be not null", response);
//        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }

    @Test
    public void testXmlRpcUpdateUserRoles() throws IOException, JAXBException {
        log.debug("Testing method USER_UPDATE");
        String userId = TestUtils.getTestProperty("rest.api.user.get");

        String idUserCreated = userId;
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        ListIds ids = new ListIds(Lists.newArrayList(43L,22L));
        String paramRpc = MarshallerUtils.marshall(ids);
        log.info("json param rpc " + paramRpc);

        Map<String,Object> var = Collections.singletonMap("username", (Object)idUserCreated);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl =  MarshallerUtils.marshall(map);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_UPDATE_ROLE, headers, paramRpc, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }
    
    
    @Test
    public void testXmlRpcGetUserSubstitutions() throws IOException, JAXBException{
        log.debug("Testing method USER_GET_SUBSTITUTIONS_BY_USER");
  
        String userId = TestUtils.getTestProperty("rest.api.user.substitutions");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        Map<String, Object> var = Collections.singletonMap("userId", (Object)userId);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_GET_SUBSTITUTIONS_BY_USER, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testXmlRpcGetUserSubstitute() throws  IOException, JAXBException{
        log.debug("Testing method USER_GET_SUBSTITUTE");

        String userId = TestUtils.getTestProperty("rest.api.user.substitute");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        Map<String, Object> var = Collections.singletonMap("userId", (Object)userId);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_GET_SUBSTITUTE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testXmlRpcGetUserDivisions() throws IOException, JAXBException{
        log.debug("Testing method USER_GET_DIVISIONS");

        String userId = TestUtils.getTestProperty("rest.api.user.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        Map<String, Object> var = Collections.singletonMap("userId", (Object)userId);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_GET_DIVISIONS, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }


    @Test
    public void testXmlRpcDeleteUser() throws IOException, JAXBException{
        log.debug("Testing method USER_DELETE");

        String userId = TestUtils.getTestProperty("rest.api.user.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
        Map<String, Object> var = Collections.singletonMap("userId", (Object)userId);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);

        
        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_DELETE, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }
}
