package pl.compan.docusafe.rest.rpc;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.TestUtils;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcJsonSubstitutionTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }


    @Test
    public void testJsonRpcCreateSubstitution_OneDay() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method SUBSTITUTION_CREATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        int numberOfDays = Integer.valueOf(TestUtils.getTestProperty("rest.api.substitution.num.days"));
        
		String paramRpcUrl = createSubstitutionInJson(numberOfDays,cookie);
        log.info("json param url rpc " + paramRpcUrl);


        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.SUBSTITUTION_CREATE, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());

    }

    @Test
    public void testJsonRpcSubstitutionGetById() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method SUBSTITUTION_GET_BY_ID");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String idSubstitution = TestUtils.getTestProperty("rest.api.substitution.id");
        Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);


        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.SUBSTITUTION_GET_BY_ID, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcSubstitutionGetAllCurrent() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method SUBSTITUTION_GET_ALL_CURRENT");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.SUBSTITUTION_GET_ALL_CURRENT, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcSubstitutionGetAllCurrentAtTime() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method SUBSTITUTION_GET_CURRENT_AT_TIME");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());
		
		Map<String, String> var = Collections.singletonMap("time", dateTime);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.SUBSTITUTION_GET_CURRENT_AT_TIME, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    
    @Test
    public void testJsonRpcSubstitutionDelete() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method SUBSTITUTION_DELETE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String idSubstitution = TestUtils.getTestProperty("rest.api.substitution.id");
        Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);


        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.SUBSTITUTION_DELETE, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }

}
