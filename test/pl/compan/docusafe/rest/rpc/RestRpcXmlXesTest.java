package pl.compan.docusafe.rest.rpc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcXmlXesTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login();
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testXmlRpcGetLog_ForDocument() throws  IOException, JAXBException{
    	log.debug("Testing method XES_GET_LOG_FOR_DOCUMENT");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
        
        String documentId = TestUtils.getTestProperty("rest.document.get");

        Map<String,Object> var = Collections.singletonMap("documentId", (Object)documentId);
        XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.XES_GET_LOG_FOR_DOCUMENT, headers, paramRpc);

        TestUtils.saveToFile(response, "DOCUMENT_ID_"+ documentId + "-xml-rpc.xml");
        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testXmlRpcGetLog_ForUser() throws IOException, JAXBException{
    	log.debug("Testing method XES_GET_LOG_FOR_USER");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
        
        String userId = TestUtils.getTestProperty("rest.api.xes.user.get");
        Calendar calendarStartTime = TestUtils.getTestPropertyGetDataTime("rest.api.xes.iso.start.time");
        String dateTime = ISODateTimeFormat.dateTime().print(calendarStartTime.getTimeInMillis());
		
        Map<String, Object> var = new LinkedHashMap<String, Object>();
        var.put("userId", (Object)userId);
        var.put("time", (Object)dateTime);
		XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.XES_GET_LOG_FOR_USER, headers, paramRpc);
        String formatDate = DateUtils.formatCommonDateTimeWithoutWhiteSpaces(calendarStartTime.getTime());
        TestUtils.saveToFile(response, "USER_DATA_"+ formatDate + "-xml-rpc.xml");
        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
	public void testXmlRpcGetLog_ForUser_Start_End_Time() throws IOException, JAXBException {
    	
    	log.debug("Testing method XES_GET_LOG_FOR_USER");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
        
        String userId = TestUtils.getTestProperty("rest.api.xes.user.get");
        Calendar calendarStartTime = TestUtils.getTestPropertyGetDataTime("rest.api.xes.iso.start.time");
        String startTime = ISODateTimeFormat.dateTime().print(calendarStartTime.getTimeInMillis());

        Calendar calendarEndTime = TestUtils.getTestPropertyGetDataTime("rest.api.xes.iso.end.time");
		String endTime = ISODateTimeFormat.dateTime().print(calendarEndTime.getTimeInMillis());

		
        Map<String, Object> var = new LinkedHashMap<String, Object>();
        var.put("userId", (Object)userId);
        var.put("starttime", (Object)startTime);
        var.put("endtime", (Object)endTime);
		XmlMapMapper map = new XmlMapMapper(var);
        String paramRpc = MarshallerUtils.marshall(map);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.XES_GET_LOG_FOR_USER_START_END_TIME, headers, paramRpc);
        String formatDate = DateUtils.formatCommonDateTimeWithoutWhiteSpaces(calendarStartTime.getTime());
        TestUtils.saveToFile(response, "USER_DATA_"+ formatDate + "-xml-rpc.xml");
        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
		
	}

}
