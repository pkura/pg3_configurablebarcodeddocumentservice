package pl.compan.docusafe.rest.rpc;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestJsonRpcJasperReportTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }
    
    @Test
    public void testJsonRpcAddReport() throws JsonGenerationException, JsonMappingException, IOException{
    	log.debug("Testing method JASPER_ADD_REPORT");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        
        String reportType = TestUtils.getTestProperty("rest.api.jasper.type");
        String userName = TestUtils.getTestProperty("rest.jasper.username");
        String reportKind = TestUtils.getTestProperty("rest.api.jasper.report");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -7);
        JasperReportDto report = new JasperReportDto();
		report.setReportType(reportType);
		report.setUserId(userName);
        report.setStartTime(cal);
        report.setEndTime(Calendar.getInstance());
        report.setReport(reportKind);
        String paramRpc = mapper.writeValueAsString(report);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.JASPER_ADD_REPORT, headers, paramRpc);
        TestUtils.saveToFile(response,"jasper-report-json-rpc." + reportType);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsoinRpcGetList_Reports() throws JsonGenerationException, JsonMappingException, IOException{
    	log.debug("Testing method JASPER_GET_REPORT_LIST");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.JASPER_GET_REPORT_LIST, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsoinRpcGetList_TypesReport() throws JsonGenerationException, JsonMappingException, IOException{
    	log.debug("Testing method JASPER_GET_REPORT_TYPE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.JASPER_GET_REPORT_TYPE, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
}
