package pl.compan.docusafe.rest.rpc;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcXmlSubstitutionTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_XML);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testXmlRpcCreateSubstitution_OneDay() throws IOException, JAXBException {
        log.debug("Testing method SUBSTITUTION_CREATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
        int numberOfDays = Integer.valueOf(TestUtils.getTestProperty("rest.api.substitution.num.days"));
        
		String paramRpcUrl = createSubstitutionInXml(numberOfDays,cookie);
        log.info("xml param url rpc " + paramRpcUrl);


        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_CREATE, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());

    }

    @Test
    public void testXmlRpcSubstitutionGetById() throws IOException, JAXBException{
        log.debug("Testing method SUBSTITUTION_GET_BY_ID");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String idSubstitution = TestUtils.getTestProperty("rest.api.substitution.id");
        Map<String, Object> var = Collections.singletonMap("substitutionId", (Object)idSubstitution);
        XmlMapMapper map = new XmlMapMapper(var);
		String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);


        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_GET_BY_ID, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testXmlRpcSubstitutionGetAllCurrent() throws IOException{
        log.debug("Testing method SUBSTITUTION_GET_ALL_CURRENT");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_GET_ALL_CURRENT, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testXmlRpcSubstitutionGetAllCurrentAtTime() throws  IOException, JAXBException{
        log.debug("Testing method SUBSTITUTION_GET_CURRENT_AT_TIME");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());
		
		Map<String, Object> var = Collections.singletonMap("time", (Object)dateTime);
		XmlMapMapper map = new XmlMapMapper(var);
        String paramRpcUrl = MarshallerUtils.marshall(map);
        log.info("xml param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_GET_CURRENT_AT_TIME, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    
    @Test
    public void testXmlRpcSubstitutionDelete() throws IOException, JAXBException{
        log.debug("Testing method SUBSTITUTION_DELETE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

        String idSubstitution = TestUtils.getTestProperty("rest.api.substitution.id");
        Map<String, Object> var = Collections.singletonMap("substitutionId", (Object)idSubstitution);
        XmlMapMapper map = new XmlMapMapper(var);
		String paramRpcUrl = MarshallerUtils.marshall(map );
        log.info("xml param url rpc " + paramRpcUrl);


        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_DELETE, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }


}
