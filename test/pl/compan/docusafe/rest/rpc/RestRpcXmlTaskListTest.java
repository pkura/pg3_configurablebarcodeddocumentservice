package pl.compan.docusafe.rest.rpc;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 03.08.14
 */
public class RestRpcXmlTaskListTest extends RestAbstractRpcTest{
    public static String cookie;

    @BeforeClass
    public static void  init(){
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_XML);
    }

    @AfterClass
    public static void  afterTest(){
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
   	public void testXmlRpcGetUserTaskList() throws IOException, JAXBException{
   		log.debug("Testing method TASK_LIST_USER");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

   		String userName = TestUtils.getTestProperty("rest.tasklist.username");
   		Integer offSet = new Integer(TestUtils.getTestProperty("rest.tasklist.offset"));
		
		Map<String,Object> var = new LinkedHashMap<String,Object>();
		var.put("username", (Object)userName);
		var.put("offset", (Object)offSet);  		
		XmlMapMapper map = new XmlMapMapper(var);
		String paramRpcUrl = MarshallerUtils.marshall(map);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.TASK_LIST_USER, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}

}
