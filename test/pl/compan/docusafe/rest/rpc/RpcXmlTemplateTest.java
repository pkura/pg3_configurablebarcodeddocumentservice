package pl.compan.docusafe.rest.rpc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RpcXmlTemplateTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }
	

    @Test
   	public void testGetAvailableTemplateForDocument() throws JsonGenerationException, JsonMappingException, IOException, JAXBException{
    	HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		String documentId = TestUtils.getTestProperty("rest.document.get");
   		
   		Map<String, Object> var = Collections.singletonMap("documentId", (Object)documentId);
		XmlMapMapper map = new XmlMapMapper(var);
		String paramRpcUrl = MarshallerUtils.marshall(map);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.TEMPLATE_GET_FOR_DOCUMENT,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);

   		
    }
    
    @Test
    public void testJsonRpcOfficeCaseAddDocument() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method OFFICECASE_ADD");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        String documentId = TestUtils.getTestProperty("rest.api.officecase.documentId.addTo");
        String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
        
		Map<String,String> var1 = Collections.singletonMap("documentId", documentId);
		Map<String,String> var2 = Collections.singletonMap("officeCaseId", officeCaseId);
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl1);
        
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_ADD,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }
    
    
}
