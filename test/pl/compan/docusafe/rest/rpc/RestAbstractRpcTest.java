package pl.compan.docusafe.rest.rpc;

import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchDto;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.RpcRequest;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;
import pl.compan.docusafe.web.filter.CasUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestAbstractRpcTest {
    public final Logger log = Logger.getLogger(getClass().getPackage().getName());
    public static String url, user, pass, ticket;
    public static final String SET_COOKIE="Set-Cookie";
    
    public ObjectMapper mapper = new ObjectMapper();

    /**
     *
     * Funkcja pobiera u�ytkownika o okre�lonym id
     *
     * @param id
     * @return UserDto
     */
    public UserDto getUserById(String cookie, String id, MediaType type) {
        Map<String, String> var = Collections.singletonMap("userId", id);
        HttpHeaders headers = TestUtils.getHeaders(cookie, type);
        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<UserDto> responseEntity = template.exchange(getUrl("/user/{userId}"),HttpMethod.GET,requestEntity,UserDto.class,var);

        return responseEntity.getBody();
    }

    protected ResponseEntity<String> executeRPCJsonRequestParameters(RpcMethod method, HttpHeaders headers, String... p) throws IOException {
        RestTemplate template = TestUtils.getRestTemplate();
        log.error("request param: {}" + p );
        String s = getJsonRpcRequest(method, p);
        log.error("full json param {}" + s);

        HttpEntity<String> e = new HttpEntity<String>(s,headers);
        ResponseEntity<String> response = template.exchange(getUrl("jsonrpc"), HttpMethod.POST, e, String.class);
        log.debug("response body: " + response.getBody());
        return response;
    }

    protected ResponseEntity<String> executeRPCXmlRequestParameter(RpcMethod method, HttpHeaders headers, String... p) throws IOException {
        RestTemplate template = TestUtils.getRestTemplate();
        log.error("request param: {}" + p );
        RpcRequest s = getXmlRpcRequest(method, p);
        log.error("full xml param {}"+ s);

        HttpEntity<RpcRequest> e = new HttpEntity<RpcRequest>(s,headers);
        ResponseEntity<String> response = template.exchange(getUrl("xmlrpc"), HttpMethod.POST, e, String.class);
        log.debug("response body: " + response.getBody());
        return response;
    }
    
    protected void executeRPCJsonRequest(RpcMethod method, HttpHeaders headers, String... p) throws IOException {
        RestTemplate template = TestUtils.getRestTemplate();
        log.error("request param: {}" + p );
        String s = getJsonRpcRequest(method, p);
        log.error("full json param {}" + s);

        HttpEntity<String> e = new HttpEntity<String>(s,headers);
        ResponseEntity<String> response = template.exchange(getUrl("jsonrpc"), HttpMethod.POST, e, String.class);
        log.debug("full text Search: " + response.getBody());
    }

    protected String getJsonRpcRequest(RpcMethod method, String... p) throws IOException {
        RpcRequest rpcR = new RpcRequest();
        rpcR.setMethodName(method.getName());
        for(String pp : p){
            rpcR.addParam(pp);
        }
        String s = mapper.writeValueAsString(rpcR);
        return s;
    }

    protected RpcRequest getXmlRpcRequest(RpcMethod method, String... p) throws IOException {
        RpcRequest rpcR = new RpcRequest();
        rpcR.setMethodName(method.getName());
        for(String pp : p){
            rpcR.addParam(pp);
        }
        return rpcR;
    }

    protected static String getUrl(String uri){
        return url  + '/'+ uri;
    }
    
    /**
     * Funkcja tworzy document dto
     * @param normal 
     * @param searchFieldName 
     * @param isSearchField 
     * @param limit 
     * @param offset 
     * @param sortingField 
     * @return OfficeDocumentDto
     */
    protected OfficeDocumentDto createOfficeDocumentDto(String normal, String searchFieldName, String isSearchField, int limit, int offset, String sortingField) {
		OfficeDocumentDto officeDocumentDto = new OfficeDocumentDto();
		officeDocumentDto.setDockindCn(normal);
		officeDocumentDto.setSortingField(sortingField);
		officeDocumentDto.setLimit(limit);
		officeDocumentDto.setOffset(offset);
        String description = TestUtils.getTestProperty("rest.document.search.description");
        if(StringUtils.isNotBlank(description))
            officeDocumentDto.setDescription(description);

		officeDocumentDto.setFieldValues(Lists.newArrayList(new FieldView(searchFieldName, isSearchField)));
		return officeDocumentDto;
	}
    
    /**
     * Funkcja tworzy nowy wpis s�ownika dokumentu
     * @param fields
     * @param dictionaryCn np. SENDER
     * @param dockindCn np. normal
     * @return DictionaryDto
     */
    protected DictionaryDto createDictionaryDto(FieldView[] fields, String dictionaryCn, String dockindCn) {
		DictionaryDto dictionaryDto = new DictionaryDto();
		dictionaryDto.setDictionaryCn(dictionaryCn);
        dictionaryDto.setDockindCn(dockindCn);
        dictionaryDto.setFieldValues(Lists.newArrayList(fields));
		return dictionaryDto;
	}
    
    /**
    *
    * Funkcja tworzy object substitution dto i mapuje go na json'a
    *
    * @param numberOfDays
    * @param cookie 
    * @return JSON String
    * @throws IOException
    * @throws JsonGenerationException
    * @throws JsonMappingException
    */
   protected String createSubstitutionInJson(int numberOfDays, String cookie) throws IOException,
           JsonGenerationException, JsonMappingException {
       Calendar calendar = Calendar.getInstance();
       calendar.add(Calendar.DAY_OF_MONTH,30);
       SubstitutionDto substitutionDto = new SubstitutionDto();
       substitutionDto.setStartTime(calendar);
       calendar.add(Calendar.DAY_OF_MONTH, numberOfDays);
       substitutionDto.setEndTime(calendar);
       UserDto userSubsituted = getUserById(cookie, TestUtils.getTestProperty("rest.api.substituted.user.id"), MediaType.APPLICATION_JSON);
       substitutionDto.setUserSubsituted(userSubsituted);
       UserDto userSubstituting = getUserById(cookie, TestUtils.getTestProperty("rest.api.substituting.user.id"), MediaType.APPLICATION_JSON);
       substitutionDto.setUserSubstituting(userSubstituting);

       String jsonRequestDto = mapper.writeValueAsString(substitutionDto);

       return jsonRequestDto;
   }
   
   /**
   *
   * Funkcja tworzy object substitution dto i mapuje go na xml'a
   *
   * @param numberOfDays
   * @param cookie 
   * @return JSON String
   * @throws IOException
 * @throws JAXBException 
   * @throws JsonGenerationException
   * @throws JsonMappingException
   */
  protected String createSubstitutionInXml(int numberOfDays, String cookie) throws IOException, JAXBException {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.MILLISECOND,2000);
      SubstitutionDto substitutionDto = new SubstitutionDto();
      substitutionDto.setStartTime(calendar);
      calendar.add(Calendar.DAY_OF_MONTH, numberOfDays);
      substitutionDto.setEndTime(calendar);
      UserDto userSubsituted = getUserById(cookie, TestUtils.getTestProperty("rest.api.substituted.user.id"), MediaType.APPLICATION_JSON);
      substitutionDto.setUserSubsituted(userSubsituted);
      UserDto userSubstituting = getUserById(cookie, TestUtils.getTestProperty("rest.api.substituting.user.id"), MediaType.APPLICATION_JSON);
      substitutionDto.setUserSubstituting(userSubstituting);

      String xmlRequestDto = MarshallerUtils.marshall(substitutionDto);

      return xmlRequestDto;
  }
   
   /**
    * Funkcja tworzy nowy dzia�
    * @param mediaType TODO
	* @param nameOfDivision 
	* @param divisionType 
	* @param code 
	* @param parentId 
    * @return DivisionDto
    */
   protected DivisionDto createDivisionDto(String mediaType, String nameOfDivision, DivisionType divisionType, String code, Long parentId) {
       DivisionDto divisionDto = new DivisionDto();
       divisionDto.setType(divisionType);
       divisionDto.setGuid(nameOfDivision + mediaType);
       divisionDto.setName(nameOfDivision + mediaType);
       divisionDto.setCode(code);
       divisionDto.setParentId(parentId);
       return divisionDto;
   }

    protected static String login() {
        return login(MediaType.valueOf(TestUtils.getTestProperty("test.mediaType")));
    }

   protected static String login(MediaType type) {
       HttpHeaders headers = TestUtils.getHeaders(null, type);

       RestTemplate template = TestUtils.getRestTemplate();

       HttpEntity<String> e = new HttpEntity<String>("", headers);
       ResponseEntity<String> loginResponse;
       if(Boolean.valueOf(TestUtils.getTestProperty("cas.enabled"))){
    	   ticket = TestUtils.getTestProperty("rest.api.proxy.ticket");
    	   System.out.println("Zalogowanie do systemu z ticket'em = " + ticket);
    	   loginResponse = template.postForEntity(
                   getUrl("login-cas?t={ticket}"), e, String.class, ticket);  
       } else {
    	   System.out.println("Zalogowanie do systemu jako " + user);
    	   loginResponse = template.postForEntity(
                   getUrl("login?username={username}&password={password}"), e, String.class, user, pass);       	
       }

       return loginResponse.getHeaders().get(SET_COOKIE).iterator().next();
   }
   
   /**
    * Funkcja zwraca dzia� dto, kt�ry zostanie uaktualniony
    * @param id
    * @param nameOfDivision 
    * @param parentId 
    * @return DivisionDto
    */
   protected DivisionDto updateDivisionDto(String id, String nameOfDivision, Long parentId) {
       DivisionDto divisionDto = new DivisionDto();
      
       nameOfDivision = nameOfDivision + "update";
       divisionDto.setId(Long.valueOf(id));
       divisionDto.setGuid(nameOfDivision);
       divisionDto.setName(nameOfDivision);
       divisionDto.setParentId(parentId);
       return divisionDto;
   }
   /**
    * Funkcja tworzy spraw�
    * @param clerk 
 * @param title 
 * @param description 
 * @param priority 
 * @param dateTime 
 * @param documentId 
 * @param portfolioId 
    * 
    * @return OfficeCaseDto
    */
   protected OfficeCaseDto createOfficeCaseDto(String clerk, String title, String description, Integer priority, String dateTime, String documentId, Long portfolioId) {
		OfficeCaseDto officeCaseDto = new OfficeCaseDto();
       if(StringUtils.isNotEmpty(clerk))
		    officeCaseDto.setClerk(clerk);

		officeCaseDto.setClosed(false);

       if(StringUtils.isNotEmpty(title))
		    officeCaseDto.setTitle(title);

       if(StringUtils.isNotEmpty(description))
		    officeCaseDto.setDescription(description);

	    officeCaseDto.setPriority(priority);

		officeCaseDto.setFinishDateIso(dateTime);

       if(StringUtils.isNotEmpty(documentId))
		    officeCaseDto.setDocumentId(documentId);

		officeCaseDto.setPortfolioId(portfolioId);

		return officeCaseDto;
	}
   
   protected HttpEntity<String> createRequestEntityForCreateSubstitution(int dayAomounts, String idUserSubsituted, String idUserSubstituting, String cookie) throws IOException,
	JsonGenerationException, JsonMappingException {
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, +200);
		
		SubstitutionDto substitutionDto = new SubstitutionDto();
		substitutionDto.setStartTime(calendar);
		calendar.add(Calendar.DAY_OF_MONTH, dayAomounts);
		substitutionDto.setEndTime(calendar);
		UserDto userSubsituted = getUserById(cookie, idUserSubsituted, MediaType.APPLICATION_JSON);
		substitutionDto.setUserSubsituted(userSubsituted);
		UserDto userSubstituting = getUserById(cookie, idUserSubstituting, MediaType.APPLICATION_JSON);
		substitutionDto.setUserSubstituting(userSubstituting);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequest = mapper.writeValueAsString(substitutionDto);

		HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest,headers);
		return requestEntity;
	}
   
     protected HttpEntity<String> createXmlRequestEntityForCreateSubstitution(int numDays, String idUserSubsituted, String idUserSubstituting, String cookie) throws JAXBException {
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);

		Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, +20);
		
		SubstitutionDto substitutionDto = new SubstitutionDto();
		substitutionDto.setStartTime(calendar);
		calendar.add(Calendar.DAY_OF_MONTH, numDays);
		substitutionDto.setEndTime(calendar);
		UserDto userSubsituted = getUserById(cookie, idUserSubsituted, MediaType.APPLICATION_XML);
		substitutionDto.setUserSubsituted(userSubsituted);
		UserDto userSubstituting = getUserById(cookie, idUserSubsituted, MediaType.APPLICATION_XML);;
		substitutionDto.setUserSubstituting(userSubstituting);
		

		String xmlRequest = MarshallerUtils.marshall(substitutionDto);

		HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest,headers);
		return requestEntity;
	}
	
	protected ResponseEntity<String> createResponseEntityForCreateSubstitution(
			HttpEntity<String> requestEntity) {
		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/substitution"), HttpMethod.POST, requestEntity, String.class);
		return responseEntity;
	}
	
	protected DictionaryDto createDictionaryDto(List<FieldView> fields) {
		DictionaryDto dictionaryDto = new DictionaryDto();
		dictionaryDto.setDictionaryCn("SENDER");
	    dictionaryDto.setDockindCn("normal");
	    dictionaryDto.setFieldValues(fields);
		return dictionaryDto;
	}
	
	
	protected DictionaryDto createDictionaryDto(String dockindCn, String dictionaryCn, List<FieldView> fields) {
		DictionaryDto dictionaryDto = new DictionaryDto();
	    dictionaryDto.setDictionaryCn(dictionaryCn);
	    dictionaryDto.setDockindCn(dockindCn);
	    dictionaryDto.setFieldValues(fields);
		return dictionaryDto;
	}
	
	protected FullTextSearchDto createFullTextSeatchDto() {
		FullTextSearchDto fts = new FullTextSearchDto();
	    fts.setText(TestUtils.getTestProperty(TestUtils.REST_FULLTEXTSEARCH_TEXT));
	    fts.setLimit(50);
	    fts.setOffset(0);
		return fts;
	}

	protected String createJsonFromDivisionDto(DivisionDto divisionDto) throws JsonGenerationException, JsonMappingException, IOException {
		return mapper.writeValueAsString(divisionDto);
	}

	protected String createJsonFromUserDto(UserDto userDto) throws JsonGenerationException, JsonMappingException, IOException {
		return mapper.writeValueAsString(userDto);
	}
	
}
