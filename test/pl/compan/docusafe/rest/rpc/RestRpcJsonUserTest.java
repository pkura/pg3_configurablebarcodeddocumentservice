package pl.compan.docusafe.rest.rpc;

import com.google.common.collect.Lists;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.ListIds;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.TestUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 03.08.14
 */
public class RestRpcJsonUserTest extends RestAbstractRpcTest{
    public static String cookie;

    @BeforeClass
    public static void  init(){
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void  afterTest(){
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testJsonRpcGetAllUsers() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method USER_FIND_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_FIND_ALL, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testJsonRpcGetAllRoles() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method ROLE_FIND_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.ROLE_FIND_ALL, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testJsonRpcGetUserRoles() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method ROLE_FIND_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        String userName = TestUtils.getTestProperty("rest.api.user.get");
        Map<String, String> var = Collections.singletonMap("username", userName);
        String paramRpc = mapper.writeValueAsString(var);
        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_ROLE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testJsoinRpcGetUser_ByName() throws JsonGenerationException, JsonMappingException, IOException{
    	log.debug("Testing method USER_FIND_ONE");

        String userName = TestUtils.getTestProperty("rest.api.user.get.name");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        Map<String, String> var = Collections.singletonMap("userId", userName);
        String paramRpc = mapper.writeValueAsString(var);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_FIND_ONE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }


    @Test
     public void testJsonRpcGetUser() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_FIND_ONE");

        String userId = TestUtils.getTestProperty("rest.api.user.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        Map<String, String> var = Collections.singletonMap("userId", userId);
        String paramRpc = mapper.writeValueAsString(var);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_FIND_ONE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testJsonRpcGetUserByName() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_FIND_ONE");

        String userId = TestUtils.getTestProperty("rest.api.user.get.name");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        Map<String, String> var = Collections.singletonMap("userId", userId);
        String paramRpc = mapper.writeValueAsString(var);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_FIND_ONE_BY_NAME, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcCreateUser_New() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_CREATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        UserDto userDto = new UserDto();
        userDto.setFirstname(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE) + "rpcJson");
        userDto.setLastname(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcJson");
        userDto.setUsername(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcJson");
        userDto.setPassword(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcJson");
        String paramRpc = mapper.writeValueAsString(userDto);
        log.info("json param rpc " + paramRpc);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_CREATE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testJsonRpcUpdateUserRoles() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_UPDATE");
        String userId = TestUtils.getTestProperty("rest.api.user.get");

        String idUserCreated = userId;
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        ListIds ids = new ListIds(Lists.newArrayList(43L,22L));
        String paramRpc = mapper.writeValueAsString(ids);
        log.info("json param rpc " + paramRpc);

        Map<String,String> var = Collections.singletonMap("username", idUserCreated);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_UPDATE_ROLE, headers, paramRpc, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }

    @Test
    public void testJsonRpcUpdateUser() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_UPDATE");
        String userId = TestUtils.getTestProperty("rest.api.user.get");

        String idUserCreated = userId;
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        UserDto userDto = new UserDto();
        userDto.setId(Long.valueOf(idUserCreated));
        userDto.setFirstname("updatefirstnameRPC");
        userDto.setLastname("updatelastaneRPC");
        userDto.setUsername("updateusernameRPC");
        String paramRpc = mapper.writeValueAsString(userDto);
        log.info("json param rpc " + paramRpc);

        Map<String,String> var = Collections.singletonMap("userId", idUserCreated);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_UPDATE, headers, paramRpc, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }
        
    @Test
    public void testJsonRpcGetUserSubstitutions() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_GET_SUBSTITUTIONS_BY_USER");

        String userId = TestUtils.getTestProperty("rest.api.user.substitutions");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        Map<String, String> var = Collections.singletonMap("userId", userId);
        String paramRpc = mapper.writeValueAsString(var);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_GET_SUBSTITUTIONS_BY_USER, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcGetUserSubstitute() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_GET_SUBSTITUTE");

        String userId = TestUtils.getTestProperty("rest.api.user.substitute");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        Map<String, String> var = Collections.singletonMap("userId", userId);
        String paramRpc = mapper.writeValueAsString(var);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_GET_SUBSTITUTE, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcGetUserDivisions() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_GET_DIVISIONS");

        String userId = TestUtils.getTestProperty("rest.api.user.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        Map<String, String> var = Collections.singletonMap("userId", userId);
        String paramRpc = mapper.writeValueAsString(var);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_GET_DIVISIONS, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must be not null", response.getBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
    
    @Test
    public void testJsonRpcDeleteUser() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_DELETE");

        String userId = TestUtils.getTestProperty("rest.api.user.get");
        String idUserCreated = userId;

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        Map<String,String> var = Collections.singletonMap("userId", idUserCreated);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.USER_DELETE, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }


}
