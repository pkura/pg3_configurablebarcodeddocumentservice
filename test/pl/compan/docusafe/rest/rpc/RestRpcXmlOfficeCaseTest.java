package pl.compan.docusafe.rest.rpc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcXmlOfficeCaseTest extends RestAbstractRpcTest {
	
	 public static String cookie;

	    @BeforeClass
	    public static void init() {
	        BasicConfigurator.configure();
	        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
	        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
	        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
	        // login one time per test whole class
	        cookie = login(MediaType.APPLICATION_XML);
	    }

	    @AfterClass
	    public static void afterTest() {
	        cookie = null;
	    }

	    @Before
	    public void setUp() throws Exception {
	        log.debug("cookie: " + cookie);
	    }

	    @After
	    public void tearDown() throws Exception {
	        log.debug("after");
	    }

	    @Test
	    public void testXmlRpcOfficeCaseCreate() throws   IOException, JAXBException{
	        log.debug("Testing method OFFICECASE_CREATE");

	        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
	        
	        String clerk = TestUtils.getTestProperty("rest.api.officecase.create.clerk");
			String title = TestUtils.getTestProperty("rest.api.officecase.create.title");
			String description = TestUtils.getTestProperty("rest.api.officecase.create.description");
			Integer priority = Integer.parseInt(TestUtils.getTestProperty("rest.api.officecase.create.priority"));
			
			Calendar calendar = TestUtils.getTestPropertyGetDataTime("rest.api.officecase.iso.time");
	        String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());
			
			String documentId = TestUtils.getTestProperty("rest.api.officecase.create.documentId");
			Long portfolioId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.create.portfolioId"));
			
			OfficeCaseDto officeCaseDto = createOfficeCaseDto(clerk, title, description, priority, dateTime, documentId, portfolioId);
	        String paramBody = MarshallerUtils.marshall(officeCaseDto);
	        
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_CREATE,
	                headers,paramBody);

	        assertNotNull("Must be not null", response);
	        assertNotNull("Response body must not be null", response.hasBody());
	        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
	    }
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGetUser() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET_USER");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
	   		String userName = TestUtils.getTestProperty("rest.api.officecase.user.case");
	   		
			Map<String, Object> var = Collections.singletonMap("username", (Object)userName);
	   		XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map);
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_USER,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    @Test
	    public void testXmlRpcOfficeCaseAddDocument() throws   IOException, JAXBException{
	        log.debug("Testing method OFFICECASE_ADD");

	        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
	        String documentId = TestUtils.getTestProperty("rest.api.officecase.documentId.addTo");
	        String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
	        
			Map<String,Object> var = new LinkedHashMap<String,Object>();
			var.put("documentId", documentId);
			var.put("officeCaseId", officeCaseId);
	   		XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
	        
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_ADD,
	                headers,paramRpcUrl);

	        assertNotNull("Must be not null", response);
	        assertNotNull("Response body must not be null", response.hasBody());
	        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
	    }
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGet() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);  		
	   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
	   		
			Map<String,Object> var = Collections.singletonMap("officeCaseId", (Object)officeCaseId);
	   		XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGetDocument() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET_DOCUMENT");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
	   		String documentId = TestUtils.getTestProperty("rest.api.officecase.get.documentId");
	   		
			Map<String, Object> var = Collections.singletonMap("documentId", (Object)documentId );
			XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_DOCUMENT,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    @Test
	   	public void testXmlRpcOfficeCaseRemoveDocument() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_REMOVE_DOCUMENT");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML, "text", MediaType.TEXT_HTML);
	   		String documentId = TestUtils.getTestProperty("rest.api.officecase.get.documentId");
	   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
	   		
			Map<String, Object> var = new LinkedHashMap<String, Object>();
			var.put("documentId", (Object)documentId );
			var.put("officeCaseId", (Object)officeCaseId );
	   		XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_REMOVE_DOCUMENT,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	   
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGet_OfficeCaseToOfficeCase() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET_ALL_OFFICECASES");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);  		
	   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
	   		
			Map<String,Object> var = Collections.singletonMap("officeCaseId", (Object)officeCaseId);
			XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_ALL_OFFICECASES,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGet_FromDivision() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET_ALL_OFFICECASES_FROM_DIVISION");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML, "xml", MediaType.TEXT_HTML);  		
	   		String guid = TestUtils.getTestProperty("rest.api.officecase.guid.case");
	   		
			Map<String,Object> var = Collections.singletonMap("guid", (Object)guid);
			XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_ALL_OFFICECASES_FROM_DIVISION,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGet_Priorities() throws   IOException{
	   		log.debug("Testing method OFFICECASE_GET_ALL_PRIORITIES_FOR_CASE");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);  		
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_ALL_PRIORITIES_FOR_CASE,
																			   headers);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	 
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGetPortfolio() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET_PORTFOLIO");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
	   		String guid = TestUtils.getTestProperty("rest.api.officecase.guid.case");
	   		
			Map<String,Object> var = Collections.singletonMap("guid", (Object)guid);
			XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_PORTFOLIO,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGetDocuments() throws   IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET_DOCUMENTS");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
	   		String officeCaseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
	   			   		
			Map<String,Object> var = Collections.singletonMap("officeCaseId", (Object)officeCaseId);
			XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_DOCUMENTS,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Must be not null", response);
			assertNotNull("Response body must not be null", response.hasBody());
	   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    
	    @Test
	   	public void testXmlRpcOfficeCaseGetAudit() throws IOException, JAXBException{
	   		log.debug("Testing method OFFICECASE_GET_AUDIT");

	   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
	   		String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
	   		
			Map<String,Object> var = Collections.singletonMap("id", (Object)id);
			XmlMapMapper map = new XmlMapMapper(var);
			String paramRpcUrl = MarshallerUtils.marshall(map );
			log.info("xml param url rpc " + paramRpcUrl);
			
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_AUDIT,
																			   headers,paramRpcUrl);
	          
			assertNotNull("Empty response - ",response);
	        assertNotNull("Empty body response  - ",response.getBody());
	        assertEquals("Do not fetch history of case - ",HttpStatus.OK, response.getStatusCode());
	   		
	    }
	    
	    @Test
	    public void testXmlRpcOfficeCaseUpdate() throws IOException, JAXBException{
	        log.debug("Testing method OFFICECASE_UPDATE");

	        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
	        
	        String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
			String clerk = TestUtils.getTestProperty("rest.api.officecase.create.clerk");
			String title = TestUtils.getTestProperty("rest.api.officecase.create.title");
			String description = TestUtils.getTestProperty("rest.api.officecase.create.description");
			Integer priority = Integer.parseInt(TestUtils.getTestProperty("rest.api.officecase.create.priority"));
			
			Calendar calendar = TestUtils.getTestPropertyGetDataTime("rest.api.officecase.iso.time");
	        String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());
			
			String documentId = TestUtils.getTestProperty("rest.api.officecase.create.documentId");
			Long portfolioId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.create.portfolioId"));
			OfficeCaseDto officeCaseDto = createOfficeCaseDto(clerk, title, description, priority, dateTime, documentId, portfolioId);
	        
	        officeCaseDto.setId(Long.valueOf(id));
	        
	        String paramBody = MarshallerUtils.marshall(officeCaseDto);
	        
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_UPDATE,
	                headers,paramBody);

	        assertNotNull("Must be not null", response);
	        assertNotNull("Response body must not be null", response.hasBody());
	        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
	    }
	    
	    @Test
	    public void testXmlRpcAddOfficeCaseToOfficeCase() throws IOException, JAXBException{
	        log.debug("Testing method OFFICECASE_ADD_TO_OFFICECASE");

	        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
	        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));
	        Long officeCaseAddId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.addId"));
	        
			Map<String,Object> vars = new LinkedHashMap<String,Object>();
			vars.put("officeCaseId", (Object)officeCaseId);
			vars.put("officeCaseAddId", (Object)officeCaseAddId);
			
			XmlMapMapper map = new XmlMapMapper(vars);
			String paramRpcUrl = MarshallerUtils.marshall(map );

			log.info("xml param url rpc " + paramRpcUrl);
	        
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_ADD_TO_OFFICECASE,
	                headers,paramRpcUrl);

	        assertNotNull("Must be not null", response);
	        assertNotNull("Response body must not be null", response.hasBody());
	        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
	    }
	    
	    @Test
	    public void testXmlRpcRemoveOfficeCaseFromOfficeCase() throws IOException, JAXBException{
	        log.debug("Testing method OFFICECASE_REMOVE_FROM_OFFICECASE");

	        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
	        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));
	        Long officeCaseRemoveId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.removeId"));
	        
			Map<String,Object> vars = new LinkedHashMap<String,Object>();
			vars.put("officeCaseId", (Object)officeCaseId);
			vars.put("officeCaseRemoveId", (Object)officeCaseRemoveId);
			
			XmlMapMapper map = new XmlMapMapper(vars);
			String paramRpcUrl = MarshallerUtils.marshall(map );

			log.info("xml param url rpc " + paramRpcUrl);
	        
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_REMOVE_FROM_OFFICECASE,
	                headers,paramRpcUrl);

	        assertNotNull("Must be not null", response);
	        assertNotNull("Response body must not be null", response.hasBody());
	        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
	    }
	    
	    @Test
	    public void testXmlRpcOfficeCaseDelete() throws IOException, JAXBException{
	        log.debug("Testing method OFFICECASE_DELETE");

	        String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
	        String reason = TestUtils.getTestProperty("rest.api.officecase.delete.reason");;
	        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
	        KeyElement keyElement = new KeyElement(id, reason);
	        
	        String paramBody = MarshallerUtils.marshall(keyElement);
	        
			ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_DELETE,
	                headers,paramBody);

	        assertNotNull("Must be not null", response);
	        assertNotNull("Response body must not be null", response.hasBody());
	        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
	    }


}
