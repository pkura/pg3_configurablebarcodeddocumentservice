package pl.compan.docusafe.rest.rpc;

import com.beust.jcommander.internal.Maps;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.TestUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class RestRpcJsonDivisionTest extends RestAbstractRpcTest {
    public static String cookie;

    @BeforeClass
    public static void init() {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterTest() {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testJsonRpcDivisionCreate_NewDivision() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method DIVISION_CREATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        String nameOfDivision = TestUtils.getTestProperty("rest.api.division.create");
        String code = TestUtils.getTestProperty("rest.api.division.code");
		Long parentId = new Long(TestUtils.getTestProperty("rest.api.division.parentId"));
        
		DivisionDto divisionDto = createDivisionDto("json", nameOfDivision, DivisionType.DIVISION, code, parentId);
        String paramRpcUrl = mapper.writeValueAsString(divisionDto);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_CREATE,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testJsonRpcDivisionGetById() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method DIVISION_GET");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_GET,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    
    @Test
    public void testJsonRpcDivisionAddUser() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method DIVISION_ADD_USER");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);

        String idUser = TestUtils.getTestProperty("rest.api.division.userId");
        UserDto userDto = getUserById(cookie, idUser,MediaType.APPLICATION_JSON);

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));

        String paramRpcUrl1 = mapper.writeValueAsString(userDto);
        String paramRpcUrl2 = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl1);
        log.info("json param url rpc " + paramRpcUrl2);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_ADD_USER,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.CREATED, response.getStatusCode());

    }

    @Test
    public void testJsonRpcDivisionRemoveUser() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method DIVISION_ADD_USER");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String idUser = TestUtils.getTestProperty("rest.api.division.userId");

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
        Map<String, Long> var2 = Collections.singletonMap("userId", Long.valueOf(idUser));
        String paramRpcUrl1 = mapper.writeValueAsString(var2);
        String paramRpcUrl2 = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl1);
        log.info("json param url rpc " + paramRpcUrl2);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_REMOVE_USER,
                headers,paramRpcUrl2, paramRpcUrl1);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());

    }


    @Test
    public void testJsonRpcDivisionGetUsers() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method DIVISION_GET_USERS");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_GET_USERS,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }

    @Test
    public void testJsonRpcDivisionGetSubdivisions() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method DIVISION_GET_SUBDIVISIONS");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String idDivision = TestUtils.getTestProperty("rest.api.division.get");
        Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_GET_SUBDIVISIONS,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    
    @Test
    public void testJsonRpcDivisionGetAll() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method DIVISION_GET_ALL");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_GET_ALL, headers);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    
    @Test
    public void testJsonRpcDivisionGetByGuid() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("Testing method DIVISION_GET_BY_GUID");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String guid = TestUtils.getTestProperty("rest.api.division.guid");
        Map<String, String> var = Collections.singletonMap("guid", guid);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_GET_BY_GUID,
                headers,paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

    }
    
    @Test
    public void testJsonRpcDivisionUpdate() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method DIVISION_UPDATE");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);

        String idDivisionUpdate = TestUtils.getTestProperty("rest.api.division.get");
        String nameOfDivision = TestUtils.getTestProperty("rest.api.division.create");
		Long parentId = new Long(TestUtils.getTestProperty("rest.api.division.parentId"));
		
		DivisionDto divisionDto = updateDivisionDto(idDivisionUpdate, nameOfDivision, parentId);
        Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivisionUpdate));
        String paramRpcUrl1 = mapper.writeValueAsString(divisionDto);
        String paramRpcUrl2 = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl1);
        log.info("json param url rpc " + paramRpcUrl2);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_UPDATE,
                headers,paramRpcUrl1,paramRpcUrl2);

        assertNotNull("Must be not null", response);
        assertNotNull("Response body must not be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());

    }

    
    
  
    
    @Test
   	public void testJsonRpcDivisionDelete() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_DELETE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"json",MediaType.TEXT_HTML);
   		String idDivisionUpdate = TestUtils.getTestProperty("rest.api.division.get");
   		
		Map<String,Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivisionUpdate));
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_DELETE,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertFalse("Must be false", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
   		
    }
      
}
