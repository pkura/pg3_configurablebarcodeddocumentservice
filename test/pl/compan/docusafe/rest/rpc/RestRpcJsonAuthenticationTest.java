package pl.compan.docusafe.rest.rpc;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.util.TestUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 03.08.14
 */
public class RestRpcJsonAuthenticationTest extends RestAbstractRpcTest{
    public static String cookie;

    @BeforeClass
    public static void  init(){
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
    }

    @AfterClass
    public static void  afterTest(){
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    @Test
    public void testJsonRpcAuthLogin() throws JsonGenerationException, JsonMappingException, IOException{
    	log.debug("Testing method AUTHENTICATION_LOGIN");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON, "json", MediaType.TEXT_HTML);
        String user = TestUtils.getTestProperty("rest.api.user");
        String pass = TestUtils.getTestProperty("rest.api.pass");
        
        Map<String, String> var1 = Collections.singletonMap("username", user);
        Map<String, String> var2 = Collections.singletonMap("password", pass);
        String paramRpc1 = mapper.writeValueAsString(var1);
        String paramRpc2 = mapper.writeValueAsString(var2);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.AUTHENTICATION_LOGIN, headers, paramRpc1,paramRpc2);

        assertNotNull("Must be not null", response);
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }

    @Test
    public void testJsonRpcAuthLogout() throws JsonGenerationException, JsonMappingException, IOException{
    	log.debug("Testing method AUTHENTICATION_LOGOUT");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON, "json", MediaType.TEXT_HTML);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.AUTHENTICATION_LOGOUT, headers);

        assertNotNull("Must be not null", response);
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }

    @Test
    public void testJsonRpcAuthLoginCAS() throws JsonGenerationException, JsonMappingException, IOException{
    	log.debug("Testing method AUTHENTICATION_LOGIN_CAS");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON, "json", MediaType.TEXT_HTML);
        String ticket = TestUtils.getTestProperty("rest.api.proxy.ticket");
        
        Map<String, String> var1 = Collections.singletonMap("ticket", ticket);

        String paramRpc = mapper.writeValueAsString(var1);

        ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.AUTHENTICATION_LOGIN_CAS, headers, paramRpc);

        assertNotNull("Must be not null", response);
        assertEquals("We espect OK status code", HttpStatus.ACCEPTED, response.getStatusCode());
    }

}
