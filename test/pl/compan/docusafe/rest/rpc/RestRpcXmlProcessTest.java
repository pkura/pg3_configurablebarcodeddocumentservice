package pl.compan.docusafe.rest.rpc;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import com.beust.jcommander.internal.Lists;

import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.ManualAssignmentDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.XmlMapMapper;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 03.08.14
 */
public class RestRpcXmlProcessTest extends RestAbstractRpcTest{
    public static String cookie;

    @BeforeClass
    public static void  init(){
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        // login one time per test whole class
        cookie = login(MediaType.APPLICATION_XML);
    }

    @AfterClass
    public static void  afterTest(){
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {
        log.debug("after");
    }

    
    @Test
   	public void testXmlRpcGetProcessActions() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_GET_ACTIONS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		String documentId = TestUtils.getTestProperty("rest.document.get");
   		
   		Map<String, Object> var = new LinkedHashMap<String,Object>();
   		var.put("documentId", (Object)documentId);

   		XmlMapMapper map = new XmlMapMapper(var);
   		String paramRpcUrl = MarshallerUtils.marshall(map);
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_GET_ACTIONS, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcExecuteProcessAction() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_EXECUTE_ACTION");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);
   		String documentId = TestUtils.getTestProperty("rest.document.get");
   		String processAction = TestUtils.getTestProperty("rest.document.processAction");
   		
   		
   		Map<String, Object> vars = new LinkedHashMap<String, Object>();
		vars.put("documentId", (Object) documentId);
		vars.put("processAction", (Object) processAction);

		XmlMapMapper map = new XmlMapMapper(vars);
   		String paramRpcUrl = MarshallerUtils.marshall(map);
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_EXECUTE_ACTION, headers, paramRpcUrl);
          
		assertNotNull("Empty response - ",response);
        assertEquals("We espect ACCEPTED status code - ",HttpStatus.ACCEPTED, response.getStatusCode());
   	}
    
    @Test
   	public void testAssignMultiManualAssignment() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_MULTI_MANUAL_ASSIGNMENT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

   		Map<String, Object> vars = new LinkedHashMap<String, Object>();
		vars.put("documentId", (Object)TestUtils.getTestProperty("rest.document.get"));
		
		List<ManualAssignmentDto> elements = Lists.newArrayList();
        String targetName = TestUtils.getTestProperty("rest.document.process.target.name");
		Boolean kind = Boolean.valueOf(TestUtils.getTestProperty("rest.document.process.kind"));
		Boolean targetKind = Boolean.valueOf(TestUtils.getTestProperty("rest.document.process.kind.target"));;
		elements.add(new ManualAssignmentDto(targetName ,kind , targetKind ));

		String paramRpcUrl = MarshallerUtils.marshall(new XmlMapMapper(vars));
		String paramBody = MarshallerUtils.marshall(new ListDtos<ManualAssignmentDto>(elements));
		log.info("xml param url rpc " + paramRpcUrl);
		log.info("xml param url rpc " + paramBody);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_MULTI_MANUAL_ASSIGNMENT, headers, paramBody,paramRpcUrl);
          
		assertNotNull("Empty response - ",response);
        assertEquals("We espect ACCEPTED status code - ",HttpStatus.ACCEPTED, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcGetDocumentProcesses() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_GET_DOCUMENT_PROCESSES");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

   		Map<String, Object> var = Collections.singletonMap("documentId", (Object)TestUtils.getTestProperty("rest.document.get"));

		String paramRpcUrl = MarshallerUtils.marshall(new XmlMapMapper(var));
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_GET_DOCUMENT_PROCESSES, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcGetProcessDocuments() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_GET_PROCESS_DOCUMENTS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

   		Map<String, Object> var = new LinkedHashMap<String, Object>();
        var.put("processName", (Object)TestUtils.getTestProperty("rest.document.processName"));

		String paramRpcUrl = MarshallerUtils.marshall(new XmlMapMapper(var));
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_GET_PROCESS_DOCUMENTS, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcGetProcessDefinitions() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_GET_PROCESS_DOCUMENTS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);

   		Map<String, Object> var = new LinkedHashMap<String, Object>();
        var.put("processName", (Object)TestUtils.getTestProperty("rest.document.processName"));

		String paramRpcUrl = MarshallerUtils.marshall(new XmlMapMapper(var));
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_GET_PROCESS_DEFINITIONS, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcGetProcessDiagram() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_GET_PROCESS_DIAGRAM");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);

   		Map<String, Object> var = new LinkedHashMap<String, Object>();
        var.put("processId", (Object)TestUtils.getTestProperty("rest.document.process.instance.id"));

		String paramRpcUrl = MarshallerUtils.marshall(new XmlMapMapper(var));
		log.info("xml param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_GET_PROCESS_DIAGRAM, headers, paramRpcUrl);
          
		byte[] encodedDocument = response.getBody().getBytes();
        byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

        File tempFile = new File(TestUtils.getTestProperty(TestUtils.REST_TEST_PATH) + "/process_" + var.get("processId") + ".png");
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

        assertNotNull("Empty response - ",response);
        assertNotNull("Empty body response  - ",response.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcLoadProcessDefinitions() throws IOException, JAXBException{
   		log.debug("Testing method PROCESS_LOAD_PROCESS_DEFINITIONS");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"xml",MediaType.TEXT_HTML);

   		Map<String, Object> var = new LinkedHashMap<String, Object>();
        var.put("processName", (Object)TestUtils.getTestProperty("rest.document.processName"));

        File f = new File(TestUtils.getTestProperty("rest.test.path") + "/zadanie.bpmn");
        String s = FileUtils.encodeByBase64(f);
        KeyElement ke = new KeyElement(null, s);

        String paramRpcUrl = MarshallerUtils.marshall(new XmlMapMapper(var));
		String bodyRpc = MarshallerUtils.marshall(ke);
		log.info("xml param url rpc " + paramRpcUrl);
		log.info("xml rpc " + bodyRpc);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.PROCESS_LOAD_PROCESS_DEFINITIONS, headers, bodyRpc, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}

}
