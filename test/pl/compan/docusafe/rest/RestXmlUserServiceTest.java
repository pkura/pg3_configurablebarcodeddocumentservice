package pl.compan.docusafe.rest;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Calendar;

import com.google.common.collect.Lists;
import net.fortuna.ical4j.util.Calendars;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.ListIds;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.rest.views.RichUserView;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;
import ucar.nc2.util.IO.HttpResult;

public class RestXmlUserServiceTest extends RestAbstractRpcTest{

    //cookies zalogowane użytkownika
    static String cookie;

    /**
     * Paramtry zwi±zane z requestami do DS API
     */
    public String actionName, actionUrl;
    public HttpMethod method;
    public HttpEntity<?> requestEntity;
    public ResponseEntity<?> responseEntity;
    public long executionTime;
    //informacje w przypaddku błedu requestu
    public String paramResp;

    public void clearParams(){
        actionName =null;
        actionUrl = null;
        requestEntity = null;
        responseEntity = null;
        method = null;
        executionTime = 0;
        paramResp = null;
    }

	@BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_XML);
	}

	@AfterClass
	public static void afterClass() throws Exception {
		cookie = null; 
	}

	@Before
	public void setUp() throws Exception {
        clearParams();
	}

	@Test
	public void testGetAllUsers(){
		log.debug("\tTest pobierania wszystkich użytkowanimów");
		actionUrl  = getUrl("/user");
        method = HttpMethod.GET;
        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            requestEntity = new HttpEntity<String>(null,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(actionUrl, method, requestEntity, ListDtos.class);

            // testowanie
            assertNotNull(responseEntity);
            assertNotNull(responseEntity.getBody());
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

            ListDtos<UserDto> list =  (ListDtos<UserDto>) responseEntity.getBody();
            assertFalse("Brak użytkowników!", CollectionUtils.isEmpty(list.getItems()));

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
	}

	@Test()
	public void testCreateUser_New_WithCorrectEmailAndPersonNumber(){
		log.debug("\tTest stworzenia nowego uzytkownika z emailem i numerem osobistym");
        actionUrl = getUrl("/user");
        method = HttpMethod.POST;

        try{
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);
            
            UserDto userDto = new UserDto();
            userDto.setFirstname("Stanislaw \"nowy ");
            userDto.setLastname("cos");
            userDto.setUsername(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE) + "ce");
            userDto.setPassword("");
            userDto.setEmail(userDto.getUsername() + "@pg.gda.pl");
            userDto.setPersonNumber(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE_PNUMBER));
           
            String xmlRequest = MarshallerUtils.marshall(userDto);

            requestEntity = new HttpEntity<String>(xmlRequest,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(actionUrl, method, requestEntity, String.class);

            assertNotNull("Pusta odpowiedz - ",responseEntity);
            assertNotNull("Puste body odpowiedzi - ",responseEntity.getBody());
            assertEquals("Stworzono uzytkownika - ",HttpStatus.CREATED, responseEntity.getStatusCode());

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }

	}

	@Test
	public void testGetUser(){
        String userId = TestUtils.getTestProperty(TestUtils.REST_API_USER_GET);
		log.debug("\tTest pobierania użytkownika");
        actionUrl = getUrl("/user/{userId}");
        method = HttpMethod.GET;
        Map<String, String> var = new HashMap<String, String> ();
        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            var = Collections.singletonMap("userId", userId);
            requestEntity = new HttpEntity<String>(null,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(actionUrl,method,requestEntity,UserDto.class,var);

            // testowanie
            assertNotNull(responseEntity);
            assertNotNull(responseEntity.getBody());
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

            UserDto userDto = (UserDto) responseEntity.getBody();

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
	}

    @Test
    public void testGetUserByName(){
        String userId = TestUtils.getTestProperty("rest.api.user.get.external.name");
        log.debug("\tTest pobierania użytkownika");
        actionUrl = getUrl("/user/{userId}/name");
        method = HttpMethod.GET;
        Map<String, String> var = new HashMap<String, String> ();
        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            var = Collections.singletonMap("userId", userId);
            requestEntity = new HttpEntity<String>(null,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(actionUrl,method,requestEntity,UserDto.class,var);

            // testowanie
            assertNotNull(responseEntity);
            assertNotNull(responseEntity.getBody());
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

            UserDto userDto = (UserDto) responseEntity.getBody();

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
    }
	
	@Test()
	public void testUpdateUser_EmailAndPersonalNumber(){
		String idUserCreated = TestUtils.getTestProperty(TestUtils.REST_API_USER_GET);
		log.debug("\tTest uaktualnienia uzytkownika z emailem i numerem osobistym");
        Map<String,String> var = new HashMap();

        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            UserDto userDto = new UserDto();
            userDto.setId(Long.valueOf(idUserCreated));
            userDto.setFirstname("updatefirstname");
            userDto.setLastname("updatelastane");
            userDto.setUsername("updateusername");
            userDto.setEmail(userDto.getFirstname() + "@pg.gda.pl");
            userDto.setPersonNumber("666");

            String xmlRequest = MarshallerUtils.marshall(userDto);
            requestEntity = new HttpEntity<String>(xmlRequest,headers);

            var = Collections.singletonMap("userId", idUserCreated);

            RestTemplate template = TestUtils.getRestTemplate();
            responseEntity = template.exchange(getUrl("/user/{userId}"), HttpMethod.PUT, requestEntity, Object.class, var);

            // testowanie
            assertNotNull("Pusta odpowiedz - ",responseEntity);
            assertNull("Niepuste body odpowiedzi - ",responseEntity.getBody());
            assertEquals("Nie zaktualizowano istniejącego uzytkownika - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
	}

	
	@Test
	public void testGetDivisionsForUser()  {
        String idUserCreated = TestUtils.getTestProperty(TestUtils.REST_API_USER_GET);
		log.debug("Test pobierania wszystkich działów dla użytkownika");
        Map<String,String> var = new HashMap();

        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            requestEntity = new HttpEntity<String>(null,headers);
            var = Collections.singletonMap("userId", idUserCreated);

            
            MarshallerUtils.marshall(new ListDtos<UserDto>());
            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(getUrl("/user/{userId}/division"), HttpMethod.GET, requestEntity, ListDtos.class, var);

            // testowanie
            assertNotNull("Null instead http respons", responseEntity);
            assertNotNull("Null instead object divisions",responseEntity.getBody());
            assertEquals("Wrong status code ",HttpStatus.OK, responseEntity.getStatusCode());

            ListDtos<DivisionDto> list = (ListDtos<DivisionDto>) responseEntity.getBody();
            assertFalse("Brak działów!", CollectionUtils.isEmpty(list.getItems()));

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
	}
	
	@Test
	public void testGetSubstitutionsByUser()  {
		String idUserSubstituting = TestUtils.getTestProperty("rest.api.user.substitutions");
		String idUserSubstiuted = TestUtils.getTestProperty("rest.api.user.substitute");
		int numDays = Integer.parseInt(TestUtils.getTestProperty("rest.api.substitution.num.days"));
		log.debug("Test pobierania osób zastępowanych przez użytkownika");
        Map<String,String> var = new HashMap();
		// przygotowanie

        try{
//		createResponseEntityForCreateSubstitution(createXmlRequestEntityForCreateSubstitution(numDays,idUserSubstiuted,idUserSubstituting,cookie));
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		requestEntity = new HttpEntity<String>(null,headers);
		var.put("userId", idUserSubstituting);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
		responseEntity = template.exchange(getUrl("/user/{userId}/substitution"),
																HttpMethod.GET,
																requestEntity,
																ListDtos.class,
																var);
		// testowanie
		assertNotNull("Null instead http respons", responseEntity);
		assertNotNull("Null instead object of substituts",responseEntity.getBody());
		assertEquals("Wrong status code",HttpStatus.OK, responseEntity.getStatusCode());
		
		ListDtos<SubstitutionDto> list = (ListDtos<SubstitutionDto>) responseEntity.getBody();
		assertFalse("Brak zastępstw!", CollectionUtils.isEmpty(list.getItems()));
		
        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
	}
	
	@Test
	public void testGetSubstituteForUser() {
        String idUser = TestUtils.getTestProperty("rest.api.user.substitute");
		log.debug("\tTest pobierania zastępstwa dla użytkownika");
        Map<String, String> var = new HashMap();
        try{
    //		// przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            var = Collections.singletonMap("userId", idUser);
            requestEntity = new HttpEntity<String>(null,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(getUrl("/user/{userId}/substitute"),
                                                                        HttpMethod.GET,
                                                                        requestEntity,
                                                                        SubstitutionDto.class,var);

            // testowanie
            assertNotNull(responseEntity);
            assertNotNull(responseEntity.getBody());
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
	}
	
	@Test
	public void testDeleteUser() throws JsonGenerationException, JsonMappingException, IOException{
        String idUser = TestUtils.getTestProperty(TestUtils.REST_API_USER_GET);
		log.debug("\tTest usuwania uzytkownika");

        Map<String, String> var;
        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            requestEntity = new HttpEntity<String>(null,headers);

            var = Collections.singletonMap("userId", idUser);
            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(getUrl("/user/{userId}"), HttpMethod.DELETE, requestEntity, Object.class, var);

            // testowanie
            assertNotNull("Pusta odpowiedz - ",responseEntity);
            assertNull("Niepuste body odpowiedzi - ",responseEntity.getBody());
            assertEquals("Stworzono uzytkownika - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
	}

    @Test
    public void testGetAvailableOfficeRoles() {
        try{
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            requestEntity = new HttpEntity<String>(null,headers);
            RestTemplate template = TestUtils.getRestTemplate();


            responseEntity = template.exchange(getUrl("/roles/list"), HttpMethod.GET, requestEntity, ListDtos.class);

            // testowanie
            assertNotNull("Pusta odpowiedz - ", responseEntity);
            assertNotNull("Niepuste body odpowiedzi - ", responseEntity.getBody());
            assertEquals("Stworzono uzytkownika - ", HttpStatus.OK, responseEntity.getStatusCode());

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
    }

    @Test()
    public void testUpdateUserRole() throws JsonGenerationException, JsonMappingException, IOException{
        String idUserCreated = TestUtils.getTestProperty("rest.api.user.get.external.name");
        List<Long> roles = Lists.newArrayList(43L,22L);
        Map<String,String> var = new HashMap();

        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);

            ListIds roleIds = new ListIds(roles);

            String jsonRequestBody = MarshallerUtils.marshall(roleIds);

            requestEntity = new HttpEntity<String>(jsonRequestBody,headers);

            var = Collections.singletonMap("username", idUserCreated);

            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(getUrl("/user/{username}/roles"), HttpMethod.POST, requestEntity, String.class, var);

            // testowanie
            assertNotNull("Pusta odpowiedz - ",responseEntity);
            assertNull("Niepuste body odpowiedzi - ",responseEntity.getBody());
            assertEquals("Nie zaktualizowano istniejącego uzytkownika - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());

            //        convertRequestResponseToJSON(requestEntity, responseEntity);

            // czas wykkonania

        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
    }

    @Test
    public void testGetUserRoles() throws JsonGenerationException, JsonMappingException, IOException {
        String userId = TestUtils.getTestProperty("rest.api.user.get.external.name");
        log.debug("\tTest pobierania użytkownika");
        actionUrl = getUrl("/user/{username}/roles");
        method = HttpMethod.GET;
        Map<String, String> var = new HashMap<String, String> ();
        try{
            // przygotowanie
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
            var = Collections.singletonMap("username", userId);
            requestEntity = new HttpEntity<String>(null,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            responseEntity = template.exchange(actionUrl, method, requestEntity, ListDtos.class, var);

            // testowanie
            assertNotNull(responseEntity);
            assertNotNull(responseEntity.getBody());
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

            ListDtos userDto = (ListDtos) responseEntity.getBody();
        }catch(Exception e){
            log.error("", e);
            paramResp = e.getMessage();
        }
    }
	
	
}
