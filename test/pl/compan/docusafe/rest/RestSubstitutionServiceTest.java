package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.util.CollectionUtils;


import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.TestUtils;

public class RestSubstitutionServiceTest extends RestAbstractRpcTest{
	
    static String cookie;
	
    
	@BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_JSON);
 	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testCreateSubstitution_OneDay() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("Test stworzenia nowego zastępstwa");

		
		int numDays = Integer.parseInt(TestUtils.getTestProperty("rest.api.substitution.num.days"));
		String idUserSubsituted = TestUtils.getTestProperty("rest.api.substituted.user.id");
		String idUserSubstituting = TestUtils.getTestProperty("rest.api.substituting.user.id");
		HttpEntity<String> requestEntity = createRequestEntityForCreateSubstitution(numDays, idUserSubsituted, idUserSubstituting, cookie);	
	
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/substitution"), HttpMethod.POST, requestEntity, String.class);

		log.info("posiada reason {}" + responseEntity.getHeaders().get(RestRequestHandler.REASON));

		// testowanie
        assertNotNull("Pusta odpowiedz - ", responseEntity);
        assertNotNull("Puste body odpowiedzi - ", responseEntity.getBody());
        assertEquals("Nie stworzono zastępstwo - ", HttpStatus.CREATED, responseEntity.getStatusCode());

	}

	@Test
	public void testGetById() throws JsonGenerationException, JsonMappingException, IOException {
		String idSubstitution = TestUtils.getTestProperty("rest.api.substitution.id");
		log.debug("Test pobierania zastępstwa");
        
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
        
        RestTemplate template = TestUtils.getRestTemplate();
        
        ResponseEntity<SubstitutionDto> responseEntity = template.exchange(getUrl("/substitution/{substitutionId}"),HttpMethod.GET,requestEntity,SubstitutionDto.class,var);
                 
        // testowanie
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        
        SubstitutionDto substitutionDto = responseEntity.getBody();
        assertEquals(Long.valueOf(idSubstitution), substitutionDto.getId());
        
	}


	@Test
	public void testGetAllCurrentSubstitutions() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("Test pobierania wszystkich aktualnych zastępstw");
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/substitution"), HttpMethod.GET, requestEntity, ListDtos.class);
		
		// testowanie
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		
	}

	@Test
	public void testGetCurrentAtTime() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("Test pobierania wszystkich aktualnych zastępstw w czasie");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		
		Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
		String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());
		log.info("czas : " + dateTime);
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/substitution?time={time}"), HttpMethod.GET, requestEntity, ListDtos.class, dateTime);
		
		// testowanie
		assertNotNull(responseEntity);
		assertNotNull(responseEntity.getBody());
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

	}


	@Test
	public void testDeleteSubstitution() throws JsonGenerationException, JsonMappingException, IOException {
		String idSubstitution = TestUtils.getTestProperty("rest.api.substitution.id");
		log.debug("Test usuwania zastępstwa");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<Object> responseEntity = template.exchange(getUrl("/substitution/{substitutionId}"), HttpMethod.DELETE, requestEntity, Object.class, var);
		
		// testowanie
        assertNotNull("Pusta odpowiedz - ",responseEntity);
        assertNull("Niepuste body odpowiedzi - ",responseEntity.getBody());
        assertEquals("Stworzono uzytkownika - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());
        
	}
	
	
}
