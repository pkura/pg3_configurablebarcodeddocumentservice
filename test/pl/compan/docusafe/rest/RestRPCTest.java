package pl.compan.docusafe.rest;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.*;
import org.apache.log4j.Logger;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.CollectionUtils;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;

import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.DictionaryDto;
import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.api.user.office.FullTextSearchDto;
import pl.compan.docusafe.api.user.office.OfficeDocumentDto;
import pl.compan.docusafe.api.user.office.RpcMethod;
import pl.compan.docusafe.api.user.office.RpcRequest;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

import javax.validation.constraints.AssertTrue;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 */
public class RestRPCTest extends RestAbstractRpcTest{
    public static final Logger log = Logger.getLogger(RestRPCTest.class);

    public static String url, user, pass;
    ObjectMapper mapper = new ObjectMapper();

    public static final String SET_COOKIE="Set-Cookie";
    static String cookie;

    private static String idUser;
	private static String idSubstitution;
	private static String idDictionary;
	private static String idDivision;

    private static XLSParamWriter xlsParamWriter;

    @BeforeClass
    public static void  init(){
        BasicConfigurator.configure();

        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);

        // login one time per test whole class
        cookie = login();

        /**xlsParamWriter = new XLSParamWriter();
        try {
            xlsParamWriter.createColumnHeaders("Test PG");
        } catch (IOException e) {
            e.printStackTrace();
        }

         */
    }


    @AfterClass
    public static void  afterTest(){
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
        log.debug("cookie: " + cookie);
    }

    @After
    public void tearDown() throws Exception {

        log.debug("after");

       // xlsParamWriter.updateFile();
       // xlsParamWriter.autoWidth();
    }

    @Test
   	public void testJsonRpcGetUserTaskList() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method TASK_LIST_USER");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);

		Map<String,String> var1 = Collections.singletonMap("username", "eodc");
		Map<String,Integer> var2 = Collections.singletonMap("offset", new Integer(0));  		
		String paramRpcUrl1 = mapper.writeValueAsString(var1);
		String paramRpcUrl2 = mapper.writeValueAsString(var2);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.TASK_LIST_USER, headers, paramRpcUrl1,paramRpcUrl2);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    

    
    @Test
   	public void testJsonRpcSubstitutionGetById() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method SUBSTITUTION_GET");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		idSubstitution = idSubstitution != null ?  idSubstitution : "6";
   		Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.SUBSTITUTION_GET_BY_ID, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcSubstitutionDelete() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method SUBSTITUTION_DELETE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		idSubstitution = idSubstitution != null ?  idSubstitution : "6";
   		Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.SUBSTITUTION_DELETE, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertFalse("Response body must be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcDocumentSearch() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_SEARCH");

   		String searchFieldName = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
		String dockindCn = TestUtils.getTestPropertyAsMap("rest.document.search.fields").keySet().iterator().next();
		String isSearchField = TestUtils.getTestPropertyAsMap("rest.document.search.fields").get(dockindCn);
   		
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		OfficeDocumentDto officeDocumentDto = createOfficeDocumentDto(searchFieldName, dockindCn, isSearchField);
   		String paramRpcUrl = mapper.writeValueAsString(officeDocumentDto);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_SEARCH, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testJsonRpcDocumentAddDictionaryToDictionariesDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_ADD_DICTIONARY_ENTITY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		String testCase = TestUtils.getTestProperty(TestUtils.REST_API_DICTIONARY_CREATE);
		FieldView[] fields = {new FieldView("LASTNAME", "", testCase),
							  new FieldView("FIRSTNAME", "", testCase)}; 
		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_ADD_DICTIONARY_ENTITY, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   		TypeReference<List<Map<String,Object>>> typeRef = new TypeReference<List<Map<String,Object>>>() {};
   		List<Map<String,List<Map<String, String>>>> ObjectFromJson = mapper.readValue(response.getBody(), typeRef);
   		idDictionary = ObjectFromJson.get(0).get("fieldValues").get(0).get("value");
   	}
    
    @Test
   	public void testJsonRpcDocumentGetDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_GET_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		Map<String, String> var1 = Collections.singletonMap("documentId", "20116");
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		Map<String, String> var2 = Collections.singletonMap("dictionary", "SENDER");
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
   		Map<String, Long> var3 = Collections.singletonMap("dictionaryId", new Long(20143));
   		String paramRpcUrl3 = mapper.writeValueAsString(var3);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		log.info("json param url rpc " + paramRpcUrl3);

		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_GET_DOCUMENT_DICTIONARY,
																		   headers, paramRpcUrl1, paramRpcUrl2, paramRpcUrl3);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   	}
    
    @Test
   	public void testJsonRpcDocumentUpdateDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_UPDATE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		idDictionary = idDictionary != null ?  idDictionary : "1";
   		FieldView[] fields = {new FieldView("id", "", idDictionary),
				  new FieldView("FIRSTNAME", "Grzegorz_update"),
			  	  new FieldView("LASTNAME", "Filip_update")};
		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_UPDATE_DOCUMENT_DICTIONARY,headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}

    @Test
   	public void testJsonRpcDocumentRemoveDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_REMOVE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		idDictionary = idDictionary != null ?  idDictionary : "1";
   		FieldView[] fields = {new FieldView("id", "", idDictionary),
				  new FieldView("FIRSTNAME", "Grzegorz_update"),
			  	  new FieldView("LASTNAME", "Filip_update")};
   		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_REMOVE_DOCUMENT_DICTIONARY,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}

    @Test
   	public void testJsonRpcOfficeCaseGetUser() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_USER");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		Map<String, String> var = Collections.singletonMap("username", "eodc");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_USER,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGetDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_DOCUMENT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		Map<String, String> var = Collections.singletonMap("documentId", "89");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_DOCUMENT,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGet() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		Map<String,String> var = Collections.singletonMap("officeCaseId", "2");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testJsonRpcOfficeCaseGetPortfolio() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_PORTFOLIO");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		Map<String,String> var = Collections.singletonMap("guid", "99133E2557187E76140ED63E3775E96915F");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.OFFICECASE_GET_PORTFOLIO,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
    public void testJsonRpcFullTextSearch() throws IOException {
        log.info("testJsonRpcFullTextSearch");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        FullTextSearchDto fts = new FullTextSearchDto();
        fts.setText("pismo");
        fts.setLimit(50);

        String p = mapper.writeValueAsString(fts);
        executeRPCJsonRequest(RpcMethod.DOCUMENT_FULL_TEXTSEARCH, headers, p);
    }

    @Test
    public void testJsonRpcFindDocumentByBarcode() throws IOException{
        System.out.println("findDocument test");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        
		Map<String, String> var = Collections.singletonMap("barcode", barcode);
		String paramRpc = mapper.writeValueAsString(var);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_BARCODE_FIND, headers, paramRpc);
    }
    
    @Test
    public void testJsonRpcFindDocumentByBarcode_MethodWithCheck() throws IOException{
        System.out.println("findDocument test");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);

        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        String check = TestUtils.getTestProperty("rest.document.barcode.check");
        
        Map<String, String> vars = new HashMap<String,String>();
		vars.put("barcode", barcode);
		vars.put("check", check);
		String paramRpc = mapper.writeValueAsString(vars);

        executeRPCJsonRequestParameters(RpcMethod.DOCUMENT_BARCODE_FIND_CHECK_BARCODE, headers, paramRpc);
    }
  
//--- deleting  
    
    @Test
   	public void testJsonRpcDivisionDelete() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_DELETE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
   		
   		String idDivisionUpdate = idDivision != null ? idDivision : "4";
		Map<String,Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivisionUpdate));
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCJsonRequestParameters(RpcMethod.DIVISION_DELETE,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertFalse("Must be false", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }

    @Test
    public void testXmlRpcDeleteUser() throws JsonGenerationException, JsonMappingException, IOException{
        log.debug("Testing method USER_DELETE");

        String idUserCreated = TestUtils.getTestProperty("rest.api.user.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        Map<String,String> var = Collections.singletonMap("userId", idUserCreated);
        String paramRpcUrl = mapper.writeValueAsString(var);
        log.info("json param url rpc " + paramRpcUrl);

        ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_DELETE, headers, paramRpcUrl);

        assertNotNull("Must be not null", response);
        assertFalse("Response body must be null", response.hasBody());
        assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
    }
  
//    ---------------------------------------  XML rpc -------------------------------------------------------

    @Test
   	public void testXmlRpcCreateUser_New() throws IOException, JAXBException{
   		log.debug("Testing method USER_CREATE");
   		
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		UserDto userDto = new UserDto();
		userDto.setFirstname(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE) + "rpcXml");
		userDto.setLastname(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcXml");
		userDto.setUsername(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcXml");
		userDto.setPassword(TestUtils.getTestProperty(TestUtils.REST_API_USER_CREATE)+ "rpcXml");
//		String paramRpc = MarshallerUtils.marshall(userDto);
//		log.info("xml param rpc " + paramRpc);
		
        String p = mapper.writeValueAsString(userDto);
        log.error(" xml: {}" + p);
      
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_CREATE, headers, p);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());

   		int beginIndex = response.getBody().lastIndexOf("/");
        beginIndex++;
		idUser = response.getBody().substring(beginIndex);		
   	}
    
    @Test
   	public void testXmlRpcGetAllUsers() throws IOException, JAXBException{
   		log.debug("Testing method USER_FIND_ALL");
   		
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
      
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_FIND_ALL, headers);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
  
   	}
    
    @Test
   	public void testXmlRpcGetUser() throws IOException, JAXBException{
    	log.debug("Testing method USER_FIND_ONE");
    	String userId = TestUtils.getTestProperty("rest.api.user.get");
   		
   		String idUserCreated = idUser != null ? idUser : userId;
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		Map<String, String> var = Collections.singletonMap("userId", idUserCreated);
		String paramRpc = mapper.writeValueAsString(var);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_FIND_ONE, headers, paramRpc);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
  
   	}
    
   
    
    @Test
   	public void testXmlRpcGetSubstitutionsByUser() throws IOException, JAXBException{
    	log.debug("Testing method USER_FIND_ALL_WITH_DIVISION");
    	String userId = TestUtils.getTestProperty("rest.api.user.get");
    	
   		String idUserCreated = idUser != null ? idUser : userId;
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		Map<String,String> var = Collections.singletonMap("userId", idUserCreated);
		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_GET_SUBSTITUTIONS_BY_USER, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
  
   	}
    
    @Test
   	public void testXmlRpcGetSubstitute() throws IOException, JAXBException{
    	log.debug("Testing method USER_GET_SUBSTITUTE");
    	// Stw�rz zast�pstwo dla usersa o userId
    	String userId = TestUtils.getTestProperty("rest.api.user.get");
   		
   		String idUserCreated = idUser != null ? idUser : userId;
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		Map<String,String> var = Collections.singletonMap("userId", idUserCreated);
		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_GET_SUBSTITUTE, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
  
   	}
    
    @Test
   	public void testXmlRpcGetDivisions() throws IOException, JAXBException{
    	log.debug("Testing method USER_GET_DIVISIONS");
    	String userId = TestUtils.getTestProperty("rest.api.user.get");
   		
   		String idUserCreated = idUser != null ? idUser : userId;
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		Map<String,String> var = Collections.singletonMap("userId", idUserCreated);
		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_GET_DIVISIONS, headers, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
  
   	}
    
    @Test
   	public void testXmlRpcUpdateUser() throws IOException, JAXBException{
    	log.debug("Testing method USER_UPDATE");
    	String userId = TestUtils.getTestProperty("rest.api.user.get");
   		
   		String idUserCreated = idUser != null ? idUser : userId;
   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		UserDto userDto = new UserDto();
   		userDto.setId(Long.valueOf(idUserCreated));
		userDto.setFirstname("updatefirstnameRPC");
		userDto.setLastname("updatelastaneRPC");
		userDto.setUsername("updateusernameRPC");
		userDto.setPassword("updatepassRPC");
		String paramRpc = mapper.writeValueAsString(userDto);
		log.info("json param rpc " + paramRpc);
		
		Map<String,String> var = Collections.singletonMap("userId", idUserCreated);
		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.USER_UPDATE, headers, paramRpc, paramRpcUrl);
          
   		assertNotNull("Must be not null", response);
   		assertFalse("Response body must be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
  
   	}
    
	@Test
    public void testXmlRpcFindDocumentByBarcode() throws IOException {
        log.info("findDocument test");
        String barcode = TestUtils.getTestProperty("rest.document.barcode.number");
        
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        
		Map<String,String> var = Collections.singletonMap("barcode", barcode );
		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);

        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_BARCODE_FIND, headers, paramRpcUrl);
		
    }
    
    @Test
    public void testXmlRpcFullTextSearch() throws IOException, JAXBException {
        log.error("fullTextSearch test");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        FullTextSearchDto fts = new FullTextSearchDto();
        fts.setText("pismo");
        fts.setLimit(50);

       // String p = MarshallerUtils.marshall(fts);
        String p = mapper.writeValueAsString(fts);
        log.error(" xml: {}" + p);
        executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_FULL_TEXTSEARCH, headers, p);
    }
    
    @Test
   	public void testXmlRpcGetUserTaskList() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method TASK_LIST_USER");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);

		Map<String,String> var1 = Collections.singletonMap("username", "eodc");
		Map<String,Integer> var2 = Collections.singletonMap("offset", new Integer(0));  		
		String paramRpcUrl1 = mapper.writeValueAsString(var1);
		String paramRpcUrl2 = mapper.writeValueAsString(var2);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.TASK_LIST_USER, headers, paramRpcUrl1,paramRpcUrl2);
          
   		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcCreateSubstitution_OneDay() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method SUBSTITUTION_CREATE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		String paramRpcUrl = createSubstitutionInXml(1);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_CREATE, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   		int beginIndex = response.getBody().lastIndexOf("/");
        beginIndex++;
		idSubstitution = response.getBody().substring(beginIndex);
   	}
    
	@Test
   	public void testXmlRpcSubstitutionGetById() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method SUBSTITUTION_GET");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idSubstitution = idSubstitution != null ?  idSubstitution : "6";
   		Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_GET_BY_ID, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
   		assertNotNull("Response body must be not null", response.getBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcSubstitutionDelete() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method SUBSTITUTION_DELETE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idSubstitution = idSubstitution != null ?  idSubstitution : "6";
   		Map<String, String> var = Collections.singletonMap("substitutionId", idSubstitution);
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.SUBSTITUTION_DELETE, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertFalse("Response body must be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcDocumentSearch() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_SEARCH");
   		String searchFieldName = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
		String dockindCn = TestUtils.getTestPropertyAsMap("rest.document.search.fields").keySet().iterator().next();
		String isSearchField = TestUtils.getTestPropertyAsMap("rest.document.search.fields").get(dockindCn);

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);   		

		OfficeDocumentDto officeDocumentDto = createOfficeDocumentDto(searchFieldName, dockindCn, isSearchField);
   		String paramRpcUrl = mapper.writeValueAsString(officeDocumentDto);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_SEARCH, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcDocumentAddDictionaryToDictionariesDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_ADD_DICTIONARY_ENTITY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		String testCase = TestUtils.getTestProperty(TestUtils.REST_API_DICTIONARY_CREATE);
		FieldView[] fields = {new FieldView("LASTNAME", "", testCase+"xml"),
							  new FieldView("FIRSTNAME", "", testCase+"xml")}; 
		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_ADD_DICTIONARY_ENTITY, headers, paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   		TypeReference<List<Map<String,Object>>> typeRef = new TypeReference<List<Map<String,Object>>>() {};
   		List<Map<String,List<Map<String, String>>>> ObjectFromJson = mapper.readValue(response.getBody(), typeRef);
   		idDictionary = ObjectFromJson.get(0).get("fieldValues").get(0).get("value");
   	}
    
    @Test
   	public void testXmlRpcDocumentGetDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_GET_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		Map<String, String> var1 = Collections.singletonMap("documentId", "20116");
   		String paramRpcUrl1 = mapper.writeValueAsString(var1);
   		Map<String, String> var2 = Collections.singletonMap("dictionary", "SENDER");
   		String paramRpcUrl2 = mapper.writeValueAsString(var2);
   		Map<String, Long> var3 = Collections.singletonMap("dictionaryId", new Long(20143));
   		String paramRpcUrl3 = mapper.writeValueAsString(var3);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		log.info("json param url rpc " + paramRpcUrl3);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_GET_DOCUMENT_DICTIONARY,
																		   headers, paramRpcUrl1, paramRpcUrl2, paramRpcUrl3);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   	}
    
    @Test
   	public void testXmlRpcDocumentUpdateDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_UPDATE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idDictionary = idDictionary != null ?  idDictionary : "1";
   		FieldView[] fields = {new FieldView("id", "", idDictionary),
				  new FieldView("FIRSTNAME", "Grzegorz_update"),
			  	  new FieldView("LASTNAME", "Filip_update")};
		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_UPDATE_DOCUMENT_DICTIONARY,headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}

    @Test
   	public void testXmlRpcDocumentRemoveDocumentDictionary() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DOCUMENT_REMOVE_DOCUMENT_DICTIONARY");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idDictionary = idDictionary != null ?  idDictionary : "1";
   		FieldView[] fields = {new FieldView("id", "", idDictionary),
				  new FieldView("FIRSTNAME", "Grzegorz_update"),
			  	  new FieldView("LASTNAME", "Filip_update")};
   		DictionaryDto dictionaryDto = createDictionaryDto(fields);
   		String paramRpcUrl = mapper.writeValueAsString(dictionaryDto);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DOCUMENT_REMOVE_DOCUMENT_DICTIONARY,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   	}
    
    @Test
   	public void testXmlRpcDivisionCreate_NewDivision() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_CREATE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		DivisionDto divisionDto = createDivisionDto("xml");
   		String paramRpcUrl = mapper.writeValueAsString(divisionDto);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_CREATE,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
   		int divisionIndex = response.getBody().lastIndexOf("/");
        divisionIndex++;
		idDivision = response.getBody().substring(divisionIndex);
   	}
    
    @Test
   	public void testXmlRpcDivisionGetByGuid() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_GET_BY_GUID");
   		String guid = TestUtils.getTestProperty("rest.api.division.guid");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		Map<String, String> var1 = Collections.singletonMap("guid", guid);
   		String paramRpcUrl = mapper.writeValueAsString(var1);
		log.info("json param url rpc " + paramRpcUrl);

		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_BY_GUID,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcDivisionGetUsers() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_GET_USERS");
   		String divisionId = TestUtils.getTestProperty("rest.api.division.get");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idDivision = idDivision != null ?  idDivision : divisionId;
   		Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_USERS,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcDivisionGetSubDivisions() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_GET_USERS");
   		String divisionId = TestUtils.getTestProperty("rest.api.division.get");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idDivision = idDivision != null ?  idDivision : divisionId;
   		Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_SUBDIVISIONS,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }

    
    @Test
   	public void testXmlRpcDivisionGetAll() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_GET_ALL");
   		String divisionId = TestUtils.getTestProperty("rest.api.division.get");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idDivision = idDivision != null ?  idDivision : divisionId;
   		Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_GET_ALL,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcDivisionAddUser() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_ADD_USER");
   		//Id user i Id Division
   		String userId = TestUtils.getTestProperty("rest.api.user.get");
   		String divisionId = TestUtils.getTestProperty("rest.api.division.get");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		idUser = idUser != null ? idUser : userId;
		UserDto userDto = getUserById(idUser);
  
   		idDivision = idDivision != null ?  idDivision : divisionId;
   		Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivision));
   		
   		String paramRpcUrl1 = mapper.writeValueAsString(userDto);
   		String paramRpcUrl2 = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl1);
   		log.info("json param url rpc " + paramRpcUrl2);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_ADD_USER,
																		   headers,paramRpcUrl1,paramRpcUrl2);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcDivisionUpdate() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_UPDATE");
   		String divisionId = TestUtils.getTestProperty("rest.api.division.get");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		String idDivisionUpdate = idDivision != null ? idDivision : divisionId;
   		DivisionDto divisionDto = updateDivisionDto(idDivisionUpdate);
   		Map<String, Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivisionUpdate));
   		String paramRpcUrl1 = mapper.writeValueAsString(divisionDto);
   		String paramRpcUrl2 = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl1);
		log.info("json param url rpc " + paramRpcUrl2);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_UPDATE,
																		   headers,paramRpcUrl1,paramRpcUrl2);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcOfficeCaseGetUser() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_USER");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		Map<String, String> var = Collections.singletonMap("username", "eodc");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_USER,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcOfficeCaseGetDocument() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_DOCUMENT");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		Map<String, String> var = Collections.singletonMap("documentId", "89");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_DOCUMENT,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcOfficeCaseGet() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		Map<String,String> var = Collections.singletonMap("officeCaseId", "2");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
    @Test
   	public void testXmlRpcOfficeCaseGetPortfolio() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method OFFICECASE_GET_PORTFOLIO");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		Map<String,String> var = Collections.singletonMap("guid", "99133E2557187E76140ED63E3775E96915F");
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.OFFICECASE_GET_PORTFOLIO,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertNotNull("Response body must not be null", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
//--- deleting  
    
    @Test
   	public void testXmlRpcDivisionDelete() throws JsonGenerationException, JsonMappingException, IOException{
   		log.debug("Testing method DIVISION_DELETE");

   		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
   		
   		String idDivisionUpdate = idDivision != null ? idDivision : "4";
		Map<String,Long> var = Collections.singletonMap("divisionId", Long.valueOf(idDivisionUpdate));
   		String paramRpcUrl = mapper.writeValueAsString(var);
		log.info("json param url rpc " + paramRpcUrl);
		
		ResponseEntity<String> response =  executeRPCXmlRequestParameter(RpcMethod.DIVISION_DELETE,
																		   headers,paramRpcUrl);
          
		assertNotNull("Must be not null", response);
		assertFalse("Must be false", response.hasBody());
   		assertEquals("We espect OK status code", HttpStatus.OK, response.getStatusCode());
   		
    }
    
//  -----------------------------------------------------------------------------------------------------------------------------------------
    
    private String createSubstitutionInXml(int dayAomounts) throws JsonGenerationException, JsonMappingException, IOException {
Calendar calendar = Calendar.getInstance();
		
		SubstitutionDto substitutionDto = new SubstitutionDto();
		substitutionDto.setStartTime(calendar);
		calendar.add(Calendar.DAY_OF_MONTH, dayAomounts);
		substitutionDto.setEndTime(calendar);
		UserDto userSubsituted = getUserById("1");
		substitutionDto.setUserSubsituted(userSubsituted);
		UserDto userSubstituting = getUserById("2");
		substitutionDto.setUserSubstituting(userSubstituting);
		
		String jsonRequestDto = mapper.writeValueAsString(substitutionDto);

		return jsonRequestDto;
	}
    
    /**
     * 
     * Funkcja pobiera u�ytkownika o okre�lonym id
     * 
     * @param id
     * @return UserDto
     */
    private UserDto getUserById(String id) {
		Map<String, String> var = Collections.singletonMap("userId", id); 
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<UserDto> responseEntity = template.exchange(getUrl("/user/{userId}"),HttpMethod.GET,requestEntity,UserDto.class,var);
		
		return responseEntity.getBody();
	}
    
    /**
     * Funkcja tworzy document dto
     * @param normal 
     * @param searchFieldName 
     * @param isSearchField 
     * @return OfficeDocumentDto
     */
    private OfficeDocumentDto createOfficeDocumentDto(String normal, String searchFieldName, String isSearchField) {
		OfficeDocumentDto officeDocumentDto = new OfficeDocumentDto();
		officeDocumentDto.setDockindCn(normal);
		officeDocumentDto.setSortingField("ID");
		officeDocumentDto.setLimit(50);
		officeDocumentDto.setOffset(0);
		officeDocumentDto.setFieldValues(Lists.newArrayList(new FieldView(searchFieldName, isSearchField)));
		return officeDocumentDto;
	}
    /**
     * Funkcja tworzy nowy wpis s�ownika dokumentu
     * @param fields
     * @return DictionaryDto
     */
    private DictionaryDto createDictionaryDto(FieldView[] fields) {
		DictionaryDto dictionaryDto = new DictionaryDto();
		dictionaryDto.setDictionaryCn("SENDER");
        dictionaryDto.setDockindCn("normal");
        dictionaryDto.setFieldValues(Lists.newArrayList(fields));
		return dictionaryDto;
	}
    /**
     * Funkcja tworzy nowy dzia� 
     * @param mediaType TODO
     * @return DivisionDto
     */
    private DivisionDto createDivisionDto(String mediaType) {
		DivisionDto divisionDto = new DivisionDto();
		String functionalTestDivision = TestUtils.getTestProperty(TestUtils.REST_API_DIVISION_CREATE);
		divisionDto.setType(DivisionType.DIVISION);
		divisionDto.setGuid(functionalTestDivision + mediaType);
		divisionDto.setName(functionalTestDivision + mediaType);
		divisionDto.setParentId(2l);
		return divisionDto;
	}
    /**
     * Funkcja zwraca dzia� dto, kt�ry zostanie uaktualniony
     * @param id
     * @return DivisionDto
     */
    private DivisionDto updateDivisionDto(String id) {
		DivisionDto divisionDto = new DivisionDto();
		String functionalTestDivision = TestUtils.getTestProperty(TestUtils.REST_API_DIVISION_CREATE);
		functionalTestDivision = functionalTestDivision + "update";
		divisionDto.setId(Long.valueOf(id));
		divisionDto.setGuid(functionalTestDivision);
		divisionDto.setName(functionalTestDivision);
		divisionDto.setParentId(2l);
		return divisionDto;
	}
}
