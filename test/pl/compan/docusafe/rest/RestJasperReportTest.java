package pl.compan.docusafe.rest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 22.07.14
 */
public class RestJasperReportTest extends RestAbstractRpcTest{

    //cookies zalogowane u�ytkownika
    static String cookie;

    @BeforeClass
    public static void initTest() throws Exception {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);

        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetReportList(){
        String actionUrl = getUrl("/jasper-report");
        HttpMethod method = HttpMethod.GET;
        // przygotowanie
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        HttpEntity<?> requestEntity = new HttpEntity<String>(null,headers);

        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<ListDtos> responseEntity = template.exchange(actionUrl, method, requestEntity, ListDtos.class);

        // testowanie
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        assertNotNull("Brak raport�w!", responseEntity.getBody().getItems().toString());
    }

    @Test
    public void testGetReportType(){
        String actionUrl = getUrl("/jasper-report/typntne");
        HttpMethod method = HttpMethod.GET;
        // przygotowanie
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        HttpEntity<?> requestEntity = new HttpEntity<String>(null,headers);

        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<ListDtos> responseEntity = template.exchange(actionUrl, method, requestEntity, ListDtos.class);

        // testowanie
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        List<UserDto> userDtos = (List<UserDto>) responseEntity.getBody().getItems();
        assertFalse("Brak typ�w raport�w!", CollectionUtils.isEmpty(userDtos));
    }

    @Test
    public void testAddReport(){
        String actionUrl = getUrl("/jasper-report");
        HttpMethod method = HttpMethod.PUT;
        String userName = TestUtils.getTestProperty("rest.jasper.username");
        String reportType = TestUtils.getTestProperty("rest.api.jasper.type");
        String reportKind = TestUtils.getTestProperty("rest.api.jasper.report");

        try{
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -7);
            JasperReportDto report = new JasperReportDto();
            report.setReportType(reportType);
            report.setUserId(userName);
            report.setStartTime(cal);
            report.setEndTime(Calendar.getInstance());
            report.setReport(reportKind);

            ObjectMapper mapper = new ObjectMapper();
            String jsonRequest = mapper.writeValueAsString(report);

            HttpEntity<?> requestEntity = new HttpEntity<String>(jsonRequest,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            ResponseEntity<String> responseEntity = template.exchange(actionUrl, method, requestEntity, String.class);

            assertNotNull("Pusta odpowiedz - ",responseEntity);
            assertNotNull("Puste body odpowiedzi - ",responseEntity.getBody());
            assertEquals("Stworzono  - ",HttpStatus.OK, responseEntity.getStatusCode());

            byte[] encodedDocument = ((String)responseEntity.getBody()).getBytes();
            byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

            File tempFile = TestUtils.getFileInTestFolder("jasper-report.xml");
            org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

            log.info("decoded Document xml {}" + decodedDocument.toString());

        }catch(Exception e){
            log.error("", e);
        }
    }

}
