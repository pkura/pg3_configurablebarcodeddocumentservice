package pl.compan.docusafe.rest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import pl.compan.docusafe.rest.cmis.CmisApiTest;
import pl.compan.docusafe.rest.cmis.CmisDocumentApiTest;
import pl.compan.docusafe.rest.cmis.CmisFolderApiTest;

@RunWith(Suite.class)
@SuiteClasses({ RestUserServiceTest.class, 
		RestDivisionServiceTest.class, RestDocumentServiceTest.class,
		RestOfficeCaseServiceTest.class, RestSubstitutionServiceTest.class,
		RestTaskListServiceTest.class, RESTXesServiceTest.class,
		RestAuthenticationServiceTest.class,RestRPCTest.class

		})
public class AllRestApiTests {

}
