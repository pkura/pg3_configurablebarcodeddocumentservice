package pl.compan.docusafe.rest.cmis;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.Acl;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.impl.Constants;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.ws.commons.util.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.compan.docusafe.core.alfresco.AlfrescoApi;
import pl.compan.docusafe.core.alfresco.AlfrescoConnector;
import pl.compan.docusafe.util.TestUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 */
public class CmisFolderApiTest {
    public static final Logger log = LoggerFactory.getLogger(CmisFolderApiTest.class);

    private AlfrescoApi alfrescoApi;
    private String newFolderName = "Nowa nazwa folderu testowego";

    @BeforeClass
    public static void  init() throws IOException {
        TestUtils.loadProperties();
        BasicConfigurator.configure();
    }

    @Before
    public void setUp() throws Exception {

        BindingType bindingType = TestUtils.getCmisBindingType();
        alfrescoApi = new AlfrescoApi(new TestBaseConnector(bindingType));
    }

    @After
    public void tearDown() throws Exception {
        log.info("after");
    }

    @Test
    public void testGetFolder() throws Exception {
        String folderId = "folder-" + TestUtils.getTestProperty("cmis.api.folder.id");
        Folder folder = getFolder(folderId);
        assertNotNull(folder == null ? null : folder.getName(), "Brak folderu o takim id");
        log.info("folder {} {}",folder,  folder.getName());
    }

    private Folder getFolder(String folderId) throws Exception {
        return (Folder) alfrescoApi.getObject(folderId);
    }

    @Test
    public void testGetFolderChildren() throws Exception {
        String folderId = "folder-" + TestUtils.getTestProperty("cmis.api.folder.id");
        Folder folder = getFolder(folderId);
        log.info("folder {}", folder);

        for(CmisObject children : folder.getChildren()){
            log.info("id->{}, title->{}", children.getId() ,children.getName());
        }
    }

    @Test
    public void testGetRootFolder(){
        Folder rootFolder = alfrescoApi.getRootFolder();
        assertNotNull(rootFolder == null ? null : rootFolder.getName(), "RootFolder Jest null");
        log.info("rootFolder {}", rootFolder);
    }


    @Test
    public void testCreateNewFolder(){
        String folderName = getCmisApiCreateFolderName();
        Folder rootFolder = alfrescoApi.getRootFolder();
        assertNotNull(rootFolder == null ? null : rootFolder.getName(), "RootFolder jest null");

        Map<String, Object> properties = Maps.newHashMap();
       // properties.put("cmisaction", Constants.CMISACTION_CREATE_FOLDER);
        properties.put(PropertyIds.BASE_TYPE_ID, BaseTypeId.CMIS_FOLDER.value());
        properties.put(PropertyIds.OBJECT_TYPE_ID, BaseTypeId.CMIS_FOLDER.value());
        properties.put(PropertyIds.NAME, folderName);
        Folder folder = (Folder)rootFolder.createFolder(properties);
        log.info("Utworozny folder {}", folder);
        assertNotNull(folder.getId(), "Folder nie zosta� utworzony");
    }

    @Test
    public void testGetRootChildren(){
        Folder rootFolder = alfrescoApi.getRootFolder();
        log.info("rootFolder {}", rootFolder);
        String folderToCheck = getCmisApiCreateFolderName();
        boolean hasCheckedFolder = false;
        for(CmisObject children : rootFolder.getChildren()){
            log.info("children {} {}", children.getId() ,children.getName());
            if(children.getName().equals(folderToCheck))
                hasCheckedFolder = true;
        }
        log.info("{}", hasCheckedFolder);
        assertTrue("folder nie posiada dziecka \'" + folderToCheck + "\'", hasCheckedFolder);
    }

    private String getCmisApiCreateFolderName() {
        return TestUtils.getTestProperty(TestUtils.CMIS_API_CREATE_FOLDER_NAME);
    }

    @Test
    public void testFolderChangeNameFirst() throws Exception {
        ;
        CmisObject folder = getFolderTested();
        log.info("before change {}", folder.getName());

        Map<String, Object> props = Maps.newHashMap();
        props.put(PropertyIds.NAME, newFolderName);
        folder.updateProperties(props);
        log.info("after change {}", folder.getName());

    }

    @Test
    public void testFolderChangeNameSecond() throws Exception {
        CmisObject folder = alfrescoApi.getObjectByPath("/G��wny/"+newFolderName);
        log.info("before second change {}", folder.getName());

        Map<String, Object> props = Maps.newHashMap();
        props.put(PropertyIds.NAME, getCmisApiCreateFolderName());
        folder.updateProperties(props);
        log.info("after second change {}", folder.getName());

    }

    @Test
    public void testDeleteFolder() throws Exception{
        CmisObject folder = getFolderTested();
        folder.delete();
    }

    /**
     * Dodatawanie uprawnie� do folderu
     * Lists.newArrayList("modify_att","read", "read_att","read_org_att","delete","create","modify")
     * @throws Exception
     */
    @Test
    public void testFolderAddAcl() throws Exception {
        CmisObject folder = getFolderTested();
        Ace ace = getFolderAceForTest();
        Acl acl = folder.addAcl(Lists.newArrayList(ace), AclPropagation.OBJECTONLY);
        log.info("acl {}", acl);
    }

    /**
     * Zwraca folder z cmisa do cel�w testowych
     * @return
     * @throws Exception
     */
    private CmisObject getFolderTested() throws Exception {
        return alfrescoApi.getObjectByPath("/G��wny/" + getCmisApiCreateFolderName());
    }

    @Test
    public void testFolderRemoveAcl() throws Exception{
        CmisObject folder = getFolderTested();
        Ace ace = getFolderAceForTest();
        Acl acl = folder.removeAcl(Lists.newArrayList(ace), AclPropagation.OBJECTONLY);
        log.info("acl {}", acl);
    }

    /**
     * Ace fodleru do testu
     * @return
     */
    private Ace getFolderAceForTest() {
        return new AccessControlEntryImpl(
                new AccessControlPrincipalDataImpl(TestUtils.getTestProperty("cmis.api.folder.principal")),
                Lists.newArrayList(TestUtils.getTestProperty("cmis.api.folder.permission")));
    }

    /**
     * Sprawdza czy folder zawiera uprawnienie testowe.
     * @throws Exception
     */
    @Test
    public void testFolderGetAcl_Contains() throws Exception{
        boolean contains = checkHasPermissionToTest();

        assertTrue("Folder nie ma tego uprawnienia", contains);
    }

    private boolean checkHasPermissionToTest() throws Exception {
        CmisObject folder = getFolderTested();
        Acl acl = alfrescoApi.getAcl(folder);
        boolean contains = false;
        for(Ace ace : acl.getAces()){
            log.info("ACE {} -> {}", ace.getPrincipal(), ace.getPermissions());
            if(ace.getPrincipal().getId().equals(TestUtils.getTestProperty("cmis.api.folder.principal")))
                contains = true;
        }
        return contains;
    }

    /**
     * Sprawdza czy folder zawiera uprawnienie testowe.
     * @throws Exception
     */
    @Test
    public void testFolderGetAcl_NotContains() throws Exception{
        boolean contains = checkHasPermissionToTest();
        assertFalse("Folder ma to uprawnienie", contains);
    }
}
