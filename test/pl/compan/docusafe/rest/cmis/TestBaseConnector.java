package pl.compan.docusafe.rest.cmis;

import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.alfresco.AlfrescoConnector;
import pl.compan.docusafe.util.TestUtils;
import pl.compan.docusafe.web.filter.CasUtils;

import java.io.IOException;
import java.util.Map;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 02.08.14
 */
// connector do cmisa
public class TestBaseConnector extends AlfrescoConnector
{

    public static final Logger log = LoggerFactory.getLogger(TestBaseConnector.class);
    //rodzaj po��czenia do Cmisa
    BindingType type = BindingType.ATOMPUB;

    public TestBaseConnector(BindingType type) {
        this.type = type;
    }

    /**
     * Zwraca parametry polaczenia
     * @return parametry polaczenia
     */
    protected Map<String, String> getConnectionParameters() throws IOException {
        Map<String, String> parameter = null;

        switch(type){
            case ATOMPUB:
                parameter = getAtomProperties();
                break;
            case WEBSERVICES:
                parameter = getWSProperties();
                break;
            case BROWSER:
                parameter = getBrowserProperties();
                break;
        }

        parameter.put(SessionParameter.HEADER + ".0", "Certificate-ssl: " + TestUtils.getCertificateFileBase64());
        return parameter;
    }

    /**
     * Application/json
     * @return
     */
    private Map<String, String> getWSProperties() {
        Map<String, String> parameter = com.google.common.collect.Maps.newHashMap();

        // Set the user credentials
        parameter.put("cmis.workbench.folder.includeAcls","false");
        parameter.put("cmis.workbench.object.includeAcls","false");
        if(Boolean.valueOf(TestUtils.getTestProperty("cas.enabled"))){
            parameter.put(SessionParameter.USER, "ticket");
            parameter.put(SessionParameter.PASSWORD, "rest.api.proxy.ticket");
        	
        } else {
            parameter.put(SessionParameter.USER, TestUtils.getTestProperty(TestUtils.CMIS_API_USER));
            parameter.put(SessionParameter.PASSWORD, TestUtils.getTestProperty(TestUtils.CMIS_API_PASS));        	
        }

        // Specify the connection settings
        log.error("url base cmis {}", TestUtils.getTestProperty("cmis.api.url.ws"));
        parameter.put(SessionParameter.REPOSITORY_ID, "docusafe-repository");
        String url = TestUtils.getTestProperty("cmis.api.url.ws");
        // connection settings
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.WEBSERVICES.value());
        parameter.put(SessionParameter.WEBSERVICES_ACL_SERVICE, getWsUrl("ACLService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_DISCOVERY_SERVICE, getWsUrl("DiscoveryService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_MULTIFILING_SERVICE, getWsUrl("MultiFilingService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_NAVIGATION_SERVICE, getWsUrl("NavigationService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_OBJECT_SERVICE, getWsUrl("ObjectService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_POLICY_SERVICE, getWsUrl("PolicyService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_RELATIONSHIP_SERVICE, getWsUrl("RelationshipService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_REPOSITORY_SERVICE, getWsUrl("RepositoryService?wsdl"));
        parameter.put(SessionParameter.WEBSERVICES_VERSIONING_SERVICE, getWsUrl("VersioningService?wsdl"));
        // Set the alfresco object factory
        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.apache.chemistry.opencmis.client.runtime.repository.ObjectFactoryImpl");
        return parameter;
    }

    public String getWsUrl(String wsdlEnd){
        String url = TestUtils.getTestProperty("cmis.api.url.ws");
        return url + '/' + wsdlEnd;
    }

    /**
     * Application/json
     * @return
     */
    private Map<String, String> getBrowserProperties() {
        Map<String, String> parameter = com.google.common.collect.Maps.newHashMap();

        // Set the user credentials
        parameter.put("cmis.workbench.folder.includeAcls","false");
        parameter.put("cmis.workbench.object.includeAcls","false");
        if(Boolean.valueOf(TestUtils.getTestProperty("cas.enabled"))){
            parameter.put(SessionParameter.USER, "ticket");
            parameter.put(SessionParameter.PASSWORD, "rest.api.proxy.ticket");
        	
        } else {
            parameter.put(SessionParameter.USER, TestUtils.getTestProperty(TestUtils.CMIS_API_USER));
            parameter.put(SessionParameter.PASSWORD, TestUtils.getTestProperty(TestUtils.CMIS_API_PASS));        	
        }

        // Specify the connection settings
        log.error("url base cmis {}", TestUtils.getTestProperty(TestUtils.CMIS_API_URL));
        parameter.put(SessionParameter.BROWSER_URL, TestUtils.getTestProperty("cmis.api.url.browser"));
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.BROWSER.value());
        // Set the alfresco object factory
        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.apache.chemistry.opencmis.client.runtime.repository.ObjectFactoryImpl");
        return parameter;
    }

    /**
     * Application/xml
     * @return
     */
    private Map<String, String> getAtomProperties() {
        Map<String, String> parameter = com.google.common.collect.Maps.newHashMap();

        // Set the user credentials
        parameter.put("cmis.workbench.folder.includeAcls","false");
        parameter.put("cmis.workbench.object.includeAcls","false");
        if(Boolean.valueOf(TestUtils.getTestProperty("cas.enabled"))){
            parameter.put(SessionParameter.USER, "ticket");
            parameter.put(SessionParameter.PASSWORD, "rest.api.proxy.ticket");
        	
        } else {
            parameter.put(SessionParameter.USER, TestUtils.getTestProperty(TestUtils.CMIS_API_USER));
            parameter.put(SessionParameter.PASSWORD, TestUtils.getTestProperty(TestUtils.CMIS_API_PASS));        	
        }

        // Specify the connection settings
        log.error("url base cmis {}", TestUtils.getTestProperty(TestUtils.CMIS_API_URL));
        parameter.put(SessionParameter.ATOMPUB_URL, TestUtils.getTestProperty(TestUtils.CMIS_API_URL));
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
        // Set the alfresco object factory
        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.apache.chemistry.opencmis.client.runtime.repository.ObjectFactoryImpl");
        return parameter;
    }
}