package pl.compan.docusafe.rest.cmis;

import com.google.common.base.Preconditions;
import static junit.framework.Assert.*;
import com.google.common.collect.Maps;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.PropertyData;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.http.entity.mime.MIME;
import org.apache.log4j.BasicConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.MimeType;
import org.testng.collections.Lists;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.alfresco.AlfrescoApi;
import pl.compan.docusafe.core.alfresco.AlfrescoConnector;
import pl.compan.docusafe.core.alfresco.AlfrescoUtils;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;
import pl.compan.docusafe.rest.RestApiTest;
import pl.compan.docusafe.util.TestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Klasa przeznaczona do test�w interfejsu Cmisa
 */
public class CmisApiTest {
    public static final Logger log = LoggerFactory.getLogger(CmisApiTest.class);

    private AlfrescoApi alfrescoApi;

    @BeforeClass
    public static void  init() throws IOException {
        TestUtils.loadProperties();
        BasicConfigurator.configure();
    }

    @Before
    public void setUp() throws Exception {
        BindingType bindingType = BindingType.fromValue(TestUtils.getTestProperty("cmis.bindingType"));
        alfrescoApi = new AlfrescoApi(new TestBaseConnector(bindingType));
    }

    @After
    public void tearDown() throws Exception {
        log.info("after");
        alfrescoApi.disconnect();
    }

    @Test
    public void testGetObject() throws Exception{
        String id = "document-metadata-20062";
        CmisObject obj = alfrescoApi.getObject(id);
        log.info("object -> {}", obj);
        assertNotNull(obj == null ? null : obj.getName(), "Obiekt " + id + " Jest null");
    }

    @Test
    public void testRootFolder() throws Exception {
        Folder rootFolder = alfrescoApi.getRootFolder();
        assertNotNull(rootFolder == null ? null : rootFolder.getName(), "RootFolder Jest null");
        log.info("rootFolder " + rootFolder);
    }
}
