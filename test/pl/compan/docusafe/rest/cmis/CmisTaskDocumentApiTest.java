package pl.compan.docusafe.rest.cmis;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.client.runtime.DocumentImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.alfresco.AlfrescoApi;
import pl.compan.docusafe.core.alfresco.AlfrescoConnector;
import pl.compan.docusafe.core.alfresco.AlfrescoUtils;
import pl.compan.docusafe.core.base.DocumentType;
import pl.compan.docusafe.core.cmis.DSObjectDataConverter;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.dockinds.dwr.FieldData;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.TestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 13.07.14
 */
public class CmisTaskDocumentApiTest {
    public static final Logger log = LoggerFactory.getLogger(CmisTaskDocumentApiTest.class);

    private AlfrescoApi alfrescoApi;

    @BeforeClass
    public static void  init() throws IOException {
        TestUtils.loadProperties();
        BasicConfigurator.configure();
    }

    @Before
    public void setUp() throws Exception {
        BindingType bindingType = BindingType.fromValue(TestUtils.getTestProperty("cmis.bindingType"));
        alfrescoApi = new AlfrescoApi(new TestBaseConnector(bindingType));
    }

    @After
    public void tearDown() throws Exception {
        log.info("after");
    }

    /**
     * sprawdzamy zawarto�� folderu dokumentu folder dokumentu
     * @throws Exception
     */
    @Test
    public void testDocumentFolder() throws Exception {
        Folder doc = (Folder) alfrescoApi.getObject("document-"+getDocumentId());
        boolean containsDocumentMetadata = false;
        for(CmisObject children : doc.getChildren()){
            log.info("id->{}, title->{}", children.getId(),  children.getName());
            if(children.getId().contains(CmisPrepend.DOCUMENT_METADATA.getPrepend()))
                containsDocumentMetadata = true;
        }

        assertTrue("Document testowy nie posiada dokumentu metdanych", containsDocumentMetadata);
    }

    /**
     * pobranie zalacznika
     */
    @Test
    public void testGetAttachmentContentStream() throws Exception {
        String attachmentId = "attachment-"+getAttachmentId();
        Document document = (Document) alfrescoApi.getObject(attachmentId);
        ContentStream contentStream = document.getContentStream();
        InputStream is = contentStream.getStream();
        FileUtils.writeIsToFile(is, TestUtils.getFileInTestFolder(attachmentId + "-" + contentStream.getFileName()));
        log.info("ContentStream {} -> \n filename={} \n length={} \n mime={}", attachmentId, contentStream.getFileName(), contentStream.getBigLength(), contentStream.getMimeType());
    }

    /**
     * Aktualizuje metdanae dokumentu
     * @throws Exception
     */
    @Test
    public void testDocumentMetaDataUpdate() throws Exception {
        Document doc = (Document) alfrescoApi.getObject("document-metadata-"+getDocumentId());
        doc.updateProperties(getUpdateProperties());
    }

    /**
     * Aktualizuje metdanae dokumentu
     * @throws Exception
     */
    @Test
    public void testAddContentStreamToDocument() throws Exception {
        Document doc = (Document) alfrescoApi.getObject("document-metadata-"+getDocumentId());
        doc.setContentStream(getContentStream2(), true);
    }

    /**
     * Pobranie za��cznika z wersja
     * @throws Exception
     */
    @Test
    public void testGetAttachmentContentStream_withVersion() throws Exception {
        String attachmentId = "attachment-"+getAttachmentId();
        String streamId = getAttachmentVersion();
        Document document = (Document) alfrescoApi.getObject(attachmentId);
        ContentStream contentStream = document.getContentStream(streamId);
        assertNotNull("Za��cznik jest pusty", contentStream);
        InputStream is = contentStream.getStream();
        FileUtils.writeIsToFile(is, TestUtils.getFileInTestFolder(attachmentId + "-" + streamId+ "-"+ contentStream.getFileName()));
        log.info("ContentStream {} -> \n filename={} \n length={} \n mime={}", attachmentId, contentStream.getFileName(), contentStream.getBigLength(), contentStream.getMimeType());
    }

    @Test
    public void testUpdateContentStream() throws Exception {
        String attachmentId = "attachment-"+getAttachmentId();
        Document document = (Document) alfrescoApi.getObject(attachmentId);
        Document document1 = document.setContentStream(getContentStream2(), true);
    }

    /**
     * Tworzy dokument procesu zadanie
     * @throws Exception
     */
    @Test
    public void testCreateTaskDocument() throws Exception {
        Folder rootFolder = alfrescoApi.getRootFolder();
        ContentStream contentStream = getContentStream();

        Map<String, Object> props = getProperties();

        log.debug("prop {}", props.toString());
        Document document = alfrescoApi.createDocument(props, rootFolder, contentStream, VersioningState.MAJOR);
        log.debug("document {}", document.getId());
        assertNotNull("Obiekt nie zosta� utworzony", document.getId());
    }

    @Test
    public void testDocumentProperties() throws Exception {
        CmisObject doc = alfrescoApi.getObject("document-metadata-"+getDocumentId());
        log.info(" {}", doc);
        List<Property<?>> properties = doc.getProperties();
        boolean hasStatus = false;
        boolean hasDescription = false;

        for(Property<?> p : properties){
            if("ds:STATUS".contains(p.getDefinition().getId())){
                log.info("{},{} -> {}", p.getDefinition().getId(), p.getDisplayName(), p.getValuesAsString());
                hasStatus= StringUtils.isNotBlank(p.getValueAsString());
            }

            if("ds:DOC_DESCRIPTION".contains(p.getDefinition().getId())){
                log.info("{},{} -> {}", p.getDefinition().getId(), p.getDisplayName(), p.getValuesAsString());
                hasDescription= StringUtils.isNotBlank(p.getValueAsString());
            }
        }

        assertTrue("Dokument nie ma ustawionego Odbiorcy", hasStatus);
        assertTrue("Dokument nie ma ustawionego Tytu�u zadania", hasDescription);


    }

    @Test
    public void testDocumentPropertiesAll() throws Exception {
        CmisObject doc = alfrescoApi.getObject("document-metadata-"+getDocumentId());
        log.info(" {}", doc);
        List<Property<?>> properties = doc.getProperties();

        for(Property<?> p : properties){
                log.info("{},{} -> {}", p.getDefinition().getId(), p.getDisplayName(), p.getValuesAsString());
        }
    }

    private Map<String, Object> getProperties() {
        Map<String, Object> props = Maps.newHashMap();
        props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
        props.put(DSObjectDataConverter._DS_DOCUMENT_KIND, "zadanie");
        props.put(DSObjectDataConverter._DS_IMPORT_KIND, DocumentType.INTERNAL.getName());
        props.put("ds:DOC_DESCRIPTION", "Opis pisma");
        props.put("ds:DOC_DATE_REMINDER","20-11-2015");
        props.put("ds:DOC_DATE_REALIZATION","20-11-2015");
        props.put("ds:RECIPIENT", TestUtils.getTestProperty("cmis.task.document.recipient"));
        props.put("ds:STATUS","10");
        return props;
    }

    private Map<String, Object> getUpdateProperties() {
        Map<String, Object> props = Maps.newHashMap();
        props.put("ds:DOC_DESCRIPTION", "Logiczny opsi pisma zmieniony");
        props.put("ds:DOC_DATE_REALIZATION","20-11-2015");
        props.put("ds:ABSTRAKT","za��� z�ci� ja��");
        props.put("ds:STATUS", "30");
        
        return props;
    }


    private ContentStream getContentStream() throws EdmException, IOException {
    	String pathFile = TestUtils.getTestProperty("cmis.api.document.path.repository") + "dokument-test.png";
        File f = new File(pathFile);
        return AlfrescoUtils.getContentStream(f, "image/png", f.getName(), -1);
    }

    private ContentStream getContentStream2() throws EdmException, IOException {
    	String pathFile = TestUtils.getTestProperty("cmis.api.document.path.repository") + "za�� _ ��.png";
        File f = new File(pathFile);
        return AlfrescoUtils.getContentStream(f, "text/html", f.getName(), -1);
    }

    public String getDocumentId() {
        return TestUtils.getTestProperty("cmis.api.document.id");
    }

    public String getAttachmentId() {
        return TestUtils.getTestProperty("cmis.api.document.attachment.id");
    }

    public String getAttachmentVersion() {
        return TestUtils.getTestProperty("cmis.api.document.attachment.version");
    }
}
