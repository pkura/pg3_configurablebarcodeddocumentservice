package pl.compan.docusafe.rest;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import edu.emory.mathcs.backport.java.util.Collections;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.*;

public class RestXmlGraphMLServiceTest extends RestAbstractRpcTest{

    static String cookie;

    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_XML);
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

    @Test
    public void testGetLogByDocumentId() throws IOException {

        log.debug("Testing getLogByDocument method");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        headers.setAccept(Arrays.asList(new MediaType(MediaType.TEXT_HTML.getType(), MediaType.TEXT_HTML.getSubtype())));

        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);

        RestTemplate template = TestUtils.getRestTemplate();
        String documentId = TestUtils.getTestProperty("graph.document.get");

        Map<String,String> var = Collections.singletonMap("documentId", documentId);

        ResponseEntity<String> responseEntity = template.exchange(getUrl("/graphml/{documentId}/document"),
                HttpMethod.GET, requestEntity, String.class,var);

        // testowanie
        assertNotNull("Empty response", responseEntity);
        assertNotNull("Empty body response", responseEntity.getBody());
        assertEquals("Wrong status code",HttpStatus.OK, responseEntity.getStatusCode());

        byte[] encodedDocument = responseEntity.getBody().getBytes();
        byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

        File tempFile = new File(TestUtils.getTestProperty(TestUtils.REST_TEST_PATH) + "/DOCUMENT_ID_"+ var.get("documentId") + ".graphml");
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

        log.info("decoded document's logs xml {}" + decodedDocument.toString());

    }

}
