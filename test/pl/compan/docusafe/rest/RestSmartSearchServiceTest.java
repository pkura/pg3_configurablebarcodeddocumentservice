package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import edu.emory.mathcs.backport.java.util.Collections;

import pl.compan.docusafe.rest.views.DocumentView;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

public class RestSmartSearchServiceTest {

	public static final Logger log = LoggerFactory.getLogger(RestSmartSearchServiceTest.class);

    public static String url, user, pass;
    public static final String SET_COOKIE="Set-Cookie";

    static String cookie;
    
    long beginTime, endTime;
    
//    private static XLSParamWriter xlsParamWriter;

	@BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login();
//        xlsParamWriter = new XLSParamWriter();
//        try {
//    		xlsParamWriter.createColumnHeaders(XLSParamWriter.SMART_SEARCH_TEST);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
//		xlsParamWriter.updateFile(XLSParamWriter.SMART_SEARCH_TEST);
//		xlsParamWriter.autoWidth(XLSParamWriter.SMART_SEARCH_TEST);
	}

	@Test
	public void testSearchDocument() throws JsonGenerationException, JsonMappingException, IOException {
//		xlsParamWriter.setActionName("Test wyszukiwania dokumetnu");
		log.debug("\tTest wyszukiwania dokumetnu - smart search");
		
		// przygotowanie
		HttpHeaders headers = getJsonHeaders(cookie);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String,String> var = Collections.singletonMap("barcode", "Fajny QR Code");
		
		RestTemplate template = TestUtils.getRestTemplate();
		beginTime = (Calendar.getInstance()).getTimeInMillis();
		ResponseEntity<DocumentView> responseEntity = template.exchange(getUrl("/smartsearch?barcode={barcode}"),
																HttpMethod.GET, requestEntity, DocumentView.class,
																var);
		endTime = (Calendar.getInstance()).getTimeInMillis();  
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not fetch document - ",HttpStatus.OK, responseEntity.getStatusCode());
//        
//        List<Map<String, String>> taskList = responseEntity.getBody();
//        assertFalse("Task list collection must be populeted", CollectionUtils.isEmpty(taskList));
        
        
//        convertRequestResponseToJSON(requestEntity, responseEntity);
        
     // czas wykkonania
 		long executionTime = endTime - beginTime;
// 		xlsParamWriter.setTestOutputData(getUrl("/tasklist/"+vars.get("username")+"/user/"+vars.get("offset")+"/offset"),
// 										 requestEntity,responseEntity,HttpMethod.GET,executionTime);
	}
	
//	----------------------------------------------------------------------------------------------------------------------------------------
	
	private void convertRequestResponseToJSON(HttpEntity<?> requestEntity,
			ResponseEntity<?> responseEntity) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		System.out.println("\nRequest\n");
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestEntity.getHeaders())); 
		// if is string we must bind this String to Object
		if (requestEntity.getBody() == null){
			System.out.println("null");
		} else if (requestEntity.getBody().getClass() == String.class){
			String requestString = (String) requestEntity.getBody();
			Object json = mapper.readValue(requestString , Object.class);
			System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json));
		} else {
			System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestEntity.getBody()));
		}
		
		System.out.println("\nResponse\n"); 
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getHeaders()));
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getBody()));
		
	}
	
	private static String login() {
		System.out.println("Zalogowanie do systemu jako " + user + "\n");
        HttpHeaders headers = getJsonHeaders(null);

        RestTemplate template = TestUtils.getRestTemplate();

        HttpEntity<String> e = new HttpEntity<String>("",headers);
        ResponseEntity<String> loginResponse = template.postForEntity(
                getUrl("/login?username={username}&password={password}"), e, String.class, user, pass);

        return loginResponse.getHeaders().get(SET_COOKIE).iterator().next();
	}

	private static String getUrl(String uri) {
		return url + uri;
	}

	private static HttpHeaders getJsonHeaders(String cookie) {
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        if(StringUtils.isNotEmpty(cookie)){
            headers.add("Cookie", cookie);
        }
        return headers;
	}

}
