package pl.compan.docusafe.rest;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.LinkedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.LinkedList;

import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;







import edu.emory.mathcs.backport.java.util.Collections;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeFolderDto;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

public class RestTemplateDocumentTest extends RestAbstractRpcTest{

    static String cookie;
    
	    
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_JSON);
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Before
	public void setUp() throws Exception {

	}



    @Test
    public void testGetAvailableTemplateForDocument(){
        String documentId = TestUtils.getTestProperty("rest.document.get");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();

        Map<String, String> var = Collections.singletonMap("documentId", documentId);
        ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/template/documentId={documentId}"), HttpMethod.GET, requestEntity, ListDtos.class, var);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not fetch history of case - ",HttpStatus.OK, responseEntity.getStatusCode());

    }


	@Test
	public void testAddTemplate() throws JsonGenerationException, JsonMappingException, IOException {
        String dockind = TestUtils.getTestProperty("rest.document.dictionary.search.dockindCn");
        String templateName = TestUtils.getTestProperty("rest.api.template.name");

        File file = TestUtils.getFileInTestFolder("test2.rtf");
        MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();
        mvm.add("file", new FileSystemResource(file)); // MultipartFile
		
		// przygotowanie
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.MULTIPART_FORM_DATA, "text", MediaType.TEXT_PLAIN);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(mvm, headers);
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("dockind", dockind);
		vars.put("templateName", templateName);
		
		
        // Message Converters
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new FormHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new MappingJacksonHttpMessageConverter());

        // RestTemplate
        RestTemplate template = TestUtils.getRestTemplate();
        template.setMessageConverters(messageConverters);

		ResponseEntity<String> responseEntity = template.exchange(getUrl("/template/dockind={dockind}/{templateName:.+}"), 
				HttpMethod.POST, request, String.class,vars);
		
        
	}

}
