package pl.compan.docusafe.rest;

import java.io.IOException;
import java.util.*;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RestConverterTest extends RestAbstractRpcTest{

    //cookies zalogowane użytkownika
    static String cookie;

    @BeforeClass
    public static void initTest() throws Exception {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user = TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass = TestUtils.getTestProperty(TestUtils.REST_API_PASS);

        cookie = login(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        cookie = null;
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testConvertAttachmentToPdf(){
        log.debug("\tTest konwersji załącznika do pdf -> base64");
        String actionUrl = getUrl("/converter/{attachmentId}");
        HttpMethod method = HttpMethod.PUT;
        String attachmentId = TestUtils.getTestProperty("rest.converter.attachmentId");

        try{
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
            
            HttpEntity<?> requestEntity = new HttpEntity<String>("",headers);

            RestTemplate template = TestUtils.getRestTemplate();
            Map<String, Long> var = Collections.singletonMap("attachmentId", Long.valueOf(attachmentId));
            ResponseEntity<String> responseEntity = template.exchange(actionUrl, method, requestEntity, String.class, var);

            assertNotNull("Pusta odpowiedz - ",responseEntity);
            assertNotNull("Puste body odpowiedzi - ",responseEntity.getBody());
            assertEquals("Stworzono  - ", HttpStatus.OK, responseEntity.getStatusCode());

            byte[] encodedDocument = ((String)responseEntity.getBody()).getBytes();
            byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

            File tempFile = TestUtils.getFileInTestFolder("przekonwerotwany.pdf");
            org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

            log.info("decoded Document {}" + decodedDocument.toString());

        }catch(Exception e){
            log.error("", e);
        }
    }

}
