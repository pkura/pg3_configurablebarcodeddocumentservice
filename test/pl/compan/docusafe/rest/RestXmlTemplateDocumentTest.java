package pl.compan.docusafe.rest;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import edu.emory.mathcs.backport.java.util.Collections;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeFolderDto;
import pl.compan.docusafe.api.user.office.TemplateDto;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.AssociativeDocumentStore.AssociativeDocumentBean;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

public class RestXmlTemplateDocumentTest extends RestAbstractRpcTest{

    static String cookie;
	    
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_XML);
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}
	

	@Test
	public void testGetAvailableTemplateForDocument() throws JAXBException {	
		String documentId = TestUtils.getTestProperty("rest.document.get");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		Map<String, String> var = Collections.singletonMap("documentId", documentId);

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/template/documentId={documentId}"), HttpMethod.GET, requestEntity, ListDtos.class, var);
		
		// testowanie
		assertNotNull("Empty response",responseEntity);
		assertNotNull("Empty body response",responseEntity.getBody());
		assertEquals("Status code must be ok",HttpStatus.OK, responseEntity.getStatusCode());
		
		List<TemplateDto> templateDtoDtos = responseEntity.getBody().getItems();
		assertFalse("No any office case for user eodc ! ", CollectionUtils.isEmpty(templateDtoDtos));
		
	}

	

	@Test
	public void testAddTemplate() throws JAXBException {
		String documentId = TestUtils.getTestProperty("rest.api.officecase.documentId.addTo");
        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));

		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);		
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("documentId", documentId);
		vars.put("officeCaseId", officeCaseId);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/officecase/{documentId}/document/{officeCaseId}/case/add"),
																	HttpMethod.PUT,
																	requestEntity,
																	String.class,
																	vars);
		
		// testow
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Not empty body response  - ",responseEntity.getBody());
        assertEquals("Do not add document to office case - ",HttpStatus.CREATED, responseEntity.getStatusCode());
        
	}

	
    
}
