package pl.compan.docusafe.rest;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import edu.emory.mathcs.backport.java.util.Collections;
import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.OfficeFolderDto;
import pl.compan.docusafe.core.office.AssociativeDocumentStore;
import pl.compan.docusafe.core.office.CasePriority;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

public class RestOfficeCaseServiceTest extends RestAbstractRpcTest{

    static String cookie;
    
	    
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_JSON);
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Before
	public void setUp() throws Exception {

	}

	
	@Test
	public void testCreateOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest stworzenia nowej sprawy");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        OfficeCaseDto officeCaseDto = createOfficeCaseDto();
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonRequest = mapper.writeValueAsString(officeCaseDto);
		
		HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/officecase/create"), HttpMethod.POST, requestEntity, String.class);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not create user - ",HttpStatus.CREATED, responseEntity.getStatusCode());
	}


    @Test
    public void testUpdateOfficeCase() throws Exception{
        String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON, "text", MediaType.TEXT_HTML);
        OfficeCaseDto officeCaseDto = createOfficeCaseDto();
        officeCaseDto.setId(Long.valueOf(id));

        ObjectMapper mapper = new ObjectMapper();
        String jsonRequest = mapper.writeValueAsString(officeCaseDto);

        HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest,headers);

        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<String> responseEntity = template.exchange(getUrl("/officecase/update"), HttpMethod.POST, requestEntity, String.class);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not create user - ",HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    public void testDeleteOfficeCase() throws Exception{
        String id = TestUtils.getTestProperty("rest.api.officecase.get.id");
        String reason = TestUtils.getTestProperty("rest.api.officecase.delete.reason");
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        KeyElement officeCaseDto = new KeyElement(id, reason);

        ObjectMapper mapper = new ObjectMapper();
        String jsonRequest = mapper.writeValueAsString(officeCaseDto);

        HttpEntity<String> requestEntity = new HttpEntity<String>(jsonRequest,headers);

        RestTemplate template = TestUtils.getRestTemplate();
        ResponseEntity<Object> responseEntity = template.exchange(getUrl("/officecase/remove"), HttpMethod.POST, requestEntity, Object.class);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertEquals("Do not delete case - ",HttpStatus.ACCEPTED, responseEntity.getStatusCode());
    }


    protected OfficeCaseDto createOfficeCaseDto() {
        String clerk = TestUtils.getTestProperty("rest.api.officecase.create.clerk");
        String title = TestUtils.getTestProperty("rest.api.officecase.create.title");
        String description = TestUtils.getTestProperty("rest.api.officecase.create.description");
        Integer priority = Integer.parseInt(TestUtils.getTestProperty("rest.api.officecase.create.priority"));

        Calendar calendar = TestUtils.getTestPropertyGetDataTime("rest.api.officecase.iso.time");
        String dateTime = ISODateTimeFormat.dateTime().print(calendar.getTimeInMillis());

        String documentId = TestUtils.getTestProperty("rest.api.officecase.create.documentId");
        Long portfolioId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.create.portfolioId"));
        return createOfficeCaseDto(clerk, title, description, priority, dateTime, documentId, portfolioId);
    }

    @Test
    public void testGetAudit(){
        String id = TestUtils.getTestProperty("rest.api.officecase.get.id");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>("",headers);
        RestTemplate template = TestUtils.getRestTemplate();

        Map<String, String> var = Collections.singletonMap("id", id);
        ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/{id}/audit"), HttpMethod.GET, requestEntity, ListDtos.class, var);

        // testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not fetch history of case - ",HttpStatus.OK, responseEntity.getStatusCode());

    }



	@Test
	public void testGetUserOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\t\nTest pobierania wszystkie sprawy dla uzytkownika\n");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		Map<String, String> var = Collections.singletonMap("username", TestUtils.getTestProperty("rest.api.officecase.user.case"));

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/{username}/user"), HttpMethod.GET, requestEntity, ListDtos.class, var);
		
		// testowanie
		assertNotNull("Empty response",responseEntity);
		assertNotNull("Empty body response",responseEntity.getBody());
		assertEquals("Status code must be ok",HttpStatus.OK, responseEntity.getStatusCode());
		
	}

	@Test
	public void testGetDivisionOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest pobierania wszystkich spraw z dzia�u 'Dzia� CPP 1'");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		
		RestTemplate template = TestUtils.getRestTemplate();

		String guid = TestUtils.getTestProperty("rest.api.officecase.guid.case");
		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/division?guid=" + guid), HttpMethod.GET, requestEntity, ListDtos.class);
		
		// testowanie
		assertNotNull("Empty response",responseEntity);
		assertNotNull("Empty body response",responseEntity.getBody());
		assertEquals("Status code must be ok",HttpStatus.OK, responseEntity.getStatusCode());
		
		List<OfficeCaseDto> officeCaseDtos = responseEntity.getBody().getItems();
		assertFalse("No any office case in division ! ", CollectionUtils.isEmpty(officeCaseDtos));

	}

	@Test
	public void testGetDocumentOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest pobierania wszystkich spraw dokumentu");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		Map<String, String> var = Collections.singletonMap("documentId", TestUtils.getTestProperty("rest.api.officecase.get.documentId"));

		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/{documentId}/document"), HttpMethod.GET, requestEntity, ListDtos.class, var);
		
		// testowanie
		assertNotNull("Empty response",responseEntity);
		assertNotNull("Empty body response",responseEntity.getBody());
		assertEquals("Status code must be ok",HttpStatus.OK, responseEntity.getStatusCode());
		
	}

    @Test
    public void testGetDocuments() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("\tTest pobierania pism sprawy");

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
        Map<String, String> var = Collections.singletonMap("officeCaseId", TestUtils.getTestProperty("rest.api.officecase.get.id"));

        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/{officeCaseId}/documents/get"), HttpMethod.GET, requestEntity, ListDtos.class, var);

        // testowanie
        assertNotNull("Empty response",responseEntity);
        assertNotNull("Empty body response",responseEntity.getBody());
        assertEquals("Status code must be ok",HttpStatus.OK, responseEntity.getStatusCode());

    }

	@Test
	public void testGetOfficeCaseToOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
        String caseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
		log.debug("\tTest pobierania wszystkich spraw ze sprawy");
		
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		Map<String, String> var = Collections.singletonMap("officeCaseId", caseId);
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/{officeCaseId}/getcases"), HttpMethod.GET, requestEntity, ListDtos.class, var);
		
		// testowanie
		assertNotNull("Empty response",responseEntity);
		assertNotNull("Empty body response",responseEntity.getBody());
		assertEquals("Status code must be ok",HttpStatus.OK, responseEntity.getStatusCode());
		
	}

	@Test
	public void testAddDocumentToOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
        String documentId = TestUtils.getTestProperty("rest.api.officecase.documentId.addTo");
        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));

		log.debug("\tTest dodawania dokumentu do sprawy");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);		
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("documentId", documentId);
		vars.put("officeCaseId", officeCaseId);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/officecase/{documentId}/document/{officeCaseId}/case/add"),
																	HttpMethod.PUT,
																	requestEntity,
																	String.class,
																	vars);
		
		// testow
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Not empty body response  - ",responseEntity.getBody());
        assertEquals("Do not add document to office case - ",HttpStatus.CREATED, responseEntity.getStatusCode());
        
	}

    @Test
    public void testAddOfficeCaseToOfficeCase() throws Exception{
        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));
        Long officeCaseAddId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.addId"));

        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
        Map<String,Object> vars = new HashMap<String, Object>();
        vars.put("officeCaseAddId", officeCaseAddId);
        vars.put("officeCaseId", officeCaseId);

        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<String> responseEntity = template.exchange(getUrl("/officecase/{officeCaseId}/officecase/{officeCaseAddId}/add"),
                HttpMethod.PUT,
                requestEntity,
                String.class,
                vars);

        // testow
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Not empty body response  - ",responseEntity.getBody());
        assertEquals("Do not add document to office case - ",HttpStatus.CREATED, responseEntity.getStatusCode());
    }

	@Test
	public void testRemoveDocumentFromCase() throws JsonGenerationException, JsonMappingException, IOException {
        String documentId = TestUtils.getTestProperty("rest.api.officecase.documentId.addTo");
        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));
		log.debug("\tTest usuwania dokumento ze sprawy");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);	
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		Map<String, Object> vars = new HashMap<String,Object>();
		vars.put("documentId", documentId);
		vars.put("officeCaseId", officeCaseId);
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<String> responseEntity = template.exchange(getUrl("/officecase/{documentId}/document/{officeCaseId}/case/remove"),
																  HttpMethod.DELETE,
																  requestEntity,
																  String.class,
																  vars);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty response body - ",responseEntity.getBody());
        assertEquals("Not created status on response - ",HttpStatus.OK, responseEntity.getStatusCode());
        
	}

	@Test
	public void testGetOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
        String caseId = TestUtils.getTestProperty("rest.api.officecase.get.id");
		log.debug("\tTest pobierania sprawy");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);		
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String,String> var = Collections.singletonMap("officeCaseId", caseId);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<OfficeCaseDto> responseEntity = template.exchange(getUrl("/officecase/{officeCaseId}/case"),
																	HttpMethod.GET,
																	requestEntity,
																	OfficeCaseDto.class,
																	var);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not find case - ",HttpStatus.OK, responseEntity.getStatusCode());
        
        OfficeCaseDto officeCaseDto = responseEntity.getBody();
        assertEquals(Long.valueOf(caseId), officeCaseDto.getId());
	}

	@Test
	public void testGetCasePriority() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\t\nTest pobierania wszystkich stopni spraw\n");
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/priority"), HttpMethod.GET, requestEntity, ListDtos.class);
		
		// testowanie
		assertNotNull("Empty response", responseEntity);
		assertNotNull("Empty body response", responseEntity.getBody());
		assertEquals("Wrong status code",HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	public void testGetPortfolio() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\t\nTest pobierania teczki przez guida\n");
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
		String guid = TestUtils.getTestProperty("rest.api.officecase.guid.case");
		Map<String,String> var = Collections.singletonMap("guid", guid);
		
		RestTemplate template = TestUtils.getRestTemplate();

		ResponseEntity<ListDtos> responseEntity = template.exchange(getUrl("/officecase/portfolio/{guid}"), HttpMethod.GET, requestEntity, ListDtos.class, var);
		
		// testowanie
		assertNotNull("Empty response", responseEntity);
		assertNotNull("Empty body response", responseEntity.getBody());
		assertEquals("Wrong status code",HttpStatus.OK, responseEntity.getStatusCode());
	}
	
	@Test
	public void testRemoveOfficeCaseFromoOfficeCase() throws JsonGenerationException, JsonMappingException, IOException {
        String officeCaseRemoveId = TestUtils.getTestProperty("rest.api.officecase.removeId");
        Long officeCaseId = Long.valueOf(TestUtils.getTestProperty("rest.api.officecase.get.id"));

		log.debug("\tTest usuwania sprawy ze sprawy");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON,"text",MediaType.TEXT_HTML);		
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String,Object> vars = new HashMap<String, Object>();
		vars.put("officeCaseRemoveId", officeCaseRemoveId);
		vars.put("officeCaseId", officeCaseId);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
		ResponseEntity<String> responseEntity = template.exchange(getUrl("/officecase/{officeCaseId}/officecase/{officeCaseRemoveId}/remove"),
																	HttpMethod.DELETE,
																	requestEntity,
																	String.class,
																	vars);
		
		// testow
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Not empty body response  - ",responseEntity.getBody());
        assertEquals("Do not add document to office case - ",HttpStatus.OK, responseEntity.getStatusCode());
        
	}
}
