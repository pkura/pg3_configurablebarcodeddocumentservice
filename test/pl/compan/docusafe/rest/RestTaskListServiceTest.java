package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.office.OfficeCaseDto;
import pl.compan.docusafe.api.user.office.TaskList;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

public class RestTaskListServiceTest extends RestAbstractRpcTest{

  static String cookie;
    
	    
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        cookie = login(MediaType.APPLICATION_JSON);
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Before
	public void setUp() throws Exception {

	}


	@Test
	public void testGetUserTaskList() throws JsonGenerationException, JsonMappingException, IOException {
		String username = TestUtils.getTestProperty("rest.tasklist.username");
		Integer offset = new Integer(TestUtils.getTestProperty("rest.tasklist.offset"));
		log.debug("\tTest pobierania listy zada� u�ytkownika");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		Map<String,Object> vars = new HashMap<String,Object>();
		vars.put("username", username);
		vars.put("offset", offset );
		
		RestTemplate template = TestUtils.getRestTemplate();
		ResponseEntity<TaskList> responseEntity = template.exchange(getUrl("/tasklist/{username}/user/{offset}/offset"),
																HttpMethod.GET, requestEntity, TaskList.class,
																vars);
		
		// testowanie
        assertNotNull("Empty response - ",responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Do not fetch user task list - ",HttpStatus.OK, responseEntity.getStatusCode());

        
	}

}
