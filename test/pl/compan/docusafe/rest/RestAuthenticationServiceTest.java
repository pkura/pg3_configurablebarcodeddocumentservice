package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.bouncycastle.util.encoders.Base64;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;

import edu.emory.mathcs.backport.java.util.Collections;
import edu.emory.mathcs.backport.java.util.LinkedList;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

public class RestAuthenticationServiceTest {

	public static final Logger log = LoggerFactory.getLogger(RestAuthenticationServiceTest.class);

    public static String url, user, pass;
    public static final String SET_COOKIE="Set-Cookie";
    static String cookie;

    public ObjectMapper mapper = new ObjectMapper();
    private static XLSParamWriter xlsParamWriter;
    
    long beginTime, endTime;

	    
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
        
        xlsParamWriter = new XLSParamWriter();
        try {
    		xlsParamWriter.createColumnHeaders(XLSParamWriter.AUTH_TEST);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
		xlsParamWriter.updateFile(XLSParamWriter.AUTH_TEST);
		xlsParamWriter.autoWidth(XLSParamWriter.AUTH_TEST);
	}

	@Test
	public void testLogin() throws JsonGenerationException, JsonMappingException, IOException {
		xlsParamWriter.setActionName("Test logowania użytkownika");
		log.debug("\tTest logowania użytkownika");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(null, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new LinkedList();
		messageConverters.add(new StringHttpMessageConverter());
		
		template.setMessageConverters(messageConverters);
		String uriLogin = "/login?username={username}&password={password}";
		
		beginTime = (Calendar.getInstance()).getTimeInMillis();
		ResponseEntity<String> loginResponse = template.postForEntity(
                getUrl(uriLogin) , requestEntity, String.class, user, pass);
		endTime = (Calendar.getInstance()).getTimeInMillis(); 
		
		// testowanie
        assertNotNull("Empty response - ",loginResponse);
        assertNull("Must be empty body response  - ",loginResponse.getBody());
        assertEquals("There is no ACCEPTED status code - ",HttpStatus.ACCEPTED, loginResponse.getStatusCode());
              
        cookie = loginResponse.getHeaders().get(SET_COOKIE).iterator().next();
        
//        convertRequestResponseToJSON(requestEntity, loginResponse);
        
        // czas wykkonania
 		long executionTime = endTime - beginTime;
 		xlsParamWriter.setTestOutputData(getUrl("/login?username="+user+"&password="+pass),
 										 requestEntity,loginResponse,HttpMethod.POST,executionTime);
	}
	
	@Test
	public void testLoginCAS() throws JsonGenerationException, JsonMappingException, IOException {
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(null, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		String ticket = TestUtils.getTestProperty("rest.api.proxy.ticket");
		
		RestTemplate template = TestUtils.getRestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new LinkedList();
		messageConverters.add(new StringHttpMessageConverter());
		
		template.setMessageConverters(messageConverters);
		String uriLogin = "/login-cas?t={ticket}";
		
		ResponseEntity<String> loginResponse = template.postForEntity(
                getUrl(uriLogin) , requestEntity, String.class, ticket);
		
		// testowanie
        assertNotNull("Empty response - ",loginResponse);
        assertNull("Must be empty body response  - ",loginResponse.getBody());
        assertEquals("There is no ACCEPTED status code - ",HttpStatus.ACCEPTED, loginResponse.getStatusCode());
              
        cookie = loginResponse.getHeaders().get(SET_COOKIE).iterator().next();

	}

    @Test
    public void testLoginCertificate() throws JsonGenerationException, JsonMappingException, IOException {
        log.debug("\tTest logowania użytkownika");

        File cert = TestUtils.getFileInTestFolder(TestUtils.getTestProperty("rest.api.certificateName"));
        byte[] bytesFromFile = FileUtils.getBytesFromFile(cert);
        String s = org.apache.commons.codec.binary.Base64.encodeBase64String(bytesFromFile);
        KeyElement ke = new KeyElement(null, s);
        // przygotowanie
        HttpHeaders headers = TestUtils.getHeaders(null, MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(mapper.writeValueAsString(ke), headers);

        RestTemplate template = TestUtils.getRestTemplate();
        List<HttpMessageConverter<?>> messageConverters = new LinkedList();
        messageConverters.add(new StringHttpMessageConverter());

        template.setMessageConverters(messageConverters);
        String uriLogin = "/login-certificate";

        ResponseEntity<String> loginResponse = template.postForEntity(getUrl(uriLogin) , requestEntity, String.class);

        // testowanie
        assertNotNull("Empty response - ",loginResponse);
        assertNull("Must be empty body response  - ",loginResponse.getBody());
        assertEquals("There is no ACCEPTED status code - ",HttpStatus.ACCEPTED, loginResponse.getStatusCode());

        cookie = loginResponse.getHeaders().get(SET_COOKIE).iterator().next();

//        convertRequestResponseToJSON(requestEntity, loginResponse);
    }

	@Test
	public void testLogout() throws JsonGenerationException, JsonMappingException, IOException {
		xlsParamWriter.setActionName("Test wylogowywania użytkownika");
		log.debug("\tTest wylogowywania użytkownika");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
		beginTime = (Calendar.getInstance()).getTimeInMillis();
        ResponseEntity<String> logoutResponse = template.postForEntity(getUrl("/logout"), requestEntity, String.class);
		endTime = (Calendar.getInstance()).getTimeInMillis(); 
		
		// testowanie
        assertNotNull("Empty response - ",logoutResponse);
        assertNull("Must be empty body response  - ",logoutResponse.getBody());
        assertEquals("There is no ACCEPTED status code - ",HttpStatus.ACCEPTED, logoutResponse.getStatusCode());
              
//        convertRequestResponseToJSON(requestEntity, loginResponse);
        
        // czas wykkonania
 		long executionTime = endTime - beginTime;
 		xlsParamWriter.setTestOutputData(getUrl("/logout"),
				 requestEntity,logoutResponse,HttpMethod.POST,executionTime);
	}
	
	private void convertRequestResponseToJSON(HttpEntity<?> requestEntity,
			ResponseEntity<?> responseEntity) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		System.out.println("\nRequest\n");
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestEntity.getHeaders())); 
		// if is string we must bind this String to Object
		if (requestEntity.getBody() == null){
			System.out.println("null");
		} else if (requestEntity.getBody().getClass() == String.class){
			String requestString = (String) requestEntity.getBody();
			Object json = mapper.readValue(requestString , Object.class);
			System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json));
		} else {
			System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestEntity.getBody()));
		}
		
		System.out.println("\nResponse\n"); 
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getHeaders()));
		System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getBody()));
			
	}
		
	private static String getUrl(String uri) {
		return url + uri;
	}

	private static HttpHeaders getJsonHeaders(String cookie) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        if(StringUtils.isNotEmpty(cookie)){
            headers.add("Cookie", cookie);
        }
        return headers;

    }	
}
