package pl.compan.docusafe.rest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.*;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.api.user.office.JasperReportDto;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.MarshallerUtils;
import pl.compan.docusafe.util.TestUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 22.07.14
 */
public class RestXmlJasperReportTest  extends RestAbstractRpcTest{

    //cookies zalogowane u�ytkownika
    static String cookie;

    @BeforeClass
    public static void initTest() throws Exception {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);

        cookie = login(MediaType.APPLICATION_XML);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        cookie = null;
    }

    @Test
    public void testGetReportList() throws JAXBException{
    	log.debug("\tTestowanie metody zwracaj�cej wszystkie dost�pne raporty");

        String actionUrl = getUrl("/jasper-report");
        HttpMethod method = HttpMethod.GET;
        // przygotowanie
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);

        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<ListDtos> responseEntity = template.exchange(actionUrl, method, requestEntity, ListDtos.class);

        // testowanie
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        assertNotNull("Brak raport�w!", responseEntity.getBody().getItems().toString());
    }

    @Test
    public void testGetReportType(){
    	log.debug("\tTestowanie metody zwracaj�cej wszystkie dost�pne typy raporty");
    	
        String actionUrl = getUrl("/jasper-report/typntne");
        HttpMethod method = HttpMethod.GET;
        // przygotowanie
        HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
        HttpEntity<?> requestEntity = new HttpEntity<String>(null,headers);

        RestTemplate template = TestUtils.getRestTemplate();

        ResponseEntity<ListDtos> responseEntity = template.exchange(actionUrl, method, requestEntity, ListDtos.class);

        // testowanie
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        List<KeyElement> userDtos = (List<KeyElement>) responseEntity.getBody().getItems();
        assertFalse("Brak typ�w raport�w!", CollectionUtils.isEmpty(userDtos));
    }

    @Test
    public void testAddReport() throws JAXBException{
        log.debug("\tTest dodania nowego raportu");
                
        String actionUrl = getUrl("/jasper-report");
        HttpMethod method = HttpMethod.PUT;
        String userName = TestUtils.getTestProperty("rest.jasper.username");
        String reportType = TestUtils.getTestProperty("rest.api.jasper.type");
        String reportKind = TestUtils.getTestProperty("rest.api.jasper.report");

        try{
            HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML,"text",MediaType.TEXT_HTML);

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, -7);
            JasperReportDto report = new JasperReportDto();
			report.setReportType(reportType);
			report.setUserId(userName);
            report.setStartTime(cal);
            report.setEndTime(Calendar.getInstance());
            report.setReport(reportKind);

            String xmlRequest = MarshallerUtils.marshall(report);

            HttpEntity<String> requestEntity = new HttpEntity<String>(xmlRequest,headers);

            RestTemplate template = TestUtils.getRestTemplate();

            ResponseEntity<String> responseEntity = template.exchange(actionUrl, method, requestEntity, String.class);

            assertNotNull("Pusta odpowiedz - ",responseEntity);
            assertNotNull("Puste body odpowiedzi - ",responseEntity.getBody());
            assertEquals("Stworzono  - ",HttpStatus.OK, responseEntity.getStatusCode());

            byte[] encodedDocument = responseEntity.getBody().getBytes();
            byte[] decodedDocument = Base64.decodeBase64(encodedDocument);

            File tempFile = TestUtils.getFileInTestFolder("jasper-report.xml");
            org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);

            log.info("decoded Document xml {}" + decodedDocument.toString());

        }catch(Exception e){
            log.error("", e);
        }
    }

}
