package pl.compan.docusafe.rest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;

import edu.emory.mathcs.backport.java.util.Collections;
import edu.emory.mathcs.backport.java.util.LinkedList;
import pl.compan.docusafe.api.user.DivisionDto;
import pl.compan.docusafe.api.user.DivisionType;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

public class RestXmlAuthenticationServiceTest extends RestAbstractRpcTest {
    public static String cookie;
        
    @BeforeClass
	public static void initTest() throws Exception {
		BasicConfigurator.configure();
        url = TestUtils.getTestProperty(TestUtils.REST_API_URL);
        user =TestUtils.getTestProperty(TestUtils.REST_API_USER);
        pass= TestUtils.getTestProperty(TestUtils.REST_API_PASS);
       
	}

	@AfterClass
	public static void afterTest() throws Exception {
		cookie = null; 
	}

	@Test
	public void testLogin() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest logowania użytkownika");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(null, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
		String uriLogin = "/login?username={username}&password={password}";
		
		
		ResponseEntity<String> loginResponse = template.postForEntity(
                getUrl(uriLogin) , requestEntity, String.class, user, pass);
		
		
		// testowanie
        assertNotNull("Empty response - ",loginResponse);
        assertNull("Must be empty body response  - ",loginResponse.getBody());
        assertEquals("There is no ACCEPTED status code - ",HttpStatus.ACCEPTED, loginResponse.getStatusCode());
              
        cookie = loginResponse.getHeaders().get(SET_COOKIE).iterator().next();
        
        
	}

	@Test
	public void testLogout() throws JsonGenerationException, JsonMappingException, IOException {
		log.debug("\tTest wylogowywania użytkownika");
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(cookie, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		RestTemplate template = TestUtils.getRestTemplate();
		
        ResponseEntity<String> logoutResponse = template.postForEntity(getUrl("/logout"), requestEntity, String.class);

		
		// testowanie
        assertNotNull("Empty response - ",logoutResponse);
        assertNull("Must be empty body response  - ",logoutResponse.getBody());
        assertEquals("There is no ACCEPTED status code - ",HttpStatus.ACCEPTED, logoutResponse.getStatusCode());
              
	}
	
	@Test
	public void testLoginCAS() throws IOException {
		
		// przygotowanie
		HttpHeaders headers = TestUtils.getHeaders(null, MediaType.APPLICATION_XML);
		HttpEntity<String> requestEntity = new HttpEntity<String>(null,headers);
		
		String ticket = TestUtils.getTestProperty("rest.api.proxy.ticket");
		
		RestTemplate template = TestUtils.getRestTemplate();

		String uriLogin = "/login-cas?t={ticket}";
		
		ResponseEntity<String> loginResponse = template.postForEntity(
                getUrl(uriLogin) , requestEntity, String.class, ticket);
		
		// testowanie
        assertNotNull("Empty response - ",loginResponse);
        assertNull("Must be empty body response  - ",loginResponse.getBody());
        assertEquals("There is no ACCEPTED status code - ",HttpStatus.ACCEPTED, loginResponse.getStatusCode());
              
        cookie = loginResponse.getHeaders().get(SET_COOKIE).iterator().next();

	}


}
