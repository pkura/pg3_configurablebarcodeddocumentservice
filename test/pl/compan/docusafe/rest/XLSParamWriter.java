package pl.compan.docusafe.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.axis2.json.JSONMessageFormatter;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.json.JSONUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.util.TestUtils;

public class XLSParamWriter {

    public static final Logger log = LoggerFactory.getLogger(RestUserServiceTest.class);

	public static final int USER_TEST = 0;
	public static final int SUBSTITUTION_TEST = 1;
	public static final int OFFICE_CASE_TEST = 2;
	public static final int DOCUMENT_TEST = 3;
	public static final int DIVISION_TEST = 4;
	public static final int AUTH_TEST = 5;
	public static final int TASK_LIST_TEST = 6;
	public static final int SMART_SEARCH_TEST = 7;
	public static final int XES_TEST = 8;

	String fileName = TestUtils.getTestProperty(TestUtils.REST_TEST_PATH) + "\\testy.xls";
	
	private FileOutputStream out;
	private FileInputStream in;
	
	private HSSFWorkbook workBook;
	
	private int rowCount = 1;
	
	private String tlp;
	private String actionName;
	private String url;
	private String paramReq;
	private String paramResp;
	private String httpMethod;
	private String time;

	public XLSParamWriter() {
		workBook = new HSSFWorkbook();
	}
	
	public void createFile() throws IOException
	{
		File fileTestsPG = new File(fileName);				
		if(!fileTestsPG.exists()){
			out = new FileOutputStream(fileTestsPG);
					
			HSSFSheet sheetUser = workBook.createSheet("Test rest u�ytkownika");
			HSSFSheet sheetSubstitution = workBook.createSheet("Test rest zast�pstwa");
			HSSFSheet sheetCase = workBook.createSheet("Test rest spraw");
			HSSFSheet sheetDocument = workBook.createSheet("Test rest dokumentu");
			HSSFSheet sheetDivision = workBook.createSheet("Test rest dzia��w");
			HSSFSheet sheetAuth = workBook.createSheet("Test rest logowania");
			HSSFSheet sheetTaskList = workBook.createSheet("Test rest listy zada�");
			HSSFSheet sheetSmartSearch = workBook.createSheet("Test rest smart search");
			HSSFSheet sheetXes = workBook.createSheet("Test rest XesLog");
			
			HSSFFont font = workBook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle style = workBook.createCellStyle();
			
			style.setFont(font);
	
			Row row = sheetUser.createRow(0);
			Row rowSub = sheetSubstitution.createRow(0);
			Row rowCas = sheetCase.createRow(0);
			Row rowDoc = sheetDocument.createRow(0);
			Row rowDiv = sheetDivision.createRow(0);
			Row rowAut = sheetAuth.createRow(0);
			Row rowTaskList = sheetTaskList.createRow(0);
			Row rowSmartSearch = sheetSmartSearch.createRow(0);
			Row rowXes = sheetXes.createRow(0);
				
			
			int i=0;
			
			createSheetCell(sheetUser, style, row, i);
			createSheetCell(sheetSubstitution, style, rowSub, i);
			createSheetCell(sheetCase, style, rowCas, i);
			createSheetCell(sheetDocument, style, rowDoc, i);
			createSheetCell(sheetDivision, style, rowDiv, i);
			createSheetCell(sheetAuth, style, rowAut, i);
			createSheetCell(sheetTaskList, style, rowTaskList, i);
			createSheetCell(sheetSmartSearch, style, rowSmartSearch, i);
			createSheetCell(sheetXes, style, rowXes, i);
			
			workBook.write(out);
			out.close();
		} else {
			FileInputStream inputStream = new FileInputStream(fileName);
			InputStream input = new BufferedInputStream(inputStream);
			POIFSFileSystem fs = new POIFSFileSystem(input);
			workBook = new HSSFWorkbook(fs);
		}
	}

	private void createSheetCell(HSSFSheet sheetUser, HSSFCellStyle style,Row row, int i) {
		Cell cell;		
		cell = row.createCell(i);
		cell.setCellValue(tlp);
		cell.setCellStyle(style);
		i++;
		
		cell = row.createCell(i);		
		cell.setCellValue(actionName);
		cell.setCellStyle(style);
		i++;
		
		cell = row.createCell(i);
		cell.setCellValue(url);
		cell.setCellStyle(style);
		i++;
		
		cell = row.createCell(i);
		cell.setCellValue(paramReq);
		cell.setCellStyle(style);
		i++;
		
		cell = row.createCell(i);
		cell.setCellValue(paramResp);
		cell.setCellStyle(style);
		i++;
		
		cell = row.createCell(i);
		cell.setCellValue(httpMethod);
		cell.setCellStyle(style);
		i++;
		
		cell = row.createCell(i);
		cell.setCellValue(time);
		cell.setCellStyle(style);
						
		for(i=1;i<150;i++)
		{
			row = sheetUser.createRow(i);
			
			for(int j=0; j<7; j++)
			{
				cell = row.createCell(j);			
			}
		}
	}
		
	public void updateFile(int indexSheet) throws IOException {

		in = new FileInputStream(new File(fileName));
        System.out.println("row count " + rowCount + " index " + indexSheet);
        HSSFSheet sheet = workBook.getSheetAt(indexSheet);
						
		HSSFCellStyle wrap = workBook.createCellStyle();
		wrap.setWrapText(true);
		
		Cell cell;
		
		cell = sheet.getRow(rowCount).getCell(0);
		cell.setCellValue(rowCount);
				
		cell = sheet.getRow(rowCount).getCell(1);
		cell.setCellValue(actionName);
			
		cell = sheet.getRow(rowCount).getCell(2);
		cell.setCellValue(url);
				
		cell = sheet.getRow(rowCount).getCell(3);
		cell.setCellValue(paramReq);
		cell.setCellStyle(wrap);
		
		cell = sheet.getRow(rowCount).getCell(4);
		cell.setCellValue(paramResp);
		cell.setCellStyle(wrap);
				
		cell = sheet.getRow(rowCount).getCell(5);
		cell.setCellValue(httpMethod);
				
		cell = sheet.getRow(rowCount).getCell(6);
		cell.setCellValue(time);
		
		in.close();
		
		out = new FileOutputStream(new File(fileName));
		
		workBook.write(out);
		out.close();
		autoWidth(indexSheet); 
		
		rowCount++;	
	    
	}

	public void autoWidth(int indexSheet) throws IOException {
		
		in = new FileInputStream(new File(fileName));

		HSSFSheet sheet = workBook.getSheetAt(indexSheet);
		for(int i=0; i<7;i++)
		{
			sheet.autoSizeColumn(i);
		}
		
		in.close();
		
		out = new FileOutputStream(new File(fileName));
		
		workBook.write(out);
		out.close();
	}	

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public void setTlp(String tlp) {
		this.tlp = tlp;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setParamReq(String paramReq) {
		this.paramReq = paramReq;
	}

	public void setParamResp(String paramResp) {
		this.paramResp = paramResp;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void createColumnHeaders(int indexSheet) throws IOException, InvalidFormatException {
		
		setTlp("Lp.");
		setActionName("Wykonywana czynno��");
		setUrl("Url");
		setParamReq("Request");
		setParamResp("Response�");
		setHttpMethod("Metoda http");
		setTime("Czas wykonywnia");
		createFile();
		
		autoWidth(indexSheet);
	}

    /**
     *
     * @param url2
     * @param requestEntity
     * @param  obiekt jaki otrzymali�ym po wykonaniu requestu
     * @param httpMethod
     * @param executionTime
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
	public void setTestOutputData(String url2,
			HttpEntity<?> requestEntity,
			ResponseEntity<?> responseEntity, HttpMethod httpMethod,
			long executionTime) throws JsonGenerationException, JsonMappingException, IOException {
		
		setUrl(url2);
		setRequest(requestEntity);
		setResponse(responseEntity);
		setHttpMethod(httpMethod.toString());
		setTime(executionTime);
		
		
	}

    /**
     *
     * @param url2
     * @param requestEntity
     * @param responseEntity komunikat b��du
     * @param httpMethod
     * @param executionTime
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public void setTestOutputData(String url2,
                                  HttpEntity<?> requestEntity,
                                  String responseEntity, HttpMethod httpMethod,
                                  long executionTime) throws JsonGenerationException, JsonMappingException, IOException {

        setUrl(url2);
        setRequest(requestEntity);
        setParamResp(responseEntity);
        setHttpMethod(httpMethod.toString());
        setTime(executionTime);


    }


    public void setResponse(ResponseEntity<?> responseEntity) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
        String headers = mapper.writeValueAsString(responseEntity.getHeaders());
        String response = mapper.writeValueAsString(responseEntity.getBody());
		String responseString = headers + "\n\n" + response;
		setParamResp(responseString);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        log.info("Nag��wki {}", responseString);

	}

    public void setRequest(HttpEntity<?> requestEntity) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String requestString = mapper.writeValueAsString(requestEntity.getHeaders()) + "\n\n";
		if (requestEntity.getBody() == null){
			requestString = requestString + "null";
		} else if (requestEntity.getBody().getClass() == String.class){
			String requestBodyString = (String) requestEntity.getBody();
			Object json = mapper.readValue(requestBodyString , Object.class);
			requestString = requestString + mapper.writeValueAsString(json);
		} else {
			requestString = requestString + mapper.writeValueAsString(requestEntity.getBody());
		}
		setParamReq(requestString);
	}

	public void setTime(long executionTime) {
		setTime(Long.toString(executionTime) + " ms");
	}
	
	
}
