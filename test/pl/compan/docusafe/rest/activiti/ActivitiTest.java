package pl.compan.docusafe.rest.activiti;

import com.sdicons.json.mapper.JSONMapper;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.office.ListDtos;
import pl.compan.docusafe.rest.rpc.RestAbstractRpcTest;
import pl.compan.docusafe.util.TestUtils;

import javax.imageio.ImageIO;
import javax.xml.transform.Source;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class ActivitiTest extends RestAbstractRpcTest {

    @BeforeClass
    public static void initTest() throws Exception {
        BasicConfigurator.configure();
        url = TestUtils.getTestProperty("activiti.base.url");
    }

    @AfterClass
    public static void afterTest() throws Exception {
    }

    @Test
    public void testDiagrams() throws IOException {
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,getHeaders());
        RestTemplate template = TestUtils.getRestTemplate();
        List<HttpMessageConverter<?>> messageConverters = template.getMessageConverters();
        messageConverters.add(new BufferedImageHttpMessageConverter());
        String param = TestUtils.getTestProperty("rest.document.process.instance.id");
        
		ResponseEntity<BufferedImage> responseEntity = template.exchange(getUrl("/process-instance/"+ param +"/diagram"), HttpMethod.GET, requestEntity, BufferedImage.class,param );

        File f = new File( TestUtils.getTestProperty("rest.test.path") + "/picture.png");
        ImageIO.write(responseEntity.getBody(),"png",f);

        assertNotNull("Empty response - ", responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());

    }

    @Test
    public void testGetProcessDefinitions() throws Exception{
        //kermit:kermit@localhost:8081/activiti-rest/service/repository/process-definitions?key=zadanie
        String processName = TestUtils.getTestProperty("rest.document.processName");
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,getHeaders());
        RestTemplate template = TestUtils.getRestTemplate();
        List<HttpMessageConverter<?>> messageConverters = template.getMessageConverters();
        messageConverters.add(new BufferedImageHttpMessageConverter());
        ResponseEntity<String> responseEntity = template.exchange(getUrl("repository/process-definitions?key=" + processName), HttpMethod.GET, requestEntity, String.class);

        String s = responseEntity.getBody();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(s);
        JsonNode data = node.get("data");
        JsonNode resource = data.get(0).get("resource");
        String textValue = resource.getTextValue();

        String s1 = textValue.replaceFirst("resources", "resourcedata");

        ResponseEntity<String> exchange = template.exchange(s1, HttpMethod.GET, requestEntity, String.class);

        log.error("textValue = " + textValue);
        log.error("xl =" + exchange.getBody());
        assertNotNull("Empty response - ", responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testProcessDefinitions() throws IOException {
        HttpEntity<String> requestEntity = new HttpEntity<String>(null,getHeaders());
        RestTemplate template = TestUtils.getRestTemplate();
        List<HttpMessageConverter<?>> messageConverters = template.getMessageConverters();
        messageConverters.add(new BufferedImageHttpMessageConverter());
        ResponseEntity<String> responseEntity = template.exchange(getUrl("/repository/process-definitions"), HttpMethod.GET, requestEntity, String.class);

        assertNotNull("Empty response - ", responseEntity);
        assertNotNull("Empty body response  - ",responseEntity.getBody());
        assertEquals("Wrong status code - ",HttpStatus.OK, responseEntity.getStatusCode());

    }

    public static HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
       // headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return headers;
    }


}
