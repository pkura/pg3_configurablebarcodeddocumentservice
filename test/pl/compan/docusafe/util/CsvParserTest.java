package pl.compan.docusafe.util;

import junit.framework.TestCase;

import java.util.Arrays;

/**
 * @author Micha� Sankowski <michal.sankowski@docusafe.pl>
 */
public class CsvParserTest extends TestCase{

    private static void test(String line, String... separated){
        String[] result = CsvParser.parseLine(line);
        assertEquals("results not same size " + Arrays.toString(result) + " vs " + Arrays.toString(separated), result.length, separated.length);
        for(int i = 0; i < result.length; ++i){
            assertEquals("Parts " + i+ " not the same ", separated[i], result[i]);
        }
    }

    public void testSimpleSingle(){
        test("test","test");
        test("test1","test1");
    }

    public void testEscapeCharSingle(){
        test("test\\\\","test\\");
    }

    public void testQuotedSingle(){
        test("\"test\"","test");
        test("\"123\"","123");
    }

    public void testEscapedSeparatorSingle(){
        test("123\\;123","123;123");
        test("123\\\"123","123\"123"); //left=escaped "
    }

    public void testDoubleQuoteSingle(){
        test("\"\"\"\"","\"");// """" -> "
        test("\"\"\"hello\"\"\"","\"hello\""); //"""hello"""   -> "hello"
    }

    public void testSimpleMulti(){
        test("123;456","123","456");
        test("321;321;123","321","321","123");
    }

    public void testMultiEscapedSeparator(){
        test("123\\;123;123","123;123","123");
        test("123\\\"123;123","123\"123","123");
    }

    public void testQuotedSeparatorMulti(){
        test("\"123 456\";789","123 456","789");
        test("\"123;456\";789","123;456","789");
    }

    public void testDoubleQuoteMulti(){
        test("\"\"\"hello\"\"\";hello","\"hello\"","hello"); //"""hello"""   -> "hello"
        test("\"\"\"hello\"\";helo\"","\"hello\";helo");
    }
}
