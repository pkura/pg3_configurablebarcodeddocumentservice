package pl.compan.docusafe.util;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.RestSubstitutionServiceTest;
import pl.compan.docusafe.rest.XLSParamWriter;

import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl> 24.07.14
 */
public class DateUtilsTest {

    public static final Logger log = LoggerFactory.getLogger(DateUtilsTest.class);

    @BeforeClass
    public static void initTest() throws Exception {
        BasicConfigurator.configure();
    }

    @Test
    public void testValidateFutureRange() throws EdmException {
        Date from = DateUtils.parseJsDate("23-07-2014");
        Date to = DateUtils.parseJsDate("22-07-2014");

        assertTrue("B��dny zakres", DateUtils.validateFutureRange(from, to));
    }

    @Test
    public void testValidateFutureRangeCross() throws EdmException{
        Date from = DateUtils.parseJsDate("28-07-2014");
        Date to = DateUtils.parseJsDate("25-07-2014");

        boolean isOk = false;
        try{
            isOk = DateUtils.validateFutureRange(from, to);

        } catch (Exception e){
            log.info("walidacja poprawna ->{}", e.getMessage());
        }

        assertFalse("walidacja przesz�a a nie powinna, cross data", isOk);
    }
}
