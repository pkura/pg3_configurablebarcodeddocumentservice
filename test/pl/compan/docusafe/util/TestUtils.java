package pl.compan.docusafe.util;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.commons.codec.binary.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import pl.compan.docusafe.api.user.office.FieldView;
import pl.compan.docusafe.rest.RestUserServiceTest;
import pl.compan.docusafe.rest.XLSParamWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

/**
 *  Klasa pomocnicza do wyszustkich test�w
 */
public class TestUtils {


    public static final Logger log = LoggerFactory.getLogger(TestUtils.class);
    /**
     * Plik z parametrami
     */
    public static final String LOG4J_FILE = "log4j.properties.file";
    public static final String TEST_FILE = "test.properties";

    public static final String REST_API_URL = "rest.api.url";
    public static final String REST_API_USER="rest.api.user";
    public static final String REST_API_PASS="rest.api.pass";
    public static final String CMIS_API_URL = "cmis.api.url";
    public static final String CMIS_API_USER="cmis.api.user";
    public static final String CMIS_API_PASS="cmis.api.pass";
	public static final String REST_API_USER_CREATE = "rest.api.user.create";
    public static final String REST_API_USER_GET = "rest.api.user.get";
    public static final String REST_API_USER_CREATE_PNUMBER = "rest.api.user.create.personNumber";
	public static final String REST_API_DIVISION_CREATE = "rest.api.division.create";
	public static final String REST_API_DICTIONARY_CREATE = "rest.api.dictionary.create";
    public static final String CMIS_API_CREATE_FOLDER_NAME = "cmis.api.create.folder.name";
    public static final String REST_DOCUMENT_AS_XML_ID = "rest.document.asxml.id";
    public static final String REST_TEST_PATH = "rest.test.path";
    public static final String REST_DOCUMENT_FINDBYBARCODE = "rest.document.findByBarcode";
    public static final String REST_DOCUMENT_FINDBYBARCODE_NO_DOC = "rest.document.findByBarcode.noDocument";
    public static final String REST_GET_DOCUMENT_ID_DICTIONARY = "rest.get.documentId.dictionary" ;
    public static final String REST_GET_DOCUMENT_DICTIONARY_CN = "rest.get.document.dictionaryCn";
    public static final String REST_GET_DOCUMENT_DICTIONARY_ID = "rest.get.document.dictionaryId" ;

    public static final String REST_FULLTEXTSEARCH_TEXT = "rest.fulltextsearch.text";
    public static final String REST_DOCUMENT_SEARCH_FIELDS = "rest.document.search.fields";

    public static final String REST_DOCUMENT_DICTIONARY_UPDATE = "rest.document.dictionary.update";
    public static final String REST_DOCUMENT_DICTIONARY_ADD = "rest.document.dictionary.add";
    public static final String REST_DOCUMENT_DICTIONARY_SEARCH = "rest.document.dictionary.search";
    public static final String REST_DOCUMENT_DICTIONARY_SEARCH_CN = "rest.document.dictionary.search.cn";
    public static final String REST_DOCUMENT_DICTIONARY_SEARCH_DOCKIND_CN = "rest.document.dictionary.search.dockindCn";

    static{
        try {
            loadProperties();
           // System.getProperties().list(System.out);
            loadLog4jFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadProperties() throws IOException {
        FileInputStream propFile = new FileInputStream( System.getProperty(TEST_FILE) );
        Properties props = System.getProperties();
        props.load(propFile);
        System.setProperties(props);
    }


    public static String getTestProperty(String s){
        return System.getProperty(s);
    }

    /**
     * <p>
     *     Zwraca property jako kolekcj�, dziel�c po przecinku.
     *     Np. je�li mamy addsa "superLista=abc,a,b,c"
     *     to <code>getPropertyAsList("superLista")</code>
     *     zwr�ci list� <code>["abc", "a", "b", "c"]</code>.
     * </p>
     *
     * <p>
     *     Je�li adds nie istnieje lub jest pusty,
     *     wtedy zwracana jest pusta kolekcja.
     * </p>
     *
     * @author Jan �wi�cki <a href="jan.swiecki@docusafe.pl">jan.swiecki@docusafe.pl</a>
     * @param name
     * @return lista string�w z addsa <code>name</code> (dzielone po przecinku)
     */
    public static List<String> getTestPropertyAsList(String name) {
        List<String> list = new ArrayList<String>();

        String str = getTestProperty(name);

        if(str == null) {
            return list;
        } else {
            final String[] split = org.apache.commons.lang.StringUtils.split(str, ",");
            return new ArrayList<String>(Collections2.transform(Arrays.asList(split), new Function<String, String>() {
                @Override
                public String apply(String s) {
                    return s.trim();
                }
            }));
        }
    }

    public static Map<String, String> getTestPropertyAsMap(String name) {
        Map<String, String> map = new HashMap<String, String>();

        Collection<String> list = getTestPropertyAsList(name);

        for(String s: list) {
            String[] split = s.split(":");
            map.put(split[0], split[1]);
        }

        return map;
    }

    public static List<FieldView> getTestFieldView(String name){
        Map<String, String> props = getTestPropertyAsMap(name);
        List<FieldView> fvs = Lists.newArrayList();

        for(Map.Entry<String, String> entry : props.entrySet()){
            fvs.add(new FieldView(entry.getKey(), entry.getValue()));
        }

        return fvs;
    }

    public static void loadLog4jFile(){
        String s = getTestProperty(LOG4J_FILE);
        PropertyConfigurator.configure(s);
    }

    /**
     * Zwraca nag��wek do zapyta� RestTemplate
     * @param cookie
     * @param mediaType
     * @return
     */
    public static HttpHeaders getHeaders(String cookie, MediaType mediaType){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType(mediaType.getType(), mediaType.getSubtype()));
        headers.setAccept(Arrays.asList(new MediaType(mediaType.getType(), mediaType.getSubtype())));

        if(StringUtils.isNotEmpty(cookie)){
            headers.add("cookie", cookie);
        }
        try {
            String base64 = getCertificateFileBase64();
            headers.add("Certificate-ssl",base64);
        } catch (IOException e) {
            log.error("{}", e.getMessage());
        }
        return headers;
    }

    public static String getCertificateFileBase64() throws IOException {
        return FileUtils.encodeByBase64(new File(TestUtils.getTestProperty("rest.test.path") + "/compan.crt"));
    }

    /**
     * Zwraca nag��wek do zapyta� RestTemplate
     * @param cookie
     * @param mediaType
     * @return
     */
    public static HttpHeaders getHeaders(String cookie, MediaType mediaType, String requestMediaType, MediaType accept) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("requestedMediaType", requestMediaType);
        headers.setContentType(new MediaType(mediaType.getType(), mediaType.getSubtype()));
        headers.setAccept(Arrays.asList(new MediaType(accept.getType(), accept.getSubtype())));

        if(StringUtils.isNotEmpty(cookie)){
            headers.add("cookie", cookie);
        }
        try {
            String base64 = getCertificateFileBase64();
            headers.add("Certificate-ssl", base64);
        } catch (IOException e) {
            log.error("{}", e.getMessage());
        }

        return headers;
    }

    /**
     * Tworzy plik w katalogu docelowym wynik�w test�w
     * @param fileName
     * @return
     */
    public static File getFileInTestFolder(String fileName){
        return new File(TestUtils.getTestProperty(TestUtils.REST_TEST_PATH) + "/"+ fileName);
    }

    public static RestTemplate getRestTemplate() {
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory());
    }

    public static BindingType getCmisBindingType() {
        return BindingType.fromValue(TestUtils.getTestProperty("cmis.bindingType"));
    }

    public static void printResponseJSon(ResponseEntity<?> responseEntity) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String headers = mapper.writeValueAsString(responseEntity.getHeaders());
        String response = mapper.writeValueAsString(responseEntity.getBody());
        String responseString = headers + "\n\n" + response;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        log.info("Nag��wki: {}", prettyPrint(headers));
        log.info("odpowiedz {}", prettyPrint(response));
    }

    public static String prettyPrint(String jsonString){
        JsonParser parser = new JsonParser();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonElement el = parser.parse(jsonString);
        return gson.toJson(el);
    }
    
    /**
     * 
     * Method returns string of ids connected with comma 
     * 
     * @param docIds String np. 13,69
     * @return
     */
    public static String convertToSimpleString(List<String> docIds) {
		String stringDocIds="";
		int size = docIds.size();
		for (String id : docIds) {
			if (--size == 0) {
				stringDocIds += id;
		    } else {
		    	stringDocIds += id + ",";
		    }
		}
		return stringDocIds;
	}

    public static void saveToFile(ResponseEntity responseEntity, String fileName) throws IOException {
        byte[] encodedDocument = ((String)responseEntity.getBody()).getBytes();
        byte[] decodedDocument = org.apache.commons.codec.binary.Base64.decodeBase64(encodedDocument);

        File tempFile = TestUtils.getFileInTestFolder(fileName);
        org.apache.commons.io.FileUtils.writeByteArrayToFile(tempFile, decodedDocument);
    }


	public static Calendar getTestPropertyGetDataTime(String stringDataTime) {
		stringDataTime = TestUtils.getTestProperty(stringDataTime);
		String[] tableOfDataTime = stringDataTime.split("/");
		
		Calendar calendar = Calendar.getInstance();
		int year = Integer.parseInt(tableOfDataTime[0]);
		int month = Integer.parseInt(tableOfDataTime[1]);
		month--;
		int day = Integer.parseInt(tableOfDataTime[2]);
		calendar.set(year,month,day);
		
		return calendar;
	}
}
