package pl.compan.docusafe.util;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import static org.junit.Assert.*;
import pl.compan.docusafe.core.dockinds.field.Field;

/**
 * �ukasz Wo�niak <lukasz.wozniak@docusafe.pl>
 */
public class StringTest {

    @Test
    public void testMultipleUnderscoreFieldCutter(){
        String c = StringUtils.substringAfterLast("USER_ID", Field.MULTI_FIELD_SEPARATOR);
        System.out.println(StringUtils.isNumeric(c));
        assertFalse("powinno by� false", StringUtils.isNumeric(c));
    }
}
