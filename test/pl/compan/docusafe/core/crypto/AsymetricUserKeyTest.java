package pl.compan.docusafe.core.crypto;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import junit.framework.TestCase;
import pl.compan.docusafe.core.crypto.CryptographicFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.codec.binary.Base64;

public class AsymetricUserKeyTest extends TestCase 
{
	CryptographicFactory  factory = new CryptographicFactory ();
	@Override
	public void setUp()
	{
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());				
	}
	public void testAsymetricKeys() throws Exception
	{	
		String secret = "alamakota";
		IndividualRepositoryKey repositoryKey =  new IndividualRepositoryKey();
		repositoryKey.setSecret(secret);
		repositoryKey.createKey();
		String symetricKey = "symetric";
		repositoryKey.importKey(symetricKey.getBytes());
	} 

	
}
