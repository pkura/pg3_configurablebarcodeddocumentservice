package pl.compan.docusafe.core.crypto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import junit.framework.TestCase;

import org.apache.commons.io.IOUtils;

public class CryptoTest extends TestCase 
{
	CryptographicFactory  factory = new CryptographicFactory ();
	@Override
	public void setUp()
	{
		//Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());				
	}

	public void testProcessStream() throws Exception
	{
		
		String data = "przykladowe dane do testu";
		//Security.addProvider(new BouncyCastleProvider());

		
		//KeyGenerator keyGen = KeyGenerator.getInstance("AES", "BC");
		//keyGen.init(new SecureRandom());
		
		//Key key = keyGen.generateKey();
		byte[] key = "twojastararazdwa".getBytes();
		//byte[] result = new byte[2048];
		InputStream encrypted = factory.processStream(new ByteArrayInputStream(data.getBytes()), key, true, key.length);
		InputStream desncrypted = factory.processStream(encrypted, key, false, key.length);		
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		IOUtils.copy(desncrypted, os);		
		byte[] resultingBytes = os.toByteArray();
		//assertTrue(Arrays.equals(data.getBytes(), resultingBytes));
		//System.out.println("Dlugosc" + resultingBytes.length + "|" + data.getBytes().length);
		//for(int i = 0; i<resultingBytes.length; i++)
		//for(int i = 0; i<data.getBytes().length; i++)
		//{
			//System.out.println( resultingBytes[i]);
		//}
		//ystem.out.println(IOUtils.toString(resultingBytes));
		//boolean ass = Arrays.areEqual(resultingBytes, data.getBytes());
		
		
	}
	
	
	public void testProcessFileStream() throws Exception
	{
		

		File file = new File("C:\\Users\\Kuba\\Documents\\Downloads\\bledy.jpg");
		FileInputStream fis = new FileInputStream(file);
		
		byte[] key = "twojastararazdwa".getBytes();
		InputStream encrypted = factory.processStream(fis, key, true, key.length);
		InputStream desncrypted = factory.processStream(encrypted, key, false, key.length);		
		
		File result = new File("C:\\Users\\Kuba\\Documents\\Downloads\\ODSZYFROWANYbledy.jpg");
		FileOutputStream fos = new FileOutputStream(result);
		IOUtils.copy(desncrypted, fos);		
		fos.close();
		fis.close();
		
		
	}
}
