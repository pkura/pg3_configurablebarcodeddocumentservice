package pl.compan.docusafe.core.dockinds.logic;

import pl.compan.docusafe.parametrization.ald.ALDLogic;
import pl.compan.docusafe.parametrization.ic.InvoiceICLogic;
import pl.compan.docusafe.parametrization.ic.SadLogic;
import pl.compan.docusafe.parametrization.prosika.Prosikalogic;
import pl.compan.docusafe.parametrization.ra.RockwellLogic;
import junit.framework.TestCase;

public class PermissionFilteringTest extends TestCase
{
	public void testProsika()
	{
		Prosikalogic prosikaLogic = new Prosikalogic();
		assertTrue(prosikaLogic.isReadPermissionCode("PROSIKA_COK_READ"));
		assertFalse(prosikaLogic.isReadPermissionCode("PROSIKA_COK_MODIFY"));
		assertFalse(prosikaLogic.isReadPermissionCode("FOO"));

		ALDLogic aldLogic = new ALDLogic();
		assertFalse(aldLogic.isReadPermissionCode("COKOLWIEK"));
		assertFalse(aldLogic.isReadPermissionCode("NW_SWIADCZENIOWE"));
		assertFalse(aldLogic.isReadPermissionCode("PROSIKA_COK_MODIFY"));

		RockwellLogic raLogic = new RockwellLogic();
		assertTrue(raLogic.isReadPermissionCode("BUSINESS_UNIT_S30KA_MODIFY"));
		assertFalse(raLogic.isReadPermissionCode("NW_SWIADCZENIOWE"));
		assertTrue(raLogic.isReadPermissionCode("ROCKWELL_READ"));
	}

	public void testInvoice(){
		DocumentLogic icLogic = new InvoiceICLogic();
		assertTrue(icLogic.isReadPermissionCode("INVOICE_READ"));
		assertFalse(icLogic.isReadPermissionCode("INOICE_READ"));
		assertTrue(icLogic.isReadPermissionCode("INVOICE_BLABLA_READ"));

		icLogic = new SadLogic();
		assertTrue(icLogic.isReadPermissionCode("SAD_READ"));
		assertTrue(icLogic.isReadPermissionCode("SAD_XXXXX_READ"));
		assertFalse(icLogic.isReadPermissionCode("INOICE_READ"));
		assertTrue(icLogic.isReadPermissionCode("INVOICE_BLABLA_READ"));

		InvoiceLogic iLogic = new InvoiceLogic();
		assertTrue(iLogic.isReadPermissionCode("INVOICE_READ"));
		assertFalse(iLogic.isReadPermissionCode("INOICE_READ"));
		assertTrue(iLogic.isReadPermissionCode("INVOICE_BLABLA_READ"));
	}

	public void testDpPte(){
		DpPteLogic logic = new DpPteLogic();
		assertTrue(logic.isReadPermissionCode("DP_PTE_READ"));
		assertFalse(logic.isReadPermissionCode("INOICE_READ"));
		assertTrue(logic.isReadPermissionCode("DP_PTE_XXX_READ"));
	}
}
