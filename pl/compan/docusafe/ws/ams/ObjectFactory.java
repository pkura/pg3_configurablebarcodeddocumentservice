//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.12.19 at 04:40:27 PM CET 
//


package pl.compan.docusafe.ws.ams;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pl.compan.docusafe.ws.ams package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ZamdostGenDokzak_QNAME = new QName("http://www.simple.com.pl/SimpleErp", "zamdost_gen_dokzak");
    private final static QName _IDNIDCTYPEId_QNAME = new QName("", "id");
    private final static QName _IDNIDCTYPEIdn_QNAME = new QName("", "idn");
    private final static QName _IDMIDCTYPEIdm_QNAME = new QName("", "idm");
    private final static QName _SYMBOLWALUTYIDCTYPESymbolwaluty_QNAME = new QName("", "symbolwaluty");
    private final static QName _IDSIDCTYPEIds_QNAME = new QName("", "ids");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pl.compan.docusafe.ws.ams
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZamdostGenDokzakTYPE }
     * 
     */
    public ZamdostGenDokzakTYPE createZamdostGenDokzakTYPE() {
        return new ZamdostGenDokzakTYPE();
    }

    /**
     * Create an instance of {@link IDMIDCTYPE }
     * 
     */
    public IDMIDCTYPE createIDMIDCTYPE() {
        return new IDMIDCTYPE();
    }

    /**
     * Create an instance of {@link ZamdostpozTYPE }
     * 
     */
    public ZamdostpozTYPE createZamdostpozTYPE() {
        return new ZamdostpozTYPE();
    }

    /**
     * Create an instance of {@link IDSIDTYPE }
     * 
     */
    public IDSIDTYPE createIDSIDTYPE() {
        return new IDSIDTYPE();
    }

    /**
     * Create an instance of {@link SYMBOLWALUTYIDTYPE }
     * 
     */
    public SYMBOLWALUTYIDTYPE createSYMBOLWALUTYIDTYPE() {
        return new SYMBOLWALUTYIDTYPE();
    }

    /**
     * Create an instance of {@link ZamdostpozycjeTYPE }
     * 
     */
    public ZamdostpozycjeTYPE createZamdostpozycjeTYPE() {
        return new ZamdostpozycjeTYPE();
    }

    /**
     * Create an instance of {@link RepWartoscTYPE }
     * 
     */
    public RepWartoscTYPE createRepWartoscTYPE() {
        return new RepWartoscTYPE();
    }

    /**
     * Create an instance of {@link IDMIDTYPE }
     * 
     */
    public IDMIDTYPE createIDMIDTYPE() {
        return new IDMIDTYPE();
    }

    /**
     * Create an instance of {@link SysDokZalacznikTYPE }
     * 
     */
    public SysDokZalacznikTYPE createSysDokZalacznikTYPE() {
        return new SysDokZalacznikTYPE();
    }

    /**
     * Create an instance of {@link SysDokZalacznikiTYPE }
     * 
     */
    public SysDokZalacznikiTYPE createSysDokZalacznikiTYPE() {
        return new SysDokZalacznikiTYPE();
    }

    /**
     * Create an instance of {@link ZamdostiTYPE }
     * 
     */
    public ZamdostiTYPE createZamdostiTYPE() {
        return new ZamdostiTYPE();
    }

    /**
     * Create an instance of {@link SYMBOLWALUTYIDCTYPE }
     * 
     */
    public SYMBOLWALUTYIDCTYPE createSYMBOLWALUTYIDCTYPE() {
        return new SYMBOLWALUTYIDCTYPE();
    }

    /**
     * Create an instance of {@link IDNIDCTYPE }
     * 
     */
    public IDNIDCTYPE createIDNIDCTYPE() {
        return new IDNIDCTYPE();
    }

    /**
     * Create an instance of {@link IDSIDCTYPE }
     * 
     */
    public IDSIDCTYPE createIDSIDCTYPE() {
        return new IDSIDCTYPE();
    }

    /**
     * Create an instance of {@link RepWartosciTYPE }
     * 
     */
    public RepWartosciTYPE createRepWartosciTYPE() {
        return new RepWartosciTYPE();
    }

    /**
     * Create an instance of {@link IDNIDTYPE }
     * 
     */
    public IDNIDTYPE createIDNIDTYPE() {
        return new IDNIDTYPE();
    }

    /**
     * Create an instance of {@link ZamdostTYPE }
     * 
     */
    public ZamdostTYPE createZamdostTYPE() {
        return new ZamdostTYPE();
    }

    /**
     * Create an instance of {@link SYMKODIDTYPE }
     * 
     */
    public SYMKODIDTYPE createSYMKODIDTYPE() {
        return new SYMKODIDTYPE();
    }

    /**
     * Create an instance of {@link EANIDTYPE }
     * 
     */
    public EANIDTYPE createEANIDTYPE() {
        return new EANIDTYPE();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZamdostGenDokzakTYPE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.simple.com.pl/SimpleErp", name = "zamdost_gen_dokzak")
    public JAXBElement<ZamdostGenDokzakTYPE> createZamdostGenDokzak(ZamdostGenDokzakTYPE value) {
        return new JAXBElement<ZamdostGenDokzakTYPE>(_ZamdostGenDokzak_QNAME, ZamdostGenDokzakTYPE.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "id", scope = IDNIDCTYPE.class)
    public JAXBElement<BigDecimal> createIDNIDCTYPEId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IDNIDCTYPEId_QNAME, BigDecimal.class, IDNIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idn", scope = IDNIDCTYPE.class)
    public JAXBElement<String> createIDNIDCTYPEIdn(String value) {
        return new JAXBElement<String>(_IDNIDCTYPEIdn_QNAME, String.class, IDNIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "id", scope = IDMIDCTYPE.class)
    public JAXBElement<BigDecimal> createIDMIDCTYPEId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IDNIDCTYPEId_QNAME, BigDecimal.class, IDMIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "idm", scope = IDMIDCTYPE.class)
    public JAXBElement<String> createIDMIDCTYPEIdm(String value) {
        return new JAXBElement<String>(_IDMIDCTYPEIdm_QNAME, String.class, IDMIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "id", scope = SYMBOLWALUTYIDCTYPE.class)
    public JAXBElement<BigDecimal> createSYMBOLWALUTYIDCTYPEId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IDNIDCTYPEId_QNAME, BigDecimal.class, SYMBOLWALUTYIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "symbolwaluty", scope = SYMBOLWALUTYIDCTYPE.class)
    public JAXBElement<String> createSYMBOLWALUTYIDCTYPESymbolwaluty(String value) {
        return new JAXBElement<String>(_SYMBOLWALUTYIDCTYPESymbolwaluty_QNAME, String.class, SYMBOLWALUTYIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "id", scope = IDSIDCTYPE.class)
    public JAXBElement<BigDecimal> createIDSIDCTYPEId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IDNIDCTYPEId_QNAME, BigDecimal.class, IDSIDCTYPE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ids", scope = IDSIDCTYPE.class)
    public JAXBElement<String> createIDSIDCTYPEIds(String value) {
        return new JAXBElement<String>(_IDSIDCTYPEIds_QNAME, String.class, IDSIDCTYPE.class, value);
    }

}
